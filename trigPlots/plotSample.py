#!/usr/bin/env python3

import os
import pathlib
import matplotlib
matplotlib.use( "agg" )
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import copy
import numpy as np
import sys

from ml_share import ml_config
from ml_share import plotting
from ml_share import utils

### Declaring the Config & Paths ###
### ---------------------------- ###

channel             = "electron"
input_dir           = "/storage/agrp/dvijm/ntuples/feather/electron/refitPRWUnseededPassThrough"
config_file         = "../ml_main/configs/gnn1_electron_multiclass_goblin_v2.yaml"
output_dir          = "./"

config_file_default = "../ml_main/configs/default_" + channel + ".yaml"
config              = utils.config_load( config_file )
output_dir          = os.path.join( output_dir, "plots" )
input_data_nr       = utils.get_list_make_list( "ntuple-30059[0-1]_part_0[1-2].ftr", input_dir )
input_data_r        = utils.get_list_make_list( "ntuple-30059[2-3]_part_0[1-2].ftr", input_dir )
pathlib.Path( output_dir ).mkdir( parents = True, exist_ok = True )

### Selections ###
### ---------- ###

selections = {}
selections[ "baseline_q2low"  ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high" ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full" ] = "( $selection_main & $selection_mc & $selection_usr )"
selections[ "baseline_q2low_all"  ] = "( $selection_main & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high_all" ] = "( $selection_main & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full_all" ] = "( $selection_main & $selection_usr )"
selections[ "trigger"         ] = "( $selection_trigger     )"
selections[ "trigger_e5"      ] = "( $selection_trigger_e5  )"
selections[ "trigger_2e5"     ] = "( $selection_trigger_2e5 )"
for selection_name in selections.keys():
    selections[ selection_name ] = utils.get_selection( selections[ selection_name ], config[ "common" ] )


### Load Data ###
### --------- ###
    
features                    = config[ "common"   ].get( "features", [] )
additional_features_to_plot = config[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot += [
    "dR_positron_electron",
    "Lxy_significance_over_B_chi2",
    "electron0_pT",
    "electron1_pT"
]
additional_features_to_load = [ 
    "info_sample", 
    "info_event_number", 
    "info_weight", 
    "info_truth_matching", 
    "info_pure_pileup_weight",
    "info_trigger_decision_SpecialL1OR",
    "info_trigger_decision_SpecialHLTOR",
    "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_trigger_decision",
    "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_SpecialHLTOR", 
    "info_prescale_weight_SpecialHLTOR_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialHLTOR", 
    "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"
]
if features == []:
    graph_dict  = utils.json_load( os.path.join( "../ml_main/", config[ "common" ][ "graph_dict" ] ) )
    parameters  = config[ "common" ] 
    nested_list = []
    if isinstance( parameters[ "graph_version" ], dict ):
        graph_topo_names = [ item["graph_topo"] for item in parameters[ "graph_version" ].values() ]
    else:
        graph_topo_names = [ parameters["graph_version"] ]

    for item in graph_topo_names:
        if item in graph_dict:
            nested_list += list( graph_dict.get(item).get("nodes").values() )
            nested_list += list( graph_dict.get(item).get("edges").values() )
            nested_list += [ graph_dict.get(item).get("goblins", []) ]
        else:
            print(utils.red(sys._getframe().f_code.co_name + ": " + item + " does not exist in " + parameters["graph_dict"] + "!"))
            exit(1)

    parameters["features"] = list( set( [ item for sublist in nested_list for item in sublist ] ) )
    parameters["features"] = [ item for item in parameters["features"] if item != "empty_feature" ]
    parameters["features"].sort()
    features = parameters["features"]

items_to_load       = features + additional_features_to_plot + additional_features_to_load

def df_load( input_data, q2bin = "low", nBestDict = None, selections = selections ):
    df = utils.df_load( input_data, selection = selections[ "baseline_q2" + q2bin ], items_to_load = items_to_load )
    if nBestDict is not None:
        df = utils.get_n_best( df, nBestDict[ "nBest" ], feature = nBestDict[ "feature" ], sort = nBestDict[ "sort" ], data_only = False )
    return df

### Trigger-Related Functions ###
### ------------------------- ###

def get_trigger_filters( df ):
    f_trigger_e5_or_2e5  = df[ "info_trigger_decision" ] == 1
    f_trigger_e5         = ( df[ "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t"  ] == 1 ) & ( df[ "info_trigger_decision_SpecialL1OR" ] == 1 )
    f_trigger_2e5        = ( df[ "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t" ] == 1 ) & ( df[ "info_trigger_decision_SpecialL1OR" ] == 1 )
    f_trigger_l1or       = df[ "info_trigger_decision_SpecialL1OR" ] == 1
    trigger_filters_dict = {
        "( e5 || 2e5 ) && SpecialL1OR" : f_trigger_e5_or_2e5, 
        "e5 && SpecialL1OR"            : f_trigger_e5,
        "2e5 && SpecialL1OR"           : f_trigger_2e5,
        "( e5 && !2e5 ) && SpecialL1OR"  : f_trigger_e5  & ~ f_trigger_2e5,
        "( 2e5 && !e5 ) && SpecialL1OR"  : f_trigger_2e5 & ~ f_trigger_e5,
        "SpecialL1OR"                  : f_trigger_l1or
    }
    return trigger_filters_dict

def get_yield_contributions( df, trigger_filters_dict = None ):
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    yield_contributions    = {}
    w_total                = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ].sum()
    w_2e5                  = df[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ].sum()
    w_e5                   = df[ "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"  ].sum()
    w_e5_unique            = w_total - w_2e5
    w_2e5_unique           = w_total - w_e5
    w_total_for_e5_unique  = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    w_total_for_2e5_unique = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()
    yield_contributions[ "Total Weight"                ] = round( w_total                / w_total, 4 )
    yield_contributions[ "Total 2e5 Weight"            ] = round( w_2e5                  / w_total, 4 )
    yield_contributions[ "Total e5 Weight"             ] = round( w_e5                   / w_total, 4 )
    yield_contributions[ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / w_total, 4 )
    yield_contributions[ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / w_total, 4 )
    yield_contributions[ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / w_total, 4 )
    yield_contributions[ "Unique e5 Weight"            ] = round( w_e5_unique            / w_total, 4 )
    return yield_contributions

def get_yield_contributions_verbose( df, trigger_filters_dict = None ):
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    yield_contributions                      = {}
    yield_contributions_unprescaled          = {}
    relative_yield_contributions             = {}
    relative_yield_contributions_unprescaled = {}

    total_pileup_weight = df[ "info_pure_pileup_weight" ].sum()

    w_total                = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ].sum()
    w_2e5                  = df[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ].sum()
    w_e5                   = df[ "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"  ].sum()
    w_e5_unique            = w_total - w_2e5
    w_2e5_unique           = w_total - w_e5
    w_total_for_e5_unique  = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    w_total_for_2e5_unique = df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    w_unprescaled_total                = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ].sum()
    w_unprescaled_2e5                  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "2e5 && SpecialL1OR" ] ].sum()
    w_unprescaled_e5                   = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "e5 && SpecialL1OR" ] ].sum()
    w_unprescaled_e5_unique            = w_unprescaled_total - w_unprescaled_2e5
    w_unprescaled_2e5_unique           = w_unprescaled_total - w_unprescaled_e5
    w_unprescaled_total_for_e5_unique  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    w_unprescaled_total_for_2e5_unique = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    yield_contributions[ "Total Weight"                ] = round( w_total                / total_pileup_weight, 4 )
    yield_contributions[ "Total 2e5 Weight"            ] = round( w_2e5                  / total_pileup_weight, 4 )
    yield_contributions[ "Total e5 Weight"             ] = round( w_e5                   / total_pileup_weight, 4 )
    yield_contributions[ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions[ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions[ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions[ "Unique e5 Weight"            ] = round( w_e5_unique            / total_pileup_weight, 4 )

    yield_contributions_unprescaled[ "Total Weight"                ] = round( w_unprescaled_total                / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Total 2e5 Weight"            ] = round( w_unprescaled_2e5                  / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Total e5 Weight"             ] = round( w_unprescaled_e5                   / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Total Weight for Unique 2e5" ] = round( w_unprescaled_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Total Weight for Unique e5"  ] = round( w_unprescaled_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Unique 2e5 Weight"           ] = round( w_unprescaled_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions_unprescaled[ "Unique e5 Weight"            ] = round( w_unprescaled_e5_unique            / total_pileup_weight, 4 )

    relative_yield_contributions[ "Total Weight"                ] = round( w_total                / w_total, 4 )
    relative_yield_contributions[ "Total 2e5 Weight"            ] = round( w_2e5                  / w_total, 4 )
    relative_yield_contributions[ "Total e5 Weight"             ] = round( w_e5                   / w_total, 4 )
    relative_yield_contributions[ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / w_total, 4 )
    relative_yield_contributions[ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / w_total, 4 )
    relative_yield_contributions[ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / w_total, 4 )
    relative_yield_contributions[ "Unique e5 Weight"            ] = round( w_e5_unique            / w_total, 4 )

    relative_yield_contributions_unprescaled[ "Total Weight"                ] = round( w_unprescaled_total                / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Total 2e5 Weight"            ] = round( w_unprescaled_2e5                  / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Total e5 Weight"             ] = round( w_unprescaled_e5                   / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Total Weight for Unique 2e5" ] = round( w_unprescaled_total_for_2e5_unique / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Total Weight for Unique e5"  ] = round( w_unprescaled_total_for_e5_unique  / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Unique 2e5 Weight"           ] = round( w_unprescaled_2e5_unique           / w_unprescaled_total, 4 )
    relative_yield_contributions_unprescaled[ "Unique e5 Weight"            ] = round( w_unprescaled_e5_unique            / w_unprescaled_total, 4 )

    return yield_contributions, yield_contributions_unprescaled, relative_yield_contributions, relative_yield_contributions_unprescaled


def plotFeatures( df, trigger_filters_dict = None, title = "", slug = ""):
    if slug == "":
        slug = title
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    pathlib.Path( "plots" ).mkdir( parents = True, exist_ok = True )
    df_baseline          = copy.deepcopy( df )
    df_either_unweighted = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_either            = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_l1or_unweighted   = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    df_l1or              = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    df_baseline          [ "info_weight" ] = np.ones( len( df_baseline ) )
    df_either_unweighted [ "info_weight" ] = np.ones( len( df_either_unweighted ) )
    df_either            [ "info_weight" ] = np.ones( len( df_either ) ) * df_either[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ]
    df_l1or_unweighted   [ "info_weight" ] = np.ones( len( df_l1or_unweighted ) )
    df_l1or              [ "info_weight" ] = np.ones( len( df_l1or ) ) * df_l1or[ "info_pileup_prescale_weight_SpecialL1OR" ]
    list_df        = [ df_baseline, df_l1or, df_either ]
    list_df_names  = [ "Baseline", "L1OR", "( e5 || 2e5 ) && L1OR" ]
    list_df_colors = [ "tab:blue", "tab:green", "tab:red" ]
    plotting.plot_baseline_features(
       list_df,
       features = features,
       title = title,
       output_filename = "plots/trigger_reweighting_" + slug + ".pdf",
       input_labels = list_df_names,
       input_colors = list_df_colors,
       additional_features = additional_features_to_plot,
       normalized = True
    )
    df_unique_e5 = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_unique_e5[ "info_weight" ] = df_unique_e5[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ] - df_unique_e5[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ]
    df_inclusive_2e5 = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_inclusive_2e5[ "info_weight" ] = df_inclusive_2e5[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ]
    list_df = [ df_either, df_unique_e5, df_inclusive_2e5 ]
    list_df_names = [ "( e5 || 2e5 )", "Unique e5", "Inclusive 2e5" ]
    list_df_colors = [ "tab:blue", "tab:red", "tab:green" ]
    plotting.plot_baseline_features(
       list_df,
       features = features,
       title = title,
       output_filename = "plots/trigger_reweighting_" + slug + "_unique_e5.pdf",
       input_labels = list_df_names,
       input_colors = list_df_colors,
       additional_features = additional_features_to_plot,
       normalized = False
    )

def triggerDecPRWConsistency( df ):
    l1trigs  = ml_config.electronTrigListsConfig[ "L1BKeeSeeds" ][ "Run2" ]
    hlttrigs = [ "HLT_e5_lhvloose_nod0_bBeexM6000t", "HLT_2e5_lhvloose_nod0_bBeexM6000t" ]
    allTrigs = l1trigs + hlttrigs
    for trigItem in allTrigs:
        assert ( ( df[ "prescale_weight_" + trigItem ] == 0 & ( df[ "pure_pileup_weight" ] * df[ trigItem + "_acceptance" ] == 0 ) ) | ( df[ "prescale_weight_" + trigItem ] != 0 & df[ trigItem + "_acceptance" ] != 0 ) )


def main( slug, truth_matching = True):
    nBestDict = {
        "nBest"   : 1,
        "feature" : "Lxy_significance_over_B_chi2",
        "sort"    : "highest"
    }
    df_nr_q2low  = df_load( input_data_nr, q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None )
    df_nr_q2high = df_load( input_data_nr, q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None )
    df_nr_q2full = df_load( input_data_nr, q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None )
    df_r_q2low   = df_load( input_data_r,  q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None )
    df_r_q2high  = df_load( input_data_r,  q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None )
    df_r_q2full  = df_load( input_data_r,  q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None )
    trigger_filters_dict_nr_q2low  = get_trigger_filters( df_nr_q2low  )
    trigger_filters_dict_nr_q2high = get_trigger_filters( df_nr_q2high )
    trigger_filters_dict_nr_q2full = get_trigger_filters( df_nr_q2full )
    trigger_filters_dict_r_q2low   = get_trigger_filters( df_r_q2low   )
    trigger_filters_dict_r_q2high  = get_trigger_filters( df_r_q2high  )
    trigger_filters_dict_r_q2full  = get_trigger_filters( df_r_q2full  )

    y_nr_q2low   , y_unprescaled_nr_q2low   , y_relative_nr_q2low   , y_relative_unprescaled_nr_q2low    = get_yield_contributions_verbose( df_nr_q2low  , trigger_filters_dict_nr_q2low  )
    y_nr_q2high  , y_unprescaled_nr_q2high  , y_relative_nr_q2high  , y_relative_unprescaled_nr_q2high   = get_yield_contributions_verbose( df_nr_q2high , trigger_filters_dict_nr_q2high )
    y_nr_q2full  , y_unprescaled_nr_q2full  , y_relative_nr_q2full  , y_relative_unprescaled_nr_q2full   = get_yield_contributions_verbose( df_nr_q2full , trigger_filters_dict_nr_q2full )
    y_r_q2low    , y_unprescaled_r_q2low    , y_relative_r_q2low    , y_relative_unprescaled_r_q2low     = get_yield_contributions_verbose( df_r_q2low   , trigger_filters_dict_r_q2low   )
    y_r_q2high   , y_unprescaled_r_q2high   , y_relative_r_q2high   , y_relative_unprescaled_r_q2high    = get_yield_contributions_verbose( df_r_q2high  , trigger_filters_dict_r_q2high  )
    y_r_q2full   , y_unprescaled_r_q2full   , y_relative_r_q2full   , y_relative_unprescaled_r_q2full    = get_yield_contributions_verbose( df_r_q2full  , trigger_filters_dict_r_q2full  )

    mega_y_nr = {
        "q2low" : y_nr_q2low,
        "q2high": y_nr_q2high,
        "q2full": y_nr_q2full
    }
    mega_y_unprescaled_nr = {
        "q2low" : y_unprescaled_nr_q2low,
        "q2high": y_unprescaled_nr_q2high,
        "q2full": y_unprescaled_nr_q2full
    }
    mega_y_relative_nr = {
        "q2low" : y_relative_nr_q2low,
        "q2high": y_relative_nr_q2high,
        "q2full": y_relative_nr_q2full
    }
    mega_y_relative_unprescaled_nr = {
        "q2low" : y_relative_unprescaled_nr_q2low,
        "q2high": y_relative_unprescaled_nr_q2high,
        "q2full": y_relative_unprescaled_nr_q2full
    }

    mega_y_r = {
        "q2low" : y_r_q2low,
        "q2high": y_r_q2high,
        "q2full": y_r_q2full
    }
    mega_y_unprescaled_r = {
        "q2low" : y_unprescaled_r_q2low,
        "q2high": y_unprescaled_r_q2high,
        "q2full": y_unprescaled_r_q2full
    }
    mega_y_relative_r = {
        "q2low" : y_relative_r_q2low,
        "q2high": y_relative_r_q2high,
        "q2full": y_relative_r_q2full
    }
    mega_y_relative_unprescaled_r = {
        "q2low" : y_relative_unprescaled_r_q2low,
        "q2high": y_relative_unprescaled_r_q2high,
        "q2full": y_relative_unprescaled_r_q2full
    }

    mega_y_nr = pd.DataFrame( mega_y_nr )
    mega_y_unprescaled_nr = pd.DataFrame( mega_y_unprescaled_nr )
    mega_y_relative_nr = pd.DataFrame( mega_y_relative_nr )
    mega_y_relative_unprescaled_nr = pd.DataFrame( mega_y_relative_unprescaled_nr )

    mega_y_r = pd.DataFrame( mega_y_r )
    mega_y_unprescaled_r = pd.DataFrame( mega_y_unprescaled_r )
    mega_y_relative_r = pd.DataFrame( mega_y_relative_r )
    mega_y_relative_unprescaled_r = pd.DataFrame( mega_y_relative_unprescaled_r )

    pathlib.Path( "yields" ).mkdir( parents = True, exist_ok = True )

    mega_y_nr.to_csv( f"yields/mega_y_nr_{slug}.csv", index = True )
    mega_y_unprescaled_nr.to_csv( f"yields/mega_y_unprescaled_nr_{slug}.csv", index = True )
    mega_y_relative_nr.to_csv( f"yields/mega_y_relative_nr_{slug}.csv", index = True )
    mega_y_relative_unprescaled_nr.to_csv( f"yields/mega_y_relative_unprescaled_nr_{slug}.csv", index = True )

    mega_y_r.to_csv( f"yields/mega_y_r_{slug}.csv", index = True )
    mega_y_unprescaled_r.to_csv( f"yields/mega_y_unprescaled_r_{slug}.csv", index = True )
    mega_y_relative_r.to_csv( f"yields/mega_y_relative_r_{slug}.csv", index = True )
    mega_y_relative_unprescaled_r.to_csv( f"yields/mega_y_relative_unprescaled_r_{slug}.csv", index = True )


    plotFeatures( df_nr_q2low, trigger_filters_dict_nr_q2low, title = "Non-Resonant Signal MC at Baseline (TM), low$-q^2$", slug = f"nr_tm_q2low_{slug}" if truth_matching else f"nr_q2low_all_{slug}" )
    plotFeatures( df_r_q2high, trigger_filters_dict_r_q2high, title = "Resonant Signal MC at Baseline (TM), high$-q^2$", slug = f"r_tm_q2high_{slug}" if truth_matching else f"r_q2high_all_{slug}" )

if __name__ == "__main__":
    pass
    #main( "unseeded_passthru_ntm", False )
    #main( "unseeded_passthru_tm", True )
