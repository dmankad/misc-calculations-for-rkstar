#!/usr/bin/env python3

###
### Run the pre-selection cuts and store the dataframe in csv/feather files for neural networks
###
### Instructions: Set the channel and input/output setup in the script, and then run 
###
###                  python3 -u prepare_data.py <list of ntuples>
###
### The list of ntuples can also be specified in the script
###
### For batch production, use prepare_data_batch.sub to submit the job
###

# WARNING: most of the dataframe operation are taken IN PLACE, be careful when saving dataframes!

# TODO: check ml_share.utils.add_features() before the next production.


# Imports
# =======

import pandas as pd
import numpy as np
import collections
import awkward0
import math
import sys
from os import path
from fnmatch import fnmatch
from scipy.stats import chi2

from split_data_frac import split_frac

from ml_share import utils
from ml_share import plotnames
from ml_share import samples
from ml_share import ml_config

import warnings
warnings.simplefilter(action = "ignore", category = FutureWarning)

import uproot3 as uproot
import uproot3_methods as um


# Input data
# ==========

max_events = 0       # 0 for all events

channel = "electron"   # electron / muon

track_type = "refit"  # refit / orig

if len(sys.argv) > 1:
    ntuples = sys.argv[1:]
else:
    ntuples = [
        # list of ntuples
    ]

# Output setup
# ============

all_charges     = True

save_dataframes = True
crop_columns    = True
prepare_splits  = True
check_back      = True


if channel != "electron" and channel != "muon":
    print ('"channel" should be either "electron" or "muon"')
    quit()

if track_type != "orig" and track_type != "refit":
    print ('"track_type" should be either "orig" or "refit"')
    quit()    
    
#output_dir = "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/" + channel + "/" + track_type + "/feather/"
output_dir = "/storage/agrp/dvijm/ntuples/feather/electron/refitPRWUnseededPassThrough"

output_type     = "ftr" # csv / hdf / h5 / hdf5 / feather / f / fth / ftr

# Prefix and abbreviations
# =========
if channel == "muon":
    tree_name    = "Nominal/BaseSelection_KStarMuMu_Selection"
    prefix       = "BmumuKst"
    lepton       = "muon"
    lepton_big   = "Muon"
    lepton_short = "mu"
    antilepton   = "antimuon"
elif channel == "electron":
    tree_name    = "Nominal/BaseSelection_KStaree_Selection"
    prefix       = "BeeKst"
    lepton       = "electron"
    lepton_big   = "Electron"
    lepton_short = "e"
    antilepton   = "positron"
else:
    print ('"channel" should be either "electron" or "muon"')
    quit()

# Pre-selection cuts
# ============
if channel == "electron":
    B_mass_low = 3000 
    B_mass_up = 6500
    kaonPion_mass_low = 690
    kaonPion_mass_up = 1110
    pionKaon_mass_low = 690
    pionKaon_mass_up = 1110
    dR_low = 0.1
    lepton_pT_low = 5000
    lepton_eta_limit = 2.5
    meson_pT_low = 500
    meson_eta_limit = 2.5
    diLepton_mass_up = 7000
    InDet_pT_low = 500
    InDet_eta_limit = 2.5
    GSF_pT_low = 5000
    GSF_eta_limit = 2.5
    mc_NonresBd = 300590
    mc_NonresBd_str = "300590"
    mc_NonresBdbar = 300591
    mc_NonresBdbar_str = "300591"
    mc_ResBd = 300592
    mc_ResBd_str = "300592"
    mc_ResBdbar = 300593
    mc_ResBdbar_str = "300593"
    
    
elif channel == "muon":
    B_mass_low = 4700
    B_mass_up = 6000
    kaonPion_mass_low = 690
    kaonPion_mass_up = 1110
    pionKaon_mass_low = 690
    pionKaon_mass_up = 1110
    dR_low = 0.1
    lepton_pT_low = 6000
    lepton_eta_limit = 2.5
    meson_pT_low = 500
    meson_eta_limit = 2.5
    diLepton_mass_up = 7000
    InDet_pT_low = 500
    InDet_eta_limit = 2.5
    mc_NonresBd = 300700
    mc_NonresBd_str = "300700"
    mc_NonresBdbar = 300701
    mc_NonresBdbar_str = "300701"
    mc_ResBd = 300702
    mc_ResBd_str = "300702"
    mc_ResBdbar = 300703
    mc_ResBdbar_str = "300703"

# Error variables to check
# ========================

error_list = [
    "cos_Theta_pseudo_err",
]


# Functions
# =========

def df_to_namedtuple(df):   #Convert a pandas dataframe to namedtuple
    df_replace = df.copy()
    for branch in df.columns:
        branch_replace = branch.replace(prefix,"BllKst")
        if "passLoose" not in branch and "DFCommon" not in branch:
            branch_replace = branch_replace.replace(lepton_big,"Lepton")
        branch_replace = branch_replace.replace(lepton,"lepton")
        if track_type == "refit":
            branch_replace = branch_replace.replace("_refit","")
        if track_type == "orig":
            branch_replace = branch_replace.replace("_orig","")
        df_replace.rename(columns = {branch:branch_replace}, inplace = True)
        if "info" in branch:
            df_replace[ branch ] = np.nan_to_num( np.array( df_replace[ branch ] ) )
    Vtx = collections.namedtuple("Vtx", df_replace.columns)
    values = []
    for col in df_replace.columns:
        values.append(awkward0.fromiter(df_replace[col].tolist()))
    vtx = Vtx._make(values)
    return vtx


def distance_significance(vtx1, vtx1_err, vtx2, vtx2_err):   # Calculate distance significance of two vertices
    dist_sign = -999.
    vect = vtx2 - vtx1
    d2 = vect.mag2
    x2 = vect.x * vect.x
    y2 = vect.y * vect.y
    z2 = vect.z * vect.z

    x_err2 = vtx1_err.x * vtx1_err.x + vtx2_err.x * vtx2_err.x
    y_err2 = vtx1_err.y * vtx1_err.y + vtx2_err.y * vtx2_err.y
    z_err2 = vtx1_err.z * vtx1_err.z + vtx2_err.z * vtx2_err.z

    denom = math.sqrt( x2 * x_err2 + y2 * y_err2 + z2 * z_err2 )

    if denom > 0:
        dist_sign = d2 / denom

    return dist_sign

distance_significance_vectorized = np.vectorize(distance_significance)

def remove_null_errors(df):  # Remove row if there are any null errors in it
    print("Removing candidates with null errors ...")
    for error in error_list:
        if error in df.columns.values:
            df = df[df[error] != 0]
    print()

def remove_unfilled_variables(df):  # Remove row if there are any unfilled variables
    print(utils.red("TODO:"), "Removing candidates with unfilled variables (NaN) ...")
    df.replace([np.inf, -np.inf], np.nan, inplace = True)
    df.dropna(inplace = True)
    print()

def remove_duplicated_candidates(df, columns_to_check = []): # Remove duplicated row
    print("Removing duplicated candidates...")
    if len(columns_to_check) == 0:
        df.drop_duplicates(inplace = True)
    else:
        df.drop_duplicates(subset = columns_to_check, inplace = True)
    print()

def crop_dataframe(df, to_remove = []):  # Remove unnecessary columns
    print("Cropping the dataframe - removing unnecessary variables:", to_remove)
    print()
    if len(to_remove) > 0:
        df_new = df.copy()
        df_new.drop(columns = to_remove, inplace = True)
        return df_new
    else:
        return df


# Loop over input files
# =========================================================================================================

for ntuple in ntuples:
    # Load data
    # =========
    utils.print_time()
    
    print("Opening:", ntuple)
    tree = uproot.open(ntuple)[tree_name]
    
    if max_events and max_events > 0:
        vtx = tree.arrays("/HLT_2mu6_bBmumuxv2_L1LFV_MU6_.|event_number|weight|mc_channel_number|guess_.|pileup_.|prescale_.|pure_.|HLT_e5_.|HLT_2e5_.|pass_.|"+prefix+"_.+|InDet_pass.+|InDet_pT|InDet_eta|GSF_pass.+|GSF_pT|GSF_eta/", outputtype=pd.DataFrame, entrystop = max_events)
    else:
        vtx = tree.arrays("/HLT_2mu6_bBmumuxv2_L1LFV_MU6_.|event_number|weight|mc_channel_number|guess_.|pileup_.|prescale_.|pure_.|HLT_e5_.|HLT_2e5_.|pass_.|"+prefix+"_.+|InDet_pass.+|InDet_pT|InDet_eta|GSF_pass.+|GSF_pT|GSF_eta/", outputtype=pd.DataFrame)
    
    vtx = df_to_namedtuple(vtx)
    print("Loaded", len(vtx.BllKst_B_mass), "events.")
    print()   
    
    # Sample helper functions
    # =======================
    mc_sample     = "data" if vtx.mc_channel_number[0] < 0 else str(vtx.mc_channel_number[0])
    # TODO: We don't have proper weighting yet; weight = 1
    sample_weight = samples.get_weight_for_sample_number(mc_sample)    

    # Selection helper function
    # =========================
    utils.print_time()
    print("Selecting candidates and filling a dataframe...")

    vtx_l0p4               = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_lepton0_pT, vtx.BllKst_lepton0_eta, vtx.BllKst_lepton0_phi, ml_config.PDG[lepton_short+"_mass"])
    vtx_l1p4               = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_lepton1_pT, vtx.BllKst_lepton1_eta, vtx.BllKst_lepton1_phi, ml_config.PDG[lepton_short+"_mass"])

    vtx_dRll               = vtx_l0p4.delta_r2(vtx_l1p4)

    vtx_selection_Bd       = (vtx.BllKst_B_mass    > B_mass_low) * (vtx.BllKst_B_mass    < B_mass_up)
    vtx_selection_Bdbar    = (vtx.BllKst_Bbar_mass > B_mass_low) * (vtx.BllKst_Bbar_mass < B_mass_up)

    # Correct mass assignment
    vtx_selection_kst      = vtx_selection_Bd    * (vtx.BllKst_kaonPion_mass > kaonPion_mass_low) * (vtx.BllKst_kaonPion_mass < kaonPion_mass_up)
    vtx_selection_kstbar   = vtx_selection_Bdbar * (vtx.BllKst_pionKaon_mass > pionKaon_mass_low) * (vtx.BllKst_pionKaon_mass < pionKaon_mass_up)

    # Selection cuts
    # ==============
    
    if channel == "muon":
        vtx_selection_trigger = (vtx.BllKst_B_mass * 0).astype(bool)
        for event_i in range(len(vtx_selection_trigger)):
            if (mc_sample == "data" and vtx.HLT_2mu6_bBmumuxv2_L1LFV_MU6_acceptance[event_i] == 1) or (mc_sample != "data" and vtx.HLT_2mu6_bBmumuxv2_L1LFV_MU6_acceptance[event_i] == 1 and vtx.HLT_2mu6_bBmumuxv2_L1LFV_MU6_was_online[event_i] == 1):
                for j in range(len(vtx_selection_trigger[event_i])):
                    vtx_selection_trigger[event_i][j] = True
        if all_charges:
            vtx_selection_dRll = vtx_selection_trigger * (vtx_dRll > dR_low * dR_low)
        else:
            vtx_selection_charge   = vtx_selection_trigger * (vtx.BllKst_lepton0_charge*vtx.BllKst_lepton1_charge < 0) * (vtx.BllKst_meson0_charge*vtx.BllKst_meson1_charge < 0)
            vtx_selection_dRll   = vtx_selection_charge * (vtx_dRll > dR_low * dR_low)
        vtx_selection_trk_qual = vtx_selection_dRll * (vtx.BllKst_lepton0_passMedium == 1) * (vtx.BllKst_lepton1_passMedium == 1) * (vtx.BllKst_lepton0_ID_passLoose == 1) * (vtx.BllKst_lepton1_ID_passLoose == 1) * (vtx.BllKst_meson0_passLoose == 1) * (vtx.BllKst_meson1_passLoose == 1) * (vtx.BllKst_meson0_pixeldEdx > 0) * (vtx.BllKst_meson1_pixeldEdx > 0)
    
    elif channel == "electron":
        vtx_selection_gsf_id   = vtx.BllKst_diLepton_gsf_id_isOK == 1
        if all_charges:
            vtx_selection_dRll = vtx_selection_gsf_id * (vtx_dRll > dR_low * dR_low) 
        else:
            vtx_selection_charge   =  vtx_selection_gsf_id * ((vtx.BllKst_lepton0_charge*vtx.BllKst_lepton1_charge < 0) * (vtx.BllKst_meson0_charge*vtx.BllKst_meson1_charge < 0))
            vtx_selection_dRll   = vtx_selection_charge * (vtx_dRll > dR_low * dR_low)
        vtx_selection_trk_qual = vtx_selection_dRll * (vtx.BllKst_lepton0_passLoose == 1) * (vtx.BllKst_lepton1_passLoose == 1) * (vtx.BllKst_lepton0_passLooseElectron == 1) * (vtx.BllKst_lepton1_passLooseElectron == 1) * (vtx.BllKst_meson0_passLoose == 1) * (vtx.BllKst_meson1_passLoose == 1) * (vtx.BllKst_meson0_pixeldEdx > 0) * (vtx.BllKst_meson1_pixeldEdx > 0)
               
    vtx_selection_l_pT     = vtx_selection_trk_qual * (vtx.BllKst_lepton0_pT >= lepton_pT_low) * (vtx.BllKst_lepton1_pT >= lepton_pT_low)
    vtx_selection_l_eta    = vtx_selection_l_pT     * (abs(vtx.BllKst_lepton0_eta) <= lepton_eta_limit) * (abs(vtx.BllKst_lepton1_eta) <= lepton_eta_limit)
    
    vtx_selection_m_pT     = vtx_selection_l_eta    * (vtx.BllKst_meson0_pT >= meson_pT_low) * (vtx.BllKst_meson1_pT >= meson_pT_low)
    vtx_selection_m_eta    = vtx_selection_m_pT     * (abs(vtx.BllKst_meson0_eta) <= meson_eta_limit) * (abs(vtx.BllKst_meson1_eta) <= meson_eta_limit)
    
    vtx_selection_ll_mass  = vtx_selection_m_eta    * (vtx.BllKst_diLepton_mass <= diLepton_mass_up)
    vtx_selection_B_mass   = vtx_selection_ll_mass  * (vtx_selection_Bd + vtx_selection_Bdbar)
    vtx_selection_mm_mass  = vtx_selection_B_mass   * (vtx_selection_kst + vtx_selection_kstbar)

    vtx_selection_final    = vtx_selection_mm_mass
    
    vtx_selection_truth    =  (vtx.mc_channel_number == mc_NonresBd) * (vtx.BllKst_isTrueNonresBd == 1) + (vtx.mc_channel_number == mc_NonresBdbar) * (vtx.BllKst_isTrueNonresBdbar == 1) + (vtx.mc_channel_number == mc_ResBd) * (vtx.BllKst_isTrueResBd == 1) + (vtx.mc_channel_number == mc_ResBdbar) * (vtx.BllKst_isTrueResBdbar == 1)   

    # VTX helper functions
    # ====================
    # "== 1" is needed to convert int to bool
    vtx_truth    = (mc_sample == mc_NonresBd_str) * (vtx.BllKst_isTrueNonresBd[vtx_selection_final] == 1) + (mc_sample == mc_NonresBdbar_str) * (vtx.BllKst_isTrueNonresBdbar[vtx_selection_final] == 1) + (mc_sample == mc_ResBd_str) * (vtx.BllKst_isTrueResBd[vtx_selection_final] == 1) + (mc_sample == mc_ResBdbar_str) * (vtx.BllKst_isTrueResBdbar[vtx_selection_final] == 1)    
    vtx_l0_p4    = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_lepton0_pT[vtx_selection_final], vtx.BllKst_lepton0_eta[vtx_selection_final], vtx.BllKst_lepton0_phi[vtx_selection_final], ml_config.PDG[lepton_short+"_mass"])
    vtx_l1_p4    = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_lepton1_pT[vtx_selection_final], vtx.BllKst_lepton1_eta[vtx_selection_final], vtx.BllKst_lepton1_phi[vtx_selection_final], ml_config.PDG[lepton_short+"_mass"])
    vtx_m0_pi_p4 = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_meson0_pT[vtx_selection_final], vtx.BllKst_meson0_eta[vtx_selection_final], vtx.BllKst_meson0_phi[vtx_selection_final], ml_config.PDG["pipm_mass"])
    vtx_m0_K_p4  = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_meson0_pT[vtx_selection_final], vtx.BllKst_meson0_eta[vtx_selection_final], vtx.BllKst_meson0_phi[vtx_selection_final], ml_config.PDG["Kpm_mass"])
    vtx_m1_pi_p4 = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_meson1_pT[vtx_selection_final], vtx.BllKst_meson1_eta[vtx_selection_final], vtx.BllKst_meson1_phi[vtx_selection_final], ml_config.PDG["pipm_mass"])
    vtx_m1_K_p4  = um.TLorentzVectorArray.from_ptetaphim(vtx.BllKst_meson1_pT[vtx_selection_final], vtx.BllKst_meson1_eta[vtx_selection_final], vtx.BllKst_meson1_phi[vtx_selection_final], ml_config.PDG["Kpm_mass"])

    vtx_diLepton_p4 = vtx_l0_p4 + vtx_l1_p4
    vtx_Kpi_p4      = vtx_m0_K_p4 + vtx_m1_pi_p4
    vtx_piK_p4      = vtx_m0_pi_p4 + vtx_m1_K_p4
    vtx_Bd_p4       = vtx_diLepton_p4 + vtx_Kpi_p4
    vtx_Bdbar_p4    = vtx_diLepton_p4 + vtx_piK_p4

    vtx_l0_p3       = vtx_l0_p4.p3
    vtx_l1_p3       = vtx_l1_p4.p3
    vtx_m0_p3       = vtx_m0_K_p4.p3
    vtx_m1_p3       = vtx_m1_pi_p4.p3
    vtx_diLepton_p3 = vtx_diLepton_p4.p3
    vtx_diMeson_p3  = vtx_Kpi_p4.p3
    vtx_Bd_p3       = vtx_Bd_p4.p3
    # We do not have access to the B_p_error on the DAOD level -> dummy (zero) error, used later only for "pseudo" cos_Theta_error and cos_Theta_significance variables (assuming the error is driven by the PV-SV error)!
    vtx_Bd_p3_err   = um.TVector3Array.from_cartesian( vtx.BllKst_B_mass[vtx_selection_final] * 0, vtx.BllKst_B_mass[vtx_selection_final] * 0, vtx.BllKst_B_mass[vtx_selection_final] * 0 )

    vtx_Bd_pv            = um.TVector3Array.from_cartesian(vtx.BllKst_PV_minA0_x[vtx_selection_final]        , vtx.BllKst_PV_minA0_y[vtx_selection_final]        , vtx.BllKst_PV_minA0_z[vtx_selection_final])
    vtx_Bd_pv_err        = um.TVector3Array.from_cartesian(vtx.BllKst_PV_minA0_x_err[vtx_selection_final]    , vtx.BllKst_PV_minA0_y_err[vtx_selection_final]    , vtx.BllKst_PV_minA0_z_err[vtx_selection_final])
    vtx_Bd_vtx           = um.TVector3Array.from_cartesian(vtx.BllKst_x[vtx_selection_final]                 , vtx.BllKst_y[vtx_selection_final]                 , vtx.BllKst_z[vtx_selection_final])
    vtx_Bd_vtx_err       = um.TVector3Array.from_cartesian(vtx.BllKst_x_err[vtx_selection_final]             , vtx.BllKst_y_err[vtx_selection_final]             , vtx.BllKst_z_err[vtx_selection_final])
    vtx_diMeson_vtx      = um.TVector3Array.from_cartesian(vtx.BllKst_diMeson_vtx_x[vtx_selection_final]     , vtx.BllKst_diMeson_vtx_y[vtx_selection_final]     , vtx.BllKst_diMeson_vtx_z[vtx_selection_final])
    vtx_diMeson_vtx_err  = um.TVector3Array.from_cartesian(vtx.BllKst_diMeson_vtx_x_err[vtx_selection_final] , vtx.BllKst_diMeson_vtx_y_err[vtx_selection_final] , vtx.BllKst_diMeson_vtx_z_err[vtx_selection_final])
    vtx_diLepton_vtx     = um.TVector3Array.from_cartesian(vtx.BllKst_diLepton_vtx_x[vtx_selection_final]    , vtx.BllKst_diLepton_vtx_y[vtx_selection_final]    , vtx.BllKst_diLepton_vtx_z[vtx_selection_final])
    vtx_diLepton_vtx_err = um.TVector3Array.from_cartesian(vtx.BllKst_diLepton_vtx_x_err[vtx_selection_final], vtx.BllKst_diLepton_vtx_y_err[vtx_selection_final], vtx.BllKst_diLepton_vtx_z_err[vtx_selection_final])

    # Using 4-vector because of the pre-defined functions -> dummy mass
    vtx_pv2B          = vtx_Bd_vtx - vtx_Bd_pv
    vtx_pv2B_err      = um.TVector3Array.from_cartesian( np.sqrt( vtx_Bd_vtx_err.x * vtx_Bd_vtx_err.x + vtx_Bd_pv_err.x * vtx_Bd_pv_err.x ), np.sqrt( vtx_Bd_vtx_err.y * vtx_Bd_vtx_err.y + vtx_Bd_pv_err.y * vtx_Bd_pv_err.y ), np.sqrt( vtx_Bd_vtx_err.z * vtx_Bd_vtx_err.z + vtx_Bd_pv_err.z * vtx_Bd_pv_err.z ) )
    vtx_B2diMeson     = vtx_diMeson_vtx - vtx_Bd_vtx
    vtx_B2diMeson_v4  = um.TLorentzVectorArray.from_p3(vtx_B2diMeson, 0.)
    vtx_B2diLepton    = vtx_diLepton_vtx - vtx_Bd_vtx
    vtx_B2diLepton_v4 = um.TLorentzVectorArray.from_p3(vtx_B2diLepton, 0.)

    vtx_n1     = um.TVector3Array.cross(vtx_B2diLepton, vtx_B2diMeson)
    vtx_n1_pp  = um.TVector3Array.cross(vtx_diLepton_p3, vtx_diMeson_p3)
    vtx_n2_pll = um.TVector3Array.cross(vtx_n1, vtx_diLepton_p3)
    vtx_n2_ll  = um.TVector3Array.cross(vtx_l0_p3, vtx_l1_p3)
    vtx_n2_pmm = um.TVector3Array.cross(vtx_n1, vtx_diMeson_p3)
    vtx_n2_mm  = um.TVector3Array.cross(vtx_m0_p3, vtx_m1_p3)
    vtx_n3_pll = um.TVector3Array.cross(vtx_n2_pll, vtx_n1)
    vtx_n3_pmm = um.TVector3Array.cross(vtx_n2_pmm, vtx_n1)

    
    # New dataframe
    # =============
    df = pd.DataFrame()
    
    df["info_is_true_nonres_Bd"]    = ( (mc_sample == mc_NonresBd_str) * vtx.BllKst_isTrueNonresBd[vtx_selection_final].astype(bool) ).flatten()
    df["info_is_true_nonres_BdBar"] = ( (mc_sample == mc_NonresBdbar_str) * vtx.BllKst_isTrueNonresBdbar[vtx_selection_final].astype(bool) ).flatten()
    df["info_is_true_res_Bd"]       = ( (mc_sample == mc_ResBd_str) * vtx.BllKst_isTrueResBd[vtx_selection_final].astype(bool) ).flatten()
    df["info_is_true_res_BdBar"]    = ( (mc_sample == mc_ResBdbar_str) * vtx.BllKst_isTrueResBdbar[vtx_selection_final].astype(bool) ).flatten()
    df["B_p"]                       = vtx_Bd_p4.p.flatten()
    df["di"+lepton_big+"_p"]        = vtx_diLepton_p4.p.flatten()
    df["diMeson_p"]                 = vtx_Kpi_p4.p.flatten()
    df[antilepton + "_p"]           = vtx_l0_p4.p.flatten()
    df[lepton + "_p"]               = vtx_l1_p4.p.flatten()
    df["trackPlus_p"]               = vtx_m0_pi_p4.p.flatten()
    df["trackMinus_p"]              = vtx_m1_pi_p4.p.flatten()

    df["B_pz"]                      = vtx_Bd_p4.z.flatten()
    df["di"+lepton_big+"_pz"]       = vtx_diLepton_p4.z.flatten()
    df["diMeson_pz"]                = vtx_Kpi_p4.z.flatten()
    df[antilepton + "_pz"]          = vtx_l0_p4.z.flatten()
    df[lepton + "_pz"]              = vtx_l1_p4.z.flatten()
    df["trackPlus_pz"]              = vtx_m0_pi_p4.z.flatten()
    df["trackMinus_pz"]             = vtx_m1_pi_p4.z.flatten()

    df["B_pT"]                      = vtx_Bd_p4.pt.flatten()
    df["di"+lepton_big+"_pT"]       = vtx_diLepton_p4.pt.flatten()
    df["diMeson_pT"]                = vtx_Kpi_p4.pt.flatten()
    df[antilepton + "_pT"]          = vtx.BllKst_lepton0_pT[vtx_selection_final].flatten()
    df[lepton + "_pT"]              = vtx.BllKst_lepton1_pT[vtx_selection_final].flatten()
    df["trackPlus_pT"]              = vtx.BllKst_meson0_pT[vtx_selection_final].flatten()
    df["trackMinus_pT"]             = vtx.BllKst_meson1_pT[vtx_selection_final].flatten()

    df["B_eta"]                     = vtx_Bd_p4.eta.flatten()
    df["di"+lepton_big+"_eta"]      = vtx_diLepton_p4.eta.flatten()
    df["diMeson_eta"]               = vtx_Kpi_p4.eta.flatten()
    df[antilepton + "_eta"]         = vtx.BllKst_lepton0_eta[vtx_selection_final].flatten()
    df[lepton + "_eta"]             = vtx.BllKst_lepton1_eta[vtx_selection_final].flatten()
    df["trackPlus_eta"]             = vtx.BllKst_meson0_eta[vtx_selection_final].flatten()
    df["trackMinus_eta"]            = vtx.BllKst_meson1_eta[vtx_selection_final].flatten()

    df["B_phi"]                     = vtx_Bd_p4.phi.flatten()
    df["di"+lepton_big+"_phi"]      = vtx_diLepton_p4.phi.flatten()
    df["diMeson_phi"]               = vtx_Kpi_p4.phi.flatten()
    df[antilepton + "_phi"]         = vtx.BllKst_lepton0_phi[vtx_selection_final].flatten()
    df[lepton + "_phi"]             = vtx.BllKst_lepton1_phi[vtx_selection_final].flatten()
    df["trackPlus_phi"]             = vtx.BllKst_meson0_phi[vtx_selection_final].flatten()
    df["trackMinus_phi"]            = vtx.BllKst_meson1_phi[vtx_selection_final].flatten()

    df["energy_"+antilepton]        = vtx_l0_p4.E.flatten()
    df["energy_"+lepton]            = vtx_l1_p4.E.flatten()
    df["energy_trackPlus_pion"]     = vtx_m0_pi_p4.E.flatten()
    df["energy_trackMinus_pion"]    = vtx_m1_pi_p4.E.flatten()
    df["energy_trackPlus_kaon"]     = vtx_m0_K_p4.E.flatten()
    df["energy_trackMinus_kaon"]    = vtx_m1_K_p4.E.flatten()

    df["di"+lepton_big+"_mass"]                 = vtx.BllKst_diLepton_mass[vtx_selection_final].flatten()
    df["diMeson_Kpi_mass"]                      = vtx.BllKst_kaonPion_mass[vtx_selection_final].flatten()
    df["diMeson_piK_mass"]                      = vtx.BllKst_pionKaon_mass[vtx_selection_final].flatten()
    df["B_mass"]                                = vtx.BllKst_B_mass[vtx_selection_final].flatten()
    df["Bbar_mass"]                             = vtx.BllKst_Bbar_mass[vtx_selection_final].flatten()
    df["B_mass_true"]                           = ( (df["info_is_true_nonres_Bd"]) | (df["info_is_true_res_Bd"]) ) * df["B_mass"] + ( (df["info_is_true_nonres_BdBar"]) | (df["info_is_true_res_BdBar"]) ) * df["Bbar_mass"]
    df[ml_config.aliases["B_mass_closer"]]      = ( ( abs(vtx.BllKst_B_mass[vtx_selection_final] - ml_config.PDG["Bd_mass"]) < abs(vtx.BllKst_Bbar_mass[vtx_selection_final] - ml_config.PDG["Bd_mass"]) ) * vtx.BllKst_B_mass[vtx_selection_final] + ( abs(vtx.BllKst_B_mass[vtx_selection_final] - ml_config.PDG["Bd_mass"]) >= abs(vtx.BllKst_Bbar_mass[vtx_selection_final] - ml_config.PDG["Bd_mass"]) ) * vtx.BllKst_Bbar_mass[vtx_selection_final] ).flatten()
    df["B_mass_Kstar_mass_closer"]              = ( abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["B_mass"] + ( abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["Bbar_mass"]
    df["mass_pionKaon_kaonPion_ratio"]          = ( vtx.BllKst_pionKaon_mass[vtx_selection_final] / vtx.BllKst_kaonPion_mass[vtx_selection_final] ).flatten()
    df[ml_config.aliases["Kstar_mass_closer"]]  = ( ( abs(vtx.BllKst_pionKaon_mass[vtx_selection_final] - ml_config.PDG["Kstar_mass"]) < abs(vtx.BllKst_kaonPion_mass[vtx_selection_final] - ml_config.PDG["Kstar_mass"]) ) * vtx.BllKst_pionKaon_mass[vtx_selection_final] + ( abs(vtx.BllKst_pionKaon_mass[vtx_selection_final] - ml_config.PDG["Kstar_mass"]) >= abs(vtx.BllKst_kaonPion_mass[vtx_selection_final] - ml_config.PDG["Kstar_mass"]) ) * vtx.BllKst_kaonPion_mass[vtx_selection_final] ).flatten()
    df["mass_pionKaon_kaonPion_significance"]   = ( abs(vtx.BllKst_kaonPion_mass[vtx_selection_final] - vtx.BllKst_pionKaon_mass[vtx_selection_final]) / np.sqrt(vtx.BllKst_kaonPion_mass[vtx_selection_final]**2 + vtx.BllKst_pionKaon_mass[vtx_selection_final]**2) ).flatten()
    df["mass_"+antilepton + "_trackPlus_kaon"]  = ( vtx_l0_p4 + vtx_m0_K_p4 ).mass.flatten()
    df["mass_"+antilepton + "_trackMinus_kaon"] = ( vtx_l0_p4 + vtx_m1_K_p4 ).mass.flatten()
    df["mass_"+lepton + "_trackPlus_kaon"]      = ( vtx_l1_p4 + vtx_m0_K_p4 ).mass.flatten()
    df["mass_"+lepton + "_trackMinus_kaon"]     = ( vtx_l1_p4 + vtx_m1_K_p4 ).mass.flatten()
    df["mass_"+antilepton + "_trackPlus_pion"]  = ( vtx_l0_p4 + vtx_m0_pi_p4 ).mass.flatten()
    df["mass_"+antilepton + "_trackMinus_pion"] = ( vtx_l0_p4 + vtx_m1_pi_p4 ).mass.flatten()
    df["mass_"+lepton + "_trackPlus_pion"]      = ( vtx_l1_p4 + vtx_m0_pi_p4 ).mass.flatten()
    df["mass_"+lepton + "_trackMinus_pion"]     = ( vtx_l1_p4 + vtx_m1_pi_p4 ).mass.flatten()


    # NOTE: K <-> pi doesn't matter here...
    df["dPhi_"+antilepton + "_"+lepton + ""] = abs( vtx_l0_p4.delta_phi(vtx_l1_p4) ).flatten()
    df["dPhi_trackPlus_trackMinus"]          = abs( vtx_m0_K_p4.delta_phi(vtx_m1_pi_p4) ).flatten()
    df["dPhi_"+antilepton + "_trackPlus"]    = abs( vtx_l0_p4.delta_phi(vtx_m0_K_p4) ).flatten()
    df["dPhi_"+antilepton + "_trackMinus"]   = abs( vtx_l0_p4.delta_phi(vtx_m1_pi_p4) ).flatten()
    df["dPhi_"+lepton + "_trackPlus"]        = abs( vtx_l1_p4.delta_phi(vtx_m0_K_p4) ).flatten()
    df["dPhi_"+lepton + "_trackMinus"]       = abs( vtx_l1_p4.delta_phi(vtx_m1_pi_p4) ).flatten()
    df["dPhi_di"+lepton_big+"_Kstar"]        = abs( vtx_diLepton_p4.delta_phi(vtx_Kpi_p4) ).flatten()
    df["dPhi_"+antilepton + "_Kstar"]        = abs( vtx_l0_p4.delta_phi(vtx_Kpi_p4) ).flatten()
    df["dPhi_"+lepton + "_Kstar"]            = abs( vtx_l1_p4.delta_phi(vtx_Kpi_p4) ).flatten()
    df["dPhi_di"+lepton_big+"_trackPlus"]    = abs( vtx_diLepton_p4.delta_phi(vtx_m0_K_p4) ).flatten()
    df["dPhi_di"+lepton_big+"_trackMinus"]   = abs( vtx_diLepton_p4.delta_phi(vtx_m1_pi_p4) ).flatten()

    df["dR_di"+lepton_big+"_Kstar"]          = vtx_diLepton_p4.delta_r(vtx_Kpi_p4).flatten()
    df["dR_"+antilepton + "_Kstar"]          = vtx_l0_p4.delta_r(vtx_Kpi_p4).flatten()
    df["dR_"+lepton + "_Kstar"]              = vtx_l1_p4.delta_r(vtx_Kpi_p4).flatten()
    df["dR_di"+lepton_big+"_trackPlus"]      = vtx_diLepton_p4.delta_r(vtx_m0_K_p4).flatten()
    df["dR_di"+lepton_big+"_trackMinus"]     = vtx_diLepton_p4.delta_r(vtx_m1_pi_p4).flatten()
    df["dR_"+antilepton + "_"+lepton + ""]   = vtx_l0_p4.delta_r(vtx_l1_p4).flatten()
    df["dR_"+antilepton + "_trackPlus"]      = vtx_l0_p4.delta_r(vtx_m0_K_p4).flatten()
    df["dR_"+antilepton + "_trackMinus"]     = vtx_l0_p4.delta_r(vtx_m1_pi_p4).flatten()
    df["dR_"+lepton + "_trackPlus"]          = vtx_l1_p4.delta_r(vtx_m0_K_p4).flatten()
    df["dR_"+lepton + "_trackMinus"]         = vtx_l1_p4.delta_r(vtx_m1_pi_p4).flatten()
    df["dR_trackPlus_trackMinus"]            = vtx_m0_K_p4.delta_r(vtx_m1_pi_p4).flatten()


    df["cos_Theta"]                                           = vtx.BllKst_cos_Theta_PVMinA0[vtx_selection_final].flatten()
    # See the note above "vtx_Bd_p3_err" definition!!!
    df["cos_Theta_pseudo_err"]                                = vtx.BllKst_cos_Theta_PVMinA0_pseudo_err[vtx_selection_final].flatten()
    df["cos_Theta_pseudo_significance"]                       = vtx.BllKst_cos_Theta_PVMinA0_pseudo_significance[vtx_selection_final].flatten()
    df[ml_config.aliases["B_chi2"]]                           = vtx.BllKst_chi2[vtx_selection_final].flatten()
    df["di"+lepton_big+"_chi2"]                               = vtx.BllKst_diLepton_chi2[vtx_selection_final].flatten()
    df["diMeson_chi2"]                                        = vtx.BllKst_diMeson_chi2[vtx_selection_final].flatten()
    df["B_chi2_over_nDoF"]                                    = vtx.BllKst_chi2_over_nDoF[vtx_selection_final].flatten()
    df["di"+lepton_big+"_chi2_over_nDoF"]                     = vtx.BllKst_diLepton_chi2_over_nDoF[vtx_selection_final].flatten()
    df["diMeson_chi2_over_nDoF"]                              = vtx.BllKst_diMeson_chi2_over_nDoF[vtx_selection_final].flatten()
    df["B_pvalue"]                                            = chi2.sf(vtx.BllKst_chi2[vtx_selection_final].flatten(),vtx.BllKst_nDoF[vtx_selection_final].flatten())
    df["di"+lepton_big+"_pvalue"]                             = chi2.sf(vtx.BllKst_diLepton_chi2[vtx_selection_final].flatten(),vtx.BllKst_diLepton_nDoF[vtx_selection_final].flatten())
    df["diMeson_pvalue"]                                      = chi2.sf(vtx.BllKst_diMeson_chi2[vtx_selection_final].flatten(),vtx.BllKst_diMeson_nDoF[vtx_selection_final].flatten())
    df["B_tau_constM_PVMinA0"]                                = vtx.BllKst_B_tau_constM_PVMinA0[vtx_selection_final].flatten()
    df["Bbar_tau_constM_PVMinA0"]                             = vtx.BllKst_Bbar_tau_constM_PVMinA0[vtx_selection_final].flatten()
    df["B_Bbar_tau_closer_PDG"]                               = ( ( abs(vtx.BllKst_B_tau_constM_PVMinA0[vtx_selection_final] - ml_config.PDG["Bd_tau"]) < abs(vtx.BllKst_Bbar_tau_constM_PVMinA0[vtx_selection_final] - ml_config.PDG["Bd_tau"]) ) * vtx.BllKst_B_tau_constM_PVMinA0[vtx_selection_final] + ( abs(vtx.BllKst_B_tau_constM_PVMinA0[vtx_selection_final] - ml_config.PDG["Bd_tau"]) >= abs(vtx.BllKst_Bbar_tau_constM_PVMinA0[vtx_selection_final] - ml_config.PDG["Bd_tau"]) ) * vtx.BllKst_Bbar_tau_constM_PVMinA0[vtx_selection_final] ).flatten()
    df["a0_significance"]                                     = vtx.BllKst_a0_PVMinA0_significance[vtx_selection_final].flatten()
    df["z0_significance"]                                     = vtx.BllKst_z0_PVMinA0_significance[vtx_selection_final].flatten()
    df["Lxy_significance"]                                    = vtx.BllKst_Lxy_PVMinA0_significance[vtx_selection_final].flatten()
    df["di"+lepton_big+"_angle_alpha"]                        = math.pi/2. - vtx_n1.flatten().angle(vtx_diLepton_p3.flatten())
    df["diMeson_angle_alpha"]                                 = math.pi/2. - vtx_n1.flatten().angle(vtx_diMeson_p3.flatten())
    df["di"+lepton_big+"_angle_beta"]                         = vtx_n3_pll.flatten().angle(vtx_B2diLepton.flatten())
    df["diMeson_angle_beta"]                                  = vtx_n3_pmm.flatten().angle(vtx_B2diMeson.flatten())
    df["di"+lepton_big+"_angle_beta_sym"]                     = abs( abs( math.pi/2 - df["di"+lepton_big+"_angle_beta"] ) - math.pi/2 )
    df["diMeson_angle_beta_sym"]                              = abs( abs( math.pi/2 - df["diMeson_angle_beta"] ) - math.pi/2 )
    df["diMeson_angle_alpha_sym"]                             = df["diMeson_angle_alpha"].abs()
    df["di"+lepton_big+"_angle_alpha_sym"]                    = df["di"+lepton_big+"_angle_alpha"].abs()
    df["angle_diMeson_B_di"+lepton_big]                       = vtx_B2diMeson.flatten().angle(vtx_B2diLepton.flatten())
    df["dR_diMeson_B_di"+lepton_big]                          = vtx_B2diMeson_v4.delta_r(vtx_B2diLepton_v4).flatten()
    df["angle_vtx_plane_"+lepton_short+lepton_short+"_plane"] = vtx_n1.flatten().angle(vtx_n2_ll.flatten())
    df["angle_vtx_plane_mm_plane"]                            = vtx_n1.flatten().angle(vtx_n2_mm.flatten())
    df["angle_pp_plane_"+lepton_short+lepton_short+"_plane"]  = vtx_n1_pp.flatten().angle(vtx_n2_ll.flatten())
    df["angle_pp_plane_mm_plane"]                             = vtx_n1_pp.flatten().angle(vtx_n2_mm.flatten())
    df["angle_"+lepton_short+lepton_short+"_plane_mm_plane"]  = vtx_n2_ll.flatten().angle(vtx_n2_mm.flatten())
    df["d_B_diMeson_significance"]                            = distance_significance_vectorized(vtx_Bd_vtx.flatten(), vtx_Bd_vtx_err.flatten(), vtx_diMeson_vtx.flatten(), vtx_diMeson_vtx_err.flatten())
    df["d_B_di"+lepton_big+"_significance"]                   = distance_significance_vectorized(vtx_Bd_vtx.flatten(), vtx_Bd_vtx_err.flatten(), vtx_diLepton_vtx.flatten(), vtx_diLepton_vtx_err.flatten())
    df["d_diMeson_di"+lepton_big+"_significance"]             = distance_significance_vectorized(vtx_diMeson_vtx.flatten(), vtx_diMeson_vtx_err.flatten(), vtx_diLepton_vtx.flatten(), vtx_diLepton_vtx_err.flatten())
    df[antilepton + "_pvalue"]                                = chi2.sf(vtx.BllKst_lepton0_chi2[vtx_selection_final].flatten(),vtx.BllKst_lepton0_nDoF[vtx_selection_final].flatten())
    df[lepton + "_pvalue"]                                    = chi2.sf(vtx.BllKst_lepton1_chi2[vtx_selection_final].flatten(),vtx.BllKst_lepton1_nDoF[vtx_selection_final].flatten())
    df["trackPlus_pvalue"]                                    = chi2.sf(vtx.BllKst_meson0_chi2[vtx_selection_final].flatten(),vtx.BllKst_meson0_nDoF[vtx_selection_final].flatten())
    df["trackMinus_pvalue"]                                   = chi2.sf(vtx.BllKst_meson1_chi2[vtx_selection_final].flatten(),vtx.BllKst_meson1_nDoF[vtx_selection_final].flatten())
    df[antilepton + "_chi2"]                                  = vtx.BllKst_lepton0_chi2[vtx_selection_final].flatten()
    df[lepton + "_chi2"]                                      = vtx.BllKst_lepton1_chi2[vtx_selection_final].flatten()
    df["trackPlus_chi2"]                                      = vtx.BllKst_meson0_chi2[vtx_selection_final].flatten()
    df["trackMinus_chi2"]                                     = vtx.BllKst_meson1_chi2[vtx_selection_final].flatten()
    df[antilepton + "_nDoF"]                                  = vtx.BllKst_lepton0_nDoF[vtx_selection_final].flatten()
    df[lepton + "_nDoF"]                                      = vtx.BllKst_lepton1_nDoF[vtx_selection_final].flatten()
    df["trackPlus_nDoF"]                                      = vtx.BllKst_meson0_nDoF[vtx_selection_final].flatten()
    df["trackMinus_nDoF"]                                     = vtx.BllKst_meson1_nDoF[vtx_selection_final].flatten()
    df[antilepton + "_chi2_over_nDoF"]                        = ( vtx.BllKst_lepton0_chi2[vtx_selection_final] / vtx.BllKst_lepton0_nDoF[vtx_selection_final] ).flatten()
    df[lepton + "_chi2_over_nDoF"]                            = ( vtx.BllKst_lepton1_chi2[vtx_selection_final] / vtx.BllKst_lepton1_nDoF[vtx_selection_final] ).flatten()
    df["trackPlus_chi2_over_nDoF"]                            = ( vtx.BllKst_meson0_chi2[vtx_selection_final] / vtx.BllKst_meson0_nDoF[vtx_selection_final] ).flatten()
    df["trackMinus_chi2_over_nDoF"]                           = ( vtx.BllKst_meson1_chi2[vtx_selection_final] / vtx.BllKst_meson1_nDoF[vtx_selection_final] ).flatten()
    df[ml_config.aliases["event_number"]]                     = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.event_number ).astype(int).flatten()
    df[ml_config.aliases["sample_id"]]                        = mc_sample
    df["info_sample_event_number"]                            = df[ml_config.aliases["sample_id"]].astype(str) + "_" + df[ml_config.aliases["event_number"]].astype(str)
    df["track_multiplicity_after_cuts"]                       = vtx.BllKst_track_multiplicity[vtx_selection_final].flatten()
    df[lepton+"_multiplicity_after_cuts"]                     = vtx.BllKst_lepton_multiplicity[vtx_selection_final].flatten()
    
    if channel == "electron": 
        df["info_diElectron_gsf_id_isOK"]                    = vtx.BllKst_diLepton_gsf_id_isOK[vtx_selection_final].astype(bool).flatten()
        #df["positron_ptvarcone30_TightTTVA_pt1000"]          = vtx.BllKst_lepton0_ptvarcone30_TightTTVA_pt1000[vtx_selection_final].flatten()
        #df["positron_ptvarcone20_TightTTVA_pt1000"]          = vtx.BllKst_lepton0_ptvarcone20_TightTTVA_pt1000[vtx_selection_final].flatten()
        #df["positron_ptvarcone30_TightTTVA_pt500"]           = vtx.BllKst_lepton0_ptvarcone30_TightTTVA_pt500[vtx_selection_final].flatten()
        #df["positron_ptvarcone40_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton0_ptvarcone40_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["positron_ptvarcone30_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton0_ptvarcone30_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["positron_ptcone20_TightTTVALooseCone_pt1000"]    = vtx.BllKst_lepton0_ptcone20_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["positron_ptvarcone20_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton0_ptvarcone20_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["positron_ptvarcone30_TightTTVALooseCone_pt500"]  = vtx.BllKst_lepton0_ptvarcone30_TightTTVALooseCone_pt500[vtx_selection_final].flatten()
        #df["positron_ptcone20_TightTTVALooseCone_pt500"]     = vtx.BllKst_lepton0_ptcone20_TightTTVALooseCone_pt500[vtx_selection_final].flatten()
        #df["positron_ECIDSResult"]                           = vtx.BllKst_lepton0_DFCommonElectronsECIDSResult[vtx_selection_final].flatten()
        #df["positron_AddAmbiguity"]                          = vtx.BllKst_lepton0_DFCommonAddAmbiguity[vtx_selection_final].flatten()
        #df["positron_OQ"]                                    = vtx.BllKst_lepton0_OQ[vtx_selection_final].flatten()
        df["positron_LHVeryLooseIsEMValue"]                  = vtx.BllKst_lepton0_DFCommonElectronsLHVeryLooseIsEMValue[vtx_selection_final].flatten()
        df["positron_LHLooseIsEMValue"]                      = vtx.BllKst_lepton0_DFCommonElectronsLHLooseIsEMValue[vtx_selection_final].flatten()
        df["positron_LHLooseBLIsEMValue"]                    = vtx.BllKst_lepton0_DFCommonElectronsLHLooseBLIsEMValue[vtx_selection_final].flatten()
        df["positron_LHMediumIsEMValue"]                     = vtx.BllKst_lepton0_DFCommonElectronsLHMediumIsEMValue[vtx_selection_final].flatten()
        df["positron_LHTightIsEMValue"]                      = vtx.BllKst_lepton0_DFCommonElectronsLHTightIsEMValue[vtx_selection_final].flatten()
        df["positron_author"]                                = vtx.BllKst_lepton0_author[vtx_selection_final].flatten()
        df["positron_LHVeryLoose"]                           = vtx.BllKst_lepton0_DFCommonElectronsLHVeryLoose[vtx_selection_final].flatten()
        df["positron_LHLoose"]                               = vtx.BllKst_lepton0_DFCommonElectronsLHLoose[vtx_selection_final].flatten()
        df["positron_LHLooseBL"]                             = vtx.BllKst_lepton0_DFCommonElectronsLHLooseBL[vtx_selection_final].flatten()
        df["positron_LHMedium"]                              = vtx.BllKst_lepton0_DFCommonElectronsLHMedium[vtx_selection_final].flatten()
        df["positron_LHTight"]                               = vtx.BllKst_lepton0_DFCommonElectronsLHTight[vtx_selection_final].flatten()
        df["positron_ECIDS"]                                 = vtx.BllKst_lepton0_DFCommonElectronsECIDS[vtx_selection_final].flatten()
        df["positron_CrackVetoCleaning"]                     = vtx.BllKst_lepton0_DFCommonCrackVetoCleaning[vtx_selection_final].flatten()
        df["positron_LHVeryLoosenod0"]                       = vtx.BllKst_lepton0_DFCommonElectronsLHVeryLoosenod0[vtx_selection_final].flatten()
        #df["electron_ptvarcone30_TightTTVA_pt1000"]          = vtx.BllKst_lepton1_ptvarcone30_TightTTVA_pt1000[vtx_selection_final].flatten()
        #df["electron_ptvarcone20_TightTTVA_pt1000"]          = vtx.BllKst_lepton1_ptvarcone20_TightTTVA_pt1000[vtx_selection_final].flatten()
        #df["electron_ptvarcone30_TightTTVA_pt500"]           = vtx.BllKst_lepton1_ptvarcone30_TightTTVA_pt500[vtx_selection_final].flatten()
        #df["electron_ptvarcone40_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton1_ptvarcone40_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["electron_ptvarcone30_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton1_ptvarcone30_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["electron_ptcone20_TightTTVALooseCone_pt1000"]    = vtx.BllKst_lepton1_ptcone20_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["electron_ptvarcone20_TightTTVALooseCone_pt1000"] = vtx.BllKst_lepton1_ptvarcone20_TightTTVALooseCone_pt1000[vtx_selection_final].flatten()
        #df["electron_ptvarcone30_TightTTVALooseCone_pt500"]  = vtx.BllKst_lepton1_ptvarcone30_TightTTVALooseCone_pt500[vtx_selection_final].flatten()
        #df["electron_ptcone20_TightTTVALooseCone_pt500"]     = vtx.BllKst_lepton1_ptcone20_TightTTVALooseCone_pt500[vtx_selection_final].flatten()
        #df["electron_ECIDSResult"]                           = vtx.BllKst_lepton1_DFCommonElectronsECIDSResult[vtx_selection_final].flatten()
        #df["electron_AddAmbiguity"]                          = vtx.BllKst_lepton1_DFCommonAddAmbiguity[vtx_selection_final].flatten()
        #df["electron_OQ"]                                    = vtx.BllKst_lepton1_OQ[vtx_selection_final].flatten()
        df["electron_LHVeryLooseIsEMValue"]                  = vtx.BllKst_lepton1_DFCommonElectronsLHVeryLooseIsEMValue[vtx_selection_final].flatten()
        df["electron_LHLooseIsEMValue"]                      = vtx.BllKst_lepton1_DFCommonElectronsLHLooseIsEMValue[vtx_selection_final].flatten()
        df["electron_LHLooseBLIsEMValue"]                    = vtx.BllKst_lepton1_DFCommonElectronsLHLooseBLIsEMValue[vtx_selection_final].flatten()
        df["electron_LHMediumIsEMValue"]                     = vtx.BllKst_lepton1_DFCommonElectronsLHMediumIsEMValue[vtx_selection_final].flatten()
        df["electron_LHTightIsEMValue"]                      = vtx.BllKst_lepton1_DFCommonElectronsLHTightIsEMValue[vtx_selection_final].flatten()
        df["electron_author"]                                = vtx.BllKst_lepton1_author[vtx_selection_final].flatten()
        df["electron_LHVeryLoose"]                           = vtx.BllKst_lepton1_DFCommonElectronsLHVeryLoose[vtx_selection_final].flatten()
        df["electron_LHLoose"]                               = vtx.BllKst_lepton1_DFCommonElectronsLHLoose[vtx_selection_final].flatten()
        df["electron_LHLooseBL"]                             = vtx.BllKst_lepton1_DFCommonElectronsLHLooseBL[vtx_selection_final].flatten()
        df["electron_LHMedium"]                              = vtx.BllKst_lepton1_DFCommonElectronsLHMedium[vtx_selection_final].flatten()
        df["electron_LHTight"]                               = vtx.BllKst_lepton1_DFCommonElectronsLHTight[vtx_selection_final].flatten()
        df["electron_ECIDS"]                                 = vtx.BllKst_lepton1_DFCommonElectronsECIDS[vtx_selection_final].flatten()
        df["electron_CrackVetoCleaning"]                     = vtx.BllKst_lepton1_DFCommonCrackVetoCleaning[vtx_selection_final].flatten()
        df["electron_LHVeryLoosenod0"]                       = vtx.BllKst_lepton1_DFCommonElectronsLHVeryLoosenod0[vtx_selection_final].flatten()

    df[antilepton + "_charge"]     = vtx.BllKst_lepton0_charge[vtx_selection_final].flatten()
    df[lepton + "_charge"]         = vtx.BllKst_lepton1_charge[vtx_selection_final].flatten()
    df["trackPlus_charge"]         = vtx.BllKst_meson0_charge[vtx_selection_final].flatten()
    df["trackMinus_charge"]        = vtx.BllKst_meson1_charge[vtx_selection_final].flatten()

    df["di"+lepton_big+"_iso_m00"] = vtx.BllKst_diLepton_iso_m00[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m05"] = vtx.BllKst_diLepton_iso_m05[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m10"] = vtx.BllKst_diLepton_iso_m10[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m15"] = vtx.BllKst_diLepton_iso_m15[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m20"] = vtx.BllKst_diLepton_iso_m20[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m25"] = vtx.BllKst_diLepton_iso_m25[vtx_selection_final].flatten()
    df["di"+lepton_big+"_iso_m30"] = vtx.BllKst_diLepton_iso_m30[vtx_selection_final].flatten()

    df[antilepton + "_iso_c05"]    = vtx.BllKst_lepton0_iso_c05[vtx_selection_final].flatten()
    df[antilepton + "_iso_c10"]    = vtx.BllKst_lepton0_iso_c10[vtx_selection_final].flatten()
    df[antilepton + "_iso_c15"]    = vtx.BllKst_lepton0_iso_c15[vtx_selection_final].flatten()
    df[antilepton + "_iso_c20"]    = vtx.BllKst_lepton0_iso_c20[vtx_selection_final].flatten()
    df[antilepton + "_iso_c25"]    = vtx.BllKst_lepton0_iso_c25[vtx_selection_final].flatten()
    df[antilepton + "_iso_c30"]    = vtx.BllKst_lepton0_iso_c30[vtx_selection_final].flatten()
    df[antilepton + "_iso_c35"]    = vtx.BllKst_lepton0_iso_c35[vtx_selection_final].flatten()
    df[antilepton + "_iso_c40"]    = vtx.BllKst_lepton0_iso_c40[vtx_selection_final].flatten()

    df[lepton + "_iso_c05"]        = vtx.BllKst_lepton1_iso_c05[vtx_selection_final].flatten()
    df[lepton + "_iso_c10"]        = vtx.BllKst_lepton1_iso_c10[vtx_selection_final].flatten()
    df[lepton + "_iso_c15"]        = vtx.BllKst_lepton1_iso_c15[vtx_selection_final].flatten()
    df[lepton + "_iso_c20"]        = vtx.BllKst_lepton1_iso_c20[vtx_selection_final].flatten()
    df[lepton + "_iso_c25"]        = vtx.BllKst_lepton1_iso_c25[vtx_selection_final].flatten()
    df[lepton + "_iso_c30"]        = vtx.BllKst_lepton1_iso_c30[vtx_selection_final].flatten()
    df[lepton + "_iso_c35"]        = vtx.BllKst_lepton1_iso_c35[vtx_selection_final].flatten()
    df[lepton + "_iso_c40"]        = vtx.BllKst_lepton1_iso_c40[vtx_selection_final].flatten()


    df["trackPlus_iso_c05"]        = vtx.BllKst_meson0_iso_c05[vtx_selection_final].flatten()
    df["trackPlus_iso_c10"]        = vtx.BllKst_meson0_iso_c10[vtx_selection_final].flatten()
    df["trackPlus_iso_c15"]        = vtx.BllKst_meson0_iso_c15[vtx_selection_final].flatten()
    df["trackPlus_iso_c20"]        = vtx.BllKst_meson0_iso_c20[vtx_selection_final].flatten()
    df["trackPlus_iso_c25"]        = vtx.BllKst_meson0_iso_c25[vtx_selection_final].flatten()
    df["trackPlus_iso_c30"]        = vtx.BllKst_meson0_iso_c30[vtx_selection_final].flatten()
    df["trackPlus_iso_c35"]        = vtx.BllKst_meson0_iso_c35[vtx_selection_final].flatten()
    df["trackPlus_iso_c40"]        = vtx.BllKst_meson0_iso_c40[vtx_selection_final].flatten()

    df["trackMinus_iso_c05"]       = vtx.BllKst_meson1_iso_c05[vtx_selection_final].flatten()
    df["trackMinus_iso_c10"]       = vtx.BllKst_meson1_iso_c10[vtx_selection_final].flatten()
    df["trackMinus_iso_c15"]       = vtx.BllKst_meson1_iso_c15[vtx_selection_final].flatten()
    df["trackMinus_iso_c20"]       = vtx.BllKst_meson1_iso_c20[vtx_selection_final].flatten()
    df["trackMinus_iso_c25"]       = vtx.BllKst_meson1_iso_c25[vtx_selection_final].flatten()
    df["trackMinus_iso_c30"]       = vtx.BllKst_meson1_iso_c30[vtx_selection_final].flatten()
    df["trackMinus_iso_c35"]       = vtx.BllKst_meson1_iso_c35[vtx_selection_final].flatten()
    df["trackMinus_iso_c40"]       = vtx.BllKst_meson1_iso_c40[vtx_selection_final].flatten()

    df[lepton+"0_p"]   = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_p"]   + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_p"]
    df[lepton+"1_p"]   = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_p"]   + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_p"]
    df[lepton+"0_pz"]  = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_pz"]  + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_pz"]
    df[lepton+"1_pz"]  = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_pz"]  + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_pz"]
    df[lepton+"0_pT"]  = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_pT"]  + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_pT"]
    df[lepton+"1_pT"]  = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_pT"]  + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_pT"]
    df[lepton+"0_eta"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_eta"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_eta"]
    df[lepton+"1_eta"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_eta"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_eta"]
    df[lepton+"0_phi"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_phi"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_phi"]
    df[lepton+"1_phi"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_phi"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_phi"]

    df["track0_p"]     = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_p"]    + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_p"]
    df["track1_p"]     = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_p"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_p"]
    df["track0_pz"]    = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_pz"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_pz"]
    df["track1_pz"]    = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_pz"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_pz"]
    df["track0_pT"]    = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_pT"]   + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_pT"]
    df["track1_pT"]    = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_pT"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_pT"]
    df["track0_eta"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_eta"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_eta"]
    df["track1_eta"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_eta"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_eta"]
    df["track0_phi"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_phi"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_phi"]
    df["track1_phi"]   = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_phi"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_phi"]

    df["kaon_pT"]      = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_pT"]  + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_pT"]
    df["pion_pT"]      = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_pT"]   + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_pT"]
    df["kaon_eta"]     = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_eta"] + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_eta"]
    df["pion_eta"]     = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_eta"]  + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_eta"]
    df["kaon_phi"]     = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_phi"] + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_phi"]
    df["pion_phi"]     = ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) < abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackPlus_phi"]  + ( abs(df["diMeson_piK_mass"] - ml_config.PDG["Kstar_mass"]) >= abs(df["diMeson_Kpi_mass"] - ml_config.PDG["Kstar_mass"]) ) * df["trackMinus_phi"]

    df[lepton+"0_iso_c05"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c05"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c05"]
    df[lepton+"1_iso_c05"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c05"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c05"]
    df[lepton+"0_iso_c10"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c10"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c10"]
    df[lepton+"1_iso_c10"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c10"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c10"]
    df[lepton+"0_iso_c15"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c15"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c15"]
    df[lepton+"1_iso_c15"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c15"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c15"]
    df[lepton+"0_iso_c20"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c20"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c20"]
    df[lepton+"1_iso_c20"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c20"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c20"]
    df[lepton+"0_iso_c25"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c25"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c25"]
    df[lepton+"1_iso_c25"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c25"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c25"]
    df[lepton+"0_iso_c30"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c30"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c30"]
    df[lepton+"1_iso_c30"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c30"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c30"]
    df[lepton+"0_iso_c35"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c35"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c35"]
    df[lepton+"1_iso_c35"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c35"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c35"]
    df[lepton+"0_iso_c40"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[antilepton + "_iso_c40"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[lepton + "_iso_c40"]
    df[lepton+"1_iso_c40"] = (df[antilepton + "_pT"] > df[lepton + "_pT"]) * df[lepton + "_iso_c40"] + (df[antilepton + "_pT"] <= df[lepton + "_pT"]) * df[antilepton + "_iso_c40"]


    df["track0_iso_c05"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c05"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c05"]
    df["track1_iso_c05"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c05"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c05"]
    df["track0_iso_c10"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c10"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c10"]
    df["track1_iso_c10"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c10"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c10"]
    df["track0_iso_c15"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c15"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c15"]
    df["track1_iso_c15"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c15"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c15"]
    df["track0_iso_c20"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c20"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c20"]
    df["track1_iso_c20"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c20"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c20"]
    df["track0_iso_c25"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c25"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c25"]
    df["track1_iso_c25"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c25"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c25"]
    df["track0_iso_c30"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c30"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c30"]
    df["track1_iso_c30"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c30"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c30"]
    df["track0_iso_c35"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c35"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c35"]
    df["track1_iso_c35"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c35"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c35"]
    df["track0_iso_c40"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_iso_c40"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_iso_c40"]
    df["track1_iso_c40"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_iso_c40"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_iso_c40"]


    df["diMeson_KK_mass"]   = ( vtx_m0_K_p4  + vtx_m1_K_p4  ).mass.flatten()
    df["diMeson_pipi_mass"] = ( vtx_m0_pi_p4 + vtx_m1_pi_p4 ).mass.flatten()

    df["diMeson_Kpi_mass_pT"]           = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["diMeson_Kpi_mass"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["diMeson_piK_mass"]
    df["diMeson_piK_mass_pT"]           = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["diMeson_piK_mass"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["diMeson_Kpi_mass"]
    df["diMeson_Kpi_piK_mass_avg"]      = ( df["diMeson_Kpi_mass"] + df["diMeson_piK_mass"] ) / 2
    df["diMeson_Kpi_piK_mass_diff"]     = df["diMeson_Kpi_mass"]    - df["diMeson_piK_mass"]
    df["diMeson_Kpi_piK_mass_pT_diff"]  = df["diMeson_Kpi_mass_pT"] - df["diMeson_piK_mass_pT"]
    df["diMeson_Kpi_piK_mass_ratio"]    = df["diMeson_Kpi_mass"]    / df["diMeson_piK_mass"]
    df["diMeson_Kpi_piK_mass_pT_ratio"] = df["diMeson_Kpi_mass_pT"] / df["diMeson_piK_mass_pT"]

    # NOTE: calibration
    df["trackPlus_dEdx"]  = vtx.BllKst_meson0_pixeldEdx[vtx_selection_final].flatten()
    df["trackMinus_dEdx"] = vtx.BllKst_meson1_pixeldEdx[vtx_selection_final].flatten()

    df["track0_dEdx"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackPlus_dEdx"]  + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackMinus_dEdx"]
    df["track1_dEdx"] = ( df["trackPlus_pT"] > df["trackMinus_pT"] ) * df["trackMinus_dEdx"] + ( df["trackPlus_pT"] <= df["trackMinus_pT"] ) * df["trackPlus_dEdx"]

    df["tracks_dEdx_sum"]      = df["trackPlus_dEdx"] + df["trackMinus_dEdx"]
    df["tracks_dEdx_diff"]     = df["trackPlus_dEdx"] - df["trackMinus_dEdx"]
    df["tracks_dEdx_ratio"]    = df["trackPlus_dEdx"] / df["trackMinus_dEdx"]
    df["tracks_dEdx_pT_diff"]  = df["track0_dEdx"] - df["track1_dEdx"]
    df["tracks_dEdx_pT_ratio"] = df["track0_dEdx"] / df["track1_dEdx"]

    df["info_sample_weight"]         = sample_weight
    df["info_candidate_count"]       = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.BllKst_B_mass[vtx_selection_final].counts ).astype(int).flatten()
    df["info_candidate_count_orig"]  = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.BllKst_B_mass.counts ).astype(int).flatten()
    df["info_candidate_weight"]      = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.weight ).flatten()

    ### Trigger Decision & PRW Info ###
    ### --------------------------- ###
    df[ "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t"                    ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.HLT_e5_lhvloose_nod0_bBeexM6000t_acceptance  ).flatten()
    df[ "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t"                   ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.HLT_2e5_lhvloose_nod0_bBeexM6000t_acceptance ).flatten()
    df[ "info_trigger_decision_SpecialHLTOR"                                        ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.pass_UnseededHLTOR ).flatten()
    df[ "info_trigger_decision_SpecialL1OR"                                         ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.pass_L1OR          ).flatten()
    df[ "info_pure_pileup_weight"                                                   ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.pure_pileup_weight    ).flatten()
    df[ "info_prescale_weight_SpecialL1OR"                                          ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.prescale_weight_L1OR  ).flatten()
    df[ "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"                     ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t  ).flatten()
    df[ "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"                    ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t ).flatten()
    df[ "info_prescale_weight_SpecialHLTOR"                                         ] = ( vtx.BllKst_B_mass[vtx_selection_final] * 0 + vtx.prescale_weight_HLTOR ).flatten()
    df[ "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"         ] = df[ "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"  ] * df[ "info_prescale_weight_SpecialL1OR" ]
    df[ "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        ] = df[ "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t" ] * df[ "info_prescale_weight_SpecialL1OR" ]
    df[ "info_prescale_weight_SpecialHLTOR_SpecialL1OR"                             ] = df[ "info_prescale_weight_SpecialHLTOR"                      ] * df[ "info_prescale_weight_SpecialL1OR" ]
    df[ "info_trigger_decision"                                                     ] = df[ "info_trigger_decision_SpecialHLTOR" ] * df[ "info_trigger_decision_SpecialL1OR" ]
    df[ "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        ] = df[ "info_trigger_decision_SpecialL1OR" ] * df[ "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t" ]
    df[ "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        ] = df[ "info_trigger_decision_SpecialL1OR" ] * df[ "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t" ]
    df[ "info_pileup_prescale_weight_SpecialL1OR"                                   ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_SpecialL1OR"  ]
    df[ "info_pileup_prescale_weight_SpecialHLTOR"                                  ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_SpecialHLTOR" ]
    df[ "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"              ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"  ]
    df[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"             ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t" ]
    df[ "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"  ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ]
    df[ "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" ]
    df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"                      ] = df[ "info_pure_pileup_weight" ] * df[ "info_prescale_weight_SpecialHLTOR_SpecialL1OR" ]
    df[ "info_weight" ] = df[ "info_candidate_weight" ] * df[ "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR" ]

    df[ml_config.aliases["is_true"]] = vtx_truth.flatten()

    df["info_full_selection"]           = (df["trackPlus_pT"]>1000) & (df["trackMinus_pT"]>1000) & (df["B_chi2_over_nDoF"]<2) & (((df["B_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_Kpi_mass"]-ml_config.PDG["Kstar_mass"])<50) & (df["B_mass"]>4700) & (df["B_mass"]<5700)) | ((df["Bbar_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_piK_mass"]-ml_config.PDG["Kstar_mass"])<50) & (df["Bbar_mass"]>4700) & (df["Bbar_mass"]<5700)))
    df["info_full_selection_no_B_mass"] = (df["trackPlus_pT"]>1000) & (df["trackMinus_pT"]>1000) & (df["B_chi2_over_nDoF"]<2) & (((df["B_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_Kpi_mass"]-ml_config.PDG["Kstar_mass"])<50)) | ((df["Bbar_tau_constM_PVMinA0"]>0.2) & (abs(df["diMeson_piK_mass"]-ml_config.PDG["Kstar_mass"])<50)))

    print()

    # Clean the dataframe (mostly in place!)
    # ======================================
    utils.print_time()
    remove_null_errors(df)
    remove_unfilled_variables(df)
    remove_duplicated_candidates(df)

    to_remove = []
    for item in df.columns.values:  # If the variables are not present in plotnames.features_histos_dict or not started with info_, remove them
        if item not in plotnames.features_histos_dict and not fnmatch(item, "info_*"):
            to_remove += [item]
    print(utils.red("Not in plotnames:"), to_remove)
    print()

    print(utils.red("Not in dataframe:"))
    for item in plotnames.features_histos_dict:
        if item not in df.columns.values:
            print("\t", item)
    print()

    if crop_columns:
        df = crop_dataframe(df, to_remove)

    # Print cut-flow table
    # ====================
    utils.print_time()

    summary = { " " : ["Candidates", "Events", "Candidates", "Events"] }

    summary["initial"]                      = [vtx.BllKst_B_mass.count().sum()                         , vtx.BllKst_B_mass.any().sum()                         , vtx.BllKst_B_mass[vtx_selection_truth].count().sum()                          , vtx.BllKst_B_mass[vtx_selection_truth].any().sum()]
    
    if channel == "muon":
        summary["trigger"]                  = [vtx.BllKst_B_mass[vtx_selection_trigger].count().sum() , vtx.BllKst_B_mass[vtx_selection_trigger].any().sum() , vtx.BllKst_B_mass[vtx_selection_trigger * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_trigger * vtx_selection_truth].any().sum()]
    elif channel == "electron":
        summary["GSF-ID"]                       = [vtx.BllKst_B_mass[vtx_selection_gsf_id].count().sum()   , vtx.BllKst_B_mass[vtx_selection_gsf_id].any().sum()   , vtx.BllKst_B_mass[vtx_selection_gsf_id   * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_gsf_id   * vtx_selection_truth].any().sum()]
        
    if all_charges:
        summary["dR"+lepton_short+lepton_short]                     = [vtx.BllKst_B_mass[vtx_selection_dRll].count().sum()     , vtx.BllKst_B_mass[vtx_selection_dRll].any().sum()     , vtx.BllKst_B_mass[vtx_selection_dRll     * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_dRll     * vtx_selection_truth].any().sum()]
    else:
        summary["charge"]                   = [vtx.BllKst_B_mass[vtx_selection_charge].count().sum()   , vtx.BllKst_B_mass[vtx_selection_charge].any().sum()   , vtx.BllKst_B_mass[vtx_selection_charge   * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_charge   * vtx_selection_truth].any().sum()]
        summary["dR"+lepton_short+lepton_short]                     = [vtx.BllKst_B_mass[vtx_selection_dRll].count().sum()     , vtx.BllKst_B_mass[vtx_selection_dRll].any().sum()     , vtx.BllKst_B_mass[vtx_selection_dRll     * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_dRll     * vtx_selection_truth].any().sum()]
    
    summary["trk_qual"]                     = [vtx.BllKst_B_mass[vtx_selection_trk_qual].count().sum() , vtx.BllKst_B_mass[vtx_selection_trk_qual].any().sum() , vtx.BllKst_B_mass[vtx_selection_trk_qual * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_trk_qual * vtx_selection_truth].any().sum()]
    summary[lepton_short+"_pT"]                         = [vtx.BllKst_B_mass[vtx_selection_l_pT].count().sum()     , vtx.BllKst_B_mass[vtx_selection_l_pT].any().sum()     , vtx.BllKst_B_mass[vtx_selection_l_pT     * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_l_pT     * vtx_selection_truth].any().sum()]
    summary[lepton_short+"_eta"]                        = [vtx.BllKst_B_mass[vtx_selection_l_eta].count().sum()    , vtx.BllKst_B_mass[vtx_selection_l_eta].any().sum()    , vtx.BllKst_B_mass[vtx_selection_l_eta    * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_l_eta    * vtx_selection_truth].any().sum()]
    summary["m_pT"]                         = [vtx.BllKst_B_mass[vtx_selection_m_pT].count().sum()     , vtx.BllKst_B_mass[vtx_selection_m_pT].any().sum()     , vtx.BllKst_B_mass[vtx_selection_m_pT     * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_m_pT     * vtx_selection_truth].any().sum()]
    summary["m_eta"]                        = [vtx.BllKst_B_mass[vtx_selection_m_eta].count().sum()    , vtx.BllKst_B_mass[vtx_selection_m_eta].any().sum()    , vtx.BllKst_B_mass[vtx_selection_m_eta    * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_m_eta    * vtx_selection_truth].any().sum()]
    summary[lepton_short+lepton_short+"_mass"]                      = [vtx.BllKst_B_mass[vtx_selection_ll_mass].count().sum()  , vtx.BllKst_B_mass[vtx_selection_ll_mass].any().sum()  , vtx.BllKst_B_mass[vtx_selection_ll_mass  * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_ll_mass  * vtx_selection_truth].any().sum()]
    summary["B_mass"]                       = [vtx.BllKst_B_mass[vtx_selection_B_mass].count().sum()   , vtx.BllKst_B_mass[vtx_selection_B_mass].any().sum()   , vtx.BllKst_B_mass[vtx_selection_B_mass   * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_B_mass   * vtx_selection_truth].any().sum()]
    summary["mm_mass"]                      = [vtx.BllKst_B_mass[vtx_selection_mm_mass].count().sum()  , vtx.BllKst_B_mass[vtx_selection_mm_mass].any().sum()  , vtx.BllKst_B_mass[vtx_selection_mm_mass  * vtx_selection_truth].count().sum() , vtx.BllKst_B_mass[vtx_selection_mm_mass  * vtx_selection_truth].any().sum()]
    summary["remove_duplicated_candidates"] = [len(df)                                                 , len(df[ml_config.aliases["event_number"]].unique())   , len(df[df[ml_config.aliases["is_true"]]])                                     , len(df[df[ml_config.aliases["is_true"]]][ml_config.aliases["event_number"]].unique())]


    table_size = [len(max(summary, key = lambda x: len(x)))]  # Get the length of the longest key and values in "summary" to set the table width
    for i in range(0, 4):
        table_size += [len(str(max(summary.values(), key = lambda x: len(str(x[i])))[i]))]

    format_str = "{:<"+str(table_size[0])+"} | {:>"+str(table_size[1])+"} | {:>"+str(table_size[2])+"} | {:>"+str(table_size[3])+"} | {:>"+str(table_size[4])+"}"

    print()
    print(str("{:<"+str(table_size[0])+"} | {:^"+str(table_size[1]+table_size[2]+3)+"} | {:^"+str(table_size[3]+table_size[4]+3)+"}").format("", "All", "True"))
    for key in summary:
        print(format_str.format(key, summary[key][0], summary[key][1], summary[key][2], summary[key][3]))
    print()    
    
    # Save dataframe to file
    # ======================
    output_filename = path.join(output_dir, path.basename(ntuple.replace(".root", "." + output_type)))
    if save_dataframes:
        utils.print_time()
        utils.df_save(df, output_filename, "Dataframe saved to:")
        print()

        if check_back:
            orig_df_shape = df.shape
            df = utils.df_load(output_filename)
            if orig_df_shape != df.shape:
                print(utils.red("Check-back: " + str(orig_df_shape) + " vs. " + str(df.shape)))
                quit()
            else:
                print(utils.green("Check-back: OK"))
                print()


    # Prepare splits directly here...
    # ===============================
    if prepare_splits:
        utils.print_time()
        if bool([item for item in (ml_config.electron_signal_id if channel == "electron" else ml_config.muon_signal_id) if(item in output_filename)]):
            # Split signal MC 1:1
            df_sample1, df_sample2 = split_frac(df = df, input_data = output_filename, output_path = output_dir, save_df = save_dataframes, true_only = True, channel = channel)

    utils.print_time()
    print("\n", "=" * 90, "\n\n", sep = "")
