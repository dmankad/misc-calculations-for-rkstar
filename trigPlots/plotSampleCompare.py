#!/usr/bin/env python3

### Imports ###
### ------- ###

### Imports -> I/O ###
### -------------- ###
import os
import pathlib
import sys

### Imports -> Matrix Manipulation ###
### ------------------------------ ###
import pandas as pd
import numpy as np

### Imports -> Plotting ###
### ------------------- ###
import matplotlib 
matplotlib.use( "agg" )
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import copy

### Imports -> R(K*) Internal ###
### ------------------------- ###
from ml_share import ml_config
from ml_share import plotting
from ml_share import utils
from ml_share import samples

### Declaring the Config & Paths ###
### ---------------------------- ###

# Channel
channel                       = "electron"

# Input Paths
input_dir_r2                  = "/storage/agrp/dvijm/ntuples/feather/electron/refitPRWUnseededPassThrough"
input_dir_r3                  = "/storage/agrp/dvijm/ntuples/feather/electron/R3PRWv2"
input_data_nr_r2              = utils.get_list_make_list( "ntuple-30059[0-1]_part_0[1-2].ftr", input_dir_r2 )
input_data_r_r2               = utils.get_list_make_list( "ntuple-30059[2-3]_part_0[1-2].ftr", input_dir_r2 )
input_data_nr_r3              = utils.get_list_make_list( "ntuple-80187[2-3]_part_0[1-2].ftr", input_dir_r3 )
input_data_r_r3               = utils.get_list_make_list( "ntuple-80187[2-3]_part_0[1-2].ftr", input_dir_r3 ) # Placeholder

# Configs
config_file_ml1_r2            = "../ml_main/configs/gnn1_electron_multiclass_goblin_v2.yaml"
config_file_ml2_r2            = "../ml_main/configs/gnn2_electron_multiclass_goblin_v2.yaml"
config_file_ml1_r3            = "../ml_main/configs/gnn1_electron_multiclass_goblin_v2_r3.yaml"
config_file_ml2_r3            = "../ml_main/configs/gnn2_electron_multiclass_goblin_v2_r3.yaml"
config_ml1_r2                 = utils.config_load( config_file_ml1_r2  )
config_ml2_r2                 = utils.config_load( config_file_ml2_r2  )
config_ml1_r3                 = utils.config_load( config_file_ml1_r3  )
config_ml2_r3                 = utils.config_load( config_file_ml2_r3  )
config_default_ml1_r2         = utils.yaml_load( "../ml_main/configs/" + config_ml1_r2[ "default_config" ] )[ "common" ]
config_default_ml2_r2         = utils.yaml_load( "../ml_main/configs/" + config_ml2_r2[ "default_config" ] )[ "common" ]
config_default_ml1_r3         = utils.yaml_load( "../ml_main/configs/" + config_ml1_r3[ "default_config" ] )[ "common" ] 
config_default_ml2_r3         = utils.yaml_load( "../ml_main/configs/" + config_ml2_r3[ "default_config" ] )[ "common" ]
config_default                = copy.deepcopy( config_default_ml1_r2 )
assert config_default_ml1_r2 == config_default_ml2_r2
assert config_default_ml1_r3 == config_default_ml2_r3
assert config_default_ml1_r2 == config_default_ml1_r3

# Output Paths
output_dir                    = "./"
output_dir_plots              = os.path.join( output_dir, "plots"  )
output_dir_yields             = os.path.join( output_dir, "yields" )
pathlib.Path( output_dir ).mkdir( parents = True, exist_ok = True )

### Selections ###
### ---------- ###

selections = {}
selections[ "baseline_q2low"      ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high"     ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full"     ] = "( $selection_main & $selection_mc & $selection_usr )"
selections[ "baseline_q2low_all"  ] = "( $selection_main & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high_all" ] = "( $selection_main & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full_all" ] = "( $selection_main & $selection_usr )"
for selection_name in selections.keys():
    selections[ selection_name ] = utils.get_selection( selections[ selection_name ], config_default )

### Load Data ###
### --------- ###

### Load Data -> ML Features ###
### ------------------------ ### 
def getGraphFeatures( config ):
    graph_dict  = utils.json_load( os.path.join( "../ml_main/", config[ "common" ][ "graph_dict" ] ) )
    parameters  = config[ "common" ] 
    nested_list = []
    if isinstance( parameters[ "graph_version" ], dict ):
        graph_topo_names = [ item[ "graph_topo" ] for item in parameters[ "graph_version" ].values() ]
    else:
        graph_topo_names = [ parameters[ "graph_version" ] ]
    for item in graph_topo_names:
        if item in graph_dict:
            nested_list += list( graph_dict.get( item ).get( "nodes" ).values() )
            nested_list += list( graph_dict.get( item ).get( "edges" ).values() )
            nested_list += [ graph_dict.get( item ).get( "goblins", [] ) ]
        else:
            print( utils.red( sys._getframe().f_code.co_name + ": " + item + " does not exist in " + parameters[ "graph_dict" ] + "!" ) )
            exit( 1 )
    parameters[ "features" ] = list( set( [ item for sublist in nested_list for item in sublist ] ) )
    parameters[ "features" ] = [ item for item in parameters[ "features" ] if item != "empty_feature" ]
    parameters[ "features" ].sort()
    features = parameters[ "features" ]
    return features
    
features_ml1_r2                    = config_ml1_r2[ "common"   ].get( "features", [] )
features_ml2_r2                    = config_ml2_r2[ "common"   ].get( "features", [] )
features_ml1_r3                    = config_ml1_r3[ "common"   ].get( "features", [] )
features_ml2_r3                    = config_ml2_r3[ "common"   ].get( "features", [] )
features                           = list( set( features_ml1_r2 + features_ml2_r2 + features_ml1_r3 + features_ml2_r3 ) )
if features == []:
    features_ml1_r2 = getGraphFeatures( config_ml1_r2 )
    features_ml2_r2 = getGraphFeatures( config_ml2_r2 )
    features_ml1_r3 = getGraphFeatures( config_ml1_r3 )
    features_ml2_r3 = getGraphFeatures( config_ml2_r3 )
    features        = list( set( features_ml1_r2 + features_ml2_r2 + features_ml1_r3 + features_ml2_r3 ) )
    features.sort()

### Load Data -> Trigger/PRW Features ###
### --------------------------------- ###

trigger_features_to_load_r2 = [
    # Pileup Weights
    "info_pure_pileup_weight",

    # Trigger Decision
    "info_trigger_decision",
    "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    
    # Prescale Weights
    "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_SpecialHLTOR", 
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_prescale_weight_SpecialHLTOR_SpecialL1OR",
    
    # Pileup Prescale Weights
    "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_SpecialHLTOR", 
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"
]
trigger_features_to_load_r3 = [
    # Pileup Weights
    "info_pure_pileup_weight",

    # Trigger Decision
    "info_trigger_decision",
    "info_trigger_decision_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary",
    "info_trigger_decision_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary",

    # Prescale Weights
    "info_prescale_weight_L1BKeePrimary",
    "info_prescale_weight_HLT_e5_lhvloose_bBeeM6000",
    "info_prescale_weight_HLT_2e5_lhvloose_bBeeM6000",
    "info_prescale_weight_UnseededHLTOR", 
    "info_prescale_weight_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary",
    "info_prescale_weight_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary",
    "info_prescale_weight_UnseededHLTOR_L1BKeePrimary",

    # Pileup Prescale Weights
    "info_pileup_prescale_weight_L1BKeePrimary",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_bBeeM6000",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_bBeeM6000",
    "info_pileup_prescale_weight_UnseededHLTOR", 
    "info_pileup_prescale_weight_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary",
    "info_pileup_prescale_weight_UnseededHLTOR_L1BKeePrimary"
]

tf_aliases = {
    # Trigger Decision -> Run 2
    "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        : "info_trigger_decision_HLT_e5_SpecialL1OR",
    "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"       : "info_trigger_decision_HLT_2e5_SpecialL1OR",
    # Trigger Decision -> Run 3
    "info_trigger_decision_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary"             : "info_trigger_decision_HLT_e5_SpecialL1OR",
    "info_trigger_decision_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary"            : "info_trigger_decision_HLT_2e5_SpecialL1OR",

    # Prescale Weights -> Run 2
    "info_prescale_weight_SpecialL1OR"                                          : "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"                     : "info_prescale_weight_HLT_e5",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"                    : "info_prescale_weight_HLT_2e5",
    "info_prescale_weight_SpecialHLTOR"                                         : "info_prescale_weight_HLTOR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"         : "info_prescale_weight_HLT_e5_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        : "info_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_prescale_weight_SpecialHLTOR_SpecialL1OR"                             : "info_prescale_weight_HLTOR_SpecialL1OR",
    # Prescale Weights -> Run 3
    "info_prescale_weight_L1BKeePrimary"                                        : "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_bBeeM6000"                            : "info_prescale_weight_HLT_e5",
    "info_prescale_weight_HLT_2e5_lhvloose_bBeeM6000"                           : "info_prescale_weight_HLT_2e5",
    "info_prescale_weight_UnseededHLTOR"                                        : "info_prescale_weight_HLTOR",
    "info_prescale_weight_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary"              : "info_prescale_weight_HLT_e5_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary"             : "info_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_prescale_weight_UnseededHLTOR_L1BKeePrimary"                          : "info_prescale_weight_HLTOR_SpecialL1OR",
    # Pileup Prescale Weights -> Run 2
    "info_pileup_prescale_weight_SpecialL1OR"                                   : "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"              : "info_pileup_prescale_weight_HLT_e5",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"             : "info_pileup_prescale_weight_HLT_2e5",
    "info_pileup_prescale_weight_SpecialHLTOR"                                  : "info_pileup_prescale_weight_HLTOR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"  : "info_pileup_prescale_weight_HLT_e5_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" : "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"                      : "info_pileup_prescale_weight_HLTOR_SpecialL1OR",
    # Pileup Prescale Weights -> Run 3
    "info_pileup_prescale_weight_L1BKeePrimary"                                 : "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_bBeeM6000"                     : "info_pileup_prescale_weight_HLT_e5",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_bBeeM6000"                    : "info_pileup_prescale_weight_HLT_2e5",
    "info_pileup_prescale_weight_UnseededHLTOR"                                 : "info_pileup_prescale_weight_HLTOR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_bBeeM6000_L1BKeePrimary"       : "info_pileup_prescale_weight_HLT_e5_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_bBeeM6000_L1BKeePrimary"      : "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_pileup_prescale_weight_UnseededHLTOR_L1BKeePrimary"                   : "info_pileup_prescale_weight_HLTOR_SpecialL1OR"
}

### Load Data -> Additional Features ###
### -------------------------------- ###

additional_features_to_plot_ml1_r2 = config_ml1_r2[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot_ml2_r2 = config_ml2_r2[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot_ml1_r3 = config_ml1_r3[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot_ml2_r3 = config_ml2_r3[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot        = list( set( additional_features_to_plot_ml1_r2 + additional_features_to_plot_ml2_r2 + additional_features_to_plot_ml1_r3 + additional_features_to_plot_ml2_r3 ) )
additional_features_to_plot += [
    "dR_positron_electron",
    "Lxy_significance_over_B_chi2",
    "electron0_pT",
    "electron1_pT"
]
additional_features_to_load = [ 
    "info_sample", 
    "info_event_number", 
    "info_truth_matching", 
]

### Load Data -> Aggregate & Load ###
### ----------------------------- ###

items_to_load_r2       = features + additional_features_to_plot + additional_features_to_load + trigger_features_to_load_r2
items_to_load_r3       = features + additional_features_to_plot + additional_features_to_load + trigger_features_to_load_r3

def df_load( 
        input_data, 
        q2bin       = "low", 
        nBestDict   = None, 
        selections  = selections,
        run         = 2,
    ):
    items_to_load = items_to_load_r2 if run == 2 else items_to_load_r3
    df = utils.df_load( input_data, selection = selections[ "baseline_q2" + q2bin ], items_to_load = items_to_load )
    if nBestDict is not None:
        df = utils.get_n_best( df, nBestDict[ "nBest" ], feature = nBestDict[ "feature" ], sort = nBestDict[ "sort" ], data_only = False )
    for key, value in tf_aliases.items():
        if key in df.columns:
            df.rename( columns = { key : value }, inplace = True )
    df[ "info_sample_weight" ] = df[ "info_sample" ].apply( lambda x: samples.get_weights_by_sample_number( x, lumi = 1e6 ) ) # lumi in nb^-1
    return df

### Trigger-Related Functions ###
### ------------------------- ###

def get_trigger_filters( df ):
    f_trigger_e5_or_2e5  = df[ "info_trigger_decision"                     ] == 1
    f_trigger_e5         = df[ "info_trigger_decision_HLT_e5_SpecialL1OR"  ] == 1 
    f_trigger_2e5        = df[ "info_trigger_decision_HLT_2e5_SpecialL1OR" ] == 1 
    trigger_filters_dict = {
        "( e5 || 2e5 ) && SpecialL1OR"   : f_trigger_e5_or_2e5, 
        "e5 && SpecialL1OR"              : f_trigger_e5,
        "2e5 && SpecialL1OR"             : f_trigger_2e5,
        "( e5 && !2e5 ) && SpecialL1OR"  : f_trigger_e5  & ~ f_trigger_2e5,
        "( 2e5 && !e5 ) && SpecialL1OR"  : f_trigger_2e5 & ~ f_trigger_e5,
    }
    return trigger_filters_dict

def get_yield_contributions( 
        df, 
        trigger_filters_dict = None 
    ):
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )

    yield_contributions                  = {}
    yield_contributions_unprescaled      = {}
    r_yield_contributions                = {}
    r_yield_contributions_unprescaled    = {}
    yield_contributions_1ifb             = {}
    yield_contributions_unprescaled_1ifb = {}

    total_pileup_weight    = df[ "info_pure_pileup_weight" ].sum()

    w_total                = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ].sum()
    w_2e5                  = df[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR"      ].sum()
    w_e5                   = df[ "info_pileup_prescale_weight_HLT_e5_SpecialL1OR"       ].sum()
    w_e5_unique            = w_total - w_2e5
    w_2e5_unique           = w_total - w_e5
    w_total_for_e5_unique  = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    w_total_for_2e5_unique = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    u_total                = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ].sum()
    u_2e5                  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ].sum()
    u_e5                   = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ].sum()
    u_e5_unique            = u_total - u_2e5
    u_2e5_unique           = u_total - u_e5
    u_total_for_e5_unique  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    u_total_for_2e5_unique = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    w_total_1ifb                = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] * df[ "info_sample_weight" ] ).sum()
    w_2e5_1ifb                  = ( df[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR"      ] * df[ "info_sample_weight" ] ).sum()
    w_e5_1ifb                   = ( df[ "info_pileup_prescale_weight_HLT_e5_SpecialL1OR"       ] * df[ "info_sample_weight" ] ).sum()
    w_e5_unique_1ifb            = w_total_1ifb - w_2e5_1ifb
    w_2e5_unique_1ifb           = w_total_1ifb - w_e5_1ifb
    w_total_for_e5_unique_1ifb  = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] ).sum()
    w_total_for_2e5_unique_1ifb = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] ).sum()

    u_total_1ifb                = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] ).sum()
    u_2e5_1ifb                  = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ] ).sum()
    u_e5_1ifb                   = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ] ).sum()
    u_e5_unique_1ifb            = u_total_1ifb - u_2e5_1ifb
    u_2e5_unique_1ifb           = u_total_1ifb - u_e5_1ifb
    u_total_for_e5_unique_1ifb  = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] ).sum()
    u_total_for_2e5_unique_1ifb = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] ).sum()

    yield_contributions               [ "Total Weight"                ] = round( w_total                / total_pileup_weight, 4 )
    yield_contributions               [ "Total 2e5 Weight"            ] = round( w_2e5                  / total_pileup_weight, 4 )
    yield_contributions               [ "Total e5 Weight"             ] = round( w_e5                   / total_pileup_weight, 4 )
    yield_contributions               [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions               [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions               [ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions               [ "Unique e5 Weight"            ] = round( w_e5_unique            / total_pileup_weight, 4 )

    yield_contributions_unprescaled   [ "Total Weight"                ] = round( u_total                / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total 2e5 Weight"            ] = round( u_2e5                  / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total e5 Weight"             ] = round( u_e5                   / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Unique 2e5 Weight"           ] = round( u_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Unique e5 Weight"            ] = round( u_e5_unique            / total_pileup_weight, 4 )

    r_yield_contributions             [ "Total Weight"                ] = round( w_total                / w_total, 4 )
    r_yield_contributions             [ "Total 2e5 Weight"            ] = round( w_2e5                  / w_total, 4 )
    r_yield_contributions             [ "Total e5 Weight"             ] = round( w_e5                   / w_total, 4 )
    r_yield_contributions             [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / w_total, 4 )
    r_yield_contributions             [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / w_total, 4 )
    r_yield_contributions             [ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / w_total, 4 )
    r_yield_contributions             [ "Unique e5 Weight"            ] = round( w_e5_unique            / w_total, 4 )

    r_yield_contributions_unprescaled [ "Total Weight"                ] = round( u_total                / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total 2e5 Weight"            ] = round( u_2e5                  / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total e5 Weight"             ] = round( u_e5                   / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique  / u_total, 4 )
    r_yield_contributions_unprescaled [ "Unique 2e5 Weight"           ] = round( u_2e5_unique           / u_total, 4 )
    r_yield_contributions_unprescaled [ "Unique e5 Weight"            ] = round( u_e5_unique            / u_total, 4 )

    yield_contributions_1ifb             [ "Total Weight"                ] = round( w_total_1ifb                , 4 )
    yield_contributions_1ifb             [ "Total 2e5 Weight"            ] = round( w_2e5_1ifb                  , 4 )
    yield_contributions_1ifb             [ "Total e5 Weight"             ] = round( w_e5_1ifb                   , 4 )
    yield_contributions_1ifb             [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique_1ifb , 4 )
    yield_contributions_1ifb             [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique_1ifb  , 4 )
    yield_contributions_1ifb             [ "Unique 2e5 Weight"           ] = round( w_2e5_unique_1ifb           , 4 )
    yield_contributions_1ifb             [ "Unique e5 Weight"            ] = round( w_e5_unique_1ifb            , 4 )

    yield_contributions_unprescaled_1ifb [ "Total Weight"                ] = round( u_total_1ifb                , 4 )
    yield_contributions_unprescaled_1ifb [ "Total 2e5 Weight"            ] = round( u_2e5_1ifb                  , 4 )
    yield_contributions_unprescaled_1ifb [ "Total e5 Weight"             ] = round( u_e5_1ifb                   , 4 )
    yield_contributions_unprescaled_1ifb [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique_1ifb , 4 )
    yield_contributions_unprescaled_1ifb [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique_1ifb  , 4 )
    yield_contributions_unprescaled_1ifb [ "Unique 2e5 Weight"           ] = round( u_2e5_unique_1ifb           , 4 )
    yield_contributions_unprescaled_1ifb [ "Unique e5 Weight"            ] = round( u_e5_unique_1ifb            , 4 )

    yields_dict = {
        "Relative to Baseline"                                       : yield_contributions,
        "Relative to Baseline (HLT Unprescaled)"                     : yield_contributions_unprescaled,
        "Relative to ( e5 || 2e5 ) && SpecialL1OR"                   : r_yield_contributions,
        "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" : r_yield_contributions_unprescaled,
        "Absolute at 1 fb^-1"                                        : yield_contributions_1ifb,
        "Absolute at 1 fb^-1 (HLT Unprescaled)"                      : yield_contributions_unprescaled_1ifb
    }
    return yields_dict

def plotFeatures( 
        df, 
        trigger_filters_dict = None, 
        title = "", 
        slug  = ""
    ):
    if slug == "":
        slug = title
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    pathlib.Path( "plots" ).mkdir( parents = True, exist_ok = True )

    ### Plots for the Baseline, L1OR, and ( e5 || 2e5 ) && L1OR ###
    ### ------------------------------------------------------- ###
    
    df_baseline            = copy.deepcopy( df )
    df_baseline_unweighted = copy.deepcopy( df )
    df_either              = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_either_unweighted   = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    #df_l1or               = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    #df_l1or_unweighted    = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    
    df_baseline            [ "info_weight" ] = df[ "info_pure_pileup_weight" ]
    df_baseline_unweighted [ "info_weight" ] = np.ones( len( df_baseline_unweighted ) )
    df_either              [ "info_weight" ] = df_either[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ]
    df_either_unweighted   [ "info_weight" ] = df_either_unweighted[ "info_pure_pileup_weight" ]
    #df_l1or               [ "info_weight" ] = df_l1or[ "info_pileup_prescale_weight_SpecialL1OR" ]
    #df_l1or_unweighted    [ "info_weight" ] = df_l1or_unweighted[ "info_pure_pileup_weight" ]

    df_baseline            [ "info_weight" ] *= df_baseline            [ "info_sample_weight" ]
    df_baseline_unweighted [ "info_weight" ] *= df_baseline_unweighted [ "info_sample_weight" ]
    df_either              [ "info_weight" ] *= df_either              [ "info_sample_weight" ]
    df_either_unweighted   [ "info_weight" ] *= df_either_unweighted   [ "info_sample_weight" ]
    #df_l1or               [ "info_weight" ] *= df_l1or               [ "info_sample_weight" ]
    #df_l1or_unweighted    [ "info_weight" ] *= df_l1or_unweighted    [ "info_sample_weight" ]
    
    list_df        = [ df_baseline_unweighted, df_baseline, df_either ] #, df_l1or, df_l1or_unweighted ]
    list_df_names  = [ "Baseline (No Weights)", "Baseline (Only Pileup Weights)", "( e5 || 2e5 ) && L1OR" ] #, "L1OR", "L1OR (No Weights)" ]
    list_df_colors = [ "black", "grey", "tab:blue" ] #, "tab:purple", "tab:yellow" ]
    list_fills     = [ False, "stepfilled", "stepfilled" ] #, False, "stepfilled" ]
    list_alphas    = [ 1, 0.5, 0.5 ] #, 1, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features            = features,
       title               = title,
       output_filename     = f"{output_dir_plots}/trigger_reweighting_{slug}.pdf",
       input_labels        = list_df_names,
       input_colors        = list_df_colors,
       additional_features = additional_features_to_plot,
       normalized          = True,
       input_fills         = list_fills,
       input_alphas        = list_alphas
    )

    ### Plots for the Baseline, Unique e5, and Inclusive 2e5 ###
    ### ---------------------------------------------------- ###

    df_either        = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_unique_e5     = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_inclusive_2e5 = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_either        [ "info_weight" ] = df_either        [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ]
    df_unique_e5     [ "info_weight" ] = df_unique_e5     [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] - df_unique_e5[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    df_inclusive_2e5 [ "info_weight" ] = df_inclusive_2e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    
    df_either        [ "info_weight" ] *= df_either        [ "info_sample_weight" ]
    df_unique_e5     [ "info_weight" ] *= df_unique_e5     [ "info_sample_weight" ]
    df_inclusive_2e5 [ "info_weight" ] *= df_inclusive_2e5 [ "info_sample_weight" ]

    list_df        = [ df_either, df_unique_e5, df_inclusive_2e5 ]
    list_df_names  = [ "( e5 || 2e5 )", "Unique e5", "Inclusive 2e5" ]
    list_df_colors = [ "tab:blue", "tab:red", "tab:green" ]
    list_alphas    = [ 0.5, 0.5, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features             = features,
       title                = title + " \n( Normalized to $\\mathcal{L}=1{\\ \\rm fb}^{-1}$ )",
       output_filename      = f"{output_dir_plots}/trigger_reweighting_{slug}_unique_e5.pdf",
       input_labels         = list_df_names,
       input_colors         = list_df_colors,
       additional_features  = additional_features_to_plot,
       normalized           = False,
       input_alphas         = list_alphas
    )

    ### Plots for the Baseline, Unique 2e5, and Inclusive e5 ( HLT Unprescaled ) ###
    ### ------------------------------------------------------------------------ ###
    df_unprescaled_either        = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR"  ] ] )
    df_unprescaled_unique_e5     = copy.deepcopy( df[ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] )
    df_unprescaled_inclusive_2e5 = copy.deepcopy( df[ trigger_filters_dict[ "2e5 && SpecialL1OR"            ] ] )
    
    df_unprescaled_either        [ "info_weight" ] = df_unprescaled_either        [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_unique_e5     [ "info_weight" ] = df_unprescaled_unique_e5     [ "info_pileup_prescale_weight_SpecialL1OR" ] 
    df_unprescaled_inclusive_2e5 [ "info_weight" ] = df_unprescaled_inclusive_2e5 [ "info_pileup_prescale_weight_SpecialL1OR" ]

    df_unprescaled_either        [ "info_weight" ] *= df_unprescaled_either        [ "info_sample_weight" ]
    df_unprescaled_unique_e5     [ "info_weight" ] *= df_unprescaled_unique_e5     [ "info_sample_weight" ]
    df_unprescaled_inclusive_2e5 [ "info_weight" ] *= df_unprescaled_inclusive_2e5 [ "info_sample_weight" ]

    list_df        = [ df_unprescaled_either, df_unprescaled_unique_e5, df_unprescaled_inclusive_2e5 ]
    list_df_names  = [ i + " (HLT Unprescaled)" for i in [ "( e5 || 2e5 )", "Unique e5", "Inclusive 2e5" ] ]
    list_df_colors = [ "tab:blue", "tab:red", "tab:green" ]
    list_alphas    = [ 0.5, 0.5, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features             = features,
       title                = title + " \n( Normalized to $\\mathcal{L}=1{\\ \\rm fb}^{-1}$ )",
       output_filename      = f"{output_dir_plots}/trigger_reweighting_{slug}_unique_e5_unprescaled.pdf",
       input_labels         = list_df_names,
       input_colors         = list_df_colors,
       additional_features  = additional_features_to_plot,
       normalized           = False,
       input_alphas         = list_alphas
    )

def plotFeaturesComparative( 
        df_r2,
        df_r3,
        trigger_filters_dict_r2 = None,
        trigger_filters_dict_r3 = None,
        title = "",
        slug  = "",
        normed = False 
):
    if slug == "":
        slug = title
    if trigger_filters_dict_r2 is None:
        trigger_filters_dict_r2 = get_trigger_filters( df_r2 )
    if trigger_filters_dict_r3 is None:
        trigger_filters_dict_r3 = get_trigger_filters( df_r3 )
    
    title += "\n(Normalized to $\\mathcal{L}=1{\\ \\rm fb}^{-1}$)" if normed else ""
    
    df_r2_either        = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_r3_either        = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_r2_inclusive_2e5 = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "2e5 && SpecialL1OR" ] ] )
    df_r3_inclusive_2e5 = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "2e5 && SpecialL1OR" ] ] )
    df_r2_unique_e5     = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_r3_unique_e5     = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_r2_either        [ "info_weight" ] = df_r2_either        [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR"   ] 
    df_r3_either        [ "info_weight" ] = df_r3_either        [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR"   ] 
    df_r2_inclusive_2e5 [ "info_weight" ] = df_r2_inclusive_2e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    df_r3_inclusive_2e5 [ "info_weight" ] = df_r3_inclusive_2e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    df_r2_unique_e5     [ "info_weight" ] = df_r2_unique_e5     [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR"   ] - df_r2_unique_e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    df_r3_unique_e5     [ "info_weight" ] = df_r3_unique_e5     [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR"   ] - df_r3_unique_e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]

    df_unprescaled_either_r2        = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "( e5 || 2e5 ) && SpecialL1OR"  ] ] )
    df_unprescaled_unique_e5_r2     = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "( e5 && !2e5 ) && SpecialL1OR" ] ] )
    df_unprescaled_inclusive_2e5_r2 = copy.deepcopy( df_r2[ trigger_filters_dict_r2[ "2e5 && SpecialL1OR"            ] ] )
    df_unprescaled_either_r3        = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "( e5 || 2e5 ) && SpecialL1OR"  ] ] )
    df_unprescaled_unique_e5_r3     = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "( e5 && !2e5 ) && SpecialL1OR" ] ] )
    df_unprescaled_inclusive_2e5_r3 = copy.deepcopy( df_r3[ trigger_filters_dict_r3[ "2e5 && SpecialL1OR"            ] ] )
    df_unprescaled_either_r2        [ "info_weight" ] = df_unprescaled_either_r2        [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_unique_e5_r2     [ "info_weight" ] = df_unprescaled_unique_e5_r2     [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_inclusive_2e5_r2 [ "info_weight" ] = df_unprescaled_inclusive_2e5_r2 [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_either_r3        [ "info_weight" ] = df_unprescaled_either_r3        [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_unique_e5_r3     [ "info_weight" ] = df_unprescaled_unique_e5_r3     [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_inclusive_2e5_r3 [ "info_weight" ] = df_unprescaled_inclusive_2e5_r3 [ "info_pileup_prescale_weight_SpecialL1OR" ]

    df_r2_either        [ "info_weight" ] *= df_r2_either        [ "info_sample_weight" ]
    df_r3_either        [ "info_weight" ] *= df_r3_either        [ "info_sample_weight" ]
    df_r2_inclusive_2e5 [ "info_weight" ] *= df_r2_inclusive_2e5 [ "info_sample_weight" ]
    df_r3_inclusive_2e5 [ "info_weight" ] *= df_r3_inclusive_2e5 [ "info_sample_weight" ]
    df_r2_unique_e5     [ "info_weight" ] *= df_r2_unique_e5     [ "info_sample_weight" ]
    df_r3_unique_e5     [ "info_weight" ] *= df_r3_unique_e5     [ "info_sample_weight" ]
    df_unprescaled_either_r2        [ "info_weight" ] *= df_unprescaled_either_r2        [ "info_sample_weight" ]
    df_unprescaled_unique_e5_r2     [ "info_weight" ] *= df_unprescaled_unique_e5_r2     [ "info_sample_weight" ]
    df_unprescaled_inclusive_2e5_r2 [ "info_weight" ] *= df_unprescaled_inclusive_2e5_r2 [ "info_sample_weight" ]
    df_unprescaled_either_r3        [ "info_weight" ] *= df_unprescaled_either_r3        [ "info_sample_weight" ]
    df_unprescaled_unique_e5_r3     [ "info_weight" ] *= df_unprescaled_unique_e5_r3     [ "info_sample_weight" ]
    df_unprescaled_inclusive_2e5_r3 [ "info_weight" ] *= df_unprescaled_inclusive_2e5_r3 [ "info_sample_weight" ]

    if not normed:
        pp = PdfPages( f"{output_dir_plots}/trigger_reweighting_{slug}_comparative.pdf" )
    else:
        pp = PdfPages( f"{output_dir_plots}/trigger_reweighting_{slug}_comparative_normed.pdf" )

    if "_ntm" in slug and not normed:
        df_r2_all = copy.deepcopy( df_r2 )
        df_r3_all = copy.deepcopy( df_r3 )
        df_r2_tm  = copy.deepcopy( df_r2[ df_r2[ "info_truth_matching" ] ] )
        df_r3_tm  = copy.deepcopy( df_r3[ df_r3[ "info_truth_matching" ] ] )
        df_r2_all [ "info_weight" ] = df_r2_all [ "info_pure_pileup_weight" ] * df_r2_all [ "info_sample_weight" ]
        df_r3_all [ "info_weight" ] = df_r3_all [ "info_pure_pileup_weight" ] * df_r3_all [ "info_sample_weight" ]
        df_r2_tm  [ "info_weight" ] = df_r2_tm  [ "info_pure_pileup_weight" ] * df_r2_tm  [ "info_sample_weight" ]
        df_r3_tm  [ "info_weight" ] = df_r3_tm  [ "info_pure_pileup_weight" ] * df_r3_tm  [ "info_sample_weight" ]

        ### Plots for the Baseline ###
        ### ---------------------- ###
        list_df        = [ df_r2_all, df_r3_all, df_r2_tm, df_r3_tm ]
        list_df_names  = [ "Run 2", "Run 3", "Run 2 (Truth-Matched)", "Run 3 (Truth-Matched)" ]
        list_df_colors = [ "black", "tab:red", "grey", "tab:red"      ]
        list_fills     = [ False, False, "stepfilled", "stepfilled" ]
        list_alphas    = [ 1, 1, 0.4, 0.5 ]

        fig = plotting.plot_baseline_features(
              list_df,
              features            = features,
              title               = title,
              output_filename     = False,
              input_labels        = list_df_names,
              input_colors        = list_df_colors,
              additional_features = additional_features_to_plot,
              normalized          = False,
              input_fills         = list_fills,
              input_alphas        = list_alphas
          )
        pp.savefig( fig )
        plt.close( fig )
    
    ### Plots for Either ###
    ### ---------------- ###
    list_df        = [ df_r2_either, df_r3_either ]
    list_df_names  = [ "Run 2, ( e5 || 2e5 ) && SpecialL1OR", "Run 3, ( e5 || 2e5 ) && SpecialL1OR" ]
    list_df_colors = [ "tab:blue", "tab:blue" ] 
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Inclusive 2e5 ###
    ### ----------------------- ###
    list_df        = [ df_r2_inclusive_2e5, df_r3_inclusive_2e5 ]
    list_df_names  = [ "Run 2, 2e5 && SpecialL1OR", "Run 3, 2e5 && SpecialL1OR" ]
    list_df_colors = [ "tab:green", "tab:green" ]
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Unique e5 ###
    ### ------------------- ###
    list_df        = [ df_r2_unique_e5, df_r3_unique_e5 ]
    list_df_names  = [ "Run 2, Unique e5", "Run 3, Unique e5" ]
    list_df_colors = [ "tab:red", "tab:red" ]
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Either ( HLT Unprescaled ) ###
    ### ------------------------------------ ###
    list_df        = [ df_unprescaled_either_r2, df_unprescaled_either_r3 ]
    list_df_names  = [ "Run 2, ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)", "Run 3, ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ]
    list_df_colors = [ "tab:blue", "tab:blue" ]
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Inclusive 2e5 ( HLT Unprescaled ) ###
    ### ------------------------------------------- ###
    list_df        = [ df_unprescaled_inclusive_2e5_r2, df_unprescaled_inclusive_2e5_r3 ]
    list_df_names  = [ "Run 2, 2e5 && SpecialL1OR (HLT Unprescaled)", "Run 3, 2e5 && SpecialL1OR (HLT Unprescaled)" ]
    list_df_colors = [ "tab:green", "tab:green" ]
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Unique e5 ( HLT Unprescaled ) ###
    ### --------------------------------------- ###
    list_df        = [ df_unprescaled_unique_e5_r2, df_unprescaled_unique_e5_r3 ]
    list_df_names  = [ "Run 2, Unique e5 (HLT Unprescaled)", "Run 3, Unique e5 (HLT Unprescaled)" ]
    list_df_colors = [ "tab:red", "tab:red" ]
    list_fills     = [ False, "stepfilled" ]
    list_alphas    = [ 1.0, 0.5 ]
    fig = plotting.plot_baseline_features(
            list_df,
            features            = features,
            title               = title,
            output_filename     = False,
            input_labels        = list_df_names,
            input_colors        = list_df_colors,
            additional_features = additional_features_to_plot,
            normalized          = False if not normed else True,
            input_fills         = list_fills,
            input_alphas        = list_alphas
        )
    pp.savefig( fig )
    plt.close( fig )

    ### Plots for Either, Unique e5, and Inclusive 2e5 ###
    ### ---------------------------------------------- ###
    if not normed:
        list_df        = [ df_r2_either, df_r3_either, df_r2_unique_e5, df_r3_unique_e5, df_r2_inclusive_2e5, df_r3_inclusive_2e5 ]
        list_df_names  = [ "Run 2, ( e5 || 2e5 ) && SpecialL1OR", "Run 3, ( e5 || 2e5 ) && SpecialL1OR", "Run 2, Unique e5", "Run 3, Unique e5", "Run 2, Inclusive 2e5", "Run 3, Inclusive 2e5" ]
        list_df_colors = [ "tab:blue", "tab:blue", "tab:red", "tab:red", "tab:green", "tab:green" ]
        list_fills     = [ False, "stepfilled", False, "stepfilled", False, "stepfilled" ]
        list_alphas    = [ 1.0, 0.5, 1.0, 0.5, 1.0, 0.5 ]
        fig = plotting.plot_baseline_features(
                list_df,
                features            = features,
                title               = title,
                output_filename     = False,
                input_labels        = list_df_names,
                input_colors        = list_df_colors,
                additional_features = additional_features_to_plot,
                normalized          = False,
                input_fills         = list_fills,
                input_alphas        = list_alphas
            )
        pp.savefig( fig )
        plt.close( fig )

    ### Plots for Either, Unique e5, and Inclusive 2e5 ( HLT Unprescaled ) ###
    ### ------------------------------------------------------------------ ###
    if not normed:
        list_df        = [ df_unprescaled_either_r2, df_unprescaled_either_r3, df_unprescaled_unique_e5_r2, df_unprescaled_unique_e5_r3, df_unprescaled_inclusive_2e5_r2, df_unprescaled_inclusive_2e5_r3 ]
        list_df_names  = [ "Run 2, ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)", "Run 3, ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)", "Run 2, Unique e5 (HLT Unprescaled)", "Run 3, Unique e5 (HLT Unprescaled)", "Run 2, Inclusive 2e5 (HLT Unprescaled)", "Run 3, Inclusive 2e5 (HLT Unprescaled)" ]
        list_df_colors = [ "tab:blue", "tab:blue", "tab:red", "tab:red", "tab:green", "tab:green" ]
        list_fills     = [ False, "stepfilled", False, "stepfilled", False, "stepfilled" ]
        list_alphas    = [ 1.0, 0.5, 1.0, 0.5, 1.0, 0.5 ]
        fig = plotting.plot_baseline_features(
                list_df,
                features            = features,
                title               = title,
                output_filename     = False,
                input_labels        = list_df_names,
                input_colors        = list_df_colors,
                additional_features = additional_features_to_plot,
                normalized          = False,
                input_fills         = list_fills,
                input_alphas        = list_alphas
            )
        pp.savefig( fig )
        plt.close( fig )
    
    pp.close()

def main( 
        slug, 
        truth_matching = True,
        run = 2
    ):
    nBestDict = {
        "nBest"   : 1,
        "feature" : "Lxy_significance_over_B_chi2",
        "sort"    : "highest"
    }
    input_data_nr = input_data_nr_r2 if run == 2 else input_data_nr_r3
    input_data_r  = input_data_r_r2  if run == 2 else input_data_r_r3
    
    df_nr_q2low   = df_load( input_data_nr, q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = run )
    df_nr_q2high  = df_load( input_data_nr, q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = run )
    df_nr_q2full  = df_load( input_data_nr, q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = run )
    df_r_q2low    = df_load( input_data_r,  q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = run )
    df_r_q2high   = df_load( input_data_r,  q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = run )
    df_r_q2full   = df_load( input_data_r,  q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = run )
    
    trigger_filters_dict_nr_q2low  = get_trigger_filters( df_nr_q2low  )
    trigger_filters_dict_nr_q2high = get_trigger_filters( df_nr_q2high )
    trigger_filters_dict_nr_q2full = get_trigger_filters( df_nr_q2full )
    trigger_filters_dict_r_q2low   = get_trigger_filters( df_r_q2low   )
    trigger_filters_dict_r_q2high  = get_trigger_filters( df_r_q2high  )
    trigger_filters_dict_r_q2full  = get_trigger_filters( df_r_q2full  )

    y_dict_nr_q2low    = get_yield_contributions( df_nr_q2low  , trigger_filters_dict_nr_q2low  )
    y_dict_nr_q2high   = get_yield_contributions( df_nr_q2high , trigger_filters_dict_nr_q2high )
    y_dict_nr_q2full   = get_yield_contributions( df_nr_q2full , trigger_filters_dict_nr_q2full )
    y_dict_r_q2low     = get_yield_contributions( df_r_q2low   , trigger_filters_dict_r_q2low   )
    y_dict_r_q2high    = get_yield_contributions( df_r_q2high  , trigger_filters_dict_r_q2high  )
    y_dict_r_q2full    = get_yield_contributions( df_r_q2full  , trigger_filters_dict_r_q2full  )

    mega_y_nr = {
        "q2low" : y_dict_nr_q2low  [ "Relative to Baseline" ],
        "q2high": y_dict_nr_q2high [ "Relative to Baseline" ],
        "q2full": y_dict_nr_q2full [ "Relative to Baseline" ]
    }
    mega_y_unprescaled_nr = {
        "q2low" : y_dict_nr_q2low  [ "Relative to Baseline (HLT Unprescaled)" ],
        "q2high": y_dict_nr_q2high [ "Relative to Baseline (HLT Unprescaled)" ], 
        "q2full": y_dict_nr_q2full [ "Relative to Baseline (HLT Unprescaled)" ]
    }
    mega_y_relative_nr = {
        "q2low" : y_dict_nr_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ],
        "q2high": y_dict_nr_q2high [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ],
        "q2full": y_dict_nr_q2full [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ]
    }
    mega_y_relative_unprescaled_nr = {
        "q2low" : y_dict_nr_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ],
        "q2high": y_dict_nr_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ],
        "q2full": y_dict_nr_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ]
    }
    mega_y_1ifb_nr = {
        "q2low" : y_dict_nr_q2low  [ "Absolute at 1 fb^-1" ],
        "q2high": y_dict_nr_q2high [ "Absolute at 1 fb^-1" ],
        "q2full": y_dict_nr_q2full [ "Absolute at 1 fb^-1" ]
    }   
    mega_y_1ifb_unprescaled_nr = {
        "q2low" : y_dict_nr_q2low  [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ],
        "q2high": y_dict_nr_q2high [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ],
        "q2full": y_dict_nr_q2full [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ]
    }

    mega_y_r = {
        "q2low" : y_dict_r_q2low  [ "Relative to Baseline" ],
        "q2high": y_dict_r_q2high [ "Relative to Baseline" ],
        "q2full": y_dict_r_q2full [ "Relative to Baseline" ]
    }
    mega_y_unprescaled_r = {
        "q2low" : y_dict_r_q2low  [ "Relative to Baseline (HLT Unprescaled)" ],
        "q2high": y_dict_r_q2high [ "Relative to Baseline (HLT Unprescaled)" ],
        "q2full": y_dict_r_q2full [ "Relative to Baseline (HLT Unprescaled)" ]
    }
    mega_y_relative_r = {
        "q2low" : y_dict_r_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ],
        "q2high": y_dict_r_q2high [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ],
        "q2full": y_dict_r_q2full [ "Relative to ( e5 || 2e5 ) && SpecialL1OR" ]
    }
    mega_y_relative_unprescaled_r = {
        "q2low" : y_dict_r_q2low  [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ],
        "q2high": y_dict_r_q2high [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ],
        "q2full": y_dict_r_q2full [ "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" ]
    }
    mega_y_1ifb_r = {
        "q2low" : y_dict_r_q2low  [ "Absolute at 1 fb^-1" ],
        "q2high": y_dict_r_q2high [ "Absolute at 1 fb^-1" ],
        "q2full": y_dict_r_q2full [ "Absolute at 1 fb^-1" ]
    }
    mega_y_1ifb_unprescaled_r = {
        "q2low" : y_dict_r_q2low  [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ],
        "q2high": y_dict_r_q2high [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ],
        "q2full": y_dict_r_q2full [ "Absolute at 1 fb^-1 (HLT Unprescaled)" ]
    }

    mega_y_nr                      = pd.DataFrame( mega_y_nr                      )
    mega_y_unprescaled_nr          = pd.DataFrame( mega_y_unprescaled_nr          )
    mega_y_relative_nr             = pd.DataFrame( mega_y_relative_nr             )
    mega_y_relative_unprescaled_nr = pd.DataFrame( mega_y_relative_unprescaled_nr )
    mega_y_1ifb_nr                 = pd.DataFrame( mega_y_1ifb_nr                 )
    mega_y_1ifb_unprescaled_nr     = pd.DataFrame( mega_y_1ifb_unprescaled_nr     )

    mega_y_r                       = pd.DataFrame( mega_y_r                       )
    mega_y_unprescaled_r           = pd.DataFrame( mega_y_unprescaled_r           )
    mega_y_relative_r              = pd.DataFrame( mega_y_relative_r              )
    mega_y_relative_unprescaled_r  = pd.DataFrame( mega_y_relative_unprescaled_r  )
    mega_y_1ifb_r                  = pd.DataFrame( mega_y_1ifb_r                  )
    mega_y_1ifb_unprescaled_r      = pd.DataFrame( mega_y_1ifb_unprescaled_r      )

    mega_y_nr.to_csv                      ( f"{output_dir_yields}/mega_y_nr_{slug}.csv"                      , index = True )
    mega_y_unprescaled_nr.to_csv          ( f"{output_dir_yields}/mega_y_unprescaled_nr_{slug}.csv"          , index = True )
    mega_y_relative_nr.to_csv             ( f"{output_dir_yields}/mega_y_relative_nr_{slug}.csv"             , index = True )
    mega_y_relative_unprescaled_nr.to_csv ( f"{output_dir_yields}/mega_y_relative_unprescaled_nr_{slug}.csv" , index = True )
    mega_y_1ifb_nr.to_csv                 ( f"{output_dir_yields}/mega_y_1ifb_nr_{slug}.csv"                 , index = True )
    mega_y_1ifb_unprescaled_nr.to_csv     ( f"{output_dir_yields}/mega_y_1ifb_unprescaled_nr_{slug}.csv"     , index = True )

    mega_y_r.to_csv                       ( f"{output_dir_yields}/mega_y_r_{slug}.csv"                       , index = True )
    mega_y_unprescaled_r.to_csv           ( f"{output_dir_yields}/mega_y_unprescaled_r_{slug}.csv"           , index = True )
    mega_y_relative_r.to_csv              ( f"{output_dir_yields}/mega_y_relative_r_{slug}.csv"              , index = True )
    mega_y_relative_unprescaled_r.to_csv  ( f"{output_dir_yields}/mega_y_relative_unprescaled_r_{slug}.csv"  , index = True )
    mega_y_1ifb_r.to_csv                  ( f"{output_dir_yields}/mega_y_1ifb_r_{slug}.csv"                  , index = True )
    mega_y_1ifb_unprescaled_r.to_csv      ( f"{output_dir_yields}/mega_y_1ifb_unprescaled_r_{slug}.csv"      , index = True )

    plotFeatures( df_nr_q2low, trigger_filters_dict_nr_q2low, title = "Non-Resonant Signal MC at Baseline, low$-q^2$", slug = f"nr_tm_q2low_{slug}" if truth_matching else f"nr_q2low_all_{slug}" )
    plotFeatures( df_r_q2high, trigger_filters_dict_r_q2high, title = "Resonant Signal MC at Baseline, high$-q^2$", slug = f"r_tm_q2high_{slug}" if truth_matching else f"r_q2high_all_{slug}" )

def mainComparative( slug, truth_matching = False, normed = False ):
    nBestDict = {
        "nBest"   : 1,
        "feature" : "Lxy_significance_over_B_chi2",
        "sort"    : "highest"
    }
    df_r2_nr_q2low   = df_load( input_data_nr_r2, q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = 2 )
    df_r2_nr_q2high  = df_load( input_data_nr_r2, q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = 2 )
    df_r2_nr_q2full  = df_load( input_data_nr_r2, q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = 2 )
    #df_r2_r_q2low    = df_load( input_data_r_r2,  q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = 2 )
    #df_r2_r_q2high   = df_load( input_data_r_r2,  q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = 2 )
    #df_r2_r_q2full   = df_load( input_data_r_r2,  q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = 2 )

    df_r3_nr_q2low   = df_load( input_data_nr_r3, q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = 3 )
    df_r3_nr_q2high  = df_load( input_data_nr_r3, q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = 3 )
    df_r3_nr_q2full  = df_load( input_data_nr_r3, q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = 3 )
    #df_r3_r_q2low    = df_load( input_data_r_r3,  q2bin = "low"  if truth_matching else "low_all"  , nBestDict = nBestDict if not truth_matching else None, run = 3 )
    #df_r3_r_q2high   = df_load( input_data_r_r3,  q2bin = "high" if truth_matching else "high_all" , nBestDict = nBestDict if not truth_matching else None, run = 3 )
    #df_r3_r_q2full   = df_load( input_data_r_r3,  q2bin = "full" if truth_matching else "full_all" , nBestDict = nBestDict if not truth_matching else None, run = 3 )

    slug_nr_q2low = f"nr_q2low_{slug}"
    slug_nr_q2high = f"nr_q2high_{slug}"
    slug_nr_q2full = f"nr_q2full_{slug}"
    if not truth_matching and "ntm" not in slug:
        slug_nr_q2low += "_ntm"
        slug_nr_q2high += "_ntm"
        slug_nr_q2full += "_ntm"

    plotFeaturesComparative(
        df_r2_nr_q2low, df_r3_nr_q2low,
        title = "Non-Resonant Signal MC at Baseline, low$-q^2$",
        slug = slug_nr_q2low,
        normed = normed
    )
    plotFeaturesComparative(
        df_r2_nr_q2high, df_r3_nr_q2high,
        title = "Non-Resonant Signal MC at Baseline, high$-q^2$",
        slug = slug_nr_q2high,
        normed = normed
    )
    plotFeaturesComparative(
        df_r2_nr_q2full, df_r3_nr_q2full,
        title = "Non-Resonant Signal MC at Baseline, full$-q^2$",
        slug = slug_nr_q2full,
        normed = normed
    )

if __name__ == "__main__":
    #pass
    main( "r2_ntm" , False, run = 2 )
    main( "r2_tm"  , True , run = 2 )
    main( "r3_ntm" , False, run = 3 )
    main( "r3_tm"  , True , run = 3 )
    mainComparative( "ntm", False )
    mainComparative( "tm" , True  )
    mainComparative( "ntm", False, normed = True )
    mainComparative( "tm" , True , normed = True )

