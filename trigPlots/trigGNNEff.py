#!/usr/bin/env python3

### Imports ###
### ------- ###

### Imports -> I/O ###
### -------------- ###
import os
import pathlib
import sys
import glob

### Imports -> Matrix Manipulation ###
### ------------------------------ ###
import pandas as pd
import numpy as np

### Imports -> Plotting ###
### ------------------- ###
import matplotlib 
matplotlib.use( "agg" )
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import copy

### Imports -> R(K*) Internal ###
### ------------------------- ###
from ml_share import ml_config
from ml_share import plotting
from ml_share import utils
from ml_share import samples

### Declaring the Config & Paths ###
### ---------------------------- ###

# Channel
channel                       = "electron"

# Input Paths
input_dir_r2                  = "/storage/agrp/dvijm/ntuples/feather/electron/refitPRWUnseededPassThrough"
input_data_nr_r2              = utils.get_list_make_list( "ntuple-30059[0-1]_part_0[1-2].ftr", input_dir_r2 )
input_data_r_r2               = utils.get_list_make_list( "ntuple-30059[2-3]_part_0[1-2].ftr", input_dir_r2 )

# Configs
config_file_ml1_r2            = "../ml_main/configs/gnn1_electron_multiclass_goblin_v2.yaml"
config_file_ml2_r2            = "../ml_main/configs/gnn2_electron_multiclass_goblin_v2.yaml"
config_ml1_r2                 = utils.config_load( config_file_ml1_r2  )
config_ml2_r2                 = utils.config_load( config_file_ml2_r2  )
config_name_ml1_r2            = os.path.splitext( os.path.basename( config_file_ml1_r2 ) )[ 0 ]
config_name_ml2_r2            = os.path.splitext( os.path.basename( config_file_ml2_r2 ) )[ 0 ]
config_default_ml1_r2         = utils.yaml_load( "../ml_main/configs/" + config_ml1_r2[ "default_config" ] )[ "common" ]
config_default_ml2_r2         = utils.yaml_load( "../ml_main/configs/" + config_ml2_r2[ "default_config" ] )[ "common" ]
config_default                = copy.deepcopy( config_default_ml1_r2 )
assert config_default_ml1_r2 == config_default_ml2_r2

# Output Paths
output_dir                    = "./"
output_dir_nn1                = config_ml1_r2[ "application" ][ "output_dir" ]
output_dir_nn2                = config_ml2_r2[ "application" ][ "output_dir" ]
proba_files_nn1               = os.path.join( output_dir_nn1, config_name_ml1_r2 + "_applied", "*_gnn1_proba.ftr" )
proba_files_nn2               = os.path.join( output_dir_nn2, config_name_ml2_r2 + "_applied", "*_gnn2_proba.ftr" )
output_dir_plots              = os.path.join( output_dir, "plots"  )
output_dir_yields             = os.path.join( output_dir, "yields" )
pathlib.Path( output_dir ).mkdir( parents = True, exist_ok = True )

### Selections ###
### ---------- ###

selections = {}
selections[ "baseline_q2low"      ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high"     ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full"     ] = "( $selection_main & $selection_mc & $selection_usr )"
selections[ "baseline_q2low_all"  ] = "( $selection_main & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high_all" ] = "( $selection_main & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full_all" ] = "( $selection_main & $selection_usr )"
for selection_name in selections.keys():
    selections[ selection_name ] = utils.get_selection( selections[ selection_name ], config_default )

### Load Data ###
### --------- ###

### Load Data -> ML Features ###
### ------------------------ ### 
def getGraphFeatures( config ):
    graph_dict  = utils.json_load( os.path.join( "../ml_main/", config[ "common" ][ "graph_dict" ] ) )
    parameters  = config[ "common" ] 
    nested_list = []
    if isinstance( parameters[ "graph_version" ], dict ):
        graph_topo_names = [ item[ "graph_topo" ] for item in parameters[ "graph_version" ].values() ]
    else:
        graph_topo_names = [ parameters[ "graph_version" ] ]
    for item in graph_topo_names:
        if item in graph_dict:
            nested_list += list( graph_dict.get( item ).get( "nodes" ).values() )
            nested_list += list( graph_dict.get( item ).get( "edges" ).values() )
            nested_list += [ graph_dict.get( item ).get( "goblins", [] ) ]
        else:
            print( utils.red( sys._getframe().f_code.co_name + ": " + item + " does not exist in " + parameters[ "graph_dict" ] + "!" ) )
            exit( 1 )
    parameters[ "features" ] = list( set( [ item for sublist in nested_list for item in sublist ] ) )
    parameters[ "features" ] = [ item for item in parameters[ "features" ] if item != "empty_feature" ]
    parameters[ "features" ].sort()
    features = parameters[ "features" ]
    return features
    
features_ml1_r2                    = config_ml1_r2[ "common"   ].get( "features", [] )
features_ml2_r2                    = config_ml2_r2[ "common"   ].get( "features", [] )
features                           = list( set( features_ml1_r2 + features_ml2_r2 ) )
if features == []:
    features_ml1_r2 = getGraphFeatures( config_ml1_r2 )
    features_ml2_r2 = getGraphFeatures( config_ml2_r2 )
    features        = list( set( features_ml1_r2 + features_ml2_r2 ) )
    features.sort()

### Load Data -> Trigger/PRW Features ###
### --------------------------------- ###

trigger_features_to_load_r2 = [
    # Pileup Weights
    "info_pure_pileup_weight",

    # Trigger Decision
    "info_trigger_decision",
    "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    
    # Prescale Weights
    "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_prescale_weight_SpecialHLTOR", 
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_prescale_weight_SpecialHLTOR_SpecialL1OR",
    
    # Pileup Prescale Weights
    "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t",
    "info_pileup_prescale_weight_SpecialHLTOR", 
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"
]

tf_aliases = {
    # Trigger Decision -> Run 2
    "info_trigger_decision_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        : "info_trigger_decision_HLT_e5_SpecialL1OR",
    "info_trigger_decision_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"       : "info_trigger_decision_HLT_2e5_SpecialL1OR",
    # Prescale Weights -> Run 2
    "info_prescale_weight_SpecialL1OR"                                          : "info_prescale_weight_SpecialL1OR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"                     : "info_prescale_weight_HLT_e5",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"                    : "info_prescale_weight_HLT_2e5",
    "info_prescale_weight_SpecialHLTOR"                                         : "info_prescale_weight_HLTOR",
    "info_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"         : "info_prescale_weight_HLT_e5_SpecialL1OR",
    "info_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"        : "info_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_prescale_weight_SpecialHLTOR_SpecialL1OR"                             : "info_prescale_weight_HLTOR_SpecialL1OR",
    # Pileup Prescale Weights -> Run 2
    "info_pileup_prescale_weight_SpecialL1OR"                                   : "info_pileup_prescale_weight_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t"              : "info_pileup_prescale_weight_HLT_e5",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t"             : "info_pileup_prescale_weight_HLT_2e5",
    "info_pileup_prescale_weight_SpecialHLTOR"                                  : "info_pileup_prescale_weight_HLTOR",
    "info_pileup_prescale_weight_HLT_e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR"  : "info_pileup_prescale_weight_HLT_e5_SpecialL1OR",
    "info_pileup_prescale_weight_HLT_2e5_lhvloose_nod0_bBeexM6000t_SpecialL1OR" : "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR",
    "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"                      : "info_pileup_prescale_weight_HLTOR_SpecialL1OR",
}

### Load Data -> Additional Features ###
### -------------------------------- ###

additional_features_to_plot_ml1_r2 = config_ml1_r2[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot_ml2_r2 = config_ml2_r2[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot        = list( set( additional_features_to_plot_ml1_r2 + additional_features_to_plot_ml2_r2 ) )
additional_features_to_plot += [
    "dR_positron_electron",
    "Lxy_significance_over_B_chi2",
    "electron0_pT",
    "electron1_pT"
]
additional_features_to_load = [ 
    "info_sample", 
    "info_event_number", 
    "info_truth_matching", 
    "proba_gnn1",
    "proba_gnn2",
    "proba_gnn1_300590",
    "proba_gnn2_300591",
]

### Load Data -> Aggregate & Load ###
### ----------------------------- ###

items_to_load_r2       = features + additional_features_to_plot + additional_features_to_load + trigger_features_to_load_r2

def df_load( 
        input_data, 
        q2bin       = "low", 
        nBestDict   = None, 
        selections  = selections,
        lumi        = 1e6 # lumi in nb^-1
    ):
    items_to_load = items_to_load_r2 
    df = utils.df_load( 
        input_data, 
        selection = selections[ "baseline_q2" + q2bin ], 
        items_to_load = items_to_load,
        probabilities = [ proba_files_nn1, proba_files_nn2 ]
    )
    if nBestDict is not None:
        df = utils.get_n_best( df, nBestDict[ "nBest" ], feature = nBestDict[ "feature" ], sort = nBestDict[ "sort" ], data_only = False )
    for key, value in tf_aliases.items():
        if key in df.columns:
            df.rename( columns = { key : value }, inplace = True )
    df[ "info_sample_weight" ] = df[ "info_sample" ].apply( lambda x: samples.get_weights_by_sample_number( x, lumi = lumi ) ) # lumi in nb^-1
    return df

### Trigger-Related Functions ###
### ------------------------- ###

def get_trigger_filters( df ):
    f_trigger_e5_or_2e5  = df[ "info_trigger_decision"                     ] == 1
    f_trigger_e5         = df[ "info_trigger_decision_HLT_e5_SpecialL1OR"  ] == 1 
    f_trigger_2e5        = df[ "info_trigger_decision_HLT_2e5_SpecialL1OR" ] == 1 
    trigger_filters_dict = {
        "( e5 || 2e5 ) && SpecialL1OR"   : f_trigger_e5_or_2e5, 
        "e5 && SpecialL1OR"              : f_trigger_e5,
        "2e5 && SpecialL1OR"             : f_trigger_2e5,
        "( e5 && !2e5 ) && SpecialL1OR"  : f_trigger_e5  & ~ f_trigger_2e5,
        "( 2e5 && !e5 ) && SpecialL1OR"  : f_trigger_2e5 & ~ f_trigger_e5,
    }
    return trigger_filters_dict

def get_yield_contributions( 
        df, 
        trigger_filters_dict = None 
    ):
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )

    yield_contributions                  = {}
    yield_contributions_unprescaled      = {}
    r_yield_contributions                = {}
    r_yield_contributions_unprescaled    = {}
    yield_contributions_1ifb             = {}
    yield_contributions_unprescaled_1ifb = {}

    total_pileup_weight    = df[ "info_pure_pileup_weight" ].sum()

    w_total                = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ].sum()
    w_2e5                  = df[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR"      ].sum()
    w_e5                   = df[ "info_pileup_prescale_weight_HLT_e5_SpecialL1OR"       ].sum()
    w_e5_unique            = w_total - w_2e5
    w_2e5_unique           = w_total - w_e5
    w_total_for_e5_unique  = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    w_total_for_2e5_unique = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    u_total                = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ].sum()
    u_2e5                  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ].sum()
    u_e5                   = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ].sum()
    u_e5_unique            = u_total - u_2e5
    u_2e5_unique           = u_total - u_e5
    u_total_for_e5_unique  = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ].sum()
    u_total_for_2e5_unique = df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ].sum()

    w_total_1ifb                = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] * df[ "info_sample_weight" ] ).sum()
    w_2e5_1ifb                  = ( df[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR"      ] * df[ "info_sample_weight" ] ).sum()
    w_e5_1ifb                   = ( df[ "info_pileup_prescale_weight_HLT_e5_SpecialL1OR"       ] * df[ "info_sample_weight" ] ).sum()
    w_e5_unique_1ifb            = w_total_1ifb - w_2e5_1ifb
    w_2e5_unique_1ifb           = w_total_1ifb - w_e5_1ifb
    w_total_for_e5_unique_1ifb  = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] ).sum()
    w_total_for_2e5_unique_1ifb = ( df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] ).sum()

    u_total_1ifb                = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] ).sum()
    u_2e5_1ifb                  = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "2e5 && SpecialL1OR"           ] ] ).sum()
    u_e5_1ifb                   = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "e5 && SpecialL1OR"            ] ] ).sum()
    u_e5_unique_1ifb            = u_total_1ifb - u_2e5_1ifb
    u_2e5_unique_1ifb           = u_total_1ifb - u_e5_1ifb
    u_total_for_e5_unique_1ifb  = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] ).sum()
    u_total_for_2e5_unique_1ifb = ( df[ "info_pileup_prescale_weight_SpecialL1OR" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] * df[ "info_sample_weight" ][ trigger_filters_dict[ "( 2e5 && !e5 ) && SpecialL1OR" ] ] ).sum()

    yield_contributions               [ "Total Weight"                ] = round( w_total                / total_pileup_weight, 4 )
    yield_contributions               [ "Total 2e5 Weight"            ] = round( w_2e5                  / total_pileup_weight, 4 )
    yield_contributions               [ "Total e5 Weight"             ] = round( w_e5                   / total_pileup_weight, 4 )
    yield_contributions               [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions               [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions               [ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions               [ "Unique e5 Weight"            ] = round( w_e5_unique            / total_pileup_weight, 4 )

    yield_contributions_unprescaled   [ "Total Weight"                ] = round( u_total                / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total 2e5 Weight"            ] = round( u_2e5                  / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total e5 Weight"             ] = round( u_e5                   / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique  / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Unique 2e5 Weight"           ] = round( u_2e5_unique           / total_pileup_weight, 4 )
    yield_contributions_unprescaled   [ "Unique e5 Weight"            ] = round( u_e5_unique            / total_pileup_weight, 4 )

    r_yield_contributions             [ "Total Weight"                ] = round( w_total                / w_total, 4 )
    r_yield_contributions             [ "Total 2e5 Weight"            ] = round( w_2e5                  / w_total, 4 )
    r_yield_contributions             [ "Total e5 Weight"             ] = round( w_e5                   / w_total, 4 )
    r_yield_contributions             [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique / w_total, 4 )
    r_yield_contributions             [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique  / w_total, 4 )
    r_yield_contributions             [ "Unique 2e5 Weight"           ] = round( w_2e5_unique           / w_total, 4 )
    r_yield_contributions             [ "Unique e5 Weight"            ] = round( w_e5_unique            / w_total, 4 )

    r_yield_contributions_unprescaled [ "Total Weight"                ] = round( u_total                / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total 2e5 Weight"            ] = round( u_2e5                  / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total e5 Weight"             ] = round( u_e5                   / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique / u_total, 4 )
    r_yield_contributions_unprescaled [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique  / u_total, 4 )
    r_yield_contributions_unprescaled [ "Unique 2e5 Weight"           ] = round( u_2e5_unique           / u_total, 4 )
    r_yield_contributions_unprescaled [ "Unique e5 Weight"            ] = round( u_e5_unique            / u_total, 4 )

    yield_contributions_1ifb             [ "Total Weight"                ] = round( w_total_1ifb                , 4 )
    yield_contributions_1ifb             [ "Total 2e5 Weight"            ] = round( w_2e5_1ifb                  , 4 )
    yield_contributions_1ifb             [ "Total e5 Weight"             ] = round( w_e5_1ifb                   , 4 )
    yield_contributions_1ifb             [ "Total Weight for Unique 2e5" ] = round( w_total_for_2e5_unique_1ifb , 4 )
    yield_contributions_1ifb             [ "Total Weight for Unique e5"  ] = round( w_total_for_e5_unique_1ifb  , 4 )
    yield_contributions_1ifb             [ "Unique 2e5 Weight"           ] = round( w_2e5_unique_1ifb           , 4 )
    yield_contributions_1ifb             [ "Unique e5 Weight"            ] = round( w_e5_unique_1ifb            , 4 )

    yield_contributions_unprescaled_1ifb [ "Total Weight"                ] = round( u_total_1ifb                , 4 )
    yield_contributions_unprescaled_1ifb [ "Total 2e5 Weight"            ] = round( u_2e5_1ifb                  , 4 )
    yield_contributions_unprescaled_1ifb [ "Total e5 Weight"             ] = round( u_e5_1ifb                   , 4 )
    yield_contributions_unprescaled_1ifb [ "Total Weight for Unique 2e5" ] = round( u_total_for_2e5_unique_1ifb , 4 )
    yield_contributions_unprescaled_1ifb [ "Total Weight for Unique e5"  ] = round( u_total_for_e5_unique_1ifb  , 4 )
    yield_contributions_unprescaled_1ifb [ "Unique 2e5 Weight"           ] = round( u_2e5_unique_1ifb           , 4 )
    yield_contributions_unprescaled_1ifb [ "Unique e5 Weight"            ] = round( u_e5_unique_1ifb            , 4 )

    yields_dict = {
        "Relative to Baseline"                                       : yield_contributions,
        "Relative to Baseline (HLT Unprescaled)"                     : yield_contributions_unprescaled,
        "Relative to ( e5 || 2e5 ) && SpecialL1OR"                   : r_yield_contributions,
        "Relative to ( e5 || 2e5 ) && SpecialL1OR (HLT Unprescaled)" : r_yield_contributions_unprescaled,
        "Absolute at 1 fb^-1"                                        : yield_contributions_1ifb,
        "Absolute at 1 fb^-1 (HLT Unprescaled)"                      : yield_contributions_unprescaled_1ifb
    }
    return yields_dict

def plotFeatures( 
        df, 
        trigger_filters_dict = None, 
        title = "", 
        slug  = ""
    ):
    if slug == "":
        slug = title
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    pathlib.Path( "plots" ).mkdir( parents = True, exist_ok = True )

    ### Plots for the Baseline, L1OR, and ( e5 || 2e5 ) && L1OR ###
    ### ------------------------------------------------------- ###
    
    df_baseline            = copy.deepcopy( df )
    df_baseline_unweighted = copy.deepcopy( df )
    df_either              = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_either_unweighted   = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    #df_l1or               = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    #df_l1or_unweighted    = copy.deepcopy( df[ trigger_filters_dict[ "SpecialL1OR" ] ] )
    
    df_baseline            [ "info_weight" ] = df[ "info_pure_pileup_weight" ]
    df_baseline_unweighted [ "info_weight" ] = np.ones( len( df_baseline_unweighted ) )
    df_either              [ "info_weight" ] = df_either[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ]
    df_either_unweighted   [ "info_weight" ] = df_either_unweighted[ "info_pure_pileup_weight" ]
    #df_l1or               [ "info_weight" ] = df_l1or[ "info_pileup_prescale_weight_SpecialL1OR" ]
    #df_l1or_unweighted    [ "info_weight" ] = df_l1or_unweighted[ "info_pure_pileup_weight" ]

    df_baseline            [ "info_weight" ] *= df_baseline            [ "info_sample_weight" ]
    df_baseline_unweighted [ "info_weight" ] *= df_baseline_unweighted [ "info_sample_weight" ]
    df_either              [ "info_weight" ] *= df_either              [ "info_sample_weight" ]
    df_either_unweighted   [ "info_weight" ] *= df_either_unweighted   [ "info_sample_weight" ]
    #df_l1or               [ "info_weight" ] *= df_l1or               [ "info_sample_weight" ]
    #df_l1or_unweighted    [ "info_weight" ] *= df_l1or_unweighted    [ "info_sample_weight" ]
    
    list_df        = [ df_baseline_unweighted, df_baseline, df_either ] #, df_l1or, df_l1or_unweighted ]
    list_df_names  = [ "Baseline (No Weights)", "Baseline (Only Pileup Weights)", "( e5 || 2e5 ) && L1OR" ] #, "L1OR", "L1OR (No Weights)" ]
    list_df_colors = [ "black", "grey", "tab:blue" ] #, "tab:purple", "tab:yellow" ]
    list_fills     = [ False, "stepfilled", "stepfilled" ] #, False, "stepfilled" ]
    list_alphas    = [ 1, 0.5, 0.5 ] #, 1, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features            = features,
       title               = title,
       output_filename     = f"{output_dir_plots}/trigger_reweighting_{slug}.pdf",
       input_labels        = list_df_names,
       input_colors        = list_df_colors,
       additional_features = additional_features_to_plot,
       normalized          = True,
       input_fills         = list_fills,
       input_alphas        = list_alphas
    )

    ### Plots for the Baseline, Unique e5, and Inclusive 2e5 ###
    ### ---------------------------------------------------- ###

    df_either        = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_unique_e5     = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_inclusive_2e5 = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR" ] ] )
    df_either        [ "info_weight" ] = df_either        [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ]
    df_unique_e5     [ "info_weight" ] = df_unique_e5     [ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] - df_unique_e5[ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    df_inclusive_2e5 [ "info_weight" ] = df_inclusive_2e5 [ "info_pileup_prescale_weight_HLT_2e5_SpecialL1OR" ]
    
    df_either        [ "info_weight" ] *= df_either        [ "info_sample_weight" ]
    df_unique_e5     [ "info_weight" ] *= df_unique_e5     [ "info_sample_weight" ]
    df_inclusive_2e5 [ "info_weight" ] *= df_inclusive_2e5 [ "info_sample_weight" ]

    list_df        = [ df_either, df_unique_e5, df_inclusive_2e5 ]
    list_df_names  = [ "( e5 || 2e5 )", "Unique e5", "Inclusive 2e5" ]
    list_df_colors = [ "tab:blue", "tab:red", "tab:green" ]
    list_alphas    = [ 0.5, 0.5, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features             = features,
       title                = title + " \n( Normalized to $\\mathcal{L}=1{\\ \\rm fb}^{-1}$ )",
       output_filename      = f"{output_dir_plots}/trigger_reweighting_{slug}_unique_e5.pdf",
       input_labels         = list_df_names,
       input_colors         = list_df_colors,
       additional_features  = additional_features_to_plot,
       normalized           = False,
       input_alphas         = list_alphas
    )

    ### Plots for the Baseline, Unique 2e5, and Inclusive e5 ( HLT Unprescaled ) ###
    ### ------------------------------------------------------------------------ ###
    df_unprescaled_either        = copy.deepcopy( df[ trigger_filters_dict[ "( e5 || 2e5 ) && SpecialL1OR"  ] ] )
    df_unprescaled_unique_e5     = copy.deepcopy( df[ trigger_filters_dict[ "( e5 && !2e5 ) && SpecialL1OR" ] ] )
    df_unprescaled_inclusive_2e5 = copy.deepcopy( df[ trigger_filters_dict[ "2e5 && SpecialL1OR"            ] ] )
    
    df_unprescaled_either        [ "info_weight" ] = df_unprescaled_either        [ "info_pileup_prescale_weight_SpecialL1OR" ]
    df_unprescaled_unique_e5     [ "info_weight" ] = df_unprescaled_unique_e5     [ "info_pileup_prescale_weight_SpecialL1OR" ] 
    df_unprescaled_inclusive_2e5 [ "info_weight" ] = df_unprescaled_inclusive_2e5 [ "info_pileup_prescale_weight_SpecialL1OR" ]

    df_unprescaled_either        [ "info_weight" ] *= df_unprescaled_either        [ "info_sample_weight" ]
    df_unprescaled_unique_e5     [ "info_weight" ] *= df_unprescaled_unique_e5     [ "info_sample_weight" ]
    df_unprescaled_inclusive_2e5 [ "info_weight" ] *= df_unprescaled_inclusive_2e5 [ "info_sample_weight" ]

    list_df        = [ df_unprescaled_either, df_unprescaled_unique_e5, df_unprescaled_inclusive_2e5 ]
    list_df_names  = [ i + " (HLT Unprescaled)" for i in [ "( e5 || 2e5 )", "Unique e5", "Inclusive 2e5" ] ]
    list_df_colors = [ "tab:blue", "tab:red", "tab:green" ]
    list_alphas    = [ 0.5, 0.5, 0.5 ]

    plotting.plot_baseline_features(
       list_df,
       features             = features,
       title                = title + " \n( Normalized to $\\mathcal{L}=1{\\ \\rm fb}^{-1}$ )",
       output_filename      = f"{output_dir_plots}/trigger_reweighting_{slug}_unique_e5_unprescaled.pdf",
       input_labels         = list_df_names,
       input_colors         = list_df_colors,
       additional_features  = additional_features_to_plot,
       normalized           = False,
       input_alphas         = list_alphas
    )

def effCompare( 
        df, 
        proba_name = "proba_gnn1", 
        trigger_filters_dict = None, 
        title = "", 
        slug = "", 
        score_thresholds = np.linspace( 0, 1, 101 ),
        ax = None,
        dsName = None
    ):

    if slug == "":
        slug = title
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    df[ "info_sample_weight" ] = df[ "info_sample" ].apply( lambda x: samples.get_weights_by_sample_number( x, lumi = 37.47 * 1e6 ) ) # lumi in nb^-1
    df[ "info_weight" ] = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] * df[ "info_sample_weight" ]
    baseline_num     = df[ "info_sample_weight" ].sum()
    baseline_sow     = df[ "info_weight" ].sum()
    #trig_filter      = trigger_filters_dict[ "( e5 || 2e5 )" ]
    #baseline_trig    = df[ get_trigger_filters( df )[ "( e5 || 2e5 )" ] ][ "info_sample_weight" ].sum()
    score_filters    = np.array( [ df[ proba_name ] >= i for i in score_thresholds ] )
    eff_num          = np.array( [ df[ "info_sample_weight" ][ score_filter ].sum() / baseline_num for score_filter in score_filters ] )
    eff_sow          = np.array( [ df[ "info_weight" ][ score_filter ].sum() / baseline_sow for score_filter in score_filters ] )
    yield_num        = eff_num * baseline_num
    yield_sow        = eff_sow * baseline_sow
    yield_sow_hypothetical = eff_num * baseline_sow
    _ax_flag = ax is None
    if _ax_flag:
        fig, ax = plt.subplots( 1, 1, figsize = ( 10, 10 ) )
    ln1l = ax.plot( 
        score_thresholds,
        eff_num,
        label     = "Trigger Passthrough, w/o Pileup/Prescale Weights " + ( "" if dsName is None else f"({dsName})" ),
        color     = "tab:red",
        linestyle = "-",
        lw = 1.5
    )
    ln2l = ax.plot(
        score_thresholds,
        eff_sow,
        label     = "Require Trigger, w/ Pileup & Prescale Weights " + ( "" if dsName is None else f"({dsName})" ), 
        color     = "tab:blue",
        linestyle = "-",
        lw = 1.5
    )
    ax.set_xlabel( "GNN1 Signal Score Cut" if "gnn1" in proba_name else "GNN2 Signal Score Cut", fontsize = 12, loc = "right" )
    ax.set_ylabel( "Signal Efficiency", fontsize = 12, loc = "top" )
    ax.set_title( title, fontsize = 12, loc = "left" )
    if _ax_flag:
        axR = ax.twinx()
        ln3l = axR.plot(
            score_thresholds,
            yield_sow_hypothetical,
            #label     = "Yield (Trigger Passthrough, No Pileup/Prescale Weights)",
            color     = "tab:red",
            linestyle = "--",
            lw = 1.5
        )
        ln4l = axR.plot(
            score_thresholds,
            yield_sow,
            #label     = "Yield (Require Trigger, Pileup/Prescale Weights)",
            color     = "tab:blue",
            linestyle = "--",
            lw = 1.5
        )
        axR.set_ylabel( "Expected Signal Yield at $L=37.47\ {\\rm fb}^{-1}$", fontsize = 12, loc = "top" )
    lns = ln1l + ln2l
    labs = [ l.get_label() for l in lns ]
    if _ax_flag:
       ax.legend( lns, labs, loc = "lower left", fontsize = 12 )
    ax.tick_params( axis = "both", which = "major", labelsize = 10 )
    if _ax_flag:
        axR.tick_params( axis = "both", which = "major", labelsize = 10 )
    ax.tick_params( axis = "both", which = "minor", labelsize = 8 )
    if _ax_flag:
        axR.tick_params( axis = "both", which = "minor", labelsize = 8 )
    ax.grid( True, which = "both", linestyle = "--", lw = 0.5, color = "gray" )
    if not _ax_flag:
        fig = ax.get_figure()
    return fig, ax, lns

def effCompareTwoDatasets( 
        dfNonRes, 
        dfRes,
        proba_name = "proba_gnn1",
        trigger_filters_dict = None, 
        title = "", 
        slug = "", 
        score_thresholds = np.linspace( 0, 1, 101 )
    ):
    if slug == "":
        slug = title
    fig, ax = plt.subplots( 1, 1, figsize = ( 10, 10 ) )
    fig, ax, lns1 = effCompare( dfNonRes, proba_name = proba_name,title = title, slug = slug, score_thresholds = score_thresholds, ax = ax )
    fig, ax, lns2 = effCompare( dfRes, proba_name = proba_name, slug = slug, score_thresholds = score_thresholds, ax = ax )
    lns = lns1 + lns2
    labs = [ l.get_label() for l in lns ]
    ax.legend( lns, labs, loc = "lower left", fontsize = 12 )
    return fig, ax, lns


proba_custom = [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 0.995 ]
def effCompare2D(
    df, 
    trigger_filters_dict = None, 
    title = "", 
    slug = "", 
    score_thresholds_gnn1 = proba_custom, #np.linspace( 0, 1, 21 ),
    score_thresholds_gnn2 = proba_custom #np.linspace( 0, 1, 21 )
):
    if slug == "":
        slug = title
    if trigger_filters_dict is None:
        trigger_filters_dict = get_trigger_filters( df )
    df[ "info_sample_weight" ] = df[ "info_sample" ].apply( lambda x: samples.get_weights_by_sample_number( x, lumi = 37.47 * 1e6 ) ) # lumi in nb^-1
    df[ "info_weight" ] = df[ "info_pileup_prescale_weight_HLTOR_SpecialL1OR" ] * df[ "info_sample_weight" ]
    baseline_num     = df[ "info_sample_weight" ].sum()
    baseline_sow     = df[ "info_weight" ].sum()
    score_filters_gnn1 = np.array( [ df[ "proba_gnn1" ] > i for i in score_thresholds_gnn1 ] )
    score_filters_gnn2 = np.array( [ df[ "proba_gnn2" ] > i for i in score_thresholds_gnn2 ] )
    score_filters = np.array(
        [ 
            [ score_filters_gnn1[ i ] & score_filters_gnn2[ j ] for j in range( len( score_thresholds_gnn2 ) ) ] \
                for i in range( len( score_thresholds_gnn1 ) ) 
        ]
    )
    eff_num = np.array(
        [ 
            [ df[ "info_sample_weight" ][ score_filter ].sum() / baseline_num for score_filter in score_filters[ i ] ] \
                for i in range( len( score_filters ) ) 
        ]
    )
    eff_sow = np.array(
        [ 
            [ df[ "info_weight" ][ score_filter ].sum() / baseline_sow for score_filter in score_filters[ i ] ] \
                for i in range( len( score_filters ) ) 
        ]
    )
    yield_num = eff_num * baseline_num
    yield_sow = eff_sow * baseline_sow
    yield_sow_hypothetical = eff_num * baseline_sow
    fig, axs = plt.subplots( 1, 2, figsize = ( 50, 25 ) )
    
    ax_eff_num = axs[ 0 ]
    # we will set annot fontsize to 10
    sns.heatmap( eff_num, ax = ax_eff_num, annot = True, fmt = ".2f", cmap = "rocket_r", annot_kws = { "fontsize": 12 }, cbar_kws = { "shrink": 0.7 } )
    ax_eff_num.set_xlabel( "GNN2 Signal Score Cut", fontsize = 20, loc = "right" )
    ax_eff_num.set_ylabel( "GNN1 Signal Score Cut", fontsize = 20, loc = "top" )
    ax_eff_num.set_title( "GNN Efficiency A (Trigger Passthrough, No PRW Weights)", fontsize = 20, loc = "left" )
    
    ax_eff_sow = axs[ 1 ]
    sns.heatmap( eff_sow, ax = ax_eff_sow, annot = True, fmt = ".2f", cmap = "rocket_r", annot_kws = { "fontsize": 12 }, cbar_kws = { "shrink": 0.7 } )
    ax_eff_sow.set_xlabel( "GNN2 Signal Score Cut", fontsize = 20, loc = "right" )
    ax_eff_sow.set_ylabel( "GNN1 Signal Score Cut", fontsize = 20, loc = "top" )
    ax_eff_sow.set_title( "GNN Efficiency B (Trigger Required, PRW Weighted)", fontsize = 20, loc = "left" )


    fig_yield, axs_yield = plt.subplots( 1, 2, figsize = ( 50, 25 ) )
    
    ax_yield_sow_hypothetical = axs_yield[ 0 ]
    sns.heatmap( yield_sow_hypothetical, ax = ax_yield_sow_hypothetical, annot = True, fmt = ".0f", cmap = "rocket_r", annot_kws = { "fontsize": 12 }, cbar_kws = { "shrink": 0.7 } )
    ax_yield_sow_hypothetical.set_xlabel( "GNN2 Signal Score Cut", fontsize = 20, loc = "right" )
    ax_yield_sow_hypothetical.set_ylabel( "GNN1 Signal Score Cut", fontsize = 20, loc = "top" )
    ax_yield_sow_hypothetical.set_title( "Yield at $37.47\\ {\\rm fb}^{-1}$ (Under GNN Efficiency A)", fontsize = 20, loc = "left" )
    
    ax_yield_sow = axs_yield[ 1 ]
    sns.heatmap( yield_sow, ax = ax_yield_sow, annot = True, fmt = ".0f", cmap = "rocket_r", annot_kws = { "fontsize": 12 }, cbar_kws = { "shrink": 0.7 } )
    ax_yield_sow.set_xlabel( "GNN2 Signal Score Cut", fontsize = 20, loc = "right" )
    ax_yield_sow.set_ylabel( "GNN1 Signal Score Cut", fontsize = 20, loc = "top" )
    ax_yield_sow.set_title( "Yield at $37.47\\ {\\rm fb}^{-1}$ (Under GNN Efficiency B)", fontsize = 20, loc = "left" )


    for iAx in [ ax_eff_num, ax_eff_sow, ax_yield_sow_hypothetical, ax_yield_sow ]:
        iAx.set_xticks( [ i + 0.5 for i in range( len( score_thresholds_gnn2 ) ) ] )
        iAx.set_xticklabels( [ f"{i:.3f}" for i in score_thresholds_gnn2 ], fontsize = 15 )
        iAx.set_yticks( [ i + 0.5 for i in range( len( score_thresholds_gnn1 ) ) ] )
        iAx.set_yticklabels( [ f"{i:.3f}" for i in score_thresholds_gnn1 ], fontsize = 15 )
        iAx.set_aspect( 1. / iAx.get_data_ratio() )
        # reverse the y-axis
        iAx.invert_yaxis()
    fig.suptitle( title, fontsize = 15 )
    fig_yield.suptitle( title, fontsize = 15 )
    fig.tight_layout()
    fig_yield.tight_layout()
    return fig, fig_yield
    
def main():
    df_nr_q2low   = df_load( input_data_nr_r2, q2bin = "low", lumi = 37.47 * 1e6 )
    df_res_q2high = df_load( input_data_r_r2, q2bin = "high", lumi = 37.47 * 1e6 )
    pp = PdfPages( f"{output_dir_plots}/efficiency_nr_q2low_r_q2high_gnn1.pdf" )
    pp.savefig( effCompareTwoDatasets(
        df_nr_q2low,
        df_res_q2high,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$) and Resonant Signal (High$-q^2$)",
        slug      = "nr_q2low_r_q2high_gnn1",
        score_thresholds = np.linspace( 0, 1, 101 )
    )[ 0 ] )
    pp.savefig( effCompareTwoDatasets(
        df_nr_q2low,
        df_res_q2high,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$) and Resonant Signal (High$-q^2$)",
        slug      = "nr_q2low_r_q2high_gnn1",
        score_thresholds = np.linspace( 0.9, 1, 101 )
    )[ 0 ] )
    pp.savefig( effCompareTwoDatasets(
        df_nr_q2low,
        df_res_q2high,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$) and Resonant Signal (High$-q^2$)",
        slug      = "nr_q2low_r_q2high_gnn1",
        score_thresholds = np.linspace( 0.99, 1, 101 )
    )[ 0 ] )
    pp.close()


    """
    pp = PdfPages( f"{output_dir_plots}/efficiency_nr_q2low_gnn1.pdf" )
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn1",
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ])
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn1",
        score_thresholds = np.linspace( 0.9, 1, 101 ),
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ] )
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn1",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn1",
        score_thresholds = np.linspace( 0.99, 1, 101 ),
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ] )
    pp.close()
    pp = PdfPages( f"{output_dir_plots}/efficiency_nr_q2low_gnn2.pdf" )
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn2",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn2",
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ] )
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn2",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn2",
        score_thresholds = np.linspace( 0.9, 1, 101 ),
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ] )
    pp.savefig( effCompare(
        df_nr_q2low,
        proba_name = "proba_gnn2",
        title     = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug      = "nr_q2low_gnn2",
        score_thresholds = np.linspace( 0.99, 1, 101 ),
        dsName    = "Non-Resonant (Low$-q^2$)"
    )[ 0 ] )
    pp.close()
    """
    """
    pp = PdfPages( f"{output_dir_plots}/efficiency_nr_q2low_gnn1_gnn2.pdf" )
    fig_eff, fig_yield = effCompare2D(
        df_nr_q2low,
        title = "Efficiency Comparison for MC Non-Resonant Signal (Low$-q^2$)",
        slug  = "nr_q2low_gnn1_gnn2"
    )
    pp.savefig( fig_eff )
    pp.savefig( fig_yield )
    pp.close()
    """
    



    
