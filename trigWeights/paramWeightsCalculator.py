#!/usr/bin/env python3

### Imports ###
### ------- ###

### Imports -> I/O ###
### -------------- ###
import os
import pathlib
import pickle

### Imports -> Plotting ###
### ------------------- ###
import matplotlib
matplotlib.use( "agg" )
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import functools

### Imports -> Array Manipulation ###
### ----------------------------- ###
import numpy as np

### Imports -> Internal ###
### ------------------- ###
import binMaster as bmaster

### Plotting Utils ###
### -------------- ###

def cartAnHeatPlot( 
        matrix, 
        binningX, 
        binningY, 
        ticksX,
        ticksY,
        labelX,
        labelY,
        labelZ,
        logZ = False, 
        minZ = 0,
        maxZ = 1,
        pct  = False,
        ax   = None
    ):
    """
    Cartesian Annotated Heatmap Plotter
    -----------------------------------
    Plots a 2D matrix as a heatmap with annotations on the bins.

    Arguments:
    ---------
    matrix          : 2D array of values to be plotted, say, dimension ( N, M )
    binningX        : 1D array of bin edges along X, dimension ( M + 1 )
    binningY        : 1D array of bin edges along Y, dimension ( N + 1 )
    ticksX      : 1D array of tick marks along X, arbitrary length
    ticksY      : 1D array of tick marks along Y, arbitrary length
    labelX : Label for the X-axis
    labelY : Label for the Y-axis
    labelZ          : Label for the colorbar
    logZ            : Logarithmic scale for the colorbar? ( Default: False )
    minZ            : Minimum value for the colorbar ( Default: 0 )
    maxZ            : Maximum value for the colorbar ( Default: 1 )
    pct             : Display the values as percentages? ( Default: False )
    ax              : Axis to plot on ( Default: None )

    Returns:
    -------
    ax              : Axis with the plot

    Example:
    -------
    ```
    import numpy as np
    # Generating Some Random Data
    data  = np.random.rand( 100, 100 )
    # Picking the Binning Corresponding to the Data
    xBins = np.linspace( 0, 1, 101 )
    yBins = np.linspace( 0, 1, 101 )
    # Picking the Ticks for the Axes
    ticksX = np.linspace( 0, 1, 11 )
    ticksY = np.linspace( 0, 1, 11 )
    # Plotting the Heatmap
    ax = cartAnHeatPlot( 
        data, 
        xBins, 
        yBins, 
        ticksX, 
        ticksY, 
        "X", 
        "Y", 
        "Z" 
    )
    ax.get_figure().savefig( "cartAnHeatPlotExample.pdf" )
    ```
    """
    if ax is None:
        fig, ax = plt.subplots( 1, 1, figsize = ( 10, 10 ) )
    
    ### Plotting the Heatmap ###
    ### --------------------- ###
    cbar = ax.pcolormesh( binningX, binningY, matrix.T, cmap = "viridis", linewidth = 0.1, vmin = minZ, vmax = maxZ )
    fig  = ax.get_figure()

    ### Plotting the Colorbar ###
    ### --------------------- ###
    cbar = fig.colorbar( cbar, ax = ax, fraction = 0.046, pad = 0.04 )
    if logZ:
        cbar.set_clim( minZ, maxZ )
        cbar.set_norm( matplotlib.colors.LogNorm( minZ, maxZ ) )
    cbar.set_label( labelZ, fontsize = 12, loc = "center", rotation = 270, labelpad = 10 )

    ### Annotating the Bins ###
    ### ------------------- ###
    printXThreshold = ticksX[ 1 ] - ticksX[ 0 ]
    printYThreshold = ticksY[ 1 ] - ticksY[ 0 ]
    for yIt, yEdge in enumerate( binningY ):
        if yIt == len( binningY ) - 1:
            break
        if ( binningY[ yIt + 1 ] - yEdge ) < printYThreshold:
            continue
        for xIt, xEdge in enumerate( binningX ):
            if xIt == len( binningX ) - 1:
                break
            if ( binningX[ xIt + 1 ] - xEdge ) < printXThreshold:
                continue
            xLoc = ( xEdge + binningX[ xIt + 1 ] ) / ( 2 )
            yLoc = ( yEdge + binningY[ yIt + 1 ] ) / ( 2 )
            if matrix[ xIt, yIt ] == np.nan:
                continue
            ax.annotate(
                "%.2f" % matrix[ xIt, yIt ] if not pct else "%.2f%%" % ( 100 * matrix[ xIt, yIt ] ),
                ( xLoc, yLoc ),
                fontsize            = 8,
                color               = "white" if matrix[ xIt, yIt ] < ( minZ + maxZ ) / 2 else "black",
                horizontalalignment = "center",
                verticalalignment   = "center"
            )
    
    ### Decorating the Axes ###
    ### ------------------- ###
    ax.set_xlabel  ( labelX, fontsize = 12, loc = "right" )
    ax.set_ylabel  ( labelY, fontsize = 12, loc = "top"   )
    ax.tick_params ( axis = "y", which = "major", labelsize = 10                )
    ax.tick_params ( axis = "y", which = "minor", labelsize =  8                )
    ax.tick_params ( axis = "x", which = "major", labelsize = 10, rotation = 90 )
    ax.tick_params ( axis = "x", which = "minor", labelsize =  8, rotation = 90 )
    ax.set_xticks( ticksX )
    ax.set_yticks( ticksY )

    ### Global Plot Massaging ###
    ### --------------------- ###
    ax.set_aspect( 1. / ax.get_data_ratio() )

    return ax.get_figure(), ax


def plotQuasi3DPhaseSpace( 
        df, 
        x, 
        y, 
        z, 
        fWeight     = None,
        binningX    = None,
        binningY    = None,
        ticksX      = None,
        ticksY      = None,
        labelX      = "",
        labelY      = "",
        labelZ      = "",
        numFineBins = 100,
        title       = "",
        weightName  = None,
        useKDE      = False
):
    """
    Quasi-3D Phase Space Plotter
    ----------------------------
    Plots a 3D phase space with the Z-axis being represented by the color of the bins.

    Arguments:
    ---------
    df          : DataFrame with the data to be plotted
    x           : Name of the X-axis variable
    y           : Name of the Y-axis variable
    z           : Name of the Z-axis variable
    fWeight     : Name of the weight variable ( Default: None )
    binningX    : Binning for the X-axis ( Default: None )
    binningY    : Binning for the Y-axis ( Default: None )
    ticksX      : Ticks for the X-axis ( Default: None )
    ticksY      : Ticks for the Y-axis ( Default: None )
    labelX      : Label for the X-axis ( Default: "" )
    labelY      : Label for the Y-axis ( Default: "" )
    labelZ      : Label for the Z-axis ( Default: "" )
    numFineBins : Number of fine bins for the Z-axis ( Default: 100 )
    title       : Title for the plot ( Default: "" )
    weightName  : Name for the weight variable ( Default: None )
    useKDE      : Use KDE for the Z-axis? ( Default: False )

    Returns:
    -------
    ax          : Axis with the plot

    Example:
    -------
    ```
    import numpy as np
    import pandas as pd
    # Generating Some Random Data
    data = {
        "x" : np.random.rand( 1000 ),
        "y" : np.random.rand( 1000 ),
        "z" : np.random.rand( 1000 )
    }
    df = pd.DataFrame( data )
    # Plotting the Quasi-3D Phase Space
    ax = plotQuasi3DPhaseSpace( df, "x", "y", "z" )
    ax.get_figure().savefig( "plotQuasi3DPhaseSpaceExample.pdf" )
    """
    ### Massaging the Inputs ###
    ### -------------------- ###
    if labelX == "":
        labelX = x
    if labelY == "":
        labelY = y
    if labelZ == "":
        labelZ = z
    if weightName is None:
        if fWeight is None:
            weightName = "Events"
        else:
            weightName = fWeight
    
    ### Binning ###
    ### ------- ###
    xBinsFine = binningX if binningX is not None else np.linspace( df[ x ].min(), df[ x ].max(), numFineBins )
    yBinsFine = binningY if binningY is not None else np.linspace( df[ y ].min(), df[ y ].max(), numFineBins )

    ### Calculating the Plot Values ###
    ### --------------------------- ###
     
    bdf = bmaster.binnedData2D( df, x, y, binningX = xBinsFine, binningY = yBinsFine )
    
    if useKDE:
        print( "The KDE implementation is WIP. Please use the discrete implementation for now." )
        return
    else:
        denWeights = bdf.getHist( fWeight )
        zVals    = bdf.getBinnedMean( z, fWeight )
        zVals[ denWeights == 0 ] = np.nan
    
    ### Plotting ###
    ### -------- ###
    _, ax = plt.subplots( 1, 2, figsize = ( 16, 8 ) )
    if useKDE:
        print( "The KDE implementation is WIP. Please use the discrete implementation for now." )
        return
    else: 
        zVals[ denWeights == 0 ] = np.nan
        zMin = zVals[ denWeights != 0 ].min()
        zMax = zVals[ denWeights != 0 ].max()
        tMin = denWeights.min()
        tMax = denWeights.min()
        _, ax[ 0 ] = cartAnHeatPlot( zVals   , xBinsFine, yBinsFine, ticksX, ticksY, labelX, labelY, labelZ    , ax = ax[ 0 ], minZ = zMin, maxZ = zMax )
        _, ax[ 1 ] = cartAnHeatPlot( denWeights, xBinsFine, yBinsFine, ticksX, ticksY, labelX, labelY, weightName, ax = ax[ 1 ], minZ = tMin, maxZ = tMax )

    ax[ 0 ].set_title( title, fontsize = 12, loc = "left" )
    ax[ 1 ].set_title( title, fontsize = 12, loc = "left" )

    return ax.get_figure(), ax


### Effciciency Extraction ###
### ---------------------- ###

def weightFuncForRatios( 
        df, 
        binMask, 
        fWeightNum,
        fWeightDen,
        **kwargs 
    ):
    numWeights    = df[ fWeightNum ][ binMask ].sum()
    denWeights    = df[ fWeightDen ][ binMask ].sum()
    numWeightsErr = ( df[ fWeightNum ][ binMask ] ** 2 ).sum() ** 0.5
    denWeightsErr = ( df[ fWeightDen ][ binMask ] ** 2 ).sum() ** 0.5
    ratWeights    = numWeights / denWeights
    ratWeightsErr = ratWeights * ( ( numWeightsErr / numWeights ) ** 2 + ( denWeightsErr / denWeights ) ** 2 ) ** 0.5
    return ratWeights, ratWeightsErr ** 2

def mapEff(
        df, 
        parameter, 
        fWeightNum,
        fWeightDen,
        parameterLabel    = None,
        paramBinning      = None,
        paramBinsN        = 20,
        paramMin          = None,
        paramMax          = None,
        paramBinsFineN    = 100,
        paramBinsLog      = False,
        paramBinsFineLog  = False,
        title             = "",
        effName           = "Efficiency Weights", 
        oBKwargs          = { 
                               "errThreshold" : 0.1, 
                               "method"       : "finest" 
                           },
        useOptimalBinning = True,
        useKDE            = False,
        ax                = None
    ):

    ### Massaging the Inputs ###
    ### -------------------- ###
    if parameterLabel is None:
        parameterLabel = parameter

    ### Binning ###
    ### ------- ###
    bdf = bmaster.binnedData( df, parameter )
    if paramBinning is None:
        if paramMin is None:
            paramMin = bdf.xMin
        if paramMax is None:
            paramMax = bdf.xMax 
        if paramBinsLog:
            paramBinning = np.exp( np.linspace( np.log( paramMin, paramMax, paramBinsN ) ) )
        else:
            paramBinning = np.linspace( paramMin, paramMax, paramBinsN )
    else:
        paramMin = paramBinning[  0 ]
        paramMax = paramBinning[ -1 ]
    if paramBinsFineLog:
        fineBinning   = np.exp( np.linspace( np.log( paramBinning[ 0 ], paramBinning[ -1 ], paramBinsFineN ) ) )
    else:
        fineBinning       = np.linspace( paramBinning[ 0 ], paramBinning[ -1 ], paramBinsFineN )
    fineBinCenters    = ( fineBinning[ :-1 ] + fineBinning[ 1: ] ) / 2

    ### Calculating the Weights ###
    ### ----------------------- ###

    ### Calculating the Weights -> Continuous ###
    ### ------------------------------------- ###
    if useKDE:
        _denWeights = bmaster.binnedData( df, parameter, binning = fineBinning ).getHist( fWeightDen )
        sdf         = bmaster.sBinnedData( df, parameter )
        numWeights  = sdf.getDensityAt( fineBinCenters, fWeightNum, dfNormTarget = df )
        denWeights  = sdf.getDensityAt( fineBinCenters, fWeightDen, dfNormTarget = df )
        ratWeights  = numWeights / denWeights
        ratWeights[ _denWeights == 0 ] = np.nan
    
    ### Calculating the Weights -> Discrete ###
    ### ----------------------------------- ###
    else:
        if useOptimalBinning:
            oBKwargs[ "weightFunc" ] = functools.partial( weightFuncForRatios, fWeightNum = fWeightNum, fWeightDen = fWeightDen )
            bdf.setOptimalBinning( **oBKwargs )
        else:
            bdf.reBin( binning = fineBinning )
        numWeights    = bdf.getHist( fWeightNum )
        denWeights    = bdf.getHist( fWeightDen )
        ratWeights    = numWeights / denWeights
        numWeightsErr = bdf.getHistErr( fWeightNum )
        denWeightsErr = bdf.getHistErr( fWeightDen )
        ratWeightsErr = ratWeights * ( ( numWeightsErr / numWeights ) ** 2 + ( denWeightsErr / denWeights ) ** 2 ) ** 0.5 
    
    ### Plotting ###
    ### -------- ###

    if ax is None:
        fig, ax = plt.subplots( 1, 1, figsize = ( 8, 8 ) )
    
    ax.plot( 
        fineBinCenters if useKDE else bdf.binCenters,
        ratWeights, 
        color      = "tab:green", 
        marker     = "o", 
        linestyle  = "--", 
        lw         = 1.2, 
        markersize = 3 
    )
    if not useKDE:
        ax.errorbar(
            bdf.binCenters,
            ratWeights,
            yerr       = ratWeightsErr,
            xerr       = bdf.binWidths / 2,
            fmt        = "o",
            color      = "tab:orange",
            capsize    = 3,
            elinewidth = 1.2,
            barsabove  = True
        )
    for counter in range( len( bdf.binCenters ) ):
        if not useKDE:
            ax.fill_between( [ bdf.binEdges[ counter ], bdf.binEdges[ counter + 1 ] ], ratWeights[ counter ] + ratWeightsErr[ counter ], ratWeights[ counter ] - ratWeightsErr[ counter ], color = "tab:orange", alpha = 0.3 )
    
    ax.set_xlabel( parameterLabel, fontsize = 10, loc = "right" )
    ax.set_ylabel(        effName, fontsize = 10, loc = "top"   )
    ax.tick_params( axis = "both", which = "major", labelsize = 8 )
    ax.tick_params( axis = "both", which = "minor", labelsize = 6 )
    ax.set_aspect( 1. / ax.get_data_ratio() )
    ax.set_title( title, fontsize = 12, loc = "left" )
    fig.tight_layout()
    
    ### Returning ###
    ### --------- ###
    if useKDE:
        wdf = sdf 
        wdf.densities[ effName ] = wdf.compose( f"${fWeightNum} / ${fWeightDen}" )
    else:
        wdf = bmaster.binnedData( binBy = parameter, binning = fineBinning )
        wdf.binnedVals[ effName ] = ratWeights

    return wdf, ax.get_figure(), ax 

def mapEff2D( 
        df, 
        parameterX, 
        parameterY,
        fWeightNum,
        fWeightDen,  
        paramXLabel       = None,
        paramYLabel       = None,
        paramXBinning     = None,
        paramYBinning     = None,
        paramXBinsN       = 20,
        paramYBinsN       = 20,
        paramXMin         = None,
        paramYMin         = None, 
        paramXMax         = None,
        paramYMax         = None,
        paramXBinsFineN   = 100,
        paramXBinsLog     = False,
        paramXBinsFineLog = False,
        paramYBinsFineN   = 100,
        paramYBinsLog     = False,
        paramYBinsFineLog = False,
        title             = "",
        effName           = "Efficiency Weights",
        oBKwargs          = {},
        useOptimalBinning = False,
        useKDE            = False
    ):

    ### Massaging the Inputs ###
    ### -------------------- ###
    if paramXLabel is None:
        paramXLabel = parameterX
    if paramYLabel is None:
        paramYLabel = parameterY 
    
    ### Binning ###
    ### ------- ###
    if paramXBinning is None:
        if paramXMin is None:
            paramXMin = df[ parameterX ].min()
        if paramXMax is None:
            paramXMax = df[ parameterX ].max()
        if paramXBinsLog:
            paramXBinning = np.exp( np.linspace( np.log( paramXMin, paramXMax, paramXBinsN ) ) )
        else:
            paramXBinning = np.linspace( paramXMin, paramXMax, paramXBinsN )
    else:
        paramXMin = paramXBinning[  0 ]
        paramXMax = paramXBinning[ -1 ]
        paramXBinsN = len( paramXBinning ) - 1
    if paramXBinsFineLog:
        fineBinningX = np.exp( np.linspace( np.log( paramXBinning[ 0 ], paramXBinning[ -1 ], paramXBinsFineN ) ) )
    else:
        fineBinningX = np.linspace( paramXBinning[ 0 ], paramXBinning[ -1 ], paramXBinsFineN )
    fineBinCentersX = ( fineBinningX[ :-1 ] + fineBinningX[ 1: ] ) / 2

    if paramYBinning is None:
        if paramYMin is None:
            paramYMin = df[ parameterY ].min()
        if paramYMax is None:
            paramYMax = df[ parameterY ].max()
        if paramYBinsLog:
            paramYBinning = np.exp( np.linspace( np.log( paramYMin, paramYMax, paramYBinsN ) ) )
        else:
            paramYBinning = np.linspace( paramYMin, paramYMax, paramYBinsN )
    else:
        paramYMin = paramYBinning[  0 ]
        paramYMax = paramYBinning[ -1 ]
        paramYBinsN = len( paramYBinning ) - 1
    if paramYBinsFineLog:
        fineBinningY = np.exp( np.linspace( np.log( paramYBinning[ 0 ], paramYBinning[ -1 ], paramYBinsFineN ) ) )
    else:
        fineBinningY = np.linspace( paramYBinning[ 0 ], paramYBinning[ -1 ], paramYBinsFineN )
    fineBinCentersY = ( fineBinningY[ :-1 ] + fineBinningY[ 1: ] ) / 2

    bdf = bmaster.binnedData2D( df, parameterX, parameterY, binningX = paramXBinning, binningY = paramYBinning )

    ### Calculating the Weights ###
    ### ----------------------- ###
    
    ### Calculating the Weights -> Continuous ###
    ### ------------------------------------- ###
    if useKDE:
        _bdf                 = bmaster.binnedData2D( df, parameterX, parameterY, binningX = fineBinningX, binningY = fineBinningY )
        sdf                  = bmaster.sBinnedData2D( df, parameterX, parameterY )
        _denWeights          = _bdf.getHist( fWeightDen ) 
        fineMeshX, fineMeshY = np.meshgrid( fineBinCentersX, fineBinCentersY )
        finePositions        = np.vstack( [ fineMeshX.ravel(), fineMeshY.ravel() ] )
        numWeights           = sdf.getDensityAt( finePositions, fWeightNum, dfNormTarget = df ).reshape( fineMeshX.shape )
        denWeights           = sdf.getDensityAt( finePositions, fWeightDen, dfNormTarget = df ).reshape( fineMeshX.shape )
        ratWeights           = numWeights / denWeights 
        ratWeights[ _denWeights == 0 ] = np.nan

    ### Calculating the Weights -> Discrete ###
    ### ----------------------------------- ###
    else:
        if useOptimalBinning:
            bdf.setOptimalBinning2D( **oBKwargs )
        numWeights     = bdf.getHist( fWeightNum )
        denWeights     = bdf.getHist( fWeightDen )
        ratWeights     = numWeights / denWeights
        numWeightsErr  = bdf.getHistErr( fWeightNum )
        denWeightsErr  = bdf.getHistErr( fWeightDen )
        ratWeightsErr  = ratWeights * ( ( numWeightsErr / numWeights ) ** 2 + ( denWeightsErr / denWeights ) ** 2 ) ** 0.5

    ### Plotting ###
    ### -------- ### 
    if useKDE:
        fig, ax = plt.subplots( 1, 1, figsize = ( 8, 8 ) )
    else:
        fig, axs = plt.subplots( 1, 2, figsize = ( 16, 8 ) )
        ax    = axs[ 0 ]
        axErr = axs[ 1 ]
    if useKDE:
        _, ax = cartAnHeatPlot( 
                ratWeights,
                binningX = fineBinningX,
                binningY = fineBinningY,
                ticksX   = paramXBinning,
                ticksY   = paramYBinning,
                labelX   = paramXLabel,
                labelY   = paramYLabel,
                labelZ   = effName,
                ax       = ax
            )
        #axErr = cartAnHeatPlot( ratWeightsErr / ratWeights, binningX, binningY, ticksX, ticksY, labelX, labelY, f"Relative {effName} Error", ax = axErr, pct = True )
    else:
        _, ax = cartAnHeatPlot(
                ratWeights,
                binningX = bdf.binningX,
                binningY = bdf.binningY,
                ticksX   = paramXBinning,
                ticksY   = paramYBinning,
                labelX   = paramXLabel,
                labelY   = paramYLabel,
                labelZ   = effName,
                ax       = ax
            )
        _, axErr = cartAnHeatPlot( 
               ratWeightsErr / ratWeights,
                binningX = bdf.binningX,
                binningY = bdf.binningY,
                ticksX   = paramXBinning,
                ticksY   = paramYBinning,
                labelX   = paramXLabel,
                labelY   = paramYLabel,
                labelZ   = f"Relative {effName} Error",
                ax       = axErr,
                pct      = True
            )
    fig.suptitle( title, fontsize = 12 )
    fig.tight_layout()

    ### Returning ###
    ### --------- ###
    if useKDE:
        wdf = sdf
        wdf.densities[ effName ] = wdf.compose( f"${fWeightNum} / ${fWeightDen}" )
    else:
        wdf = bmaster.binnedData2D( binByX = parameterX, binByY = parameterY, binningX = fineBinningX, binningY = fineBinningY )
        wdf.binnedVals[ effName ] = ratWeights

    return wdf, ax.get_figure(), ax

def wrapMapEff( 
        df,
        parameterAr,
        fWeightNum,
        fWeightDen,
        parameterLabelAr   = None,
        paramBinningAr     = None,
        paramBinsNAr       = 20,
        paramMinAr         = None,
        paramMaxAr         = None,
        paramBinsFineNAr   = 100,
        paramBinsLogAr     = False,
        paramBinsFineLogAr = False,
        title              = "",
        effName            = "Efficiency Weights",
        oBKwargs           = {},
        useOptimalBinning  = False,
        useKDE             = False,
        fileSuffix         = None,
        outputDir          = "plots"
    ):
    ### Massaging the Inputs ###
    ### -------------------- ###
    for x in [ 
                parameterAr, 
                parameterLabelAr, 
                paramBinningAr, 
                paramBinsNAr, 
                paramMinAr, 
                paramMaxAr, 
                paramBinsFineNAr, 
                paramBinsLogAr, 
                paramBinsFineLogAr 
            ]:
        if not isinstance( x, list ):
            x = [ x ] * len( parameterAr )
        if len( x ) != len( parameterAr ):
            print( "The length of the input lists do not match!" )
            return
    
    ### I/O Setup ###
    ### --------- ###
    plotFileName =  "parameteized_weights_maps_"
    plotFileName += effName.lower().replace( " ", "_" ) 
    plotFileName += "_binned_" if not useKDE else "_smooth_"
    plotFileName += fileSuffix if fileSuffix is not None else ""
    plotFileName += ".pdf"
    plotFileName = os.path.join( outputDir, plotFileName )
    pathlib.Path( outputDir ).mkdir( parents = True, exist_ok = True )
    pp = PdfPages( plotFileName )

    paramWMapsDirName = "parameterized_weights_maps"
    paramWMapsDirName += effName.lower().replace( " ", "_" )
    paramWMapsDirName += "_binned" if not useKDE else "_smooth"
    paramWMapsDirName += fileSuffix if fileSuffix is not None else ""
    paramWMapsDirName = os.path.join( outputDir, paramWMapsDirName )
    pathlib.Path( paramWMapsDirName ).mkdir( parents = True, exist_ok = True )

    ### Plotting & Saving ###
    ### ----------------- ###
    for idx, parameter in enumerate( parameterAr ):
        wdf, fig, ax = mapEff(
                        df,
                        parameter,
                        fWeightNum        = fWeightNum,
                        fWeightDen        = fWeightDen,
                        parameterLabel    = parameterLabelAr[ idx ],
                        paramBinning      = paramBinningAr[ idx ],
                        paramBinsN        = paramBinsNAr[ idx ],
                        paramMin          = paramMinAr[ idx ],
                        paramMax          = paramMaxAr[ idx ],
                        paramBinsFineN    = paramBinsFineNAr[ idx ],
                        paramBinsLog      = paramBinsLogAr[ idx ],
                        paramBinsFineLog  = paramBinsFineLogAr[ idx ],
                        title             = title,
                        effName           = effName,
                        oBKwargs          = oBKwargs,
                        useOptimalBinning = useOptimalBinning,
                        useKDE            = useKDE
                    )
        pp.savefig( fig )
        plt.close( fig )
        paramWMapsFileName = f"parametrized_wrt_{parameter}.pkl"
        paramWMapsFileName = os.path.join( paramWMapsDirName, paramWMapsFileName )
        with open( paramWMapsFileName, "wb" ) as f:
            pickle.dump( wdf, f )
            print( f"Saved the weight map for {effName} parametrized w.r.t. {parameter} as: {paramWMapsFileName}" )
    pp.close()
    print( f"Saved the plots of the parametrized weight maps for {effName} as: {plotFileName}" )

def wrapMapEff2D(
        df,
        parameterXAr,
        parameterYAr,
        fWeightNum,
        fWeightDen,
        parameterXLabelAr   = None,
        parameterYLabelAr   = None,
        paramXBinningAr     = None,
        paramYBinningAr     = None,
        paramXBinsNAr       = 20,
        paramYBinsNAr       = 20,
        paramXMinAr         = None,
        paramYMinAr         = None,
        paramXMaxAr         = None,
        paramYMaxAr         = None,
        paramXBinsFineNAr   = 100,
        paramXBinsLogAr     = False,
        paramXBinsFineLogAr = False,
        paramYBinsFineNAr   = 100,
        paramYBinsLogAr     = False,
        paramYBinsFineLogAr = False,
        title               = "",
        effName             = "Efficiency Weights",
        oBKwargs            = {},
        useOptimalBinning   = False,
        useKDE              = False,
        fileSuffix          = None,
        outputDir           = "plots" 
    ):
    ### Massaging the Inputs ###
    ### -------------------- ###
    for x in [
                parameterXAr,
                parameterYAr,
                parameterXLabelAr,
                parameterYLabelAr,
                paramXBinningAr,
                paramYBinningAr,
                paramXBinsNAr,
                paramYBinsNAr,
                paramXMinAr,
                paramYMinAr,
                paramXMaxAr,
                paramYMaxAr,
                paramXBinsFineNAr,
                paramXBinsLogAr,
                paramXBinsFineLogAr,
                paramYBinsFineNAr,
                paramYBinsLogAr,
                paramYBinsFineLogAr
            ]:
        if not isinstance( x, list ):
            x = [ x ] * len( parameterXAr )
        if len( x ) != len( parameterXAr ):
            print( "The length of the input lists do not match!" )
            return
    
    ### I/O Setup ###
    ### --------- ###
    plotFileName =  "parameteized_weights_maps_"
    plotFileName += effName.lower().replace( " ", "_" )
    plotFileName += "_binned_2d_" if not useKDE else "_smooth_2d_"
    plotFileName += fileSuffix if fileSuffix is not None else ""
    plotFileName += ".pdf"
    plotFileName = os.path.join( outputDir, plotFileName )
    pathlib.Path( outputDir ).mkdir( parents = True, exist_ok = True )
    pp = PdfPages( plotFileName )

    paramWMapsDirName = "parameterized_weights_maps"
    paramWMapsDirName += effName.lower().replace( " ", "_" )
    paramWMapsDirName += "_binned_2d" if not useKDE else "_smooth_2d"
    paramWMapsDirName += fileSuffix if fileSuffix is not None else ""
    paramWMapsDirName = os.path.join( outputDir, paramWMapsDirName )
    pathlib.Path( paramWMapsDirName ).mkdir( parents = True, exist_ok = True )

    ### Plotting & Saving ###
    ### ----------------- ###
    for idx, parameterX in enumerate( parameterXAr ):
        wdf, fig, ax = mapEff2D(
                        df,
                        parameterX,
                        parameterYAr[ idx ],
                        fWeightNum        = fWeightNum,
                        fWeightDen        = fWeightDen,
                        paramXLabel       = parameterXLabelAr[ idx ],
                        paramYLabel       = parameterYLabelAr[ idx ],
                        paramXBinning     = paramXBinningAr[ idx ],
                        paramYBinning     = paramYBinningAr[ idx ],
                        paramXBinsN       = paramXBinsNAr[ idx ],
                        paramYBinsN       = paramYBinsNAr[ idx ],
                        paramXMin         = paramXMinAr[ idx ],
                        paramYMin         = paramYMinAr[ idx ],
                        paramXMax         = paramXMaxAr[ idx ],
                        paramYMax         = paramYMaxAr[ idx ],
                        paramXBinsFineN   = paramXBinsFineNAr[ idx ],
                        paramXBinsLog     = paramXBinsLogAr[ idx ],
                        paramXBinsFineLog = paramXBinsFineLogAr[ idx ],
                        paramYBinsFineN   = paramYBinsFineNAr[ idx ],
                        paramYBinsLog     = paramYBinsLogAr[ idx ],
                        paramYBinsFineLog = paramYBinsFineLogAr[ idx ],
                        title             = title,
                        effName           = effName,
                        oBKwargs          = oBKwargs,
                        useOptimalBinning = useOptimalBinning,
                        useKDE            = useKDE
                    )
        pp.savefig( fig )
        plt.close( fig )
        paramWMapsFileName = f"parametrized_wrt_{parameterX}_and_{parameterYAr[ idx ]}.pkl"
        paramWMapsFileName = os.path.join( paramWMapsDirName, paramWMapsFileName )
        with open( paramWMapsFileName, "wb" ) as f:
            pickle.dump( wdf, f )
            print( f"Saved the weight map for {effName} parametrized w.r.t. {parameterX} and {parameterYAr[ idx ]} as: {paramWMapsFileName}" )
    pp.close()
    print( f"Saved the plots of the parametrized weight maps for {effName} as: {plotFileName}" )