#!/usr/bin/env python3

### Imports ###
### ------- ###

### Imports -> Array Manipulation ###
### ----------------------------- ###
import numpy as np 

### Imports -> I/O ###
### -------------- ###
from tqdm import tqdm

### Imports -> Math ###
### --------------- ###
import scipy.stats as st

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import functools

### Math ###
### ---- ###

def weightedMean( ar, weights ):
    return np.average( ar, weights = weights )

def weightedStd( ar, weights ):
    mean = weightedMean( ar, weights )
    return np.sqrt( np.average( ( ar - mean ) ** 2, weights = weights ) )


### Discrete Binning ###
### ---------------- ###

class binnedData:
    """
    Class to bin the data in a DataFrame by one of the features, possibly 
    weighted by another feature, and make various operations on the binned
    data.
    """
    def __init__( 
            self, 
            df       = None, 
            binBy    = None, 
            binning  = None, 
            nBins    = 50, 
            binByMin = None, 
            binByMax = None
        ):
        """
        Constructor for the `binnedData` class.

        Arguments:
        ----------
        - `df`: Data to be binned, either a `pd.DataFrame` or a 
        dictionary of `numpy.ndarray`s. Can be left `None` if 
        the class instance is to be used for bin manipulation only.
        - `binBy`: Feature to be bin the data by. Can be left `None` if
        the class instance is to be used for bin manipulation only.
        - `binning`: Binning to be used. If `None`, `nBins` and `binByMin` 
        and `binByMax` are used to create a uniformly linear binning.
        - `nBins`: Number of bins to be used if `binning` is `None`.
        - `binByMin`: Minimum value of the `binBy` feature to be used if 
        `binning` is `None`. If `None`, the minimum value of the `binBy` 
        feature in the data is used.
        - `binByMax`: Maximum value of the `binBy` feature to be used if
        `binning` is `None`. If `None`, the maximum value of the `binBy` 
        feature in the data is used.
        """
        self.df = df
        self.x  = binBy
        self.binnedHists     = {}
        self.binnedHistsErr  = {}
        self.binnedVals      = {}
        self.reBin( binning, nBins, binByMin, binByMax )

    def reBin( 
            self, 
            binning  = None,
            nBins    = 50,
            binByMin = None,
            binByMax = None
        ):
        """
        Method to rebin the data.

        Arguments:
        ----------
        - `binning`: Binning to be used. If `None`, `nBins` and `binByMin` 
        and `binByMax` are used to create a uniformly linear binning.
        - `nBins`: Number of bins to be used if `binning` is `None`.
        - `binByMin`: Minimum value of the `binBy` feature to be used if 
        `binning` is `None`. If `None`, the minimum value of the `binBy` 
        feature in the data is used.
        - `binByMax`: Maximum value of the `binBy` feature to be used if
        `binning` is `None`. If `None`, the maximum value of the `binBy` 
        feature in the data is used.
        """
        xMin    = binByMin
        xMax    = binByMax
        if binning is not None:
            nBins = len( binning ) - 1
            xMin  = binning[  0 ]
            xMax  = binning[ -1 ]
            if not isinstance( binning, np.ndarray ):
                binning = np.array( binning )
        else:
            if self.df is not None and self.x is not None:
                xMin    = self.df[ self.x ].min() if xMin is None else xMin
                xMax    = self.df[ self.x ].max() if xMax is None else xMax
                binning = np.linspace( xMin, xMax, nBins + 1 )
        self.xMin       = xMin
        self.xMax       = xMax
        self.binning    = binning
        self.nBins      = len( binning ) - 1 
        self.binCenters = ( binning[ :-1 ] + binning[ 1: ] ) / 2 
        self.binWidths  = binning[ 1: ] - binning[ :-1 ] 
        self.binEdges   = binning
        if self.df is not None and self.x is not None:
            self.binMasks  = np.array( 
                [ ( self.df[ self.x ] >= binning[ i ] ) & ( self.df[ self.x ] < binning[ i + 1 ] ) for i in range( self.nBins ) ] 
            )
        else:
            self.binMasks = None
        return self
    
    def getHist( self, weightBy = "__unweighted__" ):
        """
        Method to get the histogram of the binned data.

        Arguments:
        ----------
        `weightBy`: Feature to be used to weight the data. If 
        `"__unweighted__"`, the data is not weighted.

        Returns:
        --------
        - `np.ndarray`: Histogram of the binned data.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        import matplotlib
        matplotlib.use( "agg" )
        import matplotlib.pyplot as plt
        # Generate Random Data
        df = pd.DataFrame( 
            { 
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Histogram
        hist = bdf.getHist( weightBy = "w" )
        # Plot the Histogram
        fig, ax = plt.subplots()
        ax.bar( bdf.binCenters, hist, width = bdf.binWidths )
        ax.set_xlabel( "x" )
        ax.set_ylabel( "Counts (Weighted)" )
        fig.savefig( "binnedDataHistExample.pdf" )
        ```
        """
        if weightBy not in self.binnedHists.keys():
            self.binnedHists[ weightBy ] = np.histogram( 
                self.df[ self.x ], 
                bins    = self.binning, 
                weights = self.df[ weightBy ] if weightBy != "__unweighted__" else None
            )[ 0 ]
        return self.binnedHists[ weightBy ]
        
    def getHistErr( self, weightBy = "__unweighted__" ):
        """
        Method to get the error of the histogram of the binned data. The 
        error is calculated as the square root of the sum of the squares of 
        the weights in each bin.

        Arguments:
        ----------
        `weightBy`: Feature to be used to weight the data. If 
        `"__unweighted__"`, the data is not weighted.

        Returns:
        --------
        - `np.ndarray`: Error of the histogram of the binned data.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        import matplotlib
        matplotlib.use( "agg" )
        import matplotlib.pyplot as plt
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Histogram & Error
        hist = bdf.getHist( weightBy = "w" )
        err  = bdf.getHistErr( weightBy = "w" )
        # Plot the Histogram
        fig, ax = plt.subplots()
        ax.plot( 
            bdf.binCenters, 
            hist, 
            color     = "tab:blue", 
            marker    = "o", 
            linestyle = "--" 
        )
        ax.errorbar( 
            bdf.binCenters, 
            hist, 
            xerr  = bdf.binWidths / 2, 
            yerr  = err, 
            fmt   = "none", 
            color = "tab:blue" 
        )
        for i in range( len( bdf.binCenters ) ):
            ax.fill_between( 
                [ bdf.binEdges[ i ], bdf.binEdges[ i + 1 ] ], 
                hist[ i ] - err[ i ], 
                hist[ i ] + err[ i ], 
                color = "tab:red", 
                alpha = 0.5 
            )
        ax.set_xlabel( "x" )
        ax.set_ylabel( "Counts (Weighted)" )
        fig.savefig( "binnedDataHistErrExample.pdf" )
        ```
        """
        if weightBy not in self.binnedHistsErr.keys():
            self.binnedHistsErr[ weightBy ] = np.histogram( 
                self.df[ self.x ], 
                bins    = self.binning, 
                weights = self.df[ weightBy ] ** 2 if weightBy != "__unweighted__" else None
            )[ 0 ] ** 0.5
        return self.binnedHistsErr[ weightBy ]

    def getDataInBinByIdx( self, y, idx ):
        """
        Method to get the data in a bin by the bin index.

        Arguments:
        ----------
        - `y`: Feature to be returned.
        - `idx`: Index of the bin.

        Returns:
        --------
        - `np.ndarray`: Data in the bin.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Data in Bin
        yInBin5 = bdf.getDataInBinByIdx( "y", 5 )
        assert len( yInBin5 ) == bdf.binMasks[ 5 ].sum()
        """
        return self.df[ y ][ self.binMasks[ idx ] ]
    
    def getBinIdxByValue( self, val ):
        """
        Method to get the bin index by the value of the feature used to bin
        the data.

        Arguments:
        ----------
        - `val`: Value of the binning feature in the bin.
        
        Returns:
        --------
        - `int`: Index of the bin. If the value is outside the binning
        range, `None` is returned.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Bin Index
        idx = bdf.getBinIdxByValue( bdf.binCenters[ 0 ] )
        assert idx == 0
        """
        if val < self.xMin: 
            return None
        if val > self.xMax:
            return None
        return np.digitize( val, self.binEdges ) - 1

    def getDataInBinByValue( self, y, val ):
        """
        Method to get the data in a bin by the value of the feature used to
        bin the data.

        Arguments:
        ----------
        - `y`: Feature to be returned.
        - `val`: Value of the binning feature in the bin.

        Returns:
        --------
        - `np.ndarray`: Data in the bin.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Data in Bin
        yInBin0 = bdf.getDataInBinByValue( "y", bdf.binCenters[ 0 ] )
        assert len( yInBin0 ) == bdf.binMasks[ 0 ].sum()
        yInBin0Again = bdf.getDataInBinByValue( "y", bdf.binCenters[ 0 ] + bdf.binWidths[ 0 ] / 4 )
        assert len( yInBin0Again ) == bdf.binMasks[ 0 ].sum()
        assert np.all( yInBin0 == yInBin0Again )
        """
        idx = self.getBinIdxByValue( val )
        if idx is None:
            return None
        return self.getDataInBinByIdx( y, idx )
            
    def getBinnedMean( self, y, weightBy ):
        """
        Method to get the mean of the data in each bin.

        Arguments:
        ----------
        - `y`: Feature to mean over each bin.
        - `weightBy`: Feature to weight the data.

        Returns:
        --------
        - `np.ndarray`: Bin-by-bin mean of the feature to be meaned over 
        each bin.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randn( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 100 )
        # Get Binned Mean
        meanY = bdf.getBinnedMean( "y", "w" )
        assert len( meanY ) == bdf.nBins
        # Plot the Binned Mean
        import matplotlib
        matplotlib.use( "agg" )
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots()
        ax.plot( 
            bdf.binCenters, 
            meanY, 
            marker    = "o", 
            linestyle = "--",
            color     = "tab:blue"
        )
        ax.set_xlabel( "x" )
        ax.set_ylabel( "Mean of y (Weighted)" )
        fig.savefig( "binnedDataMeanExample.pdf" )
        ```
        """
        return np.array( 
                [   
                    weightedMean( 
                        self.getDataInBinByIdx( y, i ), 
                        self.df[ weightBy ][ self.binMasks[ i ] ] 
                    ) for i in range( self.nBins ) 
                ] 
            )
    
    def getBinnedStd( self, y, weightBy ):
        """
        Method to get the standard deviation of the data in each bin.

        Arguments:
        ----------
        - `y`: Feature to get the standard deviation of in each bin.
        - `weightBy`: Feature to weight the data.

        Returns:
        --------
        - `np.ndarray`: Bin-by-bin standard deviation of the feature to be
        standard deviationed over each bin.

        Example:
        --------
        ```python
        import numpy as np
        import pandas as pd
        import binMaster as bmaster
        # Generate Random Data
        df = pd.DataFrame(
            {
                "x": np.random.randn( 1000 ),
                "y": np.random.randn( 1000 ),
                "w": np.random.randnom( 1000 )
            }
        )
        # Create Binned Data
        bdf = bmaster.binnedData( df, binBy = "x", nBins = 20 )
        # Get Binned Standard Deviation
        meanY = bdf.getBinnedMean( "y", "w" )
        stdY  = bdf.getBinnedStd( "y", "w" )
        assert len( stdY ) == bdf.nBins
        # Plot the Binned Standard Deviation
        import matplotlib
        matplotlib.use( "agg" )
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots()
        ax.plot( 
            bdf.binCenters, 
            meanY, 
            marker    = "o", 
            linestyle = "--",
            color     = "tab:blue"
        )
        ax.errorbar(
            bdf.binCenters,
            meanY,
            yerr = stdY,
            fmt  = "none",
            color = "tab:blue"
        )
        for i in range( len( bdf.binCenters ) ):
            ax.fill_between(
                [ bdf.binEdges[ i ], bdf.binEdges[ i + 1 ] ],
                meanY[ i ] - stdY[ i ],
                meanY[ i ] + stdY[ i ],
                color = "tab:blue",
                alpha = 0.5
            )
        ax.set_xlabel( "x" )
        ax.set_ylabel( "Mean of y (Weighted)" )
        fig.savefig( "binnedDataMeanStdExample.pdf" )
        ```
        """
        return np.array( 
                [ 
                    weightedStd( 
                        self.getDataInBinByIdx( y, i ), 
                        self.df[ weightBy ][ self.binMasks[ i ] ] 
                    ) for i in range( self.nBins ) 
                ] 
            )
    def getFinestBinning( 
            self, 
            errThreshold  = 0.1,
            weightFeat    = None, 
            weightFunc    = None,
            stepSize      = 0.01,
            errType       = "relative",
            stepType      = "relative",
            **kwargs
        ):
        """
        The method to calculate the finest binning that does not exceed 
        a given error threshold in any of the bins. By default, the method
        uses relative statistical error, assuming Poisson variability,
        but the `weightFunc` argument can be used to calculate the error 
        in a different way.

        Arguments:
        ----------
        - `errThreshold`: The error threshold. The method will find the 
        finest binning that does not exceed this threshold in any of the
        bins.
        - `weightFeat`: Feature to be used to weight the data. If `None`,
        the data is not weighted.
        - `weightFunc`: Function to be used to calculate custom sum of
        weights and sum of squares of weights. If `None`, the method
        assumes Poisson variability and uses the sum of weights and the
        sum of squares of weights to calculate the relative statistical
        error.
        - `stepSize`: The smallest bin width that the method will consider.
        - `errType`: Whether the error threshold is relative (`"relative"`)
        or absolute (`"absolute"`).
        - `stepType`: Whether the step size is relative (`"relative"`) or
        absolute (`"absolute"`). If the step size is relative, the method
        will multiply the provided step size by the standard deviation of
        the feature to be binned.

        Returns:
        --------
        - `np.ndarray`: The finest binning that does not exceed the error
        threshold in any of the bins. The binning is an array of bin edges.
        """
        if weightFunc is None:
            def _weightFunc( df, binMask, weightFeat = None ):
                df[ "__weight__" ] = 1 if weightFeat is None else df[ weightFeat ]
                return df[ "__weight__" ][ binMask ].sum(), ( df[ "__weight__" ][ binMask ] ** 2 ).sum()
            weightFunc = _weightFunc

        xMax     = self.xMax
        xStd     = self.df[ self.x ].std()
        if stepType == "relative":
            stepSize = stepSize * xStd
        binning  = [ self.xMin - stepSize ]

        print( f"Finding Optimal Binning for Bin Feature: {self.x} at Error Threshold: {errThreshold}..." )
        for edgePointer in tqdm( np.arange( self.xMin + stepSize, xMax, stepSize ) ):
            binLeftEdge    = binning[ -1 ]
            binRightEdge   = edgePointer
            binMask        = ( self.df[ self.x ] >= binLeftEdge ) & ( self.df[ self.x ] < binRightEdge )
            sumW, sumW2    = weightFunc( self.df, binMask, weightFeat = weightFeat )
            errToCompare   = np.sqrt( sumW2 ) if errType == "absolute" else np.sqrt( sumW2 ) / sumW
            if errToCompare <= errThreshold:
                binning.append( binRightEdge )
        binning[ -1 ] = self.xMax + stepSize

        return np.array( binning )
    
    def getOptimalBinning( self, method = "finest", **kwargs ):
        """
        Method to calculate the optimal binning. By default, the method
        calls the `getFinestBinning` method, but other methods can be
        implemented in the future.

        Arguments:
        ----------
        - `method`: Method to be used to calculate the optimal binning.
        - `**kwargs`: Keyword arguments to be passed to the method.

        Returns:
        --------
        - `np.ndarray`: The optimal binning.
        """
        if method == "finest":
            return self.getFinestBinning( **kwargs )
        #if method == "coarsest":
        #    return self.getCoarsestBinning( **kwargs )
    
    def setOptimalBinning( self, method = "finest", **kwargs ):
        """
        Method to set the optimal binning. By default, the method calls
        the `getOptimalBinning` method and then the `reBin` method.

        Arguments:
        ----------
        - `method`: Method to be used to calculate the optimal binning.
        - `**kwargs`: Keyword arguments to be passed to the method.

        Returns:
        --------
        - `binnedData`: The instance of the `binnedData` class with the
        optimal binning set.
        """
        self.reBin( self.getOptimalBinning( method = method, **kwargs ) )
        return self 
    
class binnedData2D:
    def __init__( 
            self, 
            df = None, 
            binByX = None,
            binByY = None,
            binningX  = None, 
            binningY  = None, 
            nBinsX    = 50, 
            nBinsY    = 50,
            binByXMin = None,
            binByXMax = None,
            binByYMin = None,
            binByYMax = None
        ):
        self.df         = df
        self.x          = binByX
        self.y          = binByY
        self.binnedHists    = {}
        self.binnedHistsErr = {}
        self.binnedVals     = {}
        self.reBin( binningX, binningY, nBinsX, nBinsY, binByXMin, binByXMax, binByYMin, binByYMax )

    def reBin( 
            self, 
            binningX  = None,
            binningY  = None, 
            nBinsX    = 50,
            nBinsY    = 50,
            binByXMin = None,
            binByXMax = None,
            binByYMin = None,
            binByYMax = None
        ):

        if binningX is not None:
            if not( isinstance( binningX, np.ndarray ) ):
                binningX = np.array( binningX )
            nBinsX = len( binningX ) - 1
            xMin   = binningX[  0 ]
            xMax   = binningX[ -1 ]
        else:
            xMin     = self.df[ self.x ].min() if binByXMin is None else binByXMin
            xMax     = self.df[ self.x ].max() if binByXMax is None else binByXMax
            binningX = np.linspace( xMin, xMax, nBinsX + 1 )

        if binningY is not None:
            if not( isinstance( binningY, np.ndarray ) ):
                binningY = np.array( binningY )
            nBinsY = len( binningY ) - 1
            yMin   = binningY[ 0 ]
            yMax   = binningY[ -1 ]
        else:
            yMin     = self.df[ self.y ].min() if binByYMin is None else binByYMin
            yMax     = self.df[ self.y ].max() if binByYMax is None else binByYMax
            binningY = np.linspace( yMin, yMax, nBinsY + 1 )
        
        self.xMin        = xMin
        self.xMax        = xMax
        self.yMin        = yMin
        self.yMax        = yMax
        self.binningX    = binningX
        self.binningY    = binningY
        self.nBinsX      = nBinsX
        self.nBinsY      = nBinsY
        self.binCentersX = ( binningX[ :-1 ] + binningX[ 1: ] ) / 2
        self.binCentersY = ( binningY[ :-1 ] + binningY[ 1: ] ) / 2
        self.binWidthsX  = binningX[ 1: ] - binningX[ :-1 ] 
        self.binWidthsY  = binningY[ 1: ] - binningY[ :-1 ] 
        self.binEdgesX   = binningX
        self.binEdgesY   = binningY
        self.binnedHists = {}
        if self.df is not None and self.x is not None:
            self.binMasksX   = np.array( 
                [ ( self.df[ self.x ] >= binningX[ i ] ) & ( self.df[ self.x ] < binningX[ i + 1 ] ) for i in range( nBinsX ) ] 
            )
        else:
            self.binMasksX = None
        if self.df is not None and self.y is not None:
            self.binMasksY   = np.array( 
                [ ( self.df[ self.y ] >= binningY[ i ] ) & ( self.df[ self.y ] < binningY[ i + 1 ] ) for i in range( nBinsY ) ] 
            )
        else:
            self.binMasksY = None

    def getHist( self, weightBy = "__unweighted__" ):
        if weightBy not in self.binnedHists.keys():
            self.binnedHists[ weightBy ] = np.histogram2d( 
                self.df[ self.x ], 
                self.df[ self.y ], 
                bins = [ self.binningX, self.binningY ], 
                weights = self.df[ weightBy ] if weightBy != "__unweighted__" else None
            )[ 0 ]
        return self.binnedHists[ weightBy ]
    
    def getHistErr( self, weightBy = "__unweighted__" ):
        if weightBy not in self.binnedHistsErr.keys():
            self.binnedHistsErr[ weightBy ] = np.histogram2d(
            self.df[ self.x ], 
            self.df[ self.y ], 
            bins = [ self.binningX, self.binningY ], 
            weights = self.df[ weightBy ] ** 2 if weightBy != "__unweighted__" else None
        )[ 0 ] ** 0.5
        return self.binnedHistsErr[ weightBy ]
    
    def getDataInBinByIdx( self, z, idxX, idxY ):
        return self.df[ z ][ self.binMasksX[ idxX ] & self.binMasksY[ idxY ] ]

    def getBinIdxByValue( self, valX, valY ):
        if valX < self.xMin: 
            return None
        if valX > self.xMax:
            return None
        if valY < self.yMin: 
            return None
        if valY > self.yMax:
            return None
        idxX = np.digitize( valX, self.binEdgesX ) - 1
        idxY = np.digitize( valY, self.binEdgesY ) - 1
        return idxX, idxY
    
    def getDataInBinByValue( self, z, valX, valY ):
        idxX = self.getBinIdxByValue( valX )
        idxY = self.getBinIdxByValue( valY )
        if idxX is None or idxY is None:
            return None
        return self.getDataInBinByIdx( z, idxX, idxY )
    
    def getDensityAt( 
            self,
            xVal, 
            yVal, 
            weightBy       = "__unweighted__",
            normed         = False,
            dfNormTarget   = None 
        ):
        if dfNormTarget is None:
            dfNormTarget = self.df
        if weightBy in self.binnedVals.keys():
            vals = self.binnedVals[ weightBy ]
        elif weightBy in self.binnedHists.keys():
            vals = self.binnedHists[ weightBy ]
        elif weightBy == "__unweighted__":
            vals = self.getHist( weightBy = weightBy )
        else:
            print( f"Weight Feature: {weightBy} not found!" )
            return None
        normDen = vals.sum() if normed else 1
        if weightBy == "__unweighted__":
            normNum = len( dfNormTarget )
        else:
            normNum = dfNormTarget[ weightBy ].sum()
        if normed:
            return normNum * vals[ self.getBinIdxByValue( xVal, yVal ) ] / normDen
        return vals[ self.getBinIdxByValue( xVal, yVal ) ]

    def getBinnedMean( self, z, weightBy ):
        return np.array( 
                    [ 
                        weightedMean( 
                            self.getDataInBinByIdx( z, i, j ), 
                            self.df[ weightBy ][ self.binMasksX[ i ] & self.binMasksY[ j ] ]
                        ) for i in range( self.nBinsX ) for j in range( self.nBinsY ) 
                    ] 
                ).reshape( self.nBinsX, self.nBinsY )
    
    def getBinnedStd( self, z, weightBy ):
        return np.array( 
                    [ 
                        weightedStd( 
                            self.getDataInBinByIdx( z, i, j ), 
                            self.df[ weightBy ][ self.binMasksX[ i ] & self.binMasksY[ j ] ] 
                        ) for i in range( self.nBinsX ) for j in range( self.nBinsY ) 
                    ] 
                ).reshape( self.nBinsX, self.nBinsY )

### Pseudo-Smooth Binning ###
### --------------------- ###                            
# See: 
#    - https://en.wikipedia.org/wiki/Kernel_density_estimation
#    - https://towardsdatascience.com/histograms-vs-kdes-explained-ed62e7753f12

class sBinnedData:
    def __init__(
            self,
            df,
            binBy,
            binKernel      = "gaussian",
            binWidthMethod = "scott"
    ):
        self.df             = df
        self.x              = binBy
        self.binKernel      = binKernel
        self.binWidthMethod = binWidthMethod
        self.densities      = {}

    def getDensity( 
            self, 
            weightBy       = "__unweighted__", 
            binKernel      = None, 
            binWidthMethod = None 
        ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if weightBy not in self.densities.keys():
            if binKernel == "gaussian":
                self.densities[ weightBy ] = st.gaussian_kde( 
                    self.df[ self.x ], 
                    weights   = self.df[ weightBy ] if weightBy != "__unweighted__" else None,
                    bw_method = binWidthMethod
                )
            else:
                print( f"Kernel: {binKernel} not implemented yet!" )
                return None
        return self.densities[ weightBy ]
    
    def getDensityAt( 
            self, 
            xVal, 
            weightBy       = "__unweighted__", 
            normed         = False,
            binKernel      = None,
            binWidthMethod = None,
            dfNormTarget   = None
        ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if dfNormTarget is None:
            dfNormTarget   = self.df
        self.getDensity( weightBy, binKernel, binWidthMethod )
        if normed or weightBy == "__unweighted__":
            return self.densities[ weightBy ]( xVal )
        return self.densities[ weightBy ]( xVal ) * dfNormTarget[ weightBy ].sum()
    
    def compose( self, expr ):
        tokens = expr.split( " " )
        vars   = [ token.replace( "$", "" ) for token in tokens if token.startswith( "$" ) ]
        varIdx = { var: tokens.index( f"${var}" ) for var in vars }
        def func( self, x, vars, tokens, varIdx ):
            varVals = [ self.densities[ var ]( x ) for var in vars ]
            for i, var in enumerate( vars ):
                tokens[ varIdx[ var ] ] = "np.array( [ " + ", ".join( [ str( val ) for val in varVals[ i ] ] ) + " ] )"
            evalexpr = " ".join( tokens )
            return eval( evalexpr )
        return lambda x: functools.partial( func, vars = vars, tokens = tokens, varIdx = varIdx )( self, x )



class sBinnedData2D:
    def __init__(
            self,
            df,
            binByX,
            binByY,
            binKernel      = "gaussian",
            binWidthMethod = "scott"
    ):
        self.df             = df
        self.x              = binByX
        self.y              = binByY
        self.binKernel      = binKernel
        self.binWidthMethod = binWidthMethod
        self.densities      = {}

    def getDensity( 
            self, 
            weightBy       = "__unweighted__",
            binKernel      = None, 
            binWidthMethod = None 
        ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if weightBy not in self.densities.keys():
            if binKernel == "gaussian":
                self.densities[ weightBy ] = st.gaussian_kde( 
                    np.array( [ self.df[ self.x ], self.df[ self.y ] ] ), 
                    weights   = self.df[ weightBy ] if weightBy != "__unweighted__" else None,
                    bw_method = binWidthMethod
                )
            else:
                print( f"Kernel: {binKernel} not implemented yet!" )
                return None
        return self.densities[ weightBy ]
    
    def getDensityAt( 
            self, 
            locs, 
            weightBy       = "__unweighted__", 
            normed         = False,
            binKernel      = None,
            binWidthMethod = None,
            dfNormTarget   = None
        ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if dfNormTarget is None:
            dfNormTarget   = self.df
        self.getDensity( weightBy, binKernel, binWidthMethod )
        if normed:
            return self.densities[ weightBy ]( locs )
        return self.densities[ weightBy ]( locs ) * dfNormTarget[ weightBy ].sum()

    def compose( self, expr ):
        tokens = expr.split( " " )
        vars   = [ token.replace( "$", "" ) for token in tokens if token.startswith( "$" ) ]
        varIdx = { var: tokens.index( f"${var}" ) for var in vars }
        def func( self, x, vars, tokens, varIdx ):
            varVals = [ self.densities[ var ]( x ) for var in vars ]
            for i, var in enumerate( vars ):
                tokens[ varIdx[ var ] ] = "np.array( [ " + ", ".join( [ str( val ) for val in varVals[ i ] ] ) + " ] )"
            evalexpr = " ".join( tokens )
            return eval( evalexpr )
        return lambda x: functools.partial( func, vars = vars, tokens = tokens, varIdx = varIdx )( self, x )

class sBinnedDataND:
    def __init__(
            self,
            df,
            binByArr,
            binKernel      = "gaussian",
            binWidthMethod = "scott"
    ):
        self.df             = df
        self.xArr           = binByArr
        self.binKernel      = binKernel
        self.binWidthMethod = binWidthMethod
        self.densities      = {}
    def getDensity(
            self,
            weightBy       = "__unweighted__",
            binKernel      = None,
            binWidthMethod = None
    ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if weightBy not in self.densities.keys():
            if binKernel == "gaussian":
                self.densities[ weightBy ] = st.gaussian_kde(
                    np.array( [ self.df[ x ] for x in self.xArr ] ),
                    weights   = self.df[ weightBy ] if weightBy != "__unweighted__" else None,
                    bw_method = binWidthMethod
                )
            else:
                print( f"Kernel: {binKernel} not implemented yet!" )
                return None
        return self.densities[ weightBy ]
    def getDensityAt(
            self,
            locs,
            weightBy       = "__unweighted__",
            normed         = False,
            binKernel      = None,
            binWidthMethod = None,
            dfNormTarget   = None
    ):
        if binKernel is None:
            binKernel      = self.binKernel
        if binWidthMethod is None:
            binWidthMethod = self.binWidthMethod
        if dfNormTarget is None:
            dfNormTarget   = self.df
        self.getDensity( weightBy, binKernel, binWidthMethod )
        if normed:
            return self.densities[ weightBy ]( locs )
        return self.densities[ weightBy ]( locs ) * dfNormTarget[ weightBy ].sum()
    def compose( self, expr ):
        tokens = expr.split( " " )
        vars   = [ token.replace( "$", "" ) for token in tokens if token.startswith( "$" ) ]
        varIdx = { var: tokens.index( f"${var}" ) for var in vars }
        def func( self, x, vars, tokens, varIdx ):
            varVals = [ self.densities[ var ]( x ) for var in vars ]
            for i, var in enumerate( vars ):
                tokens[ varIdx[ var ] ] = "np.array( [ " + ", ".join( [ str( val ) for val in varVals[ i ] ] ) + " ] )"
            evalexpr = " ".join( tokens )
            return eval( evalexpr )
        return lambda x: functools.partial( func, vars = vars, tokens = tokens, varIdx = varIdx )( self, x )
    