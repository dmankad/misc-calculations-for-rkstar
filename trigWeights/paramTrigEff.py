#!/usr/bin/env python3

### Imports ###
### ------- ###

### Imports -> I/O ###
### -------------- ###
import os
import pathlib
import sys
import pickle

### Imports -> Plotting ###
### ------------------- ###
import matplotlib
matplotlib.use( "agg" )
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

### Imports -> Pythonic Utilities ###
### ----------------------------- ###
import functools
import copy

### Imports -> Array Manipulation ###
### ----------------------------- ###
import numpy as np
import pandas as pd

### Imports -> Internal ###
### ------------------- ###
import binMaster as bmaster
import paramWeightsCalculator as pWCalc
from ml_share import utils
from ml_share import plotting
#plotting.setATLAS( False )

### Declaring the Config & Paths ###
### ---------------------------- ###

channel             = "electron"
input_dir           = "/storage/agrp/dvijm/ntuples/feather/electron/refitPRWUnseededPassThrough"
config_file         = "../ml_main/configs/gnn1_electron_multiclass_goblin_v2.yaml"
output_dir          = "./"

config_file_default = "../ml_main/configs/default_" + channel + ".yaml"
config              = utils.config_load( config_file )
output_dir          = os.path.join( output_dir, "plots" )
input_data_nr_bd    = utils.get_list_make_list( "ntuple-30059[0]_part_0[1-2]_sample[1-2].ftr", input_dir )
input_data_nr_bdbar = utils.get_list_make_list( "ntuple-30059[1]_part_0[1-2]_sample[1-2].ftr", input_dir )
input_data_nr       = input_data_nr_bd + input_data_nr_bdbar 
input_data_r_bd     = utils.get_list_make_list( "ntuple-30059[2]_part_0[1-2].ftr", input_dir )
input_data_r_bdbar  = utils.get_list_make_list( "ntuple-30059[3]_part_0[1-2].ftr", input_dir )
input_data_r        = input_data_r_bd + input_data_r_bdbar 
pathlib.Path( output_dir ).mkdir( parents = True, exist_ok = True )

fDefaultDen = "info_pure_pileup_weight"
fDefaultNum = "info_pileup_prescale_weight_SpecialHLTOR_SpecialL1OR"

input_data_dict = {
        "signal_nr" : input_data_nr,
        "signal_nr_bd" : input_data_nr_bd,
        "signal_nr_bdbar" : input_data_nr_bdbar,
        "signal_r" : input_data_r,
        "signal_r_bd" : input_data_r_bd,
        "signal_r_bdbar" : input_data_r_bdbar
}
titleDict = {
        "signal_r"    : "Resonant Signal MC at Baseline (TM)",
        "signal_r_bd" : "Resonant Signal ($B_d^0$) MC at Baseline (TM)",
        "signal_r_bdbar" : "Resonant Signal ($\\bar{B}_d^0$) MC at Baseline (TM)",
        "signal_nr"    : "Non-Resonant Signal MC at Baseline (TM)",
        "signal_nr_bd" : "Non-Resonant Signal ($B_d^0$) MC at Baseline (TM)",
        "signal_nr_bdbar" : "Non-Resonant Signal ($\\bar{B}_d^0$) MC at Baseline (TM)",
}
### Selections ###
### ---------- ###

selections = {}

### Selections -> Baseline USR ###
### -------------------------- ###
selections[ "baseline_q2low"  ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2low  )" 
selections[ "baseline_q2high" ] = "( $selection_main & $selection_mc & $selection_usr & $selection_q2high )" 
selections[ "baseline_q2full" ] = "( $selection_main & $selection_mc & $selection_usr )"

for selection_name in selections.keys():
    selections[ selection_name ] = utils.get_selection( selections[ selection_name ], config[ "common" ] )

### Load Data ###
### --------- ###
    
features                     = config[ "common"   ].get( "features", [] )
additional_features_to_plot  = config[ "training" ].get( "additional_features_to_plot", [] )
additional_features_to_plot += [
    "dR_positron_electron",
    "electron0_pT",
    "electron1_pT",
    "electron0_eta",
    "electron1_eta"
]
additional_features_to_load  = config[ "common" ].get( "info_features"   , [] ) 
additional_features_to_load += config[ "common" ].get( "weight_features" , [] )
additional_features_to_load += config[ "common" ].get( "trigger_features", [] )

if features == []:
    graph_dict  = utils.json_load( os.path.join( "../ml_main/", config[ "common" ][ "graph_dict" ] ) )
    parameters  = config[ "common" ] 
    nested_list = []
    if isinstance( parameters[ "graph_version" ], dict ):
        graph_topo_names = [ item["graph_topo"] for item in parameters[ "graph_version" ].values() ]
    else:
        graph_topo_names = [ parameters["graph_version"] ]

    for item in graph_topo_names:
        if item in graph_dict:
            nested_list += list( graph_dict.get( item ).get( "nodes" ).values() )
            nested_list += list( graph_dict.get( item ).get( "edges" ).values() )
            nested_list += [ graph_dict.get(item).get("goblins", []) ]
        else:
            print(utils.red(sys._getframe().f_code.co_name + ": " + item + " does not exist in " + parameters[ "graph_dict" ] + "!"))
            exit(1)

    parameters[ "features" ] = list( set( [ item for sublist in nested_list for item in sublist ] ) )
    parameters[ "features" ] = [ item for item in parameters[ "features" ] if item != "empty_feature" ]
    parameters[ "features" ].sort()
    features = parameters["features"]

items_to_load   = features + additional_features_to_plot + additional_features_to_load

def df_load( input_data, q2bin = "low" ):
    df = utils.df_load( input_data, selection = selections[ "baseline_q2" + q2bin ], items_to_load = items_to_load )
    return df

### Custom Masks for Parametrization ###
### -------------------------------- ###

### Additional Selections ###
### --------------------- ###

selections[ "eta_bb"          ] = "( $selection_eta_bb )"
selections[ "eta_be"          ] = "( $selection_eta_be )"
selections[ "eta_eb"          ] = "( $selection_eta_eb )"
selections[ "eta_ee"          ] = "( $selection_eta_ee )"
selections[ "dr_0p10_0p20"    ] = "( $selection_dr_0p10_0p20 )"
selections[ "dr_0p20_0p30"    ] = "( $selection_dr_0p20_0p30 )"
selections[ "dr_0p10_0p30"    ] = "( $selection_dr_0p10_0p30 )"
selections[ "dr_g0p30"        ] = "( $selection_dr_g0p30 )"
selections[ "n_el_2"          ] = "( $selection_n_el_2 )"
selections[ "n_el_3"          ] = "( $selection_n_el_3 )"
selections[ "n_el_4"          ] = "( $selection_n_el_4 )"
selections[ "n_el_g5"         ] = "( $selection_n_el_g5 )"
selections[ "n_el_5"          ] = "( $selection_n_el_5 )"
selections[ "n_el_g6"         ] = "( $selection_n_el_g6 )"
selections[ "n_el_6"          ] = "( $selection_n_el_6 )"
selections[ "n_el_g7"         ] = "( $selection_n_el_g7 )"

for selection_name in selections.keys():
    selections[ selection_name ] = utils.get_selection( selections[ selection_name ], config[ "common" ] )

### Dictionaries of Selection Keys, Selection TeX, and Selection Expressions ###
### ------------------------------------------------------------------------ ###

dfSelEta = {
    "v1" : [
    ( "BB", "BB", selections[ "eta_bb" ] ),
    ( "BE", "BE", selections[ "eta_be" ] ),
    ( "EB", "EB", selections[ "eta_eb" ] ),
    ( "EE", "EE", selections[ "eta_ee" ] )
  ]
}
dfSelDRBB = {
    "v1" : [
    ( "dR_0p10_0p20", "$0.10 \\leq \\Delta R < 0.20$", selections[ "dr_0p10_0p20" ] ),
    ( "dR_0p20_0p30", "$0.20 \\leq \\Delta R < 0.30$", selections[ "dr_0p20_0p30" ] ),
    ( "dR_g0p30",     "$\\Delta R \\geq 0.30$",        selections[ "dr_g0p30"     ] )
  ]
}
dfSelDRBE = {
    "v1" : [
    ( "dR_0p10_0p30", "$0.10 \\leq \\Delta R < 0.30$", selections[ "dr_0p10_0p30" ] ),
    ( "dR_g0p30",     "$\\Delta R \\geq 0.30$",        selections[ "dr_g0p30"     ] )
   ]
}
dfSelDREB = {
    "v1" : [
    ( "dR_0p10_0p30", "$0.10 \\leq \\Delta R < 0.30$", selections[ "dr_0p10_0p30" ] ),
    ( "dR_g0p30",     "$\\Delta R \\geq 0.30$",        selections[ "dr_g0p30"     ] )
   ]
}
dfSelDREE = {
    "v1" : [
    ( "dR_0p10_0p20", "$0.10 \\leq \\Delta R < 0.20$", selections[ "dr_0p10_0p20" ] ),
    ( "dR_0p20_0p30", "$0.20 \\leq \\Delta R < 0.30$", selections[ "dr_0p20_0p30" ] ),
    ( "dR_g0p30",     "$\\Delta R \\geq 0.30$",        selections[ "dr_g0p30"     ] )
 ]
}
dfSelNEl = {
    "v3" : [
    ( "n_el_2" , "$N_{\\rm electron} = 2$" , selections[ "n_el_2"  ] ),
    ( "n_el_3" , "$N_{\\rm electron} = 3$" , selections[ "n_el_3"  ] ),
    ( "n_el_4" , "$N_{\\rm electron} = 4$" , selections[ "n_el_4"  ] ),
    ( "n_el_5" , "$N_{\\rm electron} = 5$" , selections[ "n_el_5"  ] ),
    ( "n_el_6" , "$N_{\\rm electron} = 6$" , selections[ "n_el_6"  ] ),
    ( "n_el_g7", "$N_{\\rm electron} \\geq 7$" , selections[ "n_el_g7" ] )
   ],
    "v2" : [
    ( "n_el_2" , "$N_{\\rm electron} = 2$"     , selections[ "n_el_2"  ] ),
    ( "n_el_3" , "$N_{\\rm electron} = 3$"     , selections[ "n_el_3"  ] ),
    ( "n_el_4" , "$N_{\\rm electron} = 4$"     , selections[ "n_el_4"  ] ),
    ( "n_el_5" , "$N_{\\rm electron} = 5$"     , selections[ "n_el_5"  ] ),
    ( "n_el_g6", "$N_{\\rm electron} \\geq 6$" , selections[ "n_el_g6" ] )
   ],
    "v1" : [
    ( "n_el_2" , "$N_{\\rm electron} = 2$"     , selections[ "n_el_2"  ] ),
    ( "n_el_3" , "$N_{\\rm electron} = 3$"     , selections[ "n_el_3"  ] ),
    ( "n_el_4" , "$N_{\\rm electron} = 4$"     , selections[ "n_el_4"  ] ),
    ( "n_el_g5", "$N_{\\rm electron} \\geq 5$" , selections[ "n_el_g5" ] )
   ]
}

def getCustomMaskedDicts( df, maskScheme = "eta.v1.dr.v1.combo.v1" ):
    fVersions = { "eta": None, "dr": None, "nel": None, "combo": None, "inclusive": None } 
    for feat in fVersions.keys():
        if feat + "." in maskScheme: 
            fVersions[ feat ] = maskScheme.split( f"{feat}." )[ 1 ].split( "." )[ 0 ]
    rMaskScheme = ""
    for feat in fVersions.keys():
        if fVersions[ feat ] is not None:
            if feat != "combo":
                rMaskScheme += f"{feat}.vX."
            else:
                       rMaskScheme += f"{feat}.{fVersions[feat]}"
    if rMaskScheme[ -1 ] == ".": rMaskScheme = rMaskScheme[ :-1 ]
    print( "Working with the rMaskScheme: ", rMaskScheme )
    print( "fVersions", fVersions )
    if rMaskScheme == "inclusive.v1":
        dfDict = { "inclusive": df }
        maskDict = { "inclusive": np.ones( len( df ) ) }
        latexDict = { "inclusive": "Inclusive" }
        return dfDict, maskDict, latexDict
    if rMaskScheme == "eta.vX.dr.vX.combo.v1":
        dfDict    = {}
        maskDict  = {}
        latexDict = {}
        etaVersion = fVersions[ "eta" ] 
        drVersion  = fVersions[ "dr"  ]
        for etaKey, etaLatex, etaSel in dfSelEta[ etaVersion ]:
            if etaKey == "BB":
                masksDR = dfSelDRBB[ drVersion ]
            if etaKey == "BE":
                masksDR = dfSelDRBE[ drVersion ]
            if etaKey == "EB":
                masksDR = dfSelDREB[ drVersion ]
            if etaKey == "EE":
                masksDR = dfSelDREE[ drVersion ]
            etaMask = utils.getMask( df, etaSel )
            for drKey, drLatex, drSel in masksDR:
                drMask                            = utils.getMask( df, drSel )
                dfDict   [ etaKey + "_" + drKey ] = df[ etaMask & drMask ]
                maskDict [ etaKey + "_" + drKey ] = etaMask & drMask
                latexDict[ etaKey + "_" + drKey ] = etaLatex + ", " + drLatex
        return dfDict, maskDict, latexDict
    if rMaskScheme == "eta.vX.dr.vX.nel.vX.combo.v1":
        dfDict    = {}
        maskDict  = {}
        latexDict = {}
        etaVersion  = fVersions[ "eta" ]
        drVersion   = fVersions[ "dr"  ]
        nelVersion  = fVersions[ "nel" ]
        for etaKey, etaLatex, etaSel in dfSelEta[ etaVersion ]:
            if etaKey == "BB":
                masksDR = dfSelDRBB[ drVersion ]
            if etaKey == "BE":
                masksDR = dfSelDRBE[ drVersion ]
            if etaKey == "EB":
                masksDR = dfSelDREB[ drVersion ]
            if etaKey == "EE":
                masksDR = dfSelDREE[ drVersion ]
            etaMask = utils.getMask( df, etaSel )
            for drKey, drLatex, drSel in masksDR:
                drMask                            = utils.getMask( df, drSel )
                for nelKey, nelLatex, nelSel in dfSelNEl[ nelVersion ]:
                    nelMask = utils.getMask( df, nelSel )
                    dfDict   [ etaKey + "_" + drKey + "_" + nelKey ] = df[ etaMask & drMask & nelMask ]
                    maskDict [ etaKey + "_" + drKey + "_" + nelKey ] = etaMask & drMask & nelMask
                    latexDict[ etaKey + "_" + drKey + "_" + nelKey ] = etaLatex + ", " + drLatex + ", " + nelLatex
        return dfDict, maskDict, latexDict
    if rMaskScheme == "nel.vX":
        dfDict    = {}
        maskDict  = {}
        latexDict = {}
        nelVersion = fVersions[ "nel" ]
        for nelKey, nelLatex, nelSel in dfSelNEl[ nelVersion ]:
            nelMask = utils.getMask( df, nelSel )
            dfDict   [ nelKey ] = df[ nelMask ]
            maskDict [ nelKey ] = nelMask
            latexDict[ nelKey ] = nelLatex
        return dfDict, maskDict, latexDict
    if rMaskScheme == "eta.vX":
        dfDict    = {}
        maskDict  = {}
        latexDict = {}
        etaVersion = fVersions[ "eta" ]
        for etaKey, etaLatex, etaSel in dfSelEta[ etaVersion ]:
            etaMask = utils.getMask( df, etaSel )
            dfDict   [ etaKey ] = df[ etaMask ]
            maskDict [ etaKey ] = etaMask
            latexDict[ etaKey ] = etaLatex
        return dfDict, maskDict, latexDict
    if rMaskScheme == "eta.vX.nel.vX.combo.v1":
        dfDict    = {}
        maskDict  = {}
        latexDict = {}
        etaVersion = fVersions[ "eta" ] 
        nelVersion = fVersions[ "nel" ]
        for etaKey, etaLatex, etaSel in dfSelEta[ etaVersion ]:
            etaMask = utils.getMask( df, etaSel )
            for nelKey, nelLatex, nelSel in dfSelNEl[ nelVersion ]:
                nelMask = utils.getMask( df, nelSel )
                dfDict   [ etaKey + "_" + nelKey ] = df[ etaMask & nelMask ]
                maskDict [ etaKey + "_" + nelKey ] = etaMask & nelMask
                latexDict[ etaKey + "_" + nelKey ] = etaLatex + ", " + nelLatex
        print( "returninng stuff")
        return dfDict, maskDict, latexDict

### Analysis Specific Functions ###
### --------------------------- ###

def mapTrigEff( 
        dfDict,
        maskDict,
        latexDict,
        parameterX          = "electron0_pT",
        parameterY          = "electron1_pT",
        fTrigCombo          = "SpecialHLTOR_SpecialL1OR",
        fPileUpWeight       = "info_pure_pileup_weight",
        parameterXLabel     = "$p_{\\rm T}^{\\rm lead}$ [ MeV ]",
        parameterYLabel     = "$p_{\\rm T}^{\\rm sublead}$ [ MeV ]",
        paramXBinning       = np.linspace( 5000, 80000, 20 ),
        paramYBinning       = np.linspace( 5000, 25000, 20 ),
        title               = "",
        effName             = "Trigger Weight",
        useKDE              = True,
        oBKwargs1D          = { "errThreshold": 0.1, "method": "finest" },
        oBKwargs2D          = { "errThreshold": 0.03, "method": "finest" },
    ):
            
    for key, df in dfDict.items():
        
        keyLatex = latexDict [ key ]
        keyMask  = maskDict  [ key ]

        ### 1D Parameter Binning ###
        ### -------------------- ###
        oBKwargs = {
            "weightFunc" : functools.partial(
                pWCalc.weightFuncForRatios,
                fWeightNum = "info_pileup_prescale_weight_" + fTrigCombo,
                fWeightDen = fPileUpWeight
            )
        }
        oBKwargs.update( oBKwargs1D )
        pWCalc.wrapMapEff(
            df,
            parameters        = [ parameterX, parameterY ],
            fWeightNum        = "info_pileup_prescale_weight_" + fTrigCombo,
            fWeightDen        = fPileUpWeight,
            parameterLabelAr  = [ parameterXLabel, parameterYLabel ],
            parameterBinning  = [ paramXBinning, paramYBinning ],
            title             = title + "\n" + keyLatex,
            effName           = effName,
            oBKwargs          = oBKwargs,
            useKDE            = useKDE,
            useOptimalBinning = True
        )

        ### 2D Parameter Binning ###
        ### -------------------- ###
        if not useKDE:
            bdfX     = bmaster.binnedData( df, parameterX, binning = None, nBins = 50 )
            bdfY     = bmaster.binnedData( df, parameterY, binning = None, nBins = 50 )
            oBKwargs = { 
                "weightFunc" : functools.partial( 
                    pWCalc.weightFuncForRatios, 
                    fWeightNum = "info_pileup_prescale_weight_" + fTrigCombo, 
                    fWeightDen = fPileUpWeight 
                ) 
            }
            oBKwargs.update( oBKwargs2D )
            binningX = bdfX.getOptimalBinning( **oBKwargs )
            binningY = bdfY.getOptimalBinning( **oBKwargs )

        pWCalc.wrapMapEff2D( 
            df, 
            parameterX, 
            parameterY, 
            fWeightNum      = "info_pileup_prescale_weight_" + fTrigCombo, 
            fWeightDen      = fPileUpWeight, 
            parameterXLabel = parameterXLabel, 
            parameterYLabel = parameterYLabel, 
            binningX        = binningX, 
            binningY        = binningY, 
            title           = title + "\n" + keyLatex, 
            effName         = effName, 
            oBKwargs        = oBKwargs if useKDE else None,
            useKDE          = useKDE
        )

class weighter:
    def __init__( self, df, xArr, weightBy = None ):
        self.df       = df
        self.bdf      = bmaster.sBinnedDataND( df, xArr )
        self.weightBy = weightBy
    def get( self, locs, normed = False ):
        return self.bdf.getDensityAt( np.array( locs ), weightBy = self.weightBy, normed = normed )
    def decorate( self, df, decorationFeature = None, normed = False ):
        arWeight = self.get( [ df[ self.bdf.df[ x ] ] for x in self.bdf.xArr ], normed = normed )
        if decorationFeature is None:
            return arWeight
        df[ decorationFeature ] = arWeight
        return df 

def getWeightsCustom( 
        dfSource,
        dfTarget,
        maskScheme,
        parameterArr  = [ "electron0_pT", "electron1_pT" ],
        fTrigCombo    = "SpecialHLTOR_SpecialL1OR",
        fPileUpWeight = "info_pure_pileup_weight",
        fWeightName   = "trigger_weight"
    ):
    weighters_num = {}
    weighters_den = {}
    fWeightNum    = "info_pileup_prescale_weight_" + fTrigCombo
    fWeightDen    = fPileUpWeight
    dfDictSource, maskDictSource, latexDict = getCustomMaskedDicts( dfSource, maskScheme = maskScheme )
    dfDictTarget, maskDictTarget, latexDict = getCustomMaskedDicts( dfTarget, maskScheme = maskScheme )
    print( "Getting Weighters: " )
    for key in latexDict.keys():
        df    = dfDictSource[ key ] 
        mask  = maskDictSource[ key ]
        latex = latexDict[ key ]
        print( f"Working on {latex}..." )
        weighters_num[ key ] = weighter( df, parameterArr, fWeightNum )
        weighters_den[ key ] = weighter( df, parameterArr, fWeightDen )
    dfTarget[ fWeightName ] = 0
    print( "Decorating Dataframe: " )
    for key in latexDict.keys():
        print( f"\t{key}" )
        weighter_num = weighters_num[ key ]
        weighter_den = weighters_den[ key ]
        mask         = maskDictTarget[ key ]
        weight_num   = dfTarget[ mask ][ fWeightNum ].sum() * weighter_num.get( [ dfTarget[ x ] for x in parameterArr ], normed = True )
        weight_den   = dfTarget[ mask ][ fWeightDen ].sum() * weighter_den.get( [ dfTarget[ x ] for x in parameterArr ], normed = True )
        dfTarget[ fWeightName ] = mask * ( weight_num / weight_den ) + dfTarget[ fWeightName ]
    dfTarget = dfTarget[ ~np.isnan( dfTarget[ fWeightName ] ) ]
    return dfTarget

def compareWeightedFeatures( 
        df, 
        title,
        fWeightName   = "trigger_weight",
        fPileUpWeight = "info_pure_pileup_weight",
        fTrigCombo    = "SpecialHLTOR_SpecialL1OR",
        features_typeA = None,
        features_typeB = None,
        fileSuffix     = ""
    ):
    if features_typeA is None:
        features_typeA = features
    if features_typeB is None:
        features_typeB = additional_features_to_plot
    dfOrig       = copy.deepcopy( df )
    dfWeighted   = copy.deepcopy( df )
    dfUnWeighted = copy.deepcopy( df )
    dfOrig       [ "info_weight" ] = dfOrig[ "info_pileup_prescale_weight_" + fTrigCombo ]
    dfWeighted   [ "info_weight" ] = dfWeighted[ fPileUpWeight ] * dfWeighted[ fWeightName ]
    dfUnWeighted [ "info_weight" ] = 1
    plotting.plot_baseline_features(
        input_data      = [ dfUnWeighted, dfOrig, dfWeighted ],
        input_labels    = [ "Trigger Passthrough, No Weights", "Require Trigger, PRW Weights", "Trigger Passthrough, (Trigger + PRW) Weights" ],
        input_colors    = [ "black", "crimson", "olive" ],
        input_alphas    = [ 1, 1, 0.6 ],
        input_fills     = [ False, False, True ],
        normalized      = True,
        features                    = features_typeA,
        additional_features         = features_typeB,
        title                       = title,
        output_filename             = f"plots/trigger_reweighting_{fileSuffix}.pdf" if fileSuffix is not None else "plots/trigger_reweighting.pdf"
    )

def main( 
         paramTargets = [ "signal_nr_bd", "signal_nr_bdbar" ],
         paramSources = None,
         q2BinTarget = "low",
         q2BinSource = None,
         maskScheme  = "eta.v1.nel.v1.combo.v1",
         dsTagTarget = "signal_nr",
         dsTagSource = None,
         version     = "v4",
         parameterArrExtra = [ "dR_positron_electron", "electron0_eta", "electron1_eta" ],
         parameterArrBasic = [ "electron0_pT", "electron1_pT" ]
    ):
    if paramSources is None:
        paramSources = paramTargets
    if q2BinSource is None:
        q2BinSource = q2BinTarget
    if dsTagSource is None:
        dsTagSource = dsTagTarget
    input_data_sources = [ input_data_dict[ x ] for x in paramSources ]
    input_data_targets = [ input_data_dict[ x ] for x in paramTargets ]
    dfSources    = [ df_load( input_data_sources[ x ], q2bin = q2BinSource ) for x in range( len( paramSources ) ) ]
    dfTargets    = [ df_load( input_data_targets[ x ], q2bin = q2BinTarget ) for x in range( len( paramTargets ) ) ]
    dfTargets    = [ getWeightsCustom( 
                         dfSource = dfSources[ x ],
                         dfTarget = dfTargets[ x ],
                         maskScheme = maskScheme,
                         parameterArr = parameterArrBasic + parameterArrExtra 
                    ) for x in range( len( paramSources ) ) ]
    dfTarget = pd.concat( dfTargets )
    title = titleDict[ dsTagTarget ] + f" {q2BinTarget}$-q^2$ Bin"
    compareWeightedFeatures(
        df            = dfTarget,
        title         = titleDict[ dsTagTarget ],
        fTrigCombo    = "SpecialHLTOR_SpecialL1OR",
        fWeightName   = "trigger_weight",
        fPileUpWeight = "info_pure_pileup_weight",
        fileSuffix    = f"from.{dsTagSource}.to.{dsTagTarget}.db.{maskScheme}.{version}"
    )

if __name__ == "__main__":
    # Does Making DeltaR Continuous Help (Check by Making It Discrete v/s Continuous In Simple Setting Otherwise)
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1.dr.v1.combo.v1", dsTagTarget = "signal_nr", version = "v1.direct", parameterArrExtra = [] )
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1", dsTagTarget = "signal_nr", version = "v2.direct", parameterArrExtra = [ "dR_positron_electron" ]  )

    # Compare: Eta Bin + No Eta Parameter (Previous), Eta Bin + Eta Parameter, No Eta Bin + Eta Parameter
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1", dsTagTarget = "signal_nr", version = "v3.direct", parameterArrExtra = [ "dR_positron_electron", "electron0_eta", "electron1_eta" ] )
    ##main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "inclusive.v1", dsTagTarget = "signal_nr", version = "v3.direct", parameterArrExtra = [ "dR_positron_electron", "electron0_eta", "electron1_eta" ] )

    # Does Adding More Bins of NEl Help (Check By Varying)
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v3.combo.v1", dsTagTarget = "signal_nr", version = "v3.direct" )
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v2.combo.v1", dsTagTarget = "signal_nr", version = "v3.direct" )
    #main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v1.combo.v1", dsTagTarget = "signal_nr", version = "v3.direct" )

    main( paramTargets = [ "signal_nr" ], q2BinTarget = "low", maskScheme = "eta.v1.dr.v1.nel.v3.combo.v1", dsTagTarget = "signal_nr", version = "v3.direct" )
    

    #main( paramTargets = [ "signal_nr_bd", "signal_nr_bdbar" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v3.combo.v1", dsTagTarget = "signal_nr", version = "v4.merged" )
    #main( paramTargets = [ "signal_nr_bd", "signal_nr_bdbar" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v2.combo.v1", dsTagTarget = "signal_nr", version = "v4.merged" )
    #main( paramTargets = [ "signal_nr_bd", "signal_nr_bdbar" ], q2BinTarget = "low", maskScheme = "eta.v1.nel.v1.combo.v1", dsTagTarget = "signal_nr", version = "v4.merged" )
    #main( paramTargets = [ "signal_nr_bd", "signal_nr_bdbar" ], q2BinTarget = "low", maskScheme = "nel.v3", dsTagTarget = "signal_nr", version = "v4.merged" )
