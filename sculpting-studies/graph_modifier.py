import json
import copy


def load_json( filename ):
    with open( filename, "r" ) as f:
        return json.load(f)

def dump_json( data, filename ):
    with open( filename, "w" ) as f:
        json.dump( data, f, indent = 4 )

def break_node(
        graph_name,
        node_names,
        new_graph_name = None,
        keep_edges = False,
        keep_edge_features = True,
        scatter_edge_features = True,
        save = True,
        inplace = False,
        input_file = "graph_dict.json",
        export_file = "graph_dict_broken_node.json",
        ):
    input_graphs = load_json( input_file )
    graph = input_graphs[ graph_name ]

    relevant_node_names = []
    for node_name in copy.deepcopy(node_names):
        if( node_name in graph["nodes"].keys() ):
            relevant_node_names.append( node_name )

    node_names = relevant_node_names

    new_graph = copy.deepcopy( graph )
    node1 = []
    node2 = []
    for node_name in node_names:
        for src_dst_link in graph["src_dst_nodes"]:
            if node_name in src_dst_link:
                #new_graph[ "src_dst_nodes" ].remove( src_dst_link )
                #del new_graph[ "edges" ][ src_dst_link[0].replace("-node", "") + "-" + src_dst_link[ 1 ].replace("-node","") + "-edge"]
                which_place = 0 if node_name == src_dst_link[0] else 1
                if keep_edges:
                    for node_feature in graph["nodes"][ node_name ]:
                        replacement = node_name + "-" + node_feature + "-node"
                        keeping     = src_dst_link[ 0 ] if which_place == 1 else src_dst_link[ 1 ]
                        replaced_link = [ replacement, keeping ] if which_place == 0 else [ keeping, replacement ]
                        if src_dst_link in new_graph[ "src_dst_nodes"]: new_graph[ "src_dst_nodes"].remove( src_dst_link )
                        new_graph[ "src_dst_nodes" ].append( replaced_link )
                        if src_dst_link[0].replace("-node", "") + "-" + src_dst_link[ 1 ].replace("-node","") + "-edge" in new_graph["edges"].keys():
                            del new_graph["edges"][ src_dst_link[0].replace("-node", "") + "-" + src_dst_link[ 1 ].replace("-node","") + "-edge" ]
                        new_graph[ "edges" ][ replaced_link[0].replace("-node", "") + "-" + replaced_link[ 1 ].replace("-node","") + "-edge" ] = []
                        if keep_edge_features:
                            new_graph[ "edges" ][ replaced_link[0].replace("-node", "") + "-" + replaced_link[ 1 ].replace("-node","") + "-edge" ] = graph[ "edges" ][ src_dst_link[0].replace("-node", "") + "-" + src_dst_link[ 1 ].replace("-node","") + "-edge" ]
                else:
                    node1.append( src_dst_link[0] )
                    node2.append( src_dst_link[1] )
    if not keep_edges:
        new_graph = remove_edge( graph_name, node1 = node1, node2 = node2, keep_features = keep_edge_features, save = False, scatter_feat = scatter_edge_features, input_file = input_file )
    for node_name in node_names:
        del new_graph[ "nodes" ][ node_name ]
        node_features = graph["nodes"][ node_name ]
        for node_feature in node_features:
            new_graph[ "nodes" ][ node_name + "-" + node_feature + "-node" ] = [ node_feature ]

    if save:
        if( new_graph_name is None ):
            new_graph_name = graph_name
            for node_name in node_names:
                if(new_graph_name[-1] == "_"):
                    new_graph_name = new_graph_name[:-1]
                new_graph_name += "_broken_node_" + node_name + "_"
            new_graph_name = new_graph_name[:-1]
            new_graph_name += "_kept_edges_" + str( keep_edges ) + "_kept_edge_features_" + str( keep_edge_features )
        input_graphs[ new_graph_name ] = new_graph
        dump_json( input_graphs, filename = export_file )

    return new_graph

def remove_edge(
        graph_name,
        node1,
        node2,
        new_graph_name = None,
        keep_features = True,
        save = True,
        inplace = False,
        scatter_feat = False,
        input_file = "graph_dict.json",
        export_file = "graph_dict_removed.json",
        ):
    edges_names = []
    if( inplace is True ):
        export_file = input_file
    if( isinstance( node1, list ) is False ):
        node1 = [ node1 ]
    if( isinstance( node2, list ) is False ):
        node2 = [ node2 ]
    counter = 0
    for node_item in node1:
        edges_names.append( node1[ counter ].replace("-node","") + "-" + node2[ counter ].replace("-node","") + "-edge") 
        edges_names.append( node2[ counter ].replace("-node","") + "-" + node1[ counter ].replace("-node","") + "-edge") 
        counter += 1
    
    input_graphs = load_json( input_file )
    graph = input_graphs[ graph_name ]
    
    relevant_edge_names = []
    for edge_name in copy.deepcopy(edges_names):
        if( edge_name in graph["edges"].keys() ):
            relevant_edge_names.append( edge_name )

    edges_names = relevant_edge_names 

    new_graph = copy.deepcopy( graph )

    counter = 0
    for node_item in node1:
        if( [node1[counter], node2[counter]] in new_graph["src_dst_nodes"]): new_graph["src_dst_nodes"].remove([node1[ counter ], node2[counter]])
        if( [node2[counter], node1[counter]] in new_graph["src_dst_nodes"]): new_graph["src_dst_nodes"].remove([node2[ counter ], node1[counter]])
        counter += 1
    for i in graph["edges"].keys():
        if( i in edges_names ):
            del new_graph["edges"][ i ]
            if( keep_features ):
                if( scatter_feat ):
                    for feat in graph["edges"][i]:
                        new_graph["nodes"][ i + "-node-" + feat + "-node"] = [ feat ]
                else:
                    new_graph["nodes"][i+"-node"] = graph["edges"][i]

    if save:
        counter = 0
        if( new_graph_name is None ):
            new_graph_name = graph_name
            for node_item in node1:
                if(new_graph_name[-1] == "_"):
                    new_graph_name = new_graph_name[:-1]
                new_graph_name += "_removed_edge_" + node1[counter] + "_" + node2[counter] + "_"
                counter += 1
            new_graph_name = new_graph_name[:-1]
        input_graphs[ new_graph_name ] = new_graph
        dump_json( input_graphs, filename = export_file )

    return new_graph

def check_struct( graph, strict = False ):
    if( isinstance(graph, dict) is False ):
        print("The graph is not a dictionary!")
        return False
    if( set(["edges", "nodes","src_dst_nodes", "type" ]) != set( list(graph.keys() )) ):
        print("The graph doesn't have required keys!")
        missing_keys = []
        for i in ["edges", "nodes","src_dst_nodes", "type"]:
            if( i not in list(graph.keys())):
                missing_keys.append( i )
        print("\tThe Missing Key(s): ", missing_keys)
        return False
    if( isinstance( graph["type"], str ) is False ):
        print("The `graph['type']` is not a string!")
        return False
    if( isinstance( graph["nodes"], dict ) is False ):
        print("The `graph['nodes']` is not a dictionary!")
        return False
    if( isinstance( graph["edges"], dict ) is False ):
        print("The `graph['edges']` is not a dictionary!")
        return False
    if( isinstance( graph["src_dst_nodes"], list ) is False ):
        print("The `graph['src_dst_nodes']` is not a list!")
        return False
    for node in graph["nodes"].keys():
        if( isinstance( graph["nodes"][node], list ) is False ):
            print("The `graph['nodes']'" + node + "']` is not a list!")
            return False
        if( node[-4:] != "node" ):
            print("The following name of a node doesn't end with 'node': '" + node + "'")
            return False
    for edge in graph["edges"].keys():
        if( isinstance( graph["edges"][edge], list ) is False ):
            print("The `graph['edges']'" + edge +"']` is not a list!")
            return False
        if( edge[-4:] != "edge" ):
            print("The following name of an edge doesn't end with 'edge': '" + edge + "'")
            return False
    edges_utilized = []
    for connect in graph["src_dst_nodes"]:
        if( connect[0] not in list(graph["nodes"].keys()) ):
            print("The follwing source-node is not a node: '"+connect[0] +"'")
            return False
        if( connect[1] not in list(graph["nodes"].keys()) ):
            print("The following destination-node not a node: '"+connect[1]+"'")
            return False
        edge_name = connect[0].replace("-node","") + "-" + connect[1].replace("-node","") + "-edge"
        if( edge_name not in list(graph["edges"].keys()) ):
            print("No edge found for the following connection: ", connect )
            return False
        edges_utilized.append( edge_name )
    if( len( graph["src_dst_nodes"]) != len( list(graph["edges"].keys())) ):
        useless_edges = list( graph["edges"].keys())
        for i in edges_utilized:
            useless_edges.remove(i)
        print("The number of connections in `graph['src_dst_nodes']` is smaller than the number of edges!\nThis means that the following edges are never actually created:")
        for i in useless_edges:
            print("\t"+i)
        if( strict ):
            return False
    return True

def get_unique_elements( list1, list2 ):
    unique_graph1_nodes = []
    unique_graph2_nodes = []
    for graph1_node in list1:
        if( graph1_node not in list2):
            unique_graph1_nodes.append( graph1_node )
    for graph2_node in list2:
        if( graph2_node not in list1):
            unique_graph2_nodes.append( graph2_node )
    return unique_graph1_nodes, unique_graph2_nodes

def diff_graphs( graph1, graph2 ):
    if( not check_struct( graph1 )):
        print("Graph1 is not correctly structured!")
        return False
    if( not check_struct( graph2 ) ):
        print("Graph2 is not correctly structured!")
        return False
    if( graph1["type"] != graph2["type"] ):
        print("------------------------")
        print("Difference in Graph Type")
        print("------------------------")
        print("\tGraph1 Type: " + graph1[ "type" ] )
        print("\tGraph2 Type: " + graph2[ "type" ] )
    print("-------------------")
    print("Difference in Nodes")
    print("-------------------")
    found_difference = False
    if( set(graph1["nodes"].keys()) != set(graph2["nodes"].keys()) ):
        found_difference = True
        unique_graph1_nodes, unique_graph2_nodes = get_unique_elements( graph1["nodes"].keys(), graph2["nodes"].keys() )
        print("\tGraphs Have Different Nodes...")
        print("\t\tUnique Graph1 Nodes: ")
        for unique_graph1_node in unique_graph1_nodes:
            print("\t\t\t'"+unique_graph1_node+"'")
            print("\t\t\t\tFeatures for the Unique Graph1 Node: '" + unique_graph1_node + "'")
            for feature in graph1["nodes"][unique_graph1_node]:
                print("\t\t\t\t\t'" + feature +"'")
        print("\t\tUnique Graph2 Nodes: ")
        for unique_graph2_node in unique_graph2_nodes:
            print("\t\t\t'"+unique_graph2_node+"'")
            print("\t\t\t\tFeatures for the Unique Graph2 Node: '" + unique_graph2_node + "'")
            for feature in graph2["nodes"][unique_graph2_node]:
                print("\t\t\t\t\t'" + feature +"'")
    if( len(graph1["nodes"].keys()) <= len(graph2["nodes"].keys())):
        for graph1_node in graph1["nodes"].keys():
            if( graph1_node in graph2["nodes"].keys()):
                if( set(graph1["nodes"][graph1_node]) != set(graph2["nodes"][graph1_node])):
                    found_difference = True
                    print("\tGraphs Have Different Node-Features...")
                    unique_node1_features, unique_node2_features = get_unique_elements( graph1["nodes"][graph1_node],graph2["nodes"][graph1_node] )
                    print("\t\tFor the Node: " + graph1_node )
                    print("\t\t\tUnique Graph1 Node Features: ", unique_node1_features)
                    print("\t\t\tUnique Graph2 Node Features: ", unique_node2_features)
    else:
        for graph2_node in graph2["nodes"].keys():
            if( graph2_node in graph1["nodes"].keys()):
                if( set(graph1["nodes"][graph2_node]) != set(graph2["nodes"][graph2_node])):
                    found_difference = True
                    print("\tGraphs Have Different Node-Features...")
                    unique_node1_features, unique_node2_features = get_unique_elements( graph1["nodes"][graph2_node],graph2["nodes"][graph2_node] )
                    print("\t\tFor the Node: " + graph2_node )
                    print("\t\t\tUnique Graph1 Node Features: ", unique_node1_features)
                    print("\t\t\tUnique Graph2 Node Features: ", unique_node2_features)
    if not found_difference:
        print("No Difference b/w the Nodes of the Two Graphs!")
    print("------------------")
    print("Differene in Edges")
    print("------------------")
    found_difference = False
    if( set(graph1["edges"].keys()) != set(graph2["edges"].keys()) ):
        found_difference = True
        unique_graph1_edges, unique_graph2_edges = get_unique_elements( graph1["edges"].keys(), graph2["edges"].keys() )
        print("\tGraphs Have Different Edges...")
        print("\t\tUnique Graph1 Edges: ")
        for unique_graph1_edge in unique_graph1_edges:
            print("\t\t\t'"+unique_graph1_edge +"'")
            print("\t\t\t\tFeatures for the Unique Graph1 Edge: '"+unique_graph1_edge + "'")
            for feature in graph1["edges"][unique_graph1_edge]:
                print("\t\t\t\t\t'" + feature +"'")
        print("\t\tUnique Graph2 Edges: ")
        for unique_graph2_edge in unique_graph2_edges:
            print("\t\t\t'"+unique_graph2_edge+"'")
            print("\t\t\t\tFeatures for the Unique Graph2 Edge: '"+unique_graph2_edge + "'")
            for feature in graph2["edges"][unique_graph2_edge]:
                print("\t\t\t\t\t'" + feature +"'" )
    if( len(graph1["edges"].keys()) <= len(graph2["edges"].keys())):
        for graph1_edge in graph1["edges"].keys():
            if( graph1_edge in graph2["edges"].keys()):
                if( set(graph1["edges"][graph1_edge]) != set(graph2["edges"][graph1_edge])):
                    found_difference = True
                    print("\tGraphs Have Different Edge-Features...")
                    unique_edge1_features, unique_edge2_features = get_unique_elements( graph1["edges"][graph1_edge],graph2["edges"][graph1_edge] )
                    print("\t\tFor the Edge: " + graph1_edge )
                    print("\t\t\tUnique Graph1 Edge Features: ", unique_edge1_features)
                    print("\t\t\tUnique Graph2 Edge Features: ", unique_edge2_features)
    else:
        for graph2_edge in graph2["edges"].keys():
            if( graph2_edge in graph1["edges"].keys()):
                if( set(graph1["edges"][graph2_edge]) != set(graph2["edges"][graph2_edge])):
                    found_difference = True
                    print("\tGraphs Have Different Edge-Features...")
                    unique_edge1_features, unique_edge2_features = get_unique_elements( graph1["edges"][graph2_edge],graph2["edges"][graph2_edge] )
                    print("\t\tFor the Edge: " + graph2_edge )
                    print("\t\t\tUnique Graph1 Edge Features: ", unique_edge1_features)
                    print("\t\t\tUnique Graph2 Edge Features: ", unique_edge2_features)
    if not found_difference:
        print("No Difference b/w the Edges of the Two Graphs!")


def main():
    graph_name = "v4_homo_multiclass"
    input_file = "graph_dict.json"
    export_file = input_file.replace(".json", "_sculpt.json")
    org_graph = load_json( input_file )[ graph_name ]
    edge_removal_schemes = {
            "1a": ( "track1-node" , "track2-node"    ),
            "1b": ( "dimeson-node", "dielectron-node"),
            "1c": ( "b-meson-node", "dimeson-node"   ), 
            "1d": ( "b-meson-node", "dielectron-node"),

            "2a": ([ "track1-node"   , "track2-node"   ], [ "dimeson-node"   , "dimeson-node"   ]),
            "2b": ([ "electron1-node", "electron2-node"], [ "dielectron-node", "dielectron-node"]),
            "2c": ([ "b-meson-node"  , "b-meson-node"  ], [ "dielectron-node", "dimeson-node"   ]),

            "3a": ([ "track1-node"   , "track2-node"   , "track1-node"     ], [ "dimeson-node"   , "dimeson-node"   , "track2-node"     ]),
            "3b": ([ "electron1-node", "electron2-node", "dielectron-node" ], [ "dielectron-node", "dielectron-node", "dimeson-node"    ]),
            "3c": ([ "b-meson-node"  , "b-meson-node"  , "dimeson-node"    ], [ "dielectron-node", "dimeson-node"   , "dielectron-node" ]),
            
            "4a": ([ "track1-node", "track2-node"   , "electron1-node", "electron2-node"  ], [ "dimeson-node", "dimeson-node"   , "dielectron-node", "dielectron-node" ]),
            "4b": ([ "track1-node", "track2-node"   , "track1-node"   , "dimeson-node"    ], [ "dimeson-node", "dimeson-node"   , "track2-node"    , "dielectron-node" ]),
            "4c": ([ "track1-node", "electron1-node", "electron2-node", "dielectron-node" ], [ "track2-node" , "dielectron-node", "dielectron-node", "dimeson-node"    ]),
            "4d": ([ "track1-node", "b-meson-node"  , "b-meson-node"  , "dielectron-node" ], [ "track2-node" , "dimeson-node"   , "dielectron-node", "dimeson-node"    ]),
            
            "5a": ([ "track1-node"   , "track2-node"     , "electron1-node", "electron2-node", "track1-node"  ], [ "dimeson-node"   , "dimeson-node"   , "dielectron-node", "dielectron-node", "track2-node"     ]),
            "5b": ([ "track1-node"   , "track2-node"     , "electron1-node", "electron2-node", "dimeson-node" ], [ "dimeson-node"   , "dimeson-node"   , "dielectron-node", "dielectron-node", "dielectron-node" ]),
            "5c": ([ "track1-node"   , "track2-node"     , "b-meson-node"  , "b-meson-node"  , "dimeson-node" ], [ "dimeson-node"   , "dimeson-node"   , "dimeson-node"   , "dielectron-node", "dielectron-node" ]),
            "5d": ([ "electron1-node", "electron2-node"  , "b-meson-node"  , "b-meson-node"  , "dimeson-node" ], [ "dielectron-node", "dielectron-node", "dimeson-node"   , "dielectron-node", "dielectron-node" ]),

            "6a": (
                   [ "track1-node" , "track2-node" , "electron1-node" , "electron2-node" , "b-meson-node", "b-meson-node"    ],
                   [ "dimeson-node", "dimeson-node", "dielectron-node", "dielectron-node", "dimeson-node", "dielectron-node" ]
                ),
            "6b": (
                   [ "electron1-node" , "electron2-node" , "b-meson-node", "b-meson-node"   , "dimeson-node"   , "track1-node" ],
                   [ "dielectron-node", "dielectron-node", "dimeson-node", "dielectron-node", "dielectron-node", "track2-node" ]
                ),

            "7a": (
                   [ "track1-node" , "track2-node" , "electron1-node" , "electron2-node" , "b-meson-node", "b-meson-node"   , "dimeson-node"    ],
                   [ "dimeson-node", "dimeson-node", "dielectron-node", "dielectron-node", "dimeson-node", "dielectron-node", "dielectron-node" ]
                ),
            "8a": (
                   [ "track1-node" , "track2-node" , "electron1-node" , "electron2-node" , "b-meson-node", "b-meson-node"   , "dimeson-node"   , "track1-node" ],
                   [ "dimeson-node", "dimeson-node", "dielectron-node", "dielectron-node", "dimeson-node", "dielectron-node", "dielectron-node", "track2-node" ]
                ),
           "21a": (
                   [ "track1-node" , "track2-node" , "electron1-node" , "electron2-node" , "b-meson-node", "b-meson-node"   , "dimeson-node"   , "track1-node", "electron1-node", "electron2-node", "electron1-node", "electron2-node", "electron1-quality-node", "electron2-quality-node", "track1-node", "track2-node", "track1-node", "track2-node", "track1-quality-node", "track2-quality-node", "b-meson-node", "electron1-node" ],
                   [ "dimeson-node", "dimeson-node", "dielectron-node", "dielectron-node", "dimeson-node", "dielectron-node", "dielectron-node", "track2-node", "electron1-quality-node", "electron2-quality-node", "electron1-kinematic-node", "electron2-kinematic-node", "electron1-kinematic-node", "electron2-kinematic-node", "track1-kinematic-node", "track2-kinematic-node", "track1-quality-node", "track2-quality-node", "track1-kinematic-node", "track2-kinematic-node", "b-meson-quality-node", "electron2-node" ]
                )
            }
    scatter_edge_removal_schemes = {}
    for i in edge_removal_schemes.keys():
        scatter_edge_removal_schemes[ i + "_scatter"] = edge_removal_schemes[ i ]

    node_break_schemes = {
            "1a": [ "b-meson-quality-node" ],
            "2a": [ "track1-kinematic-node", "track2-kinematic-node" ],
            "2b": [ "electron1-kinematic-node", "electron2-kinematic-node" ],
            "3a": [ "b-meson-quality-node", "track1-kinematic-node", "track2-kinematic-node" ],
            "3b": [ "b-meson-quality-node", "electron1-kinematic-node", "electron2-kinematic-node" ],
            "4a": [ "track1-kinematic-node", "track2-kinematic-node", "electron1-kinematic-node", "electron2-kinematic-node" ],
            "5a": [ "track1-kinematic-node", "track2-kinematic-node", "electron1-kinematic-node", "electron2-kinematic-node", "b-meson-quality-node" ]
            }
    keep_edges_node_break_schemes = {}
    remove_edges_keep_edge_features_node_break_schemes = {}
    remove_edges_scatter_edge_features_node_break_schemes = {}
    remove_edges_remove_edge_features_node_break_schemes = {}
    for i in node_break_schemes.keys():
        keep_edges_node_break_schemes[ i + "_keep_edges" ] = node_break_schemes[ i ]
        remove_edges_keep_edge_features_node_break_schemes[ i + "_remove_edges_keep_edge_features" ] = node_break_schemes[ i ]
        remove_edges_scatter_edge_features_node_break_schemes[ i + "_remove_edges_scatter_edge_features" ] = node_break_schemes[ i ]
        remove_edges_remove_edge_features_node_break_schemes[ i + "_remove_edges_remove_edge_features" ] = node_break_schemes[ i ]

    graph_removals = {}

    counter = 0
    for i in edge_removal_schemes.keys():
        print("scheme", i)
        if( counter == 0 ):
            graph_removals[i] = remove_edge( graph_name, edge_removal_schemes[i][0], edge_removal_schemes[i][1], export_file = export_file, input_file = input_file, new_graph_name = graph_name + "_" + i )
        else:
            graph_removals[i] = remove_edge( graph_name, edge_removal_schemes[i][0], edge_removal_schemes[i][1], export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i )
        counter += 1

    counter = 0
    for i in scatter_edge_removal_schemes.keys():
        print("scheme", i)
        if( counter == 0 ):
            graph_removals[i] = remove_edge( graph_name, scatter_edge_removal_schemes[i][0], scatter_edge_removal_schemes[i][1], export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i, scatter_feat = True )
        else:
            graph_removals[i] = remove_edge( graph_name, scatter_edge_removal_schemes[i][0], scatter_edge_removal_schemes[i][1], export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i, scatter_feat = True )
        counter += 1
    
    for i in keep_edges_node_break_schemes.keys():
        print("scheme", i)
        graph_removals[ i ] = break_node( graph_name, keep_edges_node_break_schemes[ i ], keep_edges = True, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i )
    for i in remove_edges_keep_edge_features_node_break_schemes.keys():
        graph_removals[ i ] = break_node( graph_name, remove_edges_keep_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = False, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i )
    for i in remove_edges_scatter_edge_features_node_break_schemes.keys():
        graph_removals[ i ] = break_node( graph_name, remove_edges_scatter_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = True, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i )
    for i in remove_edges_remove_edge_features_node_break_schemes.keys():
        graph_removals[ i ] = break_node( graph_name, remove_edges_remove_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = False, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + i )

    for j in edge_removal_schemes.keys():
        for i in keep_edges_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, keep_edges_node_break_schemes[ i ], keep_edges = True, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_keep_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_keep_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = False, export_file =          export_file, input_file =        export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_scatter_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_scatter_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = True, export_file =         export_file, input_file =       export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_remove_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_remove_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = False, export_file = export_file, input_file =            export_file, new_graph_name =      graph_name + "_" + j + "_ " + i )

    for j in scatter_edge_removal_schemes.keys():
        for i in keep_edges_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, keep_edges_node_break_schemes[ i ], keep_edges = True, export_file = export_file, input_file = export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_keep_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_keep_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = False, export_file = export_file, input_file =        export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_scatter_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_scatter_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = True, scatter_edge_features = True, export_file = export_file, input_file =       export_file, new_graph_name = graph_name + "_" + j + "_" + i )
        for i in remove_edges_remove_edge_features_node_break_schemes.keys():
            print("scheme", j, i)
            graph_removals[ j + "_" + i ] = break_node( graph_name + "_" + j, remove_edges_remove_edge_features_node_break_schemes[ i ], keep_edges = False, keep_edge_features = False, export_file = export_file, input_file = export_file, new_graph_name =      graph_name + "_" + j + "_ " + i )
        

    for i in graph_removals.keys():
        print( "---------------------------------" )
        print( "original-graph vs. modified-graph" )
        print( "---------------------------------" )
        print( "modification id", i)
        print( "---------------------------------" )
        if( diff_graphs( org_graph, graph_removals[i]  ) is False ):
            print("ERROR!")
            quit()
    print( "total modifications", len( graph_removals.keys() ) )

if( __name__ == "__main__" ):
    main()
