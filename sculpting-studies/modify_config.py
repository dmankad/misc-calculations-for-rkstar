import yaml

def load_yaml( filename ):
    with open(filename, "r") as f:
        return yaml.load( f, Loader = yaml.FullLoader )

def dump_yaml( data, filename ):
    with open( filename, "w" ) as f:
        yaml.dump( data, f  )

basefile = "gnn1_multiclass_v4d_sculpt_wildcard_dense_broadcast.yaml"
base_graph_version = "v4_homo_multiclass_wildcard"
suffix_list = [
"original",
"1a",
"1b",
"1c",
"1d",
"2a",
"2b",
"2c",
"3a",
"3b",
"3c",
"4a",
"4b",
"4c",
"4d",
"5a",
"5b",
"5c",
"5d",
"6a",
"6b",
"7a",
"8a"
]

if(__name__ == "__main__"):
    for suffix in suffix_list:
        filename = basefile.replace( "wildcard", suffix )
        config = load_yaml( filename ) 
        """
        config["common"]["graph_version"] = base_graph_version + "_" + suffix
        config["application"]["nn_suffix"] = basefile.replace("_original.yaml", "_" + suffix)
        dump_yaml( config, filename )
        
        if(len(suffix)!=0):
            filename = basefile.replace("_original.yaml", "_" + suffix + ".yaml" ).replace(".yaml", "_scatter.yaml")
        else:
            filename = basefile.replace(".yaml", "_scatter.yaml")
        config = load_yaml( filename )
        """
        config["common"]["graph_version"] = base_graph_version.replace( "wildcard", suffix + "_scatter" )
        ###config["common"]["graph_version"] = base_graph_version.replace( "wildcard", suffix  )
        config[ "application" ][ "files" ] = [
  "ntuple-data18_13TeV_periodK_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodK_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodL_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodL_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodM_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodM_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodI_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodI_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodF_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodF_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodO_part_[0-9][0-9]_[0-9].ftr",
  "ntuple-data18_13TeV_periodO_part_[0-9][0-9]_[0-9][0-9].ftr",
  "ntuple-data18_13TeV_periodQ_part_[0-9][0-9]_[0-9].ftr"
  "ntuple-data18_13TeV_periodQ_part_[0-9][0-9]_[0-9][0-9].ftr"
                ]
        filename = basefile.replace( "wildcard", suffix + "_scatter" )
        ###filename = basefile.replace( "wildcard", suffix  )
        #config["training"]["learning_rate"] = config["training"]["learning_rate"] * 2
        #config["training"]["batch_size"] = config["training"]["batch_size"] * 2
        config["application"]["nn_suffix"] = basefile.replace( "wildcard", suffix + "_scatter" ).replace(".yaml", "")
        ###config["application"]["nn_suffix"] = basefile.replace( "wildcard", suffix ).replace(".yaml", "")
        #config["training"]["output_dir"] = "/afs/cern.ch/user/d/dmankad/eoswork/gnnResults"
        #config["training"]["epochs"] = 200 
        dump_yaml( config, filename )
