#!/bin/bash

export DSID=$1
export scope="mc16_13TeV"

if [[ ! -z $2 ]]; then
    export scope=$2
fi

export EVNTSearchString="${scope}:${scope}.${DSID}.*merge.EVNT.*"
export EVNTDSName=$(rucio ls $EVNTSearchString | grep "CONT" | sed -e "s/\(.*\)${scope}\:${scope}\(.*\)\ DID\(.*\)/${scope}\2/")
export eTag=$(echo ${EVNTDSName} | sed -e "s/\(.*\)EVNT.\(.*\)_e\(.*\)/\2/")
export eTagMerge=$(echo ${EVNTDSName} | sed -e "s/\(.*\)EVNT.\(.*\)_e\(.*\)\ \(.*\)/e\3/")

mkdir -p log

echo "eTag: ${eTag}" > log/${DSID}.eTags.yaml
echo "eTagMerge: ${eTagMerge}" >> log/${DSID}.eTags.yaml
