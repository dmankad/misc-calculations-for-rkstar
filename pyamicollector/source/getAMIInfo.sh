#!/bin/bash

# Defaults for (Signal/Real Background MC Samples) Run 2

export DSID=$1
export derivTag=$2        # Derivation Tag: BPHY18 for ee channel, BPHY12 for mumu channel.
export eTag="e7074"       # Level: EVNT | EvGen Tag. NB: It's different for the fake samples due to using a newer release of AthGeneration (needed for newly introduced filter. No special care needed at the level of analysis). 
export sTag="s3126"       # Level: HITS | Detector Simulation Tag for FullSim/FastSim: "s*" for FullSim, "a*" for FastSim.
export rTag="r10724"      # Level: AOD  | Digitization & Reconstruction Tag.
export pTag="p4696"       # Level: DAOD | Pileup Tag.
export eTagMerge="e5984"  # eTag added for merged containers.
export rTagMerge="r10726" # rTag added for merged containers.
export scope="mc16_13TeV" # Scope for the Rucio Containers. Same for all Run2 MC samples.

if [[ ! -z $3 ]]; then
    export eTag=$3
fi
if [[ ! -z $4 ]]; then
    export sTag=$4
fi
if [[ ! -z $5 ]]; then
    export rTag=$5
fi
if [[ ! -z $6 ]]; then
    export pTag=$6
fi
if [[ ! -z $7 ]]; then
    export eTagMerge=$7
fi
if [[ ! -z $8 ]]; then
    export rTagMerge=$8
fi
if [[ ! -z $9 ]]; then
    export scope=$9
fi

export EVNTSearchString="${scope}:${scope}.${DSID}.*merge.EVNT.*${eTag}*${eTagMerge}"
export HITSSearchString="${scope}:${scope}.${DSID}.*HITS.*${eTag}*${eTagMerge}*${sTag}"
export AODSearchString="${scope}:${scope}.${DSID}.*merge.AOD.*${eTag}*${eTagMerge}*${sTag}*${rTag}*${rTagMerge}"
export DAODSearchString="${scope}:${scope}.${DSID}.*deriv.DAOD_${derivTag}.*${eTag}*${eTagMerge}*${sTag}*${rTag}*${rTagMerge}*${pTag}"

export amiSearchEVNT=$(rucio ls $EVNTSearchString | grep "CONT" | sed -e "s/\(.*\)${scope}\:${scope}\(.*\)${eTagMerge}\(.*\)/${scope}\2${eTagMerge}/")
export amiSearchHITS=$(rucio ls $HITSSearchString | grep "CONT" | sed -e "s/\(.*\)${scope}\:${scope}\(.*\)${sTag}\(.*\)/${scope}\2${sTag}/")
export amiSearchAOD=$(rucio ls $AODSearchString | grep "CONT" | sed -e "s/\(.*\)${scope}\:${scope}\(.*\)${rTagMerge}\(.*\)/${scope}\2${rTagMerge}/")
export amiSearchDAOD=$(rucio ls $DAODSearchString | grep "CONT" | sed -e "s/\(.*\)${scope}\:${scope}\(.*\)${pTag}\(.*\)/${scope}\2${pTag}/")

echo "    > Rucio EVNT Search String: ${EVNTSearchString}"
echo "    > Rucio HITS Search String: ${HITSSearchString}"
echo "    > Rucio  AOD Search String: ${AODSearchString}"
echo "    > Rucio DAOD Search String: ${DAODSearchString}"
echo "    > PyAMI EVNT  Query String: ${amiSearchEVNT}"
echo "    > PyAMI HITS  Query String: ${amiSearchHITS}"
echo "    > PyAMI  AOD  Query String: ${amiSearchAOD}"
echo "    > PyAMI DAOD  Query String: ${amiSearchDAOD}"

mkdir -p log

export EVNTMetaDataFile=log/${DSID}.EVNT.AMIInfo.yaml
export HITSMetaDataFile=log/${DSID}.HITS.AMIInfo.yaml
export AODMetaDataFile=log/${DSID}.AOD.AMIInfo.yaml
export DAODMetaDataFile=log/${DSID}.DAOD.AMIInfo.yaml

ami show dataset info ${amiSearchEVNT} > ${EVNTMetaDataFile}
ami show dataset info ${amiSearchHITS} > ${HITSMetaDataFile}
ami show dataset info ${amiSearchAOD} > ${AODMetaDataFile}
ami show dataset info ${amiSearchDAOD} > ${DAODMetaDataFile}

# Pruning to make it a valid YAML file, a bit hacky but needs to be done!
cp ${EVNTMetaDataFile} ${EVNTMetaDataFile}.orig
cp ${HITSMetaDataFile} ${HITSMetaDataFile}.orig
cp ${AODMetaDataFile} ${AODMetaDataFile}.orig
cp ${DAODMetaDataFile} ${DAODMetaDataFile}.orig

sed -i "s/\[\(.*\)\]/\1/" ${EVNTMetaDataFile}
sed -i "s/\[\(.*\)\]/\1/" ${HITSMetaDataFile}
sed -i "s/\[\(.*\)\]/\1/" ${AODMetaDataFile}
sed -i "s/\[\(.*\)\]/\1/" ${DAODMetaDataFile}

#echo "Diff b/w Original & Pruned in the EVNT Info..."
#diff ${EVNTMetaDataFile}  ${EVNTMetaDataFile}.orig
#echo "Diff b/w Original & Pruned in the HITS Info..."
#diff ${HITSMetaDataFile}  ${HITSMetaDataFile}.orig
#echo "Diff b/w Original & Pruned in the AOD Info..."
#diff ${AODMetaDataFile}  ${AODMetaDataFile}.orig
#echo "Diff b/w Original & Pruned in the DAOD Info..."
#diff ${DAODMetaDataFile}  ${DAODMetaDataFile}.orig

