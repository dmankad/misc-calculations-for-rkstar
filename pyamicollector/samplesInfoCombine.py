import yaml
import json
import sys
import os 

skimmedDictFile = sys.argv[ 1 ]
skimKernel = os.path.basename( skimmedDictFile )

skimmedDict = yaml.load( open( skimmedDictFile, "r" ), Loader = yaml.FullLoader )
amiInfo = yaml.load( open( os.path.join( "results", skimKernel.replace( ".yaml",         ".AMIInfo.yaml" ) ), "r" ), Loader = yaml.FullLoader )
brInfo  = yaml.load( open( os.path.join( "results", skimKernel.replace( ".yaml", ".PhysicsInfo.BRs.yaml" ) ), "r" ), Loader = yaml.FullLoader )
texInfo = json.load( open( os.path.join( "results", skimKernel.replace( ".yaml", ".PhysicsInfo.TeX.json" ) ), "r" ) )

fullDict = {}
for dsid in skimmedDict.keys():
    fullDict[ dsid ] = {}
    fullDict[ dsid ].update( amiInfo[ dsid ] )
    fullDict[ dsid ].update( { "br"         : brInfo     [ dsid ] } )
    fullDict[ dsid ].update( { "name_latex" : texInfo    [ str( dsid ) ] } )
    fullDict[ dsid ].update( { "name"       : skimmedDict[ dsid ] } )

with open( os.path.join( "results", skimKernel.replace( ".yaml", ".ComboInfo.json" ) ), "w" ) as f:
    json.dump( fullDict, f, indent = 4 )
