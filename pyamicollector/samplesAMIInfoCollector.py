### Imports ###
### ------- ###

### Imports -> I/O ###
### -------------- ###
import yaml
import os
import pathlib

### Imports -> Pythonic ###
### ------------------- ###
from collections import OrderedDict
import copy

### Declaring Tags for Datasets ###
### --------------------------- ###

run = 2

electronTagsRealMC = OrderedDict( {
    "derivTag"  : "BPHY18"     , # Derivation Tag
    "eTag"      : "e7074"      , # EVNT
    "sTag"      : "s3126"      if run == 2 else "s4162",  # HITS
    "rTag"      : "r10724"     if run == 2 else "r14622",       # AOD
    "pTag"      : "p4696"      if run == 2 else "p5836" ,       # DAOD (Pileup)
    "eTagMerge" : "e5984"      , # EVNT Merge
    "rTagMerge" : "r10726"     if run == 2 else "r14663",       # AOD Merge
    "scope"     : "mc16_13TeV" if run == 2 else "mc23_13p6TeV"  # Campaign
    }
)
electronTagsFakeMC = copy.deepcopy( electronTagsRealMC )
electronTagsFakeMC[ "eTag"      ] = "e8552"
electronTagsFakeMC[ "eTagMerge" ] = "e7400"

muonTagsRealMC = copy.deepcopy( electronTagsRealMC )
muonTagsRealMC[ "sTag"     ] = "a875"
muonTagsRealMC[ "pTag"     ] = "p4995"
muonTagsRealMC[ "derivTag" ] = "BPHY12"

muonTagsFakeMC = copy.deepcopy( electronTagsFakeMC )
muonTagsFakeMC[ "derivTag" ] = "BPHY12"

### Fetch Tags from Rucio ###
### --------------------- ###

def fetchETagsForDSID( dsid, fresh = True, campaign = "mc16_13TeV" if run == 2 else "mc23_13p6TeV" ):
    if fresh:
        fetchFromRucio = True
    elif not os.path.exists( f"./log/{dsid}.eTags.yaml" ):
        fetchFromRucio = True
    else:
        fetchFromRucio = False
    if fetchFromRucio:
        eTagsString = f"source ./source/getETags.sh {dsid} {campaign}"
        os.system( eTagsString )
    with open( f"log/{dsid}.eTags.yaml", "r" ) as f:
        data = yaml.load( f, Loader = yaml.FullLoader )
    return data

### Fetch AMI Info from PyAMI ###
### ------------------------- ###

def _fetchAMIInfoForDSID( dsid, tags, fresh = True ):
    amiInfoFiles = [ 
        f"log/{dsid}.EVNT.AMIInfo.yaml",
        f"log/{dsid}.HITS.AMIInfo.yaml",
        f"log/{dsid}.AOD.AMIInfo.yaml",
        f"log/{dsid}.DAOD.AMIInfo.yaml"
    ]
    if fresh:
        fetchFromAMI = True
    elif not all( [ os.path.exists( f ) for f in amiInfoFiles ] ):
        fetchFromAMI = True
    else:
        fetchFromAMI = False
    if fetchFromAMI:
        tagsUnpacked = " ".join( list( tags.values() ) )
        amiString = f"source ./source/getAMIInfo.sh {dsid} {tagsUnpacked}" 
        os.system( amiString )
    amiInfo = {}
    for f in amiInfoFiles:
        with open( f, "r" ) as _f:
            data = yaml.load( _f, Loader = yaml.FullLoader )
        amiInfo[ f.split( "." )[ 1 ] ] = data
    return amiInfo

### Steering Function ###
### ----------------- ###

def fetchAMIInfoForDSID( dsid, channel, fakes = False, fresh = True ):
    eTags = fetchETagsForDSID( dsid, fresh )
    if channel == "electron":
        tags = electronTagsFakeMC if fakes else electronTagsRealMC
    if channel == "muon":
        tags = muonTagsFakeMC if fakes else muonTagsRealMC
    if eTags[ "eTag" ] != tags[ "eTag" ]:
        print( f"Unexpected eTag! Overwriting: {tags[ 'eTag' ]} -> {eTags[ 'eTag' ]}..." )
        tags.update( eTags )
    amiInfo = _fetchAMIInfoForDSID( dsid, tags, fresh )
    return amiInfo

### Reformat the AMI Info ###
### --------------------- ###

def reformatAMIInfo( amiInfo ):
    reformatRules = [
        ( ( "AOD"  , "logicalDatasetName" ), ( "dataset"             , lambda x: x          ) ),
        ( ( "AOD"  , "nFiles"             ), ( "files"               , lambda x: int( x )   ) ),
        ( ( "AOD"  , "totalEvents"        ), ( "events"              , lambda x: int( x )   ) ),
        ( ( "AOD"  , "totalSize"          ), ( "size"                , lambda x: float( x ) ) ),
        ( ( "AOD"  , "prodsysStatus"      ), ( "status"              , lambda x: x          ) ),
        ( ( "AOD"  , "completion"         ), ( "completion"          , lambda x: float( x ) ) ),
        ( ( "AOD"  , "logicalDatasetName" ), ( "dataset_AOD"         , lambda x: x          ) ),
        ( ( "AOD"  , "nFiles"             ), ( "files_AOD"           , lambda x: int( x )   ) ),
        ( ( "AOD"  , "totalEvents"        ), ( "events_AOD"          , lambda x: int( x )   ) ),
        ( ( "AOD"  , "totalSize"          ), ( "size_AOD"            , lambda x: float( x ) ) ),
        ( ( "AOD"  , "prodsysStatus"      ), ( "status_AOD"          , lambda x: x          ) ),
        ( ( "AOD"  , "completion"         ), ( "completion_AOD"      , lambda x: float( x ) ) ),
        ( ( "EVNT" , "logicalDatasetName" ), ( "dataset_EVNT"        , lambda x: x          ) ),
        ( ( "EVNT" , "crossSection_mean"  ), ( "cross_section"       , lambda x: float( x ) ) ),
        ( ( "EVNT" , "GenFiltEff_mean"    ), ( "gen_filter_eff"      , lambda x: float( x ) ) ),
        ( ( "EVNT" , "totalEvents"        ), ( "events_EVNT"         , lambda x: int( x )   ) ),
        ( ( "EVNT" , "sumOfWeights@MCGN"  ), ( "sum_of_weights_EVNT" , lambda x: float( x ) ) ),
        ( ( "EVNT" , "completion"         ), ( "completion_EVNT"     , lambda x: float( x ) ) ),
        ( ( "HITS" , "logicalDatasetName" ), ( "dataset_HITS"        , lambda x: x          ) ),
        ( ( "HITS" , "totalEvents"        ), ( "events_HITS"         , lambda x: int( x )   ) ),
        ( ( "HITS" , "completion"         ), ( "completion_HITS"     , lambda x: float( x ) if x != "N/A" else -1 ) ),
        ( ( "DAOD" , "totalEvents"        ), ( "events_DAOD"         , lambda x: int( x )   ) ),
        ( ( "DAOD" , "completion"         ), ( "completion_DAOD"     , lambda x: float( x ) ) ),
        ( ( "DAOD" , "logicalDatasetName" ), ( "dataset_DAOD"        , lambda x: x          ) ),
    ]
    reformatted = {}
    ruleKeys   = [ rule[ 0 ] for rule in reformatRules ]
    ruleValues = [ rule[ 1 ] for rule in reformatRules ]
    for ruleKey, ruleValue in list( zip( ruleKeys, ruleValues ) ):
        keyDatasetType, keyInfoBit = ruleKey
        newKey        , transform  = ruleValue
        reformatted[ newKey ] = transform( amiInfo[ keyDatasetType ].get( keyInfoBit, -1 ) )
    return reformatted

### Main Function ###
### ------------- ###

def main( skimmedSamplesDictFile, channel = "electron", fakes = False, channelFixed = False ):
    with open( skimmedSamplesDictFile, "r" ) as f:
        skimmedSamples = yaml.load( f, Loader = yaml.FullLoader )
    _skimmedSamples = {}
    for dsid in skimmedSamples.keys():
        print( "DSID", dsid )
        if "mu+" in skimmedSamples[ dsid ] or "mu-" in skimmedSamples[ dsid ]:
            channel = "muon" if not channelFixed else channel
        if "e+" in skimmedSamples[ dsid ] or "e-" in skimmedSamples[ dsid ]:
            channel = "electron" if not channelFixed else channel
        amiInfo         = fetchAMIInfoForDSID( dsid, channel, fakes, fresh = True )
        reformatted     = reformatAMIInfo( amiInfo )
        _skimmedSamples[ dsid ] = reformatted
    pathlib.Path( "results" ).mkdir( exist_ok = True, parents = True )
    with open( os.path.join( "results", os.path.basename( skimmedSamplesDictFile ).replace( ".yaml", ".AMIInfo.yaml" ) ), "w" ) as f:
        yaml.dump( _skimmedSamples, f, indent = 4 )

if __name__ == "__main__":
    import sys
    skimmedSamplesDictFile = sys.argv[ 1 ]
    channel                = sys.argv[ 2 ] if len( sys.argv ) > 2 else ""
    fakes                  = bool( int( sys.argv[ 3 ] ) ) if len( sys.argv ) > 3 else False 
    channelFixed           = bool( int( sys.argv[ 4 ] ) ) if len( sys.argv ) > 4 else False
    main( skimmedSamplesDictFile, channel = channel, fakes = fakes, channelFixed = channelFixed )
