
## Quick Links to Outputs for Existing MC Samples

- [MC Samples for Signals, Real Backgrounds](./results/skimmedSamples.ComboInfo.json)
- [MC Samples for the Fake Studies in the Electorn Channel](./results/skimmedSamplesFakes.electron.ComboInfo.json)
- [MC Samples for the Fake Studies in the Muon Channel](./results/skimmedSamplesFakes.muon.ComboInfo.json)

----

These scripts can be used to compile AMI information about MC samples, BRs, and TeX names of the processes involved.
In the current setup, the BRs can only be obtained for exclusive samples. 
The scripts in this subdirectory need to be run on an lxplus-like machine where you have the necessary authentication files for your [ATLAS VO membership](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/basicSetup/grid_vo/). 

## The Very First Setup

```
git clone https://gitlab.cern.ch/dmankad/misc-calculations-for-rkstar.git
cd misc-calculations-for-rkstar/pyamicollector
source setup.sh
```

## Recurrent Setup

```
cd misc-calculations-for-rkstar/pyamicollector
source setup.sh 
```

## Running the Scripts

### 0. Put the relevant DSIDs in a YAML file of the format:
```
DSIDx: A -> B (-> C D ) X Y (-> Z K ) 
DSIDy: P -> Q (-> R S (-> T U ) V ) W
```
This YAML file can be located anywhere, but, for this README.md, I'd assume it's located at `./configs/myDSIDs.yaml`.

### 1. Get the AMI information:
```
python3 samplesAMIInformationCollector.py configs/myDSIDs.yaml <channel> <fakes? 1/0> <channelFixed? 1/0>
```
For example:
```
python3 samplesAMIInformationCollector.py configs/myDSIDs.yaml electron 1 1
python3 samplesAMIInformationCollector.py configs/myDSIDs.yaml muon 1 1
python3 samplesAMIInformationCollector.py configs/myDSIDs.yaml 
```
The output will be saved as `results/myDSIDs.AMIInfo.yaml`. 

**Some Comments**:
- You need to fix the channel only if the MC sample is for a process that involve both electrons & muons, otherwise the script will automatically recognize which channel it is.
- You need to specify the channel only if the MC sample needs channel fixing or if the MC sample is for a process that doesn't involve either the particle corresponding to the intended channel, e.g., in fake samples.
- You need to specify whether the MC sample is a fake sample or not if it's fake, otherwise the default assumption is that it is not a fake sample. This information used to decide which tags to use for the rucio queries since (a) the fake samples use a different generation tag (due to newly introduced filters) (b) the fake samples use the full simulation for muons -- whereas the real samples use fast simulation for the muons.

### 2. Get the physics information:
```
python3 samplesPhysicsInfoCollector.py configs/myDSIDs.yaml
```
The output will be saved as `results/myDSIDs.PhysicsInfo.BRs.yaml` and `results/myDSIDs.PhysicsInfo.TeX.json`. 

**Some Comments**:
- The texifying script is not foolproof. 
- The branching ratios are calculated using a privately bug-freed version of the `2022inclusive_BELLE.dec` decay file. This is the most recent decay file recommended by ATLAS for MC production. Relevant places in the submodule to change which file to use: [the place to specify the name of the decay file to use](https://gitlab.cern.ch/dmankad/decaylab/-/blob/5647dd1de9a64f8d17212e1e6a5d32121b012392/src/libs/configs/quickUtils/generator.yaml#L1), [the place to put the corresponding decay file](https://gitlab.cern.ch/dmankad/decaylab/-/tree/5647dd1de9a64f8d17212e1e6a5d32121b012392/src/libs/decayFiles).

### 3. Combine the AMI & the physics information:
```
python3 samplesInfoCombine.py configs/myDSIDs.yaml
```
The output will be saved as `results/myDISDs.ComboInfo.json`.
