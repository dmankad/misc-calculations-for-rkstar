export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup git

if git submodule status | grep --quiet '^-'; then
    git submodule update --init
fi
cd source/decaylab
git pull
source setup.sh 1 

voms-proxy-init -voms atlas
lsetup "rucio -w"
lsetup pyami

cd ../..
