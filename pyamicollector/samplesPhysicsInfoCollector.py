#!/usr/bin/env python

from decaylab import packageLoader
whip, logger = packageLoader.loadDecayLab( "quickUtils", "error" )

import yaml
import json
import sys
import os
import pathlib

skimmedDictFile = sys.argv[1]

skimmedDict = yaml.load( open( skimmedDictFile, "r" ), Loader = yaml.Loader )

from decaylab import quickUtilsStore

brDict = {}
texDict = {}
processDict = {}

for dsid, process in list( skimmedDict.items() ):
    print( dsid, process )
    brDict[ dsid ]  = quickUtilsStore.getBR( process )
    texDict[ dsid ] = quickUtilsStore.texify( process )
    processDict[ dsid ] = {}
    processDict[ dsid ][ "parentalPDGIDs" ] = quickUtilsStore.getParentalPdgIds( process )
    processDict[ dsid ][ "parentalUIDs" ] = quickUtilsStore.getParentalUIDs( process )

pathlib.Path( "./results" ).mkdir( exist_ok = True, parents = True )
with open( os.path.join( "results", os.path.basename( skimmedDictFile ).replace( ".yaml", ".PhysicsInfo.BRs.yaml" ) ), "w" ) as f:
    yaml.dump( brDict, f, indent = 4 )
with open( os.path.join( "results", os.path.basename( skimmedDictFile ).replace( ".yaml", ".PhysicsInfo.TeX.json" ) ), "w" ) as f:
    json.dump( texDict, f, indent = 4 )
with open( os.path.join( "results", os.path.basename( skimmedDictFile ).replace( ".yaml", ".PhysicsInfo.Processes.json" ) ), "w" ) as f:
    json.dump( processDict, f, indent = 4 )
    
