"""
- This script is a part of the ML analysis for event selection in the
R(K*) analysis at ATLAS.
- The purpose of this script is to produce plots for features of interest
to this analysis and the correlations among them in different parameter-
regions.
- The git-repository for the analysis: https://gitlab.cern.ch/RKstar.
"""

### I/O Imports ###
### ----------- ###
import os
from os import path
import sys

### Python Utility Imports ###
### ---------------------- ###
import copy
import collections
from collections import OrderedDict

### Matrix Manipulation Imports ###
### --------------------------- ###
import numpy as np
import pandas as pd
from pandas import DataFrame as df

### Plotting Imports ###
### ---------------- ###
import matplotlib
matplotlib.use( "agg" )
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns

### Matplotlib Config ###
### ----------------- ###
colormap = list( plt.cm.tab20.colors )
plt.rcParams[ "legend.title_fontsize" ] = "xx-small"
plt.rcParams[ "axes.prop_cycle"       ] = matplotlib.cycler( color = colormap )

### Internal R(K*) Imports ###
### ---------------------- ###
from ml_share import samples
from ml_share import utils
from ml_share import ml_config
from ml_share import plotnames


### NN Score Manipulation ###
### --------------------- ###

_conjugate_features_info = {}
_conjugate_features_info[ "B_mass_to_plot"    ] = {
    "bd_combination"    : "B_mass",
    "bdbar_combination" : "Bbar_mass",
    "name_latex"        : "selected " + plotnames.features_histos_dict[ "B_mass" ][ 0 ],
    "xlim"              : [ 4000.0, 5700.0 ],
    "axvline"           : { "value": ml_config.PDG[ "B_mass" ], "label": "PDG $m(B_d^0)$"  },
    "nbins"             : 60
}
_conjugate_features_info[ "Kstar_mass_to_plot" ] = {
    "bd_combination"    : "diMeson_Kpi_mass",
    "bdbar_combination" : "diMeson_piK_mass",
    "name_latex"        : "selected $m(K^{*0})$ [MeV]",
    "xlim"              : [ 690.0, 1110.0 ],
    "axvline"           : { "value": ml_config.PDG[ "Kstar_mass" ], "label": "PDG $m(K^{*0})$"  },
    "nbins"             : 60
}
def get_conjugate_features_info( conjugation_choice_by = "classical" ):
    conjugate_features_info = copy.deepcopy( _conjugate_features_info )
    for feature_name in _conjugate_features_info.keys():
        conjugate_features_info[ feature_name ][ "name_latex" ] = conjugation_choice_by + "-" + conjugate_features_info[ feature_name ][ "name_latex" ]
        conjugate_features_info[ feature_name + "_" + conjugation_choice_by ] = copy.deepcopy( conjugate_features_info[ feature_name ] )
        del conjugate_features_info[ feature_name ]
    return conjugate_features_info

def cook_features( df, feature_names = [], nn_suffix = False, conjugation_choice_by = "classical", return_cooked_names = False ):
    cooked_names = []
    if nn_suffix:
        nn1_suffix = nn_suffix[ 0 ]
        nn2_suffix = nn_suffix[ 1 ]
    _conjugation_choice_by   = conjugation_choice_by
    if conjugation_choice_by == "NN":
       if not nn_suffix:
           conjugation_choice_fallback_msg = """
           `nn_suffix` is not provided. Cannot use NN for conjugation choice without NN suffixes.
           Falling back on classical conjugation choice!
           """
           conjugation_choice_by = "classical"
    conjugate_features_info = get_conjugate_features_info( conjugation_choice_by )
    for feature_name in feature_names:
        _predict_among_conjugations   = feature_name in _conjugate_features_info.keys()
        if _predict_among_conjugations:
            feature_name = feature_name + "_" + conjugation_choice_by
            bd_combination_feature    = conjugate_features_info[ feature_name ][ "bd_combination"    ]
            bdbar_combination_feature = conjugate_features_info[ feature_name ][ "bdbar_combination" ]
            if conjugation_choice_by == "NN":
                df[ "conjugation_choice" + "_" + nn1_suffix ]  = ( df[ "proba_" + nn1_suffix + "_300590" ] >= df[ "proba_" + nn1_suffix + "_300591" ] )
                df[ "conjugation_choice" + "_" + nn2_suffix ]  = ( df[ "proba_" + nn2_suffix + "_300590" ] >= df[ "proba_" + nn2_suffix + "_300591" ] )
                df[ "network_choice" + "_" + nn1_suffix + "_" + nn2_suffix ] = ( df[ "total_score" + "_" + nn1_suffix  ] >= df[ "total_score" + "_" + nn2_suffix ]  )
                df[ feature_name + "_" + nn1_suffix ]  = df[ bd_combination_feature    ] *  df[ "conjugation_choice" + "_" + nn1_suffix ]
                df[ feature_name + "_" + nn1_suffix ] += df[ bdbar_combination_feature ] * ~df[ "conjugation_choice" + "_" + nn1_suffix ]
                df[ feature_name + "_" + nn2_suffix ]  = df[ bd_combination_feature    ] *  df[ "conjugation_choice" + "_" + nn2_suffix ]
                df[ feature_name + "_" + nn2_suffix ] += df[ bdbar_combination_feature ] * ~df[ "conjugation_choice" + "_" + nn2_suffix ]
                df[ feature_name ]  = ( df[  feature_name + "_" + nn1_suffix ] ) *  df[ "network_choice" + "_" + nn1_suffix + "_" + nn2_suffix ]
                df[ feature_name ] += ( df[  feature_name + "_" + nn2_suffix ] ) * ~df[ "network_choice" + "_" + nn1_suffix + "_" + nn2_suffix ]
            if conjugation_choice_by == "classical":
                if _conjugation_choice_by == "NN":
                    print( conjugation_choice_fallback_msg )
                bd_combination_feature         = conjugate_features_info[ feature_name ][ "bd_combination"    ]
                bdbar_combination_feature      = conjugate_features_info[ feature_name ][ "bdbar_combination" ]
                df[ "conjugation_choice_classical" ] = ( ( df[ "diMeson_Kpi_mass" ] - ml_config.PDG[ "Kstar_mass" ] ).abs() <= ( df[ "diMeson_piK_mass" ] - ml_config.PDG[ "Kstar_mass" ] ).abs() )
                df[ feature_name ]  = df[ bd_combination_feature    ] *  df[ "conjugation_choice_classical" ]
                df[ feature_name ] += df[ bdbar_combination_feature ] * ~df[ "conjugation_choice_classical" ]
            if conjugation_choice_by == "both":
                bd_combination_feature = conjugate_features_info[ feature_name ][ "bd_combination"    ]
                bdbar_combination_feature      = conjugate_features_info[ feature_name ][ "bdbar_combination" ]
                df[ feature_name ] = pd.contact( [ df[ bd_combination_feature ], df[ bdbar_combination_feature ] ] )
            cooked_names.append( feature_name )
        elif feature_name == "diMeson_Kpi_piK_mass_farther_PDG":
            is_Kpi_closer = ( ( df[ "diMeson_Kpi_mass" ] - ml_config.PDG[ "Kstar_mass" ] ).abs() <= ( df[ "diMeson_piK_mass" ] - ml_config.PDG[ "Kstar_mass" ] ).abs() )
            is_piK_closer = ~(is_Kpi_closer)
            farther_mass_value = df[ "diMeson_Kpi_mass" ] * is_piK_closer + df[ "diMeson_piK_mass" ] * is_Kpi_closer
            df[ feature_name ] = farther_mass_value
            cooked_names.append( feature_name )
    
    return df if not return_cooked_names else df, cooked_names
        
def cook_scores( df_scores, nn_suffix, score_names = "__all__", return_score_names = False ):
    """
    Description:
        - This function calculates the "advanced" scores out of the
        "basic" scores (the `bd_score` and the `bdbar_score`) that
        the NN model generates.
        - The primary advanced scores are the composite scores created
        using the basic scores coming from a single NN.
        - The secondary advanced scores are the composite scores created
        using the basic and/or primary advanced scores coming from
        multiple NNs (NN1 and NN2).
    Arguments:
        - `df_scores`:
            - `type`: `pandas.DataFrame`
            - Optional: No
            - Description:
                - The dataframe that has the "basic" scores generated by
                the NN model(s) that are necessary to calculate the
                "advanced" scores.
        - `nn_suffix`:
            - `type`: `str` or `list` of `str` objects
            - Optional: No
            - Description:
                - Specifies the tags that specify the NN that is used to
                generate a certain score.
                - For any given NN model, this is the `nn_suffix` key
                under the `application` key in the configuration file of
                that NN model.
                - A list of suffixes should be provided if the scores
                that you want to calculate require reading scores coming
                from multiple NNs.
        - `score_names`:
            - `type`: `str` or `list` of `str` objects
            - Optional: Yes
            - Default Value: "__all__"
            - Allowed Values:
            - Description:
                - Specifies the names of the advanced scores that need
                to be calculated.
                - If valued `"__all__"`, all possible scores will be calculated.
    """
    all_primary_score_names = [
        "total_score",
        "max_score",
        "diff_score",
        "min_score",
    ]
    all_secondary_score_names = [
        "max_total_score",
        "average_total_score",
        "min_total_score"
    ]
    
    ### Determining Scores To Be Calculated ###
    ### ----------------------------------- ###
    if score_names == "__all__":
        score_names = all_primary_score_names + all_secondary_score_names
    primary_score_names   = []
    secondary_score_names = []
    for score_name in score_names:
        if score_name in all_primary_score_names:
            primary_score_names.append( score_name )
        if score_name in all_secondary_score_names:
            secondary_score_names.append( score_name )
    calculated_scores = []
    
    ### Calculating the Scores ###
    ### ---------------------- ###
    for score_name in primary_score_names:
        if score_name not in score_names:
            continue
        for _nn_suffix in nn_suffix:
            _score_name = score_name + "_" + _nn_suffix
            calculated_scores.append( _score_name )
            if score_name == "total_score":
                df_scores[ _score_name ] = df_scores[ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bd_score" ] ] + df_scores[ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bdbar_score" ] ]
            if score_name == "max_score":
                df_scores[ _score_name ] = df_scores[ [ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bd_score" ], "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bdbar_score"] ] ].max( axis = 1 )
            if score_name == "min_score":
                df_scores[ _score_name ] = df_scores[ [ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bd_score" ], "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bdbar_score"] ] ].min( axis = 1 )
            if score_name == "diff_score":
                df_scores[ _score_name ] = ( df_scores[ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bd_score" ] ] - df_scores[ "proba_" + _nn_suffix + "_" + ml_config.aliases[ "bdbar_score" ] ] ).abs()
    for score_name in secondary_score_names:
        if score_name not in score_names:
            continue
        _score_name = score_name + "_" + nn_suffix[ 0 ] + "_" + nn_suffix[ 1 ]
        calculated_scores.append( _score_name )
        if score_name == "max_total_score":
            df_scores[ _score_name ] = df_scores[ [ "total_score" + "_" + nn_suffix[ 0 ], "total_score" + "_" + nn_suffix[ 1 ] ] ].max( axis = 1 )
        if score_name == "average_total_score":
            df_scores[ _score_name ] = df_scores[ [ "total_score" + "_" + nn_suffix[ 0 ], "total_score" + "_" + nn_suffix[ 1 ] ] ].mean( axis = 1 )
        if score_name == "min_total_score":
            df_scores[ _score_name ] = df_scores[ [ "total_score" + "_" + nn_suffix[ 0 ], "total_score" + "_" + nn_suffix[ 1 ] ] ].min( axis = 1 )
    
    return_package = df_scores if not return_score_names else df_scores, calculated_scores
    return return_package


### The Plot Function ###
### ----------------- ###

def plot_features(
        df_dict,
        pp,
        features      = None,
        title         = {},
        colordict     = {},
        xlabel        = {},
        ylabel        = {},
        legend_loc    = {},
        axvline       = {},
        nbins_rescale = 0.5,
        default_nbins = 300,
        default_scale = "log",
        collage       = True #WIP
    ):
    
    dataset_names   = list( df_dict.keys() )
    if features is None:
        features = list( df_dict[ dataset_names[ 0 ] ].keys() )
    
    #if collage:
        #fig_collage, axs_collage, lenX, lenY = utils.create_subplots( len( features ), title = "", magnification = 10, fontsize = 8  )
    
    feature_counter = -1
    for feature_name in features:
        feature_counter += 1
        #ax_collage = axs_collage[ idx_row ][ idx_co]
    
        ### Setting Up the Plot ###
        ### ------------------- ###
        
        fig, ax    = plt.subplots()
        try:
            xlims  = plotnames.features_histos_dict[ feature_name ][ 2:4 ]
        except:
            xlims  = [ 1, 0 ]
        try:
            latex_name = plotnames.features_histos_dict[ feature_name ][ 0 ]
        except:
            latex_name = feature_name
        try:
            nbins  = int( nbins_rescale * plotnames.features_histos_dict[ feature_name ][ 1 ] )
        except:
            nbins  = int( default_nbins * nbins_rescale )
        if xlims  != [ 1, 0 ]:
            binning    = np.linspace( *xlims, nbins )
        else:
            binning = None
        if feature_name in plotnames.features_histos_dict.keys():
            if len( plotnames.features_histos_dict[ feature_name ]  ) > 4:
                yscale = "log" if plotnames.features_histos_dict[ feature_name ][ 4 ] else "linear"
            else:
                yscale = "linear"
        else:
            yscale = default_scale
        
        ### Looping Over Sig/Bkg ###
        ### -------------------- ###
        
        for dataset_name in dataset_names:
            color = None if dataset_name not in colordict.keys() else colordict[ dataset_name ]
            ax.hist(
                df_dict[ dataset_name ][ feature_name ],
                bins     = binning,
                color    = color,
                alpha    = 0.8,
                histtype = "stepfilled",
                density  = True,
                label    = dataset_name
            )
        
        ### Additional Decoration of the Plot ###
        ### --------------------------------- ###
        if feature_name in axvline.keys():
            ax.axvline(
                    axvline[ feature_name ][ "value" ],
                    label     = axvline[ feature_name ][ "label" ],
                    linestyle = "--",
                    color     = "grey",
                    alpha     = 0.8,
                    linewidth = 0.8
            )
        
        ax.legend( loc = "best" if feature_name not in legend_loc else legend_loc[ feature_name ], fontsize = 6 )


        _xlabel_delimiter = "_" if not "delimiter" in xlabel.keys() else xlabel[ "delimiter" ]
        _ylabel_delimiter = "_" if not "delimiter" in ylabel.keys() else ylabel[ "delimiter" ]
        _title_delimiter  = "_" if not "delimiter" in title.keys() else title[ "delimiter" ]

        _xlabel = latex_name
        _xlabel = _xlabel if "prefix" not in xlabel.keys() else xlabel[ "prefix" ] + _xlabel_delimiter + _xlabel
        _xlabel = _xlabel if "suffix" not in xlabel.keys() else _xlabel + _xlabel_delimiter + xlabel[ "suffix" ]

        _ylabel = "candidates"
        _ylabel = _ylabel if "prefix" not in ylabel.keys() else ylabel[ "prefix" ] + _ylabel_delimiter + _ylabel
        _ylabel = _ylabel if "suffix" not in ylabel.keys() else _ylabel + _ylabel_delimiter + ylabel[ "suffix" ]
        
        _title  = feature_name + " ( %s ) " % ( latex_name )
        _title  = _title if "prefix" not in title.keys() else title[ "prefix" ] + _title_delimiter + _title
        _title  = _title if "suffix" not in title.keys() else _title + _title_delimiter + title[ "suffix" ]
        
        ax.set_xlabel( _xlabel, fontsize = 8, loc = "right" )
        ax.set_ylabel( _ylabel, fontsize = 8, loc = "top"   )
        ax.set_title( _title, fontsize = 6 )
        ax.set_yscale( yscale )

        ax.tick_params( axis = "both", which = "major", labelsize = 6 )
        ax.set_aspect( 1.0 / ax.get_data_ratio() )
        
        pp.savefig( fig )


### Plotting 2d Plots ###
### ----------------- ###
def plot_2d_plots( df_dict, pp, features = False, title = "", colormap = {}, df_correl = False, scale = 0.5, default_nbins = 100 ):
    features_x = list( df_dict.keys() )
    features_y = list( df_dict.keys() )
    done = []
    for f_x in features_x:
        for f_y in features_y:
            if features:
                if f_x not in features:
                    continue
                if f_y not in features:
                    continue
            if set( [ f_x, f_y ] ) in done:
                 continue
            print( "Preparing 2d Plots for X-feature: %s, Y-feature: %s" % ( f_x, f_y ) )
            x_arr = df_dict[ f_x ]
            y_arr = df_dict[ f_y ]
            _title = title
            correl = False
            if not isinstance( df_correl, bool ):
                try:
                    correl = df_correl[ f_x ][ f_y ]
                except:
                    pass
            if correl:
                _title = title + "\n" + "Correlation:" + str( round( correl, 4 ) )
            try:
                x_label = plotnames.features_histos_dict[ f_x ][ 0 ]
                x_range = plotnames.features_histos_dict[ f_x ][ 2:4 ]
                x_bins  = int( plotnames.features_histos_dict[ f_x ][ 1 ] * scale )
            except:
                x_label = f_x
                x_range = [ 0, 1 ]
                x_bins  = int( default_nbins * scale )
            try:
                y_label = plotnames.features_histos_dict[ f_y ][ 0 ]
                y_range = plotnames.features_histos_dict[ f_y ][ 2:4 ]
                y_bins  = int( plotnames.features_histos_dict[ f_y ][ 1 ] * scale )
            except:
                y_label = f_y
                y_range = [ 0, 1 ]
                y_bins  = int( default_nbins * scale )
                
            fig = utils.plot_profiles( x_arr, y_arr, x_label = x_label, y_label = y_label, x_range = x_range, y_range = y_range, x_bins = x_bins, y_bins = y_bins, title = _title )
            pp.savefig( fig )
            done.append( set( [ f_x, f_y ] ) )
            

### Plotting Correlation Matrix ###
### --------------------------- ###

def plot_correlation_matrix( df_dict, pp, features = False, title = {}, suptitle = "", colormap = plt.cm.coolwarm ):
    print("enters correlation matrix function")
    fig, ax = plt.subplots( 1, figsize = ( 15, 12 ) )
    if not features:
        correl_matrix = df_dict.corr()
    else:
        correl_matrix = df_dict.corr()
    
    cmap = colormap
    ax   = sns.heatmap( correl_matrix, vmin = -1, vmax = 1, cmap = cmap, xticklabels = 1, yticklabels = 1, center = 0.00, annot = True, fmt = ".1%", annot_kws = { "size" : 3 } )
    
    _title = suptitle + "\n"
    _title_delimiter  = "_" if not "delimiter" in title.keys() else title[ "delimiter" ]
    _title = _title if "prefix" not in title.keys() else title[ "prefix" ] + _title_delimiter + _title
    _title = _title if "suffix" not in title.keys() else _title + _title_delimiter + title[ "suffix" ]
    
    ax.set_title( _title, fontsize = 6 )
    ax.set_xticklabels( ax.get_xmajorticklabels(), fontsize = 6 )
    ax.set_yticklabels( ax.get_ymajorticklabels(), fontsize = 6 )
    cbar = ax.collections[ 0 ].colorbar
    cbar.ax.tick_params( labelsize = 6 )
    cbar.set_label( "",  labelpad = 10)
    #fig.suptitle( suptitle, fontsize = 6 )
    fig.tight_layout()
    print("comes here")
    pp.savefig( fig )
    return correl_matrix

### Utilities ###
### --------- ###

def make_it_snakey( string, delimiter = "-", lower = False ):
    string = str( string )
    if lower:
        string = string.lower()
    empty_replces      = [ "[", "]", "(", ")", "{", "}", "$", "^", "'", "\"" ]
    delimiter_replaces = [ "-", ".", "_", ",", " ", ":", ";", "/", "\\" ]
    custom_replaces    = { ">": "gt", "<": "lt", "=": "eq", "&": "and", "|": "or"}
    for empty_replce_target in empty_replces:
        string = string.replace( empty_replce_target, "" )
    for delimiter_replace_target in delimiter_replaces:
        string = string.replace( delimiter_replace_target, delimiter )
    for custom_replace_target in custom_replaces.keys():
        string = string.replace( custom_replace_target, custom_replaces[ custom_replace_target ] )
    return string
    
def main( config_file, config_file_default = "configs/default.yaml", proba_config_files = [] ):

    ### Loading the Config ###
    ### ------------------ ###
    
    config      = utils.config_load( config_file_default, config_file )
    config_name = path.splitext( path.basename( config_file ) )[ 0 ]
    
    config_file_nn1 = False if len( proba_config_files ) == 0 else proba_config_files[ 0 ]
    config_file_nn2 = False if len( proba_config_files )  < 2 else proba_config_files[ 1 ]
    
    config_nn1 = False if not config_file_nn1 else utils.config_load( config_file_default, config_file_nn1 )
    config_nn2 = False if not config_file_nn2 else utils.config_load( config_file_default, config_file_nn2 )
    
    config_name_nn1 = False if not config_file_nn1 else path.splitext( path.basename( config_file_nn1 ) )[ 0 ]
    config_name_nn2 = False if not config_file_nn2 else path.splitext( path.basename( config_file_nn2 ) )[ 0 ]
    
    nn1_suffix = False if not config_nn1 else config_nn1[ "application" ].get( "nn_suffix" )
    nn2_suffix = False if not config_nn2 else config_nn2[ "application" ].get( "nn_suffix" )
    
    if config_nn1:
        config_nn1[ "common" ][ "config_name" ] = config_name_nn1
    if config_nn2:
        config_nn2[ "common" ][ "config_name" ] = config_name_nn2

    output_dir_nn1  = False if not config_nn1 else config_nn1[ "application" ].get( "output_dir" )
    output_dir_nn2  = False if not config_nn2 else config_nn2[ "application" ].get( "output_dir" )
    input_dir_nn1   = False if not config_nn1 else config_nn1[ "application" ].get( "input_dir"  )
    input_dir_nn2   = False if not config_nn2 else config_nn2[ "application" ].get( "input_dir"  )

        
    ### Prepare the Plotting Utilities ###
    ### ------------------------------ ###
    
    axvline = {}
    axvline[ "B_mass"           ] = { "value": ml_config.PDG[ "Bd_mass"    ], "label": "PDG $m(B_d^0)$"      }
    axvline[ "Bbar_mass"        ] = { "value": ml_config.PDG[ "Bd_mass"    ], "label": "PDG $m(B_d^0)$"      }
    axvline[ "B_mass_Kstar_mass_closer" ] = { "value": ml_config.PDG[ "Bd_mass"    ], "label": "PDG $m(B_d^0)$"      }
    axvline[ "B_mass_KK_hypo"   ] = { "value": ml_config.PDG[ "Bd_mass"    ], "label": "PDG $m(B_d^0)$"      }
    axvline[ "diMeson_piK_mass" ] = { "value": ml_config.PDG[ "Kstar_mass" ], "label": "PDG $m(K^{\\ast0})$" }
    axvline[ "diMeson_Kpi_mass" ] = { "value": ml_config.PDG[ "Kstar_mass" ], "label": "PDG $m(K^{\\ast0})$" }

    ### Preparing Selections ###
    ### -------------------- ###
    
    ### Preparing Selections: Global Selection ###
    ### -------------------------------------- ###
    
    selections_dict = {}
    for item in [ "main", "q2low", "q2high", "usr", "nn1trb", "nn2trb", "mc", "truth" ]:
        print("item", item)
        selections_dict[ item ] = utils.get_selection( config[ "common" ].get( "selection_" + item ), config[ "common" ] )
    
    final_selections = {}
    _global_selection_usr_q2low     = selections_dict[ "main" ] + " & " + selections_dict[ "usr"    ] + " & " + selections_dict[ "q2low"  ]
    _global_selection_usr_q2high    = selections_dict[ "main" ] + " & " + selections_dict[ "usr"    ] + " & " + selections_dict[ "q2high" ]
    _global_selection_usr_or_nn1trb_q2low     = selections_dict[ "main" ] + " & " + " ( " + selections_dict[ "usr" ] + " | " + selections_dict[ "nn1trb" ] + " ) " + " & " + selections_dict[ "q2low"  ]
    _global_selection_usr_or_nn1trb_q2high    = selections_dict[ "main" ] + " & " + " ( " + selections_dict[ "usr" ] + " | " + selections_dict[ "nn1trb" ] + " ) " + " & " + selections_dict[ "q2high" ]
    _global_selection_nn1trb_q2low  = selections_dict[ "main" ] + " & " + selections_dict[ "nn1trb" ] + " & " + selections_dict[ "q2low"  ]
    _global_selection_nn1trb_q2high = selections_dict[ "main" ] + " & " + selections_dict[ "nn1trb" ] + " & " + selections_dict[ "q2high" ]
    _global_selection_nn2trb_q2low  = selections_dict[ "main" ] + " & " + selections_dict[ "nn2trb" ] + " & " + selections_dict[ "q2low"  ]
    _global_selection_nn2trb_q2high = selections_dict[ "main" ] + " & " + selections_dict[ "nn2trb" ] + " & " + selections_dict[ "q2high" ]

    #final_selections[ "USR in Low$-q^2$ Bin"          ] = { "signal": { "val": _global_selection_usr_q2low , "name": "usr-in-low-q2" }, "background": { "val": _global_selection_usr_q2low    , "name": "usr-in-low-q2"     } }
    #final_selections[ "USR in High$-q^2$ Bin"         ] = { "signal": { "val": _global_selection_usr_q2high, "name": "usr-in-high-q2"}, "background": { "val": _global_selection_usr_q2high   , "name": "usr-in-high-q2"    } }
    final_selections[ "USR+NN1TR in Low$-q^2$ Bin"    ] = { "signal": { "val": _global_selection_usr_or_nn1trb_q2low , "name": "usr-or-nn1trb-in-low-q2" }, "background": { "val": _global_selection_usr_or_nn1trb_q2low    , "name": "usr-or-nn1trb-in-low-q2"  } }
    final_selections[ "USR+NN1TR in High$-q^2$ Bin"   ] = { "signal": { "val": _global_selection_usr_or_nn1trb_q2high, "name": "usr-or-nn1trb-in-high-q2"}, "background": { "val": _global_selection_usr_or_nn1trb_q2high   , "name": "usr-or-nn1trb-in-high-q2" } }
    #final_selections[ "NN1TR in Low$-q^2$ Bin"        ] = { "signal": { "val": _global_selection_usr_q2low , "name": "usr-in-low-q2" }, "background": { "val": _global_selection_nn1trb_q2low , "name": "nn1trb-in-low-q2"  } }
    #final_selections[ "NN1TR in High$-q^2$ Bin"       ] = { "signal": { "val": _global_selection_usr_q2high, "name": "usr-in-high-q2"}, "background": { "val": _global_selection_nn1trb_q2high, "name": "nn1trb-in-high-q2" } }
    #final_selections[ "NN2TR in Low$-q^2$ Bin"        ] = { "signal": { "val": _global_selection_usr_q2low , "name": "usr-in-low-q2" }, "background": { "val": _global_selection_nn2trb_q2low , "name": "nn2trb-in-low-q2"  } }
    #final_selections[ "NN2TR in High$-q^2$ Bin"       ] = { "signal": { "val": _global_selection_usr_q2high, "name": "usr-in-high-q2"}, "background": { "val": _global_selection_nn2trb_q2high, "name": "nn2trb-in-high-q2" } }

    nn1_cuts = [ 0. ]
    nn2_cuts = [ 0. ]

    nn_based_final_selections = {}
    for _final_selection_name in final_selections.keys():
        for nn1_cut_value in nn1_cuts:
            for nn2_cut_value in nn2_cuts:
                _key = _final_selection_name + ", total_score_%s >= %s & total_score_%s >= %s" % ( nn1_suffix, nn1_cut_value, nn2_suffix, nn2_cut_value )
                nn_based_final_selections[ _key ] = {}
                nn_based_final_selections[ _key ][ "signal"     ] = {}
                nn_based_final_selections[ _key ][ "background" ] = {}
                _nn_selection_string = "( ( df[ 'total_score_%s' ] >= %s ) & ( df[ 'total_score_%s' ] >= %s ) )" % ( nn1_suffix, nn1_cut_value, nn2_suffix, nn2_cut_value )
                nn_based_final_selections[ _key ][ "signal"     ][ "val"  ] = "( " + final_selections[ _final_selection_name ][ "signal"     ][ "val" ] + " ) & ( " + _nn_selection_string + " )"
                nn_based_final_selections[ _key ][ "background" ][ "val"  ] = "( " + final_selections[ _final_selection_name ][ "background" ][ "val" ] + " ) & ( " + _nn_selection_string + " )"
                nn_based_final_selections[ _key ][ "signal"     ][ "name" ] = final_selections[ _final_selection_name ][ "signal"     ][ "name" ] + "-total-score-%s-%s-total-score-%s-%s" % ( nn1_suffix, nn1_cut_value, nn2_suffix, nn2_cut_value )
                nn_based_final_selections[ _key ][ "background" ][ "name" ] = final_selections[ _final_selection_name ][ "background" ][ "name" ] + "-total-score-%s-%s-total-score-%s-%s" % ( nn1_suffix, nn1_cut_value, nn2_suffix, nn2_cut_value )

    for _nn_based_final_selection_name in nn_based_final_selections.keys():
        final_selections[ _nn_based_final_selection_name ] = nn_based_final_selections[ _nn_based_final_selection_name ]
    
    ### Preparing Selections: Local Selection ###
    ### ------------------------------------- ###
    
    local_selection_lib = {}
    local_selection_lib[ "None"                                     ] = { "absolute": False, "relative": False }
    local_selection_lib[ "Truth-Matching"                           ] = { "absolute": selections_dict[ "truth"], "relative": False }
    local_selection_lib[ "Best$-1$ $L_{xy}-$Significance"           ] = { "absolute": False, "relative": { "feature": "Lxy_significance"   , "n": 1, "sort": "highest" } }
    local_selection_lib[ "Best$-1$ $\chi^2_B$"           ] = { "absolute": False, "relative": { "feature": "B_chi2"   , "n": 1, "sort": "lowest" } }
    local_selection_lib[ "Best$-6$ $L_{xy}-$Significance"           ] = { "absolute": False, "relative": { "feature": "Lxy_significance"   , "n": 6, "sort": "highest" } }
    local_selection_lib[ "Best$-6$ $\chi^2_B$"           ] = { "absolute": False, "relative": { "feature": "B_chi2"   , "n": 6, "sort": "lowest" } }
    local_selection_lib[ "NN-based" ] = { "absolute": False, "relative": { "feature": "max_total_score_%s_%s"%( nn1_suffix, nn2_suffix ), "n": 1, "sort": "highest" } }
    local_selection_lib[ "Truth-Matched + NN-based" ] = { "absolute": selections_dict["truth"], "relative": { "feature": "max_total_score_%s_%s"%( nn1_suffix, nn2_suffix ), "n": 1, "sort": "highest" } }
    local_selections = [ 
            { "signal": "Best$-1$ $L_{xy}-$Significance", "background": "Best$-1$ $L_{xy}-$Significance" }, 
            { "signal": "Best$-6$ $L_{xy}-$Significance", "background": "Best$-6$ $L_{xy}-$Significance" },
            { "signal": "Truth-Matching", "background": "None"}, 
            { "signal": "None", "background": "None" },
            #{ "signal": "Truth-Matching", "background": "Truth-Matching" } #WIP
            { "signal": "NN-based", "background": "NN-based" },
            { "signal": "Best$-1$ $\chi^2_B$", "background": "Best$-1$ $\chi^2_B$" }, 
            { "signal": "Best$-6$ $\chi^2_B$", "background": "Best$-6$ $\chi^2_B$" }, 
            { "signal": "Truth-Matched + NN-based", "background": "NN-based" }
            ]
    
    ### Preparing Files ###
    ### --------------- ###
    
    input_files_non_res_bd_signal    = [ "ntuple-300590_part_[0-9][0-9].ftr"     ]
    input_files_non_res_bdbar_signal = [ "ntuple-300591_part_[0-9][0-9].ftr"     ]
    input_files_res_bd_signal        = [ "ntuple-300592_part_[0-9][0-9].ftr"     ]
    input_files_res_bdbar_signal     = [ "ntuple-300593_part_[0-9][0-9].ftr"     ]
    input_files_non_res_signal       = [ "ntuple-30059[0-1]_part_[0-9][0-9].ftr" ]
    input_files_res_signal           = [ "ntuple-30059[2-3]_part_[0-9][0-9].ftr" ]

    #input_files_non_res_bd_signal    = [ "ntuple-300590_part_[0][0-1].ftr"     ] #quick
    #input_files_non_res_bdbar_signal = [ "ntuple-300591_part_[0][0-1].ftr"     ] #quick
    #input_files_res_bd_signal        = [ "ntuple-300592_part_[0][0-1].ftr"     ] #quick
    #input_files_res_bdbar_signal     = [ "ntuple-300593_part_[0][0-1].ftr"     ] #quick
    #input_files_non_res_signal       = [ "ntuple-30059[0-1]_part_[0][0-1].ftr" ] #quick
    #input_files_res_signal           = [ "ntuple-30059[2-3]_part_[0][0-1].ftr" ] #quick

    input_files_signal_dict = {}
    #input_files_signal_dict[ "Non-Resonant $B_d^0$ Signal"          ] = input_files_non_res_bd_signal
    #input_files_signal_dict[ "Non-Resonant $\\bar{B}_d^0$ Signal"   ] = input_files_non_res_bdbar_signal
    input_files_signal_dict[ "Resonant Signal"                      ] = input_files_res_signal
    #input_files_signal_dict[ "Non-Resonant Signal"                  ] = input_files_non_res_signal
    #input_files_signal_dict[ "Resonant $B_d^0$ Signal"              ] = input_files_res_bd_signal
    #input_files_signal_dict[ "Resonant $\\bar{B}_d^0$ Signal"       ] = input_files_res_bdbar_signal
    
    #background_ids = [ "data" ]#, [ "300734", "300735" ], [ "801402", "801403" ], [ "801414", "801415" ], [ "801418", "801419"], [ "300580", "300581" ] ]
    background_ids = [ "data_period[K]"]

    """
    80141[4-5]: Bs -> K+K- J/psi(ee)
    80141[8-9]: Bs -> pi+pi- J/psi(ee)
    30058[0-1]: B+ -> K+ J/psi(ee)
    30073[4-5]: B0 -> K+pi- J/psi(ee)
    80140[2-3]: B0 -> D-(K*(K+pi-)e-nu)e+nu
    """

    input_dir                  = config[ "application" ][ "input_dir" ]
    local_aux_items            = [ "info_sample", "info_truth_matching", "info_event_number" ] + [ "positron_charge", "electron_charge", "trackPlus_charge", "trackMinus_charge" ]
    local_main_items           = [ "B_mass_KK_hypo", "diMeson_Kpi_piK_mass_closer_PDG" ]
    local_cook_features        = [ "B_mass_to_plot", "Kstar_mass_to_plot", "diMeson_Kpi_piK_mass_farther_PDG" ]
    proba_items_to_load        = []
    
    if nn1_suffix:
        proba_items_to_load   += [ "proba_" + nn1_suffix + "_" + ml_config.nr_signal_id[ 0 ] ]
        proba_items_to_load   += [ "proba_" + nn1_suffix + "_" + ml_config.nr_signal_id[ 1 ] ]

    if nn2_suffix:
        proba_items_to_load   += [ "proba_" + nn2_suffix + "_" + ml_config.nr_signal_id[ 0 ] ]
        proba_items_to_load   += [ "proba_" + nn2_suffix + "_" + ml_config.nr_signal_id[ 1 ] ]
        
    items_to_load              = config[ "common" ][ "features" ] + config[ "training" ][ "additional_features_to_plot" ] + local_aux_items + local_main_items + proba_items_to_load
    
    proba_files_nn1 = False if not config_nn1 else path.join( output_dir_nn1, config_name_nn1 + "_applied", "*_" + nn1_suffix + "_proba.ftr" )
    proba_files_nn2 = False if not config_nn2 else path.join( output_dir_nn2, config_name_nn2 + "_applied", "*_" + nn2_suffix + "_proba.ftr" )
    proba_files_to_load = []
    
    if proba_files_nn1:
        proba_files_to_load += [ proba_files_nn1 ]
    if proba_files_nn2:
        proba_files_to_load += [ proba_files_nn2 ]
        
    ### Looping Over Two Channels of the Signal ###
    ### --------------------------------------- ###
    
    signals_done     = []
    backgrounds_done = []
    
    for signal_choice in input_files_signal_dict.keys():
        
        ### Loading Signal Files ###
        ### -------------------- ###
        
        input_files_signal = input_files_signal_dict[ signal_choice ]
        input_files_signal = utils.get_list_make_list([ os.path.join( input_dir, file ) for file in input_files_signal ] )
        print( "proba_files_to_load", proba_files_to_load )
        print( "input_files_signal", input_files_signal)
        if len( proba_files_to_load ) > 0:
            df_signal          = utils.df_load(
                                 input_files_signal,
                                 selection     = selections_dict[ "main" ],
                                 items_to_load = items_to_load,
                                 probabilities = proba_files_to_load
                             )
        else:
            df_signal          = utils.df_load(
                                 input_files_signal,
                                 selection     = selections_dict[ "main" ],
                                 items_to_load = items_to_load )
        cooked_features_nn = []
        if nn1_suffix and nn2_suffix:
            df_signal, calculated_scores = cook_scores( df_signal, [ nn1_suffix, nn2_suffix ], score_names = [ "total_score", "max_total_score" ] )
            df_signal, cooked_features_nn = cook_features( df_signal, local_cook_features, nn_suffix = [ nn1_suffix, nn2_suffix ], conjugation_choice_by = "NN", return_cooked_names = True )
        df_signal, cooked_features_classical = cook_features( df_signal, local_cook_features, nn_suffix = [ nn1_suffix, nn2_suffix ], conjugation_choice_by = "classical", return_cooked_names = True )
        cooked_features =  cooked_features_nn + cooked_features_classical
        print( "raw df_signal length", len( df_signal ) )
        ### Looping Over Different Backgrounds ###
        ### ---------------------------------- ###
        
        for background_id in background_ids:
            
            ### Loading Background Files ###
            ### ------------------------ ###
            
            if background_id == "data":
                input_files_background = [ "ntuple-data18_13TeV_period[A-Z]_part_[0-9][0-9].ftr" ]
            elif "data" in background_id: # background_id should be of the form data_period[X-Y]. 
                input_files_background = [ "ntuple-data18_13TeV_%s_part_[0-9][0-9].ftr" % background_id[ 5: ] ]
            else:
                input_files_background = []
                for dsid in background_id:
                    input_files_background.append( "ntuple-%s_part_[0-9][0-9].ftr" % ( dsid ) )
                    #input_files_background.append( "ntuple-%s_part_[0][0-1].ftr" % ( dsid ) ) #quick

            
            background_name = background_id if "data" in background_id else samples.samples[ background_id[ 0 ] ][ "name_latex" ]
            _input_files    = utils.get_list_make_list( [ os.path.join( input_dir, file ) for file in input_files_background ] )
            df_background   = utils.df_load(
                        _input_files,
                        selection     = selections_dict[ "main" ],
                        items_to_load = items_to_load,
                        probabilities = proba_files_to_load
                )
            cooked_features_nn = []
            if nn1_suffix and nn2_suffix:
                df_background, calculated_scores = cook_scores( df_background, [ nn1_suffix, nn2_suffix ], score_names = [ "total_score", "max_total_score" ])
                df_background, cooked_features_nn   = cook_features( df_background, local_cook_features, nn_suffix = [ nn1_suffix, nn2_suffix ], conjugation_choice_by = "NN", return_cooked_names = True )
            df_background, cooked_features_classical         = cook_features( df_background, local_cook_features, nn_suffix = [ nn1_suffix, nn2_suffix ], conjugation_choice_by = "classical", return_cooked_names = True )
            cooked_features = cooked_features_nn + cooked_features_classical
            print("raw df_background length", len( df_background ) )
                
            ### Looping Over Different Selections ###
            ### --------------------------------- ###
            
            for final_selection_name in final_selections.keys():
                final_selection = final_selections[ final_selection_name ]

            
                ### Looping Over Local Selection Schemes ###
                ### ------------------------------------ ###
                
                for local_selection_scheme in local_selections:
                    
                    df_dict = {}

                    ### Applying Global Selection ###
                    ### ------------------------- ###
                    
                    df_dict[ signal_choice   ] = utils.df_view( df_signal    , final_selection[ "signal" ][ "val"] )
                    df_dict[ background_name ] = utils.df_view( df_background, final_selection[ "background" ]["val"] )
                
                    ### Applying Local Selection ###
                    ### ------------------------ ###

                    signal_local_selection = local_selection_lib[ local_selection_scheme[ "signal" ] ]
                    if signal_local_selection[ "absolute" ]:
                        df_dict[ signal_choice ] = utils.df_view( df_dict[ signal_choice ], selection = signal_local_selection[ "absolute" ] )
                    if signal_local_selection[ "relative" ]:
                        _n = signal_local_selection[ "relative" ][ "n" ]
                        _feature = signal_local_selection[ "relative" ][ "feature" ]
                        _sort = signal_local_selection[ "relative" ][ "sort" ]
                        df_dict[ signal_choice ] = utils.get_n_best( df_dict[ signal_choice ], n = _n, feature = _feature, sort = _sort )
                    background_local_selection = local_selection_lib[ local_selection_scheme[ "background" ] ]
                    if background_local_selection[ "absolute" ]:
                        df_dict[ background_name ] = utils.df_view( df_dict[ background_name ], selection = background_local_selection[ "absolute" ] )
                    if background_local_selection[ "relative" ]:
                        _n = background_local_selection[ "relative" ][ "n" ]
                        _feature = background_local_selection[ "relative" ][ "feature" ]
                        _sort = background_local_selection[ "relative" ][ "sort" ]
                        df_dict[ background_name ] = utils.get_n_best( df_dict[ background_name ], n = _n, feature = _feature, sort = _sort )
                    print("signal name", signal_choice)
                    print("background name", background_name )
                    print( "signal_df length", len(df_dict[signal_choice ]))
                    print( "background_df length", len(df_dict[ background_name ]))
                    
                    signal_barcode     = [ make_it_snakey( signal_choice ), make_it_snakey( final_selection[ "signal" ][ "name" ] ), make_it_snakey( local_selection_scheme[ "signal"     ] ) ]
                    background_barcode = [ make_it_snakey( background_name ), make_it_snakey( final_selection[ "background" ][ "name"]  ), make_it_snakey( local_selection_scheme[ "background" ] ) ]

                    ### Plotting Features ###
                    ### ----------------- ###
                    
                    title      = "Signal: %s (Local Selection: %s)" % ( signal_choice, local_selection_scheme[ "signal" ] )
                    title     += "\n Background: %s (Local Selection: %s)" % ( background_name, local_selection_scheme[ "background" ] )
                    title     += "\n " + final_selection_name
                    colordict  = { signal_choice: "tab:red", background_name: "tab:blue" }
                    _filename  = "feature-plots-in-region-%s-signal-%s-local-selection-%s-background-%s-local-selection-%s.pdf" % ( make_it_snakey( final_selection_name ), signal_barcode[ 0 ], signal_barcode[ 2 ], background_barcode[ 0 ], background_barcode[ 2 ] )
                    filename   = _filename
                    pp         = PdfPages( os.path.join( config_name, filename ) )
                    
                    plot_features(
                        df_dict    = df_dict,
                        pp         = pp,
                        features   = config[ "common" ][ "features" ] + config[ "training" ][ "additional_features_to_plot" ] + local_main_items + proba_items_to_load + calculated_scores + cooked_features,
                        title      = { "prefix": title, "delimiter": "\n" },
                        colordict  = colordict,
                        xlabel     = {},
                        ylabel     = {},
                        legend_loc = {},
                        axvline    = axvline
                    )
                    
                    pp.close()
                    
                    ### Plotting 2d Plots of Feature-Tuples ###
                    ### ----------------------------------- ###
                    """
                    for dataset_name in df_dict.keys():
                        
                        if signal_choice == dataset_name and signal_barcode in signals_done:
                            continue
                        if background_name == dataset_name and background_barcode in backgrounds_done:
                            continue
                        if signal_choice == dataset_name:
                            title      = "Signal: %s (Local Selection: %s)" % ( signal_choice, local_selection_scheme[ "signal" ] )
                            _filename  = "feature-plots-in-region-%s-signal-%s-local-selection-%s.pdf" % ( signal_barcode[ 1 ], signal_barcode[ 0 ], signal_barcode[ 2 ] )

                        elif background_name == dataset_name:
                            title      = "Background: %s (Local Selection: %s)" % ( background_name, local_selection_scheme[ "background" ] )
                            _filename  = "feature-plots-in-region-%s-background-%s-local-selection-%s.pdf" % ( background_barcode[ 1 ], background_barcode[ 0 ], background_barcode[ 2 ] )
                        else:
                            print("background_id", background_id )
                            print("signal_choice", signal_choice )
                            print("dataset name", dataset_name)
                            print("WTF!!")
                            quit()
                        title     += "\n " + final_selection_name
                        
                        ### Plotting Correlation Matrix ###
                        ### --------------------------- ###
                        
                        filename = _filename.replace( "feature-plots", "correlation-matrix" )
                        pp       = PdfPages( os.path.join( config_name, filename ) )
                        features = config[ "common" ][ "features" ] + config[ "training" ][ "additional_features_to_plot" ] + local_main_items + proba_items_to_load + calculated_scores + cooked_features
                       
                        correl_matrix = False
                        #try:
                        correl_matrix = plot_correlation_matrix( df_dict[ dataset_name ], pp, features = features, suptitle = title, colormap = plt.cm.coolwarm )
                        #except:
                        #    pass
                        #if correl_matrix:
                        c_filename = filename.replace( ".pdf", ".csv" )
                        correl_matrix.to_csv( os.path.join( config_name, c_filename ) )

                        pp.close()
                        
                        ### Plotting 2d Plots & Profile Plots ###
                        ### --------------------------------- ###
                        
                        filename  = _filename.replace( "feature-plots", "feature-2d-analysis" )
                        pp        = PdfPages( os.path.join( config_name, filename ) )
                        print( "dataset_name", dataset_name )
                        print( "filename", filename )
                        #features = config[ "common" ][ "features" ] + config[ "training" ][ "additional_features_to_plot" ] + local_main_items + proba_items_to_load + calculated_scores
                        features  = config[ "training" ][ "additional_features_to_plot" ] + local_main_items + proba_items_to_load + calculated_scores + cooked_features

                        print( "title", title )
                        plot_2d_plots( df_dict[ dataset_name ], pp, features = features, title = title, df_correl = correl_matrix )
                        pp.close()
                        
                        ### Recording History ###
                        ### ----------------- ###
                        
                        signals_done.append( signal_barcode )
                        backgrounds_done.append( background_barcode )
                    """

if __name__ == "__main__":
    proba_config_files = []
    config_file = sys.argv[ 1 ]
    if len(sys.argv) > 2:
        proba_config_files = [ sys.argv[ 1 ], sys.argv[ 2 ] ]
    main( config_file = config_file, proba_config_files = proba_config_files )
