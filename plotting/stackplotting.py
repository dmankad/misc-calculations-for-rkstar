
def stackplot(
        to_stack_data,
        to_stack_labels,
        to_stack_histtypes = None,
        xmin               = None,
        xmax               = None,
        nbins              = None,
        binning            = False,
        colors             = False,
        alpha              = 0.8,
        xlabel             = False,
        ylabel             = False,
        title              = False,
        suptitle           = False,
        fontsize           = 8,
        facecolor          = False,
        pp                 = False
    ):
    
    # Setting Up the Plot
    if to_stack_histtypes is None:
        to_stack_histtypes = [ "stepfilled" ] * len( to_stack_data )
    if not colors:
        colors  = list( plt.cm.Dark2.colors )
    if not binning:
        if [ xmin, xmax, nbins ] != [ None ] * 3:
            binning = np.linspace( xmin, xmax, nbins )

    matplotlib.rcParams[ "axes.prop_cycle" ] = matplotlib.cycler( color = colors )
    
    # Plotting a Stacked Histogram With Non-Uniform `histtypes`s
    #if ax is None:
    fig, ax = plt.subplots()
    
    ## Manually Calculating the Stacked Histogram Heights
    stacked_heights = []
    _history = np.zeros( len( binning ) - 1 )
    for data in to_stack_data:
        height, _bins = np.histogram( data, bins = binning )
        stacked_heights.append( _history + height )
        _history = stacked_heights[ -1 ]
    
    ## Plotting a Step-Plot With the Calculated Heights
    binning = [ 0.5 * ( _bins[ counter ] + _bins[ counter + 1 ] ) for counter in range( len( _bins ) - 1 ) ]
    fill_target = np.zeros( len( binning ) )
    counter = -1
    handles = []
    if facecolor:
        if isinstance( facecolor, dict ):
            _facecolor = facecolor[ "color" ]
        else:
            _facecolor = facecolor
    else:
        _facecolor = "white"
    for data in stacked_heights:
        counter += 1
        ax.step( binning, data, where = "mid", linewidth = 0.8, alpha = alpha )
        if to_stack_histtypes[ counter ] == "stepfilled":
            ax.fill_between(
                binning,
                data,
                fill_target,
                color = colors[ counter ],
                step  = "mid",
                alpha = alpha
            )
            handles.append(
                mpatches.Patch(
                    color = colors[ counter ],
                    label = to_stack_labels[ counter ]
                )
            )
        else:
            handles.append(
                mpatches.Patch(
                    edgecolor = colors[ counter ],
                    facecolor = _facecolor,
                    label = to_stack_labels[ counter ],
                    alpha = alpha
                )
            )
        fill_target = data
    
    handles.reverse()
    ax.legend( loc = "best", handles = handles, fontsize = fontsize )
    ax.set_ylim( 0, None )
    if xlabel:
        ax.set_xlabel( xlabel, loc = "right", fontsize = fontsize )
    if ylabel:
        ax.set_ylabel( ylabel, loc = "top", fontsize = fontsize )
    if title:
        ax.set_title( title, fontsize = fontsize )
    if suptitle:
        fig.suptitle( suptitle, fontsize  = fontsize )
    if facecolor:
        if isinstance( facecolor, dict ):
            _facecolor = facecolor[ "color" ]
            _alpha     = facecolor[ "alpha" ]
            ax.set_facecolor( _facecolor, _alpha )
        if isinstance( facecolor, str ):
            ax.set_facecolor( facecolor )
    if pp:
        pp.savefig( fig )
    if not pp:
        return fig, ax

