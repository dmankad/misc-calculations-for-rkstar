import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt
from matplotlib import transforms
from mpl_toolkits.mplot3d import axes3d
from matplotlib.transforms import Affine2D

import numpy as np
import math

#from ml_share import utils

def _plot_profiles( heights, bins_x, bins_y, axs = False, rotate = "x", colordict = {} ):
    
    def get_mean( vals, freqs ):
        if isinstance( vals, list ):
            vals = np.array( vals )
        if isinstance( freqs, list ):
            freqs = np.array( freqs )
        probas = freqs / freqs.sum()
        mean   = ( vals * probas ).sum()
        return mean
    def get_std( vals, freqs ):
        if isinstance( vals, list ):
            vals  = np.array( vals )
        if isinstance( freqs, list ):
            freqs = np.array( freqs )
        probas = freqs / freqs.sum()
        mean   = ( vals * probas ).sum()
        std    = math.sqrt( ( ( ( vals - mean ) ** 2 ) * probas ).sum() )
        return std
    
    bin_centers_x = [ 0.5 * ( bins_x[ i ] + bins_x[ i + 1 ] ) for i in range( len( bins_x ) - 1 ) ]
    bin_centers_y = [ 0.5 * ( bins_y[ i ] + bins_y[ i + 1 ] ) for i in range( len( bins_y ) - 1 ) ]
    
    mean_y = np.array( [ get_mean( bin_centers_y, heights_y ) for heights_y in heights ] )
    std_y  = np.array( [ get_std(  bin_centers_y, heights_y ) for heights_y in heights ] )
    mean_x = np.array( [ get_mean( bin_centers_x, heights_x ) for heights_x in np.transpose( heights ) ] )
    std_x  = np.array( [ get_std(  bin_centers_x, heights_x ) for heights_x in np.transpose( heights ) ] )
    
    if axs:
        ax_x = axs[ 0 ]
        ax_y = axs[ 1 ]
    else:
        fig_y, ax_y = plt.subplots()
        fig_x, ax_x = plt.subplots()
    
    if "y" not in colordict.keys():
        ecolor_y = "tab:green"
    else:
        ecolor_y = colordict[ "y" ]
    if "x" not in colordict.keys():
        ecolor_x = "tab:green"
    else:
        ecolor_x = colordict[ "x" ]
        
    ax_y.errorbar(
        x         = bin_centers_x if "y" not in rotate else mean_y,
        y         = mean_y        if "y" not in rotate else bin_centers_x,
        yerr      = std_y         if "y" not in rotate else None,
        xerr      = std_x         if "y" in rotate else None,
        marker    = ".",
        ecolor    = ecolor_y,
        alpha     = 0.8,
        linestyle = "",
        linewidth = "0.9",
        color     = "black",
        capsize   = 2,
        label     = "c"
    )
    
    ax_x.errorbar(
        x         = mean_x        if "x" in rotate else bin_centers_y,
        y         = bin_centers_y if "x" in rotate else mean_x,
        xerr      = std_x         if "x" in rotate else None,
        yerr      = std_x         if "x" not in rotate else None,
        marker    = ".",
        ecolor    = ecolor_x,
        alpha     = 0.8,
        linestyle = "",
        linewidth = "0.9",
        color     = "black",
        capsize   = 2,
        label     = "c"
    )
    

    """
    ax_x.legend()
    ax_y.legend()
    handles_x, labels_x = ax_x.get_legend_handles_labels()
    handles_y, labels_y = ax_y.get_legend_handles_labels()
    handles_x = [ handles_x[ 0 ][ 0 ], ( handles_x[ 0 ][ 1 ], handles_x[ 0 ][ 2 ] ) ]
    handles_y = [ handles_y[ 0 ][ 0 ], ( handles_y[ 0 ][ 1 ], handles_y[ 0 ][ 2 ] ) ]
    labels_x  = [ "mean", "std" ]
    labels_y  = [ "mean", "std" ]
    ax_x.legend( handles_x, labels_y, loc = "best", numpoints = 1 )
    ax_y.legend( handles_x, labels_y, loc = "best", numpoints = 1 )
    """
    
    """
    if not axs:
        fig_x.savefig( "profile_x.pdf" )
        fig_y.savefig( "profile_y.pdf" )
    """
    
def plot_profiles(
        x_arr,
        y_arr,
        x_label,
        y_label,
        x_range = None,
        y_range = None,
        x_bins  = 100,
        y_bins  = 100,
        title   = "",
        magnify = 4,
        color   = "tab:green",
        cmap    = plt.cm.Greens,
        pp      = False,
        profile_rotate  = [ "profile_x" ] 
    ):


    ### Preparing the Binning ###
    ### --------------------- ###
    if x_range and y_range:
        x_binning = np.linspace( *x_range, x_bins )
        y_binning = np.linspace( *y_range, y_bins )
    elif x_range:
        x_binning = np.linspace( *x_range, x_bins )
        y_binning = y_bins
    elif y_range:
        x_binning = x_bins
        y_binning = np.linspace( *y_range, y_bins )
    else:
        x_binning = x_bins
        y_binning = y_bins
    binning = ( x_binning, y_binning )
    
    if True:
        ### Preparing the Basic Histograms for Profiles ###
        ### ------------------------------------------- ###
        
        heights, bins_x, bins_y, _ = plt.hist2d( x_arr, y_arr, bins = binning )
        plt.cla()
        plt.clf()
    
        ### Preparing the Grid for Plots ###
        ### ---------------------------- ###
        mag = magnify
        fig = plt.figure( figsize = ( 5 * mag, 5 * mag ) )
        ax_main     = plt.subplot2grid( ( 5 * mag, 5 * mag ), ( 2 * mag, 2 * mag ), rowspan = 3 * mag, colspan = 3 * mag, fig = fig )
        ax_y        = plt.subplot2grid( ( 5 * mag, 5 * mag ), ( 2 * mag, 1 * mag ), rowspan = 3 * mag, colspan = 1 * mag, sharey = ax_main, fig = fig )
        ax_y_feat   = plt.subplot2grid( ( 5 * mag, 5 * mag ), ( 2 * mag, 0 * mag ), rowspan = 3 * mag, colspan = 1 * mag, sharey = ax_y   , fig = fig )
        ax_x        = plt.subplot2grid( ( 5 * mag, 5 * mag ), ( 1 * mag, 2 * mag ), rowspan = 1 * mag, colspan = 3 * mag, sharex = ax_main, fig = fig )
        ax_x_feat   = plt.subplot2grid( ( 5 * mag, 5 * mag ), ( 0 * mag, 2 * mag ), rowspan = 1 * mag, colspan = 3 * mag, sharex = ax_x   , fig = fig )
    
        ### Calculating & Plotting the Profile Plots ###
        ### ---------------------------------------- ###
    
        rotate = ""
        rotate += "x" if "profile_x" in profile_rotate else ""
        rotate += "y" if "profile_y" in profile_rotate else ""
        
        colordict = { "y": color, "x": color }
        
        _plot_profiles( heights, bins_x, bins_y, [ ax_y, ax_x ], rotate = rotate, colordict = colordict )
        
        ### Plotting the 2D Histogram, and 1D Histograms ###
        ### -------------------------------------------- ###
        
        h2d = ax_main.hist2d(
                  x_arr,
                  y_arr,
                  bins    = binning,
                  cmap    = cmap,
                  vmin    = 0,
                  density = True
              )
        ax_x_feat.hist( x_arr, bins = x_binning, color = color, alpha = 0.8, density  = True, histtype = "stepfilled" )
        ax_y_feat.hist( y_arr, bins = y_binning, color = color, alpha = 0.8, density  = True, histtype = "stepfilled", orientation = "horizontal" )
        ax_y_feat.invert_xaxis()
        ax_y.invert_xaxis()
    
        ### Labels, Ticks, and Other Formatting ###
        ### ----------------------------------- ###
    
        ax_main.set_xlabel(   x_label     , fontsize = 15, loc = "center" )
        ax_main.set_ylabel(   y_label     , fontsize = 15, loc = "center" )
        ax_x.set_xlabel(      x_label     , fontsize = 15, loc = "center" )
        ax_x.set_ylabel(      y_label     , fontsize = 15, loc = "center" )
        ax_x_feat.set_xlabel( x_label     , fontsize = 15, loc = "center" )
        ax_x_feat.set_ylabel( "candidates", fontsize = 15, loc = "center" )
        ax_y.set_xlabel(      x_label     , fontsize = 15, loc = "center" )
        ax_y.set_ylabel(      y_label     , fontsize = 15, loc = "center" )
        ax_y_feat.set_xlabel( "candidates", fontsize = 15, loc = "center" )
        ax_y_feat.set_ylabel( y_label     , fontsize = 15, loc = "center" )
        
        ax_x.tick_params(      axis = "both", which = "major", labelsize = 12, labelleft = True , labelright = False )
        ax_x_feat.tick_params( axis = "both", which = "major", labelsize = 12, labelleft = True , labelright = False )
        ax_y.tick_params(      axis = "both", which = "major", labelsize = 12, labelleft = False, labelright = True  )
        ax_y_feat.tick_params( axis = "both", which = "major", labelsize = 12, labelleft = False, labelright = True  )
        ax_x.tick_params(      axis = "x"   , which = "major", labelsize = 12, labelleft = True , labelright = False )
        ax_x_feat.tick_params( axis = "x"   , which = "major", labelsize = 12, labelleft = True , labelright = False )
        ax_y.tick_params(      axis = "y"   , which = "major", labelsize = 12, labelleft = False, labelright = True  )
        ax_y_feat.tick_params( axis = "y"   , which = "major", labelsize = 12, labelleft = False, labelright = True  )
        ax_main.tick_params(   axis = "both", which = "major", labelsize = 12, labelleft = False, labelright = True  )
    
        ### Colorbar ###
        ### -------- ###
    
        fig.subplots_adjust( left = 0.1 )
        
        pos1    = ax_main.get_position()
        cbar_ax = fig.add_axes([pos1.x0, pos1.y0 - 0.06, pos1.width, 0.02 ] )
        fig.colorbar( h2d[ 3 ], cax = cbar_ax, orientation = "horizontal" )
        
        ### Title ###
        ### ----- ###
        fig.suptitle( title + "\n" + "x: %s | y: %s" % ( x_label, y_label ), fontsize = 20 )
        
        ### Layout ###
        ### ------ ###
        
        #fig.tight_layout()
        
        ### Returning ###
        ### --------- ###
        
        if pp:
            pp.savefig( fig )
    return fig

if __name__ == "__main__":
    N = 10000000
    x_arr   = np.random.standard_gamma( 2., size =  N )
    #x_arr   = np.random.randn( N )
    y_arr   = np.random.rayleigh( size = N )
    #_y_arr   = np.random.wald( 3, 2, size = N )
    #y_arr = 0.01 * x_arr + 0.9 * _y_arr
    title   = "USR in low$-q^2$ Bin"
    x_label = "a [ MeV ]"
    y_label = "b [ MeV ]"
    x_range = [ 0, 3 ]
    y_range = [ 0, 6 ]
    x_bins  = 200
    y_bins  = 200
    fig     = plot_profiles( x_arr, y_arr, x_label = x_label, y_label = y_label, x_range = x_range, y_range = y_range, title = title, color = "tab:orange", cmap = plt.cm.Reds, x_bins = x_bins, y_bins = y_bins  )
    fig.savefig( "trial.pdf" )
