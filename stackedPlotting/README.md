- Copy the `stacked_feature_plots_by_group.py` to `MachineLearning/ml_run/post_processing/.` inside your clone of the [Run2Analysis](https://gitlab.cern.ch/RKstar/Run2Analysis) repository.
- Once you are in the root directory of your clone of `Run2Analysis` repository, run this script as follows:
  ```
  cd MachineLearning
  source setup.sh
  cd ml_run/post_processing
  python3 stacked_feature_plots_by_group.py ../ml_main/configs/gnn1_muon_multiclass_goblin_v2.yaml ../ml_main/configs/gnn2_muon_multiclass_goblin_v2.yaml
  ```
- Some changes for the script to be compatible with the muon branch might still be needed.
