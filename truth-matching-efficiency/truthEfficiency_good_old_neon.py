import os
import json
import numpy as np
from pandas import DataFrame as df
import matplotlib
matplotlib.use( "agg" )
from matplotlib import pyplot as plt
from ml_share import utils
from ml_share import ml_config

nr_bd_signal_id    = ml_config.dsid[ "nr_bd_signal_id"    ]
nr_bdbar_signal_id = ml_config.dsid[ "nr_bdbar_signal_id" ]
r_bd_signal_id     = ml_config.dsid[ "r_bd_signal_id"     ]
r_bdbar_signal_id  = ml_config.dsid[ "r_bdbar_signal_id"  ]

class finalSelector:
    def __init__(
            self,
            config_file,
            config_file_default                   = "configs/default.yaml",
            selection_string                      = "( $selection_main ) & ( $selection_mc_no_truth & $selection_usr) & ( $selection_q2low )",
            global_candidate_selection_strategies = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ],
            local_candidate_selection_strategies  = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2" ],
            mass_prediction_strategies            = [ "correct_B_mass", "B_mass_Kstar_mass_closer", "nn_predicted_B_mass" ],
            nn_score_cuts                         = [ round( i, 2 ) for i in list( np.arange( 0, 1.1, 0.1 ) ) ],
            info_nn_based_strategies              = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ],
            info_classical_strategies             = [ "B_chi2", "Lxy_significance_over_B_chi2" ],
            define_best                           = { "total_signal_score" : 1, "signal_score_diff" : 1, "max_ind_signal_score" : 1, "B_chi2" : -1, "Lxy_significance_over_B_chi2" : 1 },
            colors_dict                           = { "total_signal_score" : "tab:blue", "max_ind_signal_score" : "tab:purple", "signal_score_diff" : "tab:red", "B_chi2" : "tab:green", "Lxy_significance_over_B_chi2" : "tab:orange"  }
    ):
        """

        - Description: Creates an instance of the class `finalSelector` with either a default or a custom setup depending upon the provided arguments.
        
        - Arguments:
        
            - config_file:
                - Type: str
                - Description: The path to the configuration file used for the NN training.
                
        - Optional Arguments:
        
            - config_file_default
                - Type: str
                - Description: The path to the default configuration file to be used to complement the configuration file at `config_file`.
                - Default: "configs/default.yaml"
                
            - selection_string
                - Type: str
                - Description: The selection-string for the baseline selection. The default value is None. If None, the constructor will try to read it from the configuration file.
                - Default: None
            
            - global_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for global candidate selection (i.e., deciding which candidates to classify as signal). Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff".
                - Default: [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ]
            
            - local_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for local candidate selection (i.e., which candidate to select per each event). Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2".
                - Default: [ "total_signal_score", "max_ind_signal_score", "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2" ]
            
            - mass_prediction_strategies
                - Type: list
                - Description: The list of the names of the strategies for predicting the mass of the Bd-meson (Bd or Bdbar). Currently supported strategies: "B_mass_Kstar_mass_closer", "nn_predicted_B_mass", "correct_B_mass" (only for MC, of course!).
                - Default: [ "correct_B_mass", "B_mass_Kstar_mass_closer", "nn_predicted_B_mass" ]
            
            - define_best
                - Type: dict
                - Description: A dictionary specifying as to whether to take the highest or the lowest value as the best value of a parameter in local candidate selection. The value for a key should be 1 if its highest value is considered to be the best, and the value for a key should be -1 if its lowest value is to be considered to be the best.
                - Default: { "total_signal_score" : 1, "signal_score_diff" : 1, "max_ind_signal_score" : 1, "B_chi2" : -1, "Lxy_significance_over_B_chi2" : 1 }
            
            - nn_score_cuts:
                - Type: list
                - Description: A list of threshold scores (for the global candidate selection) at which to evaluate the final selection.
                - Default:  [ round( i, 2 ) for i in list( np.arange( 0, 1.1, 0.1 ) ) ]
            
            - info_nn_based_strategies
                - Type: list
                - Description: The list of the names specifying as to which of the global/local candidate selection strategies or the mass prediction strategies are based on NN scores. To be used for formatting plots.
                - Default: [ "total_signal_score", "max_ind_signal_score", "signal_score_diff", "nn_predicted_B_mass" ]
            
            - info_classical_strategies
                - Type: list
                - Description: The list of the names specifying as to which of the global/local candidate selection strategies are classical strategies. To be used for formatting plots.
                - Default: [ "B_chi2", "Lxy_significance_over_B_chi2", "B_mass_Kstar_mass_closer" ]
            
            - colors_dict
                - Type: dict
                - Description: A dictionary specifying a color-name (str) for each global/local candidate selection strategy.
                - Default: { "total_signal_score" : "tab:blue", "max_ind_signal_score" : "tab:purple", "signal_score_diff" : "tab:red", "B_chi2" : "tab:green", "Lxy_significance_over_B_chi2" : "tab:orange"  }
       
        - Returns: None
        
        """
        self.config                                = utils.config_load( config_file_default, config_file )
        self.model_dir_path                        = os.path.join( self.config[ "training" ][ "output_dir" ], os.path.basename( config_file ).replace( ".yaml", "" ) + "_applied" )
        self.selection                             = utils.get_selection( selection_string, self.config[ "common" ] )
        self.mass_prediction_strategies            = mass_prediction_strategies
        self.nn_score_cuts                         = nn_score_cuts
        self.info_classical_strategies             = info_classical_strategies
        self.info_nn_based_strategies              = info_nn_based_strategies
        self.local_candidate_selection_strategies  = local_candidate_selection_strategies
        self.global_candidate_selection_strategies = global_candidate_selection_strategies
        self.define_best                           = define_best
        self.colors_dict                           = colors_dict

    def load_files( self, predsFileKernel, orgFile, standalone = False ):
        """
        
        - Description: Loads the file with the features and the file with the corresponding NN predictions.

        - Arguments:
            
            - predsFileKernel:
                - Type: str
                - Description: The basename of the prediction-file without the path to the directory. Unix-style pathname pattern expansion will be applicable, so you can use the string to include multiple files, e.g., "myfile[1-5].ftr" will be expanded and all the 5 files will be loaded.
            
            - orgFile:
                - Type: str
                - Description: The full path of the file with the features for which the predictions are calculated. Unix-style pathname patter expansion will be applicable, so you can use the string to include multiple files, e.g., "myfile[1-5].ftr" will be expanded and all the 5 files will be loaded.
        
        - Optional Arguments:
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the files being loaded will be stored in the class-instance for future use.
                - Default: False

        - Returns: A tuple of the form ( preds, orgData ) where both elements of the tuple are `pandas.DataFrame` objects.
        
        """
        predsFile      = utils.get_list_make_list( os.path.join( self.model_dir_path, predsFileKernel ) )
        orgFile        = utils.get_list_make_list( orgFile )
        items_to_load  = [ "B_mass", "Bbar_mass", "info_event_number", "info_truth_matching", "info_sample" ]
        _items_to_load = [ "Lxy_significance", "B_chi2", "B_mass_Kstar_mass_closer" ]
        
        for strategy in self.global_candidate_selection_strategies + self.local_candidate_selection_strategies + self.mass_prediction_strategies:
            for _item in _items_to_load:
                if _item in strategy and _item not in items_to_load:
                    items_to_load += [ _item ]
        
        orgData        = utils.df_load( orgFile, selection = self.selection, items_to_load = items_to_load )
        preds          = utils.df_load( predsFile )
        
        if not standalone:
            self.orgData   = orgData
            self.preds     = preds
        
        return preds, orgData
    
    def prepare_final_scores( self, orgData = None, preds = None, local_candidate_selection_strategies = None, global_candidate_selection_strategies = None, standalone = False ):
        """
        
        - Description: Prepares the final scores (using the provided features and NN predictions) to be used in global and local candidate selection strategies.
        
        - Optional Arguments:
        
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData` attribute of the class-instance.
                - Default: None
            
            - preds
                - Type: pandas.DataFrame
                - Description: The dataframe with the predictions. By default, set to None. If None, the method will try to use the `preds` attribute of the class-instance.
                - Default: None
            
            - global_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for global candidate selection (i.e., deciding which candidates to classify as signal). By default, set to None. If None, the method will try to use the `global_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff".
                - Default: None
            
            - local_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for local candidate selection (i.e., which candidate to select per each event). By default, set to None. If None, the method will try to use the `local_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2".
                - Default: None
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being computed will be stored in the class-instance for future use.
                - Default: False
        
        - Returns: A pandas.dataframe object with the original features and the final scores.
        
        """

        if orgData is None:
            orgData = self.orgData
        if preds is None:
            preds = self.preds
        if local_candidate_selection_strategies is None:
            local_candidate_selection_strategies = self.local_candidate_selection_strategies
        if global_candidate_selection_strategies is None:
            global_candidate_selection_strategies = self.global_candidate_selection_strategies

        for strategy in global_candidate_selection_strategies + local_candidate_selection_strategies:
            if strategy == "total_signal_score":
                orgData[ "total_signal_score"           ] = preds[ nr_bd_signal_id ] + preds[ nr_bdbar_signal_id ]
            if strategy == "max_ind_signal_score":
                orgData[ "max_ind_signal_score"         ] = preds[ [ nr_bd_signal_id, nr_bdbar_signal_id ] ].max( axis = 1 )
            if strategy == "signal_score_diff":
                orgData[ "signal_score_diff"            ] = ( preds[ nr_bd_signal_id ] - preds[ nr_bdbar_signal_id ] ).abs()
            if strategy == "Lxy_significance_over_B_chi2":
                orgData[ "Lxy_significance_over_B_chi2" ] = orgData[ "Lxy_significance" ] / orgData[ "B_chi2" ]
        
        if not standalone:
            self.orgData = orgData

        return orgData

    def prepare_masses( self, orgData = None, preds = None, mass_prediction_strategies = None, standalone = None ):
        """
        
        - Description: Prepares the masses (using the provided features and NN predictions) to be used in the mass prediction strategies.
        
        - Optional Arguments:
        
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData` attribute of the class-instance.
                - Default: None
            
            - preds
                - Type: pandas.DataFrame
                - Description: The dataframe with the predictions. By default, set to None. If None, the method will try to use the `preds` attribute of the class-instance.
                - Default: None
            
            - mass_prediction_strategies
                - Type: list
                - Description: The list of the names of the strategies for predicting the mass of the Bd-meson (Bd or Bdbar). By default, set to None. If None, the method will try to use the `mass_prediction_strategies` attribute of the class-instance. Currently supported strategies: "B_mass_Kstar_mass_closer", "nn_predicted_B_mass", "correct_B_mass" (only for MC, of course!).
                - Default: None
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being computed will be stored in the class-instance for future use.
                - Default: False
        
        - Returns: A pandas.dataframe object with the original features and the final scores.
        
        """

        if orgData is None:
            orgData = self.orgData
        if preds is None:
            preds = self.preds
        if mass_prediction_strategies is None:
            mass_prediction_strategies = self.mass_prediction_strategies

        for mass_prediction_strategy in mass_prediction_strategies:
            if mass_prediction_strategy == "correct_B_mass":
                orgData[ "correct_B_mass"               ] = orgData[ "info_sample" ].isin( [ nr_bd_signal_id, r_bd_signal_id ] ) * orgData[ "B_mass" ] + orgData[ "info_sample" ].isin( [ nr_bdbar_signal_id, r_bdbar_signal_id ] ) * orgData[ "Bbar_mass" ]
            if mass_prediction_strategy == "nn_predicted_B_mass":
                orgData[ "nn_predicted_B_mass"          ] = orgData[ "B_mass" ] * ( preds[ nr_bd_signal_id ] >= preds[ nr_bdbar_signal_id ] ) + orgData["Bbar_mass"] * ( preds[ nr_bd_signal_id ] < preds[ nr_bdbar_signal_id ] )
            if mass_prediction_strategy == "B_mass_Kstar_mass_closer":
                continue
        
        if not standalone:
            self.orgData = orgData
        
        return orgData
    
    def load_files_and_process( self, predsFileKernel, orgFile, local_candidate_selection_strategies = None, global_candidate_selection_strategies = None, mass_prediction_strategies = None, standalone = False ):
        """
        
        - Description: Loads the file with the features and the file with the corresponding NN predictions.
    
        - Arguments:
        
            - predsFileKernel:
                - Type: str
                - Description: The basename of the prediction-file without the path to the directory. Unix-style pathname pattern expansion will be applicable, so you can use the string to include     multiple files, e.g., "myfile[1-5].ftr" will be expanded and all the 5 files will be loaded.
        
            - orgFile:
                - Type: str
                - Description: The full path of the file with the features for which the predictions are calculated. Unix-style pathname patter expansion will be applicable, so you can use the string to     include multiple files, e.g., "myfile[1-5].ftr" will be expanded and all the 5 files will be loaded.
        
        - Optional Arguments:
        
            - global_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for global candidate selection (i.e., deciding which candidates to classify as signal). By default, set to None. If None, the     method will try to use the `global_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score",     "signal_score_diff".
                - Default: None
            
            - local_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for local candidate selection (i.e., which candidate to select per each event). By default, set to None. If None, the method will     try to use the `local_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score",     "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2".
                - Default: None
        
            - mass_prediction_strategies
                - Type: list
                - Description: The list of the names of the strategies for predicting the mass of the Bd-meson (Bd or Bdbar). By default, set to None. If None, the method will try to use the `mass_prediction_strategies` attribute of the class-instance. Currently supported strategies: "B_mass_Kstar_mass_closer", "nn_predicted_B_mass", "correct_B_mass" (only for MC, of course!).
                - Default: None
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the files being loaded will be stored in the class-instance for future use.
                - Default: False
    
        - Returns: A tuple of the form ( preds, orgData ) where both elements of the tuple are `pandas.DataFrame` objects.
        
        """
        
        if local_candidate_selection_strategies is None:
            local_candidate_selection_strategies = self.local_candidate_selection_strategies
        if global_candidate_selection_strategies is None:
            global_candidate_selection_strategies = self.global_candidate_selection_strategies
        if mass_prediction_strategies is None:
            mass_prediction_strategies = self.mass_prediction_strategies
            
        preds, orgData = self.load_files( predsFileKernel, orgFile, standalone = standalone )
        orgData        = self.prepare_final_scores( orgData = orgData, preds = preds, local_candidate_selection_strategies = local_candidate_selection_strategies, global_candidate_selection_strategies = global_candidate_selection_strategies, standalone = standalone )
        orgData        = self.prepare_masses( orgData = orgData, preds = preds, mass_prediction_strategies = mass_prediction_strategies, standalone = standalone )
        
        if not standalone:
            self.orgData = orgData
        
        return orgData

    def _sort_for_local_selection( self, orgData = None, local_candidate_selection_strategies = None, standalone = False ):
        """
        
        - Description: This is an internal technique/helper function deployed to make the selection of a candidate per event fast. In particular, it sorts the indices of the dataset in the ascending or descending order (depending upon the `define_best` dict) of the of values of the final socres specified in the list `local_candidate_selection_strategies`. These sorted indices are useful in a technical way in the local candidate selection (i.e., selection of the "best" candidate within an event). See, the `get_final_selection_masks` method for the use of this method.
        
        - Optional Arguments:
            
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData` attribute of the class-instance.
                - Default: None
            
            - local_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for local candidate selection (i.e., which candidate to select per each event). By default, set to None. If None, the method will try to use the `local_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff", "B_chi2", "Lxy_significance_over_B_chi2".
                - Default: None
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being computed will be stored in the class-instance for future use.
                - Default: False
        
        - Returns: A `dict` object with keys being the names of the final scores specified in the list `local_candidate_selection_strategies` and the values being the arrays of correspondingly sorted indices.
        
        """
        if orgData is None:
            orgData  = self.orgData
        if local_candidate_selection_strategies is None:
            local_candidate_selection_strategies = self.local_candidate_selection_strategies
        
        idx_sorted_for_local_selection = {}
        for local_candidate_selection_strategy in local_candidate_selection_strategies:
            idx_sorted_for_local_selection[ local_candidate_selection_strategy ] = orgData.sort_values( local_candidate_selection_strategy, ascending = True if self.define_best[ local_candidate_selection_strategy ] < 0 else False )
        
        if not standalone:
            self.idx_sorted_for_local_selection = idx_sorted_for_local_selection
        
        return idx_sorted_for_local_selection
    
    def _get_inflated_MC_truth( self, orgData = None, standalone = False ):
        """
        
        - Description: This is an internal technique/helper function deployed to help with the evaluation of the truth-prediction accuracy. In particular, it marks as to whether a given event has any truth-matched MC candidate or not. Of course, this is only applicable for MC!
        
        - Optional Arguments:
            
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData` attribute of the class-instance.
                - Default: None
            
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being computed will be stored in the class-instance for future use.
                - Default: False
        
        - Returns: A `pandas.DataFrame` object with one column whose i-th element represents the truth-value of whether the i-th candidate in the `orgData` dataframe belongs to an event that has an MC-truth-matched candidate or not.
       
        """
        
        if orgData is None:
            orgData = self.orgData
                
        idx_inflated_MC_truth =  orgData[ [ "info_truth_matching", "info_event_number" ] ].groupby( "info_event_number", sort = False )[ "info_truth_matching"     ].transform("sum") > 0
        
        if not standalone:
            self.idx_inflated_MC_truth = idx_inflated_MC_truth
        return idx_inflated_MC_truth
    
    def get_local_candidate_selection_masks( self, orgData = None, local_candidate_selection_strategies = None, standalone = False ):
        """
        
        - Description: Creates a dictionary of boolean masks, one for each local candidate selection strategy. Notice that this is done at the baseline level, so this corresponds to applying the local candidate selection before applying the global candidate selection. Consequently, the boolean masks created via this method will be useful (only) for the final selection strategies that follow the rule of "first do local selection, then do global selection".
        
        - Optional Arguments:
        
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData`     attribute of the class-instance.
                - Default: None
        
            - local_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for local candidate selection (i.e., which candidate to select per each     event). By default, set to None. If None, the method will try to use the `local_candidate_selection_strategies` attribute     of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff",     "B_chi2", "Lxy_significance_over_B_chi2".
                - Default: None
        
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being     computed will be stored in the class-instance for future use.
                - Default: False
                
        - Returns: The dictionary with the keys being the names of the different local candidate selection strategies and the values being the corresponding boolean masks.
        
        """
        if orgData is None:
            orgData = self.orgData
        if local_candidate_selection_strategies is None:
            local_candidate_selection_strategies = self.local_candidate_selection_strategies

        idx_locally_selected  = {}
        _idx_locally_selected = {}
        local_candidate_selection_masks  = {}
        _local_candidate_selection_masks = {}

        for strategy in local_candidate_selection_strategies:
            if self.define_best[ strategy ] > 0:
                idx_locally_selected[ strategy ] = orgData[ [ strategy, "info_event_number"] ].groupby( "info_event_number" )[ strategy ].apply( lambda x: x.nlargest( self.define_best[ strategy ] ) ).reset_index( level = "info_event_number" ).index
                #_idx_locally_selected[ strategy ] = orgData[ [ strategy, "info_event_number"] ].groupby( "info_event_number" )[ strategy ].apply( lambda x: x.nlargest( self.define_best[ strategy ], keep = "all" ) ).reset_index( level = "info_event_number" ).index
            if self.define_best[ strategy ] < 0:
                idx_locally_selected[ strategy ] = orgData[ [ strategy, "info_event_number"] ].groupby( "info_event_number" )[ strategy ].apply( lambda x: x.nsmallest( -self.define_best[ strategy ] ) ).reset_index( level = "info_event_number" ).index
                #_idx_locally_selected[ strategy ] = orgData[ [ strategy, "info_event_number"] ].groupby( "info_event_number" )[ strategy ].apply( lambda x: x.nsmallest( -self.define_best[ strategy ], keep = "all" ) ).reset_index( level = "info_event_number" ).index
    
            local_candidate_selection_masks[ strategy ] = orgData.index.isin( idx_locally_selected[ strategy ] )
            #_local_candidate_selection_masks[ strategy ] = orgData.index.isin( _idx_locally_selected[ strategy ] )
        
        if not standalone:
            self.local_candidate_selection_masks = local_candidate_selection_masks
            #self._local_candidate_selection_masks = _local_candidate_selection_masks

        return local_candidate_selection_masks

    def get_global_candidate_selection_masks( self, nn_cut_value, orgData = None, global_candidate_selection_strategies = None, standalone = False ):
        """
        
        - Description: This method performs the global candidate selection, in particular, it creats the masks for global candidate selection that can be readily used in the final stage of the selection. Notice that even if these masks are created before performing local candidate selection (i.e., at the baseline selection level), they can be used in both kinds of final selection strategies (i.e., where we would have to first apply the local candidate selection as well as where we would have to first apply the global candidate selection.) This is in contrast to how the masks created by the `get_local_candidate_selection_masks` work. This is not due to any peculiarity of how the method `get_local_candidate_selection_masks` is coded, it is simply inherent to the nature of local candidate selection (in a rough sense, this asymmetry between the global and the local candidate selection comes from the fact that the global selection has a cut-off parameter whereas the local selection chooses a "best" value, i.e., a rank, rather than an absolute cutoff).
        
        - Arguments:
        
            - cutoff
                - Type: float
                - Description: The the threshold value of the final score -- whatever that final score might be based on the strategy of global selection.
        
        - Optional Arguments:
            
            - orgData
                - Type: pandas.DataFrame
                - Description: The dataframe with the features. By default, set to None. If None, the method will try to use the `orgData`     attribute of the class-instance.
                - Default: None
        
            - global_candidate_selection_strategies
                - Type: list
                - Description: The list of the names of the strategies for global candidate selection (i.e., which candidate to select as signal). By default, set to None. If None, the method will try to use the `local_candidate_selection_strategies` attribute of the class-instance. Currently supported strategies: "total_signal_score", "max_ind_signal_score", "signal_score_diff".
                - Default: None
        
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being     computed will be stored in the class-instance for future use.
                - Default: False
        
        - Returns: A dict object with the keys being the names of the different global candidate selection strategies and the values being the corresponding masks.
        
        """
        if orgData is None:
            orgData = self.orgData
        if global_candidate_selection_strategies is None:
            global_candidate_selection_strategies = self.global_candidate_selection_strategies
        
        global_candidate_selection_masks = {}
        for global_candidate_selection_strategy in global_candidate_selection_strategies:
            global_candidate_selection_masks[ global_candidate_selection_strategy ] = orgData[ global_candidate_selection_strategy ] >= nn_cut_value
        
        if not standalone:
            self.global_candidate_selection_masks = global_candidate_selection_masks
            
        return global_candidate_selection_masks

    def get_final_selection_masks(
            self,
            methods = [ "local_first", "global_first" ],
            nn_cut_value = None,
            orgData = None,
            global_candidate_selection_masks = None,
            local_candidate_selection_masks = None,
            idx_sorted_for_local_selection = None,
            standalone = False
            ):
        """
        - Description: Calculates the final prediction masks for each (ordered) combination of local and global candidate selection strategies.
        
        - Optional Arguments:
        
            - methods:
                - Type: list
                - Description: A list of strings, each of which specifies the order of applying the local and the global candidate selections. Elements of the list can be "local_first", and "global_first".
                - Default: [ "local_first", "global_first" ]
    
            - global_candidate_selection_masks:
                - Type: dict
                - Description: The dictionary of boolean makss for the various strategies in the list `global_candidate_selection_strategies`, i.e., the output of the `get_global_candidate_selection_masks` method for the relevant `global_candidate_selection_strategies` list. Relevant for both the "local_first" and "global_first" methods. By default, it is set to None. If None, the method will try to use the `global_candidate_selection_masks` attribute of the class-instance.
                - Default: None
                
             - local_candidate_selection_masks:
                 - Type: dict
                 - Description: The dictionary of boolean masks for the various strategies in the list `local_candidate_selection_strategies`, i.e., the output of the `get_local_candidate_selection_masks` method for the relevant `local_candidate_selection_strategies`. Relevant only if the `methods` includes "local_first". By default, it is set to None. If None, the method will try to use the `local_candidate_selection_masks` attribute of the class-instance.
                 - Default: None
                 
            - idx_sorted_for_local_candidate_selection:
                - Type: dict
                - Description: The output of the `_sort_for_local_selection` method for the relevant `local_candidate_selection_strategies`. Relevant only if `methods` includes "global_first". By default, it is set to None. If None, the method will try to use the `idx_sorted_for_local_candidate_selection` attribute of the class-instance.
                - Default: None
                
            - standalone
                - Type: bool
                - Description: Specifies as to whether the method is being called for standalone use. If True, none of the information being     computed will be stored in the class-instance for future use.
                - Default: False
                
        - Returns: A dict object with depth 3 of the structure that is illustrated in the following schematic example:
        
                   { "local_first" : {
                         "name_of_the_LOCAL_selection_strategy": {
                             "name_of_the_GLOBAL_selection_strategy": boolean_mask
                             }
                         },
                     "global_first" : {
                         "name_of_the_GLOBAL_selection_strategy": {
                             "name_of_the_LOCAL_selection_strategy": another_boolean_mask
                             }
                         }
                   }
            
        """
        if orgData is None:
            orgData = self.orgData
        if global_candidate_selection_masks is None:
            try:
                global_candidate_selection_masks = self.global_candidate_selection_masks
            except:
                global_candidate_selection_masks = self.get_global_candidate_selection_masks( nn_cut_value = nn_cut_value )
        global_candidate_selection_strategies = list( global_candidate_selection_masks.keys() )
 
        if "global_first" in methods:
            if idx_sorted_for_local_selection is None:
                try:
                    idx_sorted_for_local_selection   = self.idx_sorted_for_local_selection
                except:
                    idx_sorted_for_local_selection   = self._sort_for_local_selection()
            local_candidate_selection_strategies = list( idx_sorted_for_local_selection.keys() )
        if "local_first" in methods:
            if local_candidate_selection_masks is None:
                try:
                    local_candidate_selection_masks  = self.local_candidate_selection_masks
                except:
                    local_candidate_selection_masks  = self.get_local_candidate_selection_masks()
            local_candidate_selection_strategies = list( local_candidate_selection_masks.keys() )
            
        final_selection_masks = {}
        if "global_first" in methods:
            global_first_final_selection_masks  = {}
            for global_candidate_selection_strategy in global_candidate_selection_strategies:
                global_first_final_selection_masks[ global_candidate_selection_strategy ] = {}
                for local_candidate_selection_strategy in local_candidate_selection_strategies:
                
                    ### ------------------------------------------------------------------------ ###
                    ###                                Comment Box                               ###
                    ### ------------------------------------------------------------------------ ###
                    ###                                                                          ###
                    ###  The reason behind implementing the global_first final selection         ###
                    ###  in the following convoluted way is so as to not be forced to use        ###
                    ###  the `get_local_candidate_selection_masks` for every value of            ###
                    ###  score-cutoff in the global candidate selection. The helper functions    ###
                    ###  of idx sorting are designed in such a way that they do not need to      ###
                    ###  be re-evaluated for every value of the score-cutoff on                  ###
                    ###  the global candidate selection. So, the convoluted way is conceptually  ###
                    ###  convoluted but it is computationally efficient. :)                      ###
                    ###                                                                          ###
                    ### ------------------------------------------------------------------------ ###

                    global_first_final_selection_masks[ global_candidate_selection_strategy ][ local_candidate_selection_strategy ] = orgData.index.isin(
                        idx_sorted_for_local_selection[ local_candidate_selection_strategy ].loc[
                            idx_sorted_for_local_selection[ local_candidate_selection_strategy ].index.isin(
                                orgData[ global_candidate_selection_masks[ global_candidate_selection_strategy ] ].index
                            )
                        ].groupby( "info_event_number" ).head( abs( self.define_best[ local_candidate_selection_strategy ] ) ).index
                    )
            final_selection_masks[ "global_first" ] = global_first_final_selection_masks
        if "local_first" in methods:
            local_first_final_selection_masks = {}
            for local_candidate_selection_strategy in local_candidate_selection_strategies:
                local_first_final_selection_masks[ local_candidate_selection_strategy ] = {}
                for global_candidate_selection_strategy in global_candidate_selection_strategies:
                    local_first_final_selection_masks[ local_candidate_selection_strategy ][ global_candidate_selection_strategy ] = global_candidate_selection_masks[ global_candidate_selection_strategy ] & local_candidate_selection_masks[ local_candidate_selection_strategy ]
            final_selection_masks[ "local_first" ] = local_first_final_selection_masks
            
        if not standalone:
            self.final_selection_masks = final_selection_masks
            
        return final_selection_masks
        
    def get_final_selection_masks_full_range( self, nn_score_cuts = None ):
        """
        
        - Description: Runs the `get_final_selection_masks` for each of the cutoff in the list `nn_score_cuts`.
        
        - Optional Arguments:
        
            - nn_score_cuts:
                - Type: list
                - Description: A list of threshold scores (for the global candidate selection) at which to evaluate the final selection. By default, it is set to None. If None, the method will try to use the `nn_score_cuts` attribute of the class-instance.
                - Default: None
        
        - Returns: A dict with keys corresponding to the cutoff values in the list `nn_score_cuts` and the values being the output of `get_final_selection_masks` for each of the keys.
        
        """
        if nn_score_cuts is None:
            nn_score_cuts = self.nn_score_cuts
        full_range_final_selection_masks = {}
        try:
            idx_sorted_for_local_selection = self.idx_sorted_for_local_selection
        except:
            idx_sorted_for_local_selection   = self._sort_for_local_selection()
        try:
            local_candidate_selection_masks = self.local_candidate_selection_masks
        except:
            local_candidate_selection_masks  = self.get_local_candidate_selection_masks()

        #cutflow_matrix = {}
        #local_candidate_selection_strategies = self.local_candidate_selection_strategies
        #global_candidate_selection_strategies = self.global_candidate_selection_strategies
        #full_range_final_selection_masks = self.full_range_final_selection_masks
        #nn_score_cuts = list( full_range_final_selection_masks.keys() )
        
        counter = 0
        for nn_cut in nn_score_cuts:
            counter += 1
            print("NN Cut: %s Out of %s"%( counter, len( nn_score_cuts )))
            global_candidate_selection_masks = self.get_global_candidate_selection_masks( nn_cut )
            full_range_final_selection_masks[ str( nn_cut ) ] = self.get_final_selection_masks(
                global_candidate_selection_masks = global_candidate_selection_masks,
                local_candidate_selection_masks = local_candidate_selection_masks,
                idx_sorted_for_local_selection = idx_sorted_for_local_selection
                )
        self.full_range_final_selection_masks = full_range_final_selection_masks
        return full_range_final_selection_masks
    
    def init_cutflow_matrix( self ):

        methods = list( full_range_final_selection_masks[ nn_score_cuts[ 0 ] ].keys() )
        for nn_cut_value in nn_score_cuts:
            cutflow_matrix[ nn_cut_value ] = {}
            for method in methods:
                cutflow_matrix[ nn_cut_value ][ method ] = {}
                if method == "local_first":
                    for local_candidate_selection_strategy in local_candidate_selection_strategies:
                        cutflow_matrix[ nn_cut_value ][ method ][ local_candidate_selection_strategy ] = {}
                        cutflow_matrix[ nn_cut_value ][ method ][ local_candidate_selection_strategy ][ "selected_candidates" ] = full_range_final_selection_masks[ nn_cut_value ][ method ][ local_candidate_selection_strategy ]
        for method in methods:
            cutflow_matrix[ method ] = {}
    
    def mass_accuracy( self ):
        local_candidate_selection_strategies = self.local_candidate_selection_strategies
        global_candidate_selection_strategies = self.global_candidate_selection_strategies
        masks = self.full_range_final_selection_masks
        final_mass_accuracy_matrix = {}
        mass_prediction_strategies = self.mass_prediction_strategies
        for mass_prediction_strategy in mass_prediction_strategies:
            mass_accuracy_matrix = {}
            for nn_cut_value in masks.keys():
                mass_accuracy_matrix[ nn_cut_value ] = {}
                for method in masks[ nn_cut_value ].keys():
                    mass_accuracy_matrix[ nn_cut_value ][ method ] = {}
                    if method == "local_first":
                        for local_candidate_selection_strategy in local_candidate_selection_strategies:
                            mass_accuracy_matrix[ nn_cut_value ][ method ][ local_candidate_selection_strategy ] = {}
                            for global_candidate_selection_strategy in global_candidate_selection_strategies:
                                correct_B_mass = self.orgData[ masks[ nn_cut_value ][ method ][ local_candidate_selection_strategy ][     global_candidate_selection_strategy ] ][ "correct_B_mass" ]
                                nn_predicted_B_mass = self.orgData[ masks[ nn_cut_value ][ method ][ local_candidate_selection_strategy ][     global_candidate_selection_strategy ] ][ mass_prediction_strategy ]
                                is_nn_correct = ( nn_predicted_B_mass == correct_B_mass )
                                mass_accuracy_matrix[nn_cut_value][ method ][ local_candidate_selection_strategy ][ global_candidate_selection_strategy ] = float(     is_nn_correct.sum() )/len( correct_B_mass )
                    if method == "global_first":
                        for global_candidate_selection_strategy in global_candidate_selection_strategies:
                            mass_accuracy_matrix[ nn_cut_value ][ method ][ global_candidate_selection_strategy ] = {}
                            for local_candidate_selection_strategy in local_candidate_selection_strategies:
                                correct_B_mass = self.orgData[ masks[ nn_cut_value ][ method ][ global_candidate_selection_strategy ][     local_candidate_selection_strategy ] ][ "correct_B_mass" ]
                                nn_predicted_B_mass = self.orgData[ masks[ nn_cut_value ][ method ][ global_candidate_selection_strategy ][     local_candidate_selection_strategy ] ][ mass_prediction_strategy ]
                                is_nn_correct = ( nn_predicted_B_mass == correct_B_mass )
                                mass_accuracy_matrix[nn_cut_value][ method ][ global_candidate_selection_strategy ][ local_candidate_selection_strategy ] = float(     is_nn_correct.sum() )/len( correct_B_mass )
            final_mass_accuracy_matrix[ mass_prediction_strategy ] = mass_accuracy_matrix
        return final_mass_accuracy_matrix
                            
    def compare_selection_to_MC_truth( self, orgData = None, final_selection_masks = None ):
        """
        """
        if orgData is None:
            orgData = self.orgData
        if final_selection_masks is None:
            final_selection_masks = self.final_selection_masks
            
        info_truth_matching = orgData[ "info_truth_matching" ]
        final_selection_and_MC_truth = {}
        final_selection_and_MC_lie   = {}
        
        for method in final_selection_masks.keys():
            final_selection_and_MC_truth[ method ] = {}
            final_selection_and_MC_lie[ method ]   = {}
            if method == "global_first":
                for global_candidate_selection_strategy in final_selection_masks[ method ].keys():
                    final_selection_and_MC_truth[ method ][ global_candidate_selection_strategy ] = {}
                    final_selection_and_MC_lie[ method ][ global_candidate_selection_strategy   ] = {}
                    for local_candidate_selection_strategy in final_selection_masks[ method ][ global_candidate_selection_strategy ].keys():
                        final_selection_and_MC_truth = final_selection_masks[ global_candidate_selection_strategy ][ local_candidate_selection_strategy ] & info_truth_matching
                        final_selection_and_MC_lie   = final_selection_masks[ global_candidate_selection_strategy ][ local_candidate_selection_strategy ] & ~info_truth_matching
            if method == "local_first":
                for local_candidate_selection_strategy in final_selection_masks[ method ].keys():
                    final_selection_and_MC_truth[ method ][ local_candidate_selection_strategy ] = {}
                    final_selection_and_MC_lie[ method ][ local_candidate_selection_strategy   ] = {}
                    for global_candidate_selection_strategy in final_selection_masks[ method ][ local_candidate_selection_strategy ].keys():
                        final_selection_and_MC_truth = final_selection_masks[ local_candidate_selection_strategy ][ global_candidate_selection_strategy ] & info_truth_matching
                        final_selection_and_MC_lie   = final_selection_masks[ local_candidate_selection_strategy ][ global_candidate_selection_strategy ] & ~info_truth_matching
        return final_selection_and_MC_truth, final_selection_and_MC_lie

def main( predsFileKernels, orgFiles ):
    counter = -1
    for predsFileKernel in predsFileKernels:
        counter += 1
        orgFile = orgFiles[ counter ]
        selector = finalSelector( config_file = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast.yaml" )
        selector.load_files_and_process( predsFileKernel = predsFileKernel, orgFile = orgFile )
        selector.get_final_selection_masks_full_range()
    return selector
    
if __name__ == "__main__":
    predsFileKernels = [ "ntuple-30059[0]_part_0[1-2]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr" ]
    orgFiles = [ "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-30059[0]_part_0[1-2]_sample2_non_truth_matched.ftr" ]
    selector = main( predsFileKernels, orgFiles )
    
    
