# Truth Matching Efficiency/Accuracy

- We have some sorting rule X and we select top-N candidates out of all the candidates within an event according to this sorting.
- The purpose is to calculate as to how many times does this procedure capture the truth-matched candidate of the event.

### Step 1: Creating the Necessary Files

- Modify the paths as necessary in the `orthosplit.py` file.
- `python3 orthosplit.py`

### Step 2: Calculating the Truth Matching Efficiency

- Modify the paths in `truth_efficiency_bestN.py` file such that they are consistent with the output paths in `orthosplit.py`.
- Move the `truth_efficiency_bestN.py` file to the `ml_run` directory of the main `Run2Analysis` framework.
- Add missing lines from `configs/default.yaml` of this repo to the equivalent file `ml_run/configs/default.yaml`.
- `python3 truth_efficiency_bestN.py`


