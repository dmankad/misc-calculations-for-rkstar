import pandas as pd
from pandas import DataFrame as df
from ml_share import utils
import os

### Utils ###
### ----- ###

def transpose_nested_list( nested_list ):
    transposed_nested_list = []
    counter = -1
    volunt  = nested_list[ 0 ]
    for item in nested_list:
        if len( item ) != len( volunt ):
            print( "The input nested list is jagged. Cannot transpose!" )
            return False
    for item in volunt:
        counter += 1
        _transposed_array = []
        for _item in nested_list:
            _transposed_array.append( _item[ counter ] )
        transposed_nested_list.append( _transposed_array )
    return transposed_nested_list

"""
[ WIP ] [ SOFTWARE ]
    - The `quick_groupby_sort` is not actually working (as can be checked using `compare_quick_groupby_sort`), it gives worse speed than normal "groupby-sorting".
    - I'm still keeping it in the code in the hope that I can somehow tweak it to make the underlying idea work.
"""

def quick_groupby_sort( df, groupby, sortby, nlargest = 1, nsmallest = -1, factor = 100 ):
    df_array  = []
    idx_array = []
    lens = [ len( df ) // factor for i in range( factor - 1 ) ]
    lens.append( len( df ) - ( factor - 1 )* ( len( df ) // factor ) )
    _past = 0
    for i in range( factor ):
        df_array.append( df[ _past: ][ :lens[ i ] ] )
        _past += lens[ i ]
    for df_slice in df_array:
        if nlargest > 0:
            idx_array.append( df_slice[ [ groupby, sortby ] ].groupby( groupby )[ sortby ].apply( lambda x: x.nlargest( nlargest ) ).reset_index( level = groupby ).index )
        if nsmallest > 0:
            idx_array.append( df_slice[ [ groupby, sortby ] ].groupby( groupby )[ sortby ].apply( lambda x: x.nsmallest( nsmallest ) ).reset_index( level = groupby ).index )
    combined_idx = False
    for idx in idx_array:
        if isinstance( combined_idx, bool ):
            combined_idx = idx
        else:
            combined_idx = combined_idx.append( idx )
    if nlargest > 0:
        final_idx = df[ df.index.isin( combined_idx ) ][ [ groupby, sortby ] ].groupby( groupby ).apply( lambda x: x[ sortby ].nlargest( nlargest ) ).reset_index( level = groupby ).index
    if nsmallest > 0:
        final_idx = df[ df.index.isin( combined_idx ) ][ [ groupby, sortby ] ].groupby( groupby ).apply( lambda x: x[ sortby ].nsmallest( nsmallest ) ).reset_index( level = groupby ).index
    return final_idx

def compare_quick_groupby_sort( df, groupby, sortby, nlargest = 1, nsmallest = -1, factor = 100 ):
    import time
    quick_init = time.time()
    q = quick_groupby_sort( df, groupby, sortby, nlargest, nsmallest, factor )
    quick_stop = time.time()
    print("Quick Time for f %s: %s" %( factor, quick_stop - quick_init ) )
    #return df, combined_idx
    std_init = time.time()
    if nlargest > 0:
        s = df[ [ groupby, sortby ] ].groupby( groupby )[ sortby ].apply( lambda x: x.nlargest( nlargest ) ).reset_index( level = groupby ).index
    if nsmallest > 0:
        s = df[ [ groupby, sortby ] ].groupby( groupby )[ sortby ].apply( lambda x: x.nsmallest( nsmallest ) ).reset_index( level = groupby ).index
    std_stop = time.time()
    print("Standard Time: %s" %( std_stop - std_init ) )
    print( len( df[ df.index.isin(q)]))
    print( len( df[ df.index.isin(s)]))

### Generation of Non-Truth Matched Samples Corresponding to A Truth-Matched Sample ###
### ------------------------------------------------------------------------------- ###

def generate( truth_files_tuples, non_truth_files, merge_files = [], merge_name = "merger_data" ):
    merge   = True if len( merge_files ) > 0 else False
    counter = -1
    for truth_files_tuple in truth_files_tuples:
        counter        += 1
        non_truth_data  = utils.df_load( non_truth_files[ counter ] )
        print( "Length of Non-Truth Matched Data: %s" % ( len( non_truth_data) ) )
        if merge:
            merger_list = []
            for item in merge_files[ counter ]:
                merger_list.append( utils.df_load( item ) )
            merge_data = pd.concat( merger_list, axis = 1 )
        for truth_file in truth_files_tuple:
            truth_data           = utils.df_load( truth_file )
            events_truth_data    = truth_data[ "info_event_number" ]
            non_truth_selection  = non_truth_data[ "info_event_number" ].isin( events_truth_data )
            non_truth_data_slice = non_truth_data[ non_truth_selection ]
            if merge:
                merge_data_non_truth = merge_data[ non_truth_selection ]
            utils.df_save( non_truth_data_slice, filename = os.path.join( output_dir, os.path.basename( truth_file ).replace( ".ftr", "_non_truth_matched.ftr" ) ) )
            print( "Saved the file: %s" % ( os.path.join( output_dir, os.path.basename( truth_file ).replace( ".ftr", "_non_truth_matched.ftr" ) ) ) )
            if merge:
                utils.df_save( pd.concat( [non_truth_data_slice, merge_data_non_truth], axis = 1), filename = os.path.join( output_dir, os.path.basename( truth_file ).replace( ".ftr", "_non_truth_matched_with_%s.ftr" % ( merge_name ) ) ) )
                print( "Saved the file: %s" % ( os.path.join( output_dir, os.path.basename( truth_file ).replace( ".ftr", "_non_truth_matched_with_%s.ftr" % ( merge_name ) ) ) ) )
                

### Creating a Separate Sample of All Events with No Truth-Matched Candidate ###
### ------------------------------------------------------------------------ ###

"""
[ CODE ] [ CONTEXT ]
    - This is useful in some internal consistency checks. See, the `verify` function below.
"""

def extract_empty_events( data = None, non_truth_file = "", save_empty = False, return_num_candidates = False, return_df = False, return_num_events = False, file_message = "" ):
    if data is None:
        data = utils.df_load( non_truth_file )
    if non_truth_file != "" and file_message == "":
        file_message = non_truth_file
    highest_truth_indices        = data[ [ "info_truth_matching", "info_event_number" ] ].groupby( "info_event_number" )[ "info_truth_matching" ].apply( lambda x: x.nlargest( 1 ) ).reset_index( level = "info_event_number" ).index
    highest_truth_candidates     = data[ data.index.isin( highest_truth_indices ) ]
    highest_truth_fails          = highest_truth_candidates[ "info_truth_matching" ].isin( [ 0, False ] )
    empty_event_numbers          = highest_truth_candidates[ highest_truth_fails ][ "info_event_number" ]
    candidates_from_empty_events = data[ data[ "info_event_number" ].isin( empty_event_numbers ) ]
    num_candidates               = len( candidates_from_empty_events )
    num_events                   = len( empty_event_numbers )
    if save_empty:
        if file_message == "":
            print( "Can't save the empty-events file with an empty filename. Either provide `non_truth_file` of `file_message` when calling `extract_empty_events`." )
        else:
            utils.df_save( candidates_from_empty_events, filename = os.path.join( output_dir, os.path.basename( file_message ).replace( ".ftr", "_empty_of_truth.ftr" ) ) )
            print( "Saved the file: %s" % ( os.path.join( output_dir, os.path.basename( file_message ).replace( ".ftr", "_empty_of_truth.ftr" ) ) ) )
    if file_message != "":
        print( "Emptiness summary for %s:" % ( file_message ) )
        print( "Empty Events: %s" % ( num_events ) )
        print( "Candidates from Empty Events: %s" %( num_candidates ) )
    return_package = []
    if return_df:
        return_package.append( df )
    if return_num_events:
        return_package.append( num_events )
    if return_num_candidates:
        return_package.append( num_candidates )
    return tuple( return_package )

def generate_empty( non_truth_files ):
    for non_truth_file in non_truth_files:
        extract_empty_events( non_truth_file = non_truth_file, save_empty = True )
    

### Checking Internal Consistency ###
### ----------------------------- ###

def verify( truth_files_tuples, non_truth_files ):
    counter = -1
    for truth_files_tuple in truth_files_tuples:
        counter += 1
        non_truth_data                       = utils.df_load( non_truth_files[ counter ] )
        non_truth_candidate_num              = len( non_truth_data )
        non_truth_empty_candidate_num        = extract_empty_events( non_truth_data, return_num_candidates = True )[ 0 ]
        non_truth_candidate_num_from_slices  = 0
        for truth_file in truth_files_tuple:
            truth_data                       = utils.df_load( truth_file )
            events_truth_data                = truth_data[ "info_event_number" ]
            non_truth_data_slice             = utils.df_load( os.path.join( output_dir, os.path.basename( truth_file ).replace( ".ftr", "_non_truth_matched.ftr" ) ) )
            non_truth_data_slice_truth_mask  = ( non_truth_data_slice["info_truth_matching"].isin( [ 1, True ] ) )
            non_truth_data_slice_truth_slice = non_truth_data_slice[ non_truth_data_slice_truth_mask ].reindex( axis = "index" )
            non_truth_data_slice_truth_slice = non_truth_data_slice_truth_slice[ truth_data.columns ]
            match = non_truth_data_slice_truth_slice.merge( truth_data, indicator = True, how = "outer" )[ "_merge" ].eq( "both" ).all()
            """
            [ CODE ] [ WIP ]
                - Need to understand this trick better. Taken from https://stackoverflow.com/a/62378819/8792904. 
            """
            if( not match ):
                print( "The truth-matched version of non-truth slice failed to match with the truth-matched slice." )
                print( "Relevant truth-matched slice: %s" % ( truth_file ) )
                print( "Relevant non-truth-matched slice: %s" % ( non_truth_files[ counter ] ) )
                return False
            non_truth_candidate_num_from_slices += len( non_truth_data_slice )
        if( non_truth_candidate_num_from_slices != non_truth_candidate_num - non_truth_empty_candidate_num ):
            print( "Total number of candidates in slices of non-truth samples does not match the total number of candidates in the full non-truth matched sample." )
            print( "Relevant non-truth-matched sample: %s" % ( non_truth_files[ counter ] ) )
            return False
        print( "Verification succeeded for slicings of %s" % ( non_truth_files[ counter ] ) )
    return True

### Running ###
### ------- ###

def main():
    ### The `proba_files_gnnX` used here are not needed. This is only if you want to merge the proba files with the original files -- perhaps for some other tests.
    proba_files_gnn1 = utils.get_list_make_list( [ "/eos/user/j/jakoubek/RKstar/ml-analysis/gnn1_multiclass_t2_3_applied/ntuple-30059[0-1]_part_0[1]_gnn1_proba.ftr" ] )
    proba_files_gnn2 = utils.get_list_make_list( [ "/eos/user/j/jakoubek/RKstar/ml-analysis/gnn2_multiclass_t2_3_2_applied/ntuple-30059[0-1]_part_0[1]_gnn2_proba.ftr" ] )
    merger_proba_files = transpose_nested_list( [ proba_files_gnn1, proba_files_gnn2 ] )
    truth_files_sample1 = utils.get_list_make_list( [ "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v07/ntuple-30059[0-1]_part_0[1]_sample1.ftr" ] )
    truth_files_sample2 = utils.get_list_make_list( [ "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v07/ntuple-30059[0-1]_part_0[1]_sample2.ftr" ] )
    truth_files_tuples = transpose_nested_list( [ truth_files_sample1, truth_files_sample2 ])
    non_truth_files = utils.get_list_make_list( [ "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v07/ntuple-30059[0-1]_part_0[1].ftr" ] )
    
    ### This is the main generation of ntuple-samples without truth-matching
    generate( truth_files_tuples, non_truth_files, merger_proba_files, merge_name = "merged_proba" )

    ### This is also not necessarily part of the main workline. It only generates events with no truth matched candidates.
    #generate_empty( non_truth_files )

    verify( truth_files_tuples, non_truth_files )

output_dir = "/afs/cern.ch/user/d/dmankad/eoswork/NTuplesExpCheck"
if __name__ == "__main__":
    main()
