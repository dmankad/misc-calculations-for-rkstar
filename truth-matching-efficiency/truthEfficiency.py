### I/O Imports ###
### ----------- ###
import os
import json

### Matrix Manipulation Imports ###
### --------------------------- ###
import numpy as np
from pandas import DataFrame as df

### Plotting Imports ###
### ---------------- ###
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt

### Utils Imports ###
### ------------- ###
from ml_share import utils

### Declaring Paths ###
### --------------- ###
configFile        = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast.yaml"
configFileDefault = "configs/default.yaml"
model_dir_base    = "/afs/cern.ch/user/d/dmankad/eoswork/gnnResults/"
model_dir_path    = os.path.join( model_dir_base, os.path.basename( configFile ).replace( ".yaml", "" ) + "_applied" )

### Loading Configs/Selections ###
### -------------------------- ###
config           = utils.config_load( configFileDefault, configFile )
selection_scheme = "( $selection_main ) & ( $selection_mc_no_truth & $selection_usr) & ( $selection_q2low ) "
selection        = utils.get_selection( selection_scheme, config[ "common" ] )

### Declaring Strategies ###
### -------------------- ###
signal_prediction_strategies = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ]
truth_prediction_strategies  = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ]
colors_dict    = {}
linestyle_dict = {}
colors_dict[    "total_signal_score"   ] = "tab:blue"
colors_dict[    "max_ind_signal_score" ] = "tab:purple"
colors_dict[    "signal_score_diff"    ] = "tab:red"
linestyle_dict[ "total_signal_score"   ] = "-"
linestyle_dict[ "max_ind_signal_score" ] = "-."
linestyle_dict[ "signal_score_diff"    ] = ":"

### Getting Truth Efficiency ###
### ------------------------ ###
def get_truth_efficiency( predsFileKernel, orgFile, save = True ):

    ### Loading Files ###
    ### ------------- ###
    
    predsFile = utils.get_list_make_list( os.path.join( model_dir_path, predsFileKernel ) )
    orgFile   = utils.get_list_make_list( orgFile )
    orgData   = utils.df_load( orgFile, selection = selection, items_to_load = [ "B_mass", "Bbar_mass", "info_sample", "info_truth_matching", "info_event_number" ] )
    preds     = utils.df_load( predsFile )
    
    ### Creating Scores for Different Strategies ###
    ### ---------------------------------------- ###
    orgData[ "total_signal_score"   ] = preds[ "300590" ] + preds[ "300591" ]
    orgData[ "max_ind_signal_score" ] = preds[ [ "300590", "300591" ] ].max( axis = 1 )
    orgData[ "signal_score_diff"    ] = ( preds[ "300590" ] - preds[ "300591" ] ).abs()
    mass_name = "correct_B_Mass"
    orgData[ mass_name ] = orgData[ "info_sample" ].isin( [ "300590" ] ) * orgData[ "B_mass" ] + orgData[ "info_sample" ].isin( [ "300591" ] ) * orgData[ "Bbar_mass" ]

    ### Creating Different Empty Dictionaries Corresponding to Combination of Different Strategies ###
    ### ------------------------------------------------------------------------------------------ ###
    efficiency_matrix = {}
    cutflow_matrix    = {}
    for signal_prediction_strategy in signal_prediction_strategies:
        efficiency_matrix[ signal_prediction_strategy ] = {}
        cutflow_matrix[ signal_prediction_strategy ]    = {}
        for truth_prediction_strategy in truth_prediction_strategies:
            efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] = []
            cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ]    = {}

    ### Predicting Truth Candidate Per Each Event ###
    ### ----------------------------------------- ###
    idx_predicted_to_be_truth = {}
    truth_prediction_mask     = {}
    for strategy in truth_prediction_strategies:
        """

        [ WIP ][ DOUBT ][ COMMENT ][ ANALYSIS ]
            - The following implementation is not perfectly correct because it might be the case that two candidates have the exact same score -- and one of them is actually truth matched but the other is not. In that case, if we randomly choose the wrong one then it would set the efficiency calculation a bit off. However, I think this might not be an issue in the end for two reasons:
                - It is highly unlikely that two candidates would have the exact same score.
                - Even if it were to be likely, we would need a broader strategy to tackle this because we ultimately need to choose one candidate per event. Until we have such a strategy, it is not possible/meaningful to resolve this ambiguity in the calculation of truth efficiency, i.e., it is not an issue with the truth efficiency calculation per se.

        """
        idx_predicted_to_be_truth[ strategy ] = orgData[ [ strategy, "info_event_number"] ].groupby( "info_event_number" )[ strategy ].apply( lambda x: x.nlargest( 1 ) ).reset_index( level = "info_event_number" ).index
        truth_prediction_mask[ strategy ] = orgData.index.isin( idx_predicted_to_be_truth[ strategy ] )
    
    ### Candidates Coming from Events with MC Truth ###
    ### ------------------------------------------- ###
    idx_MC_truth =  orgData[ [ "info_truth_matching", "info_event_number" ] ].groupby( "info_event_number", sort = False )[ "info_truth_matching" ].transform("sum") > 0

    _nn_cut  = [ 0.0, 0.5, 0.8, 0.9, 0.99 ]
    nn_cut = [ round( i, 2 ) for i in list( np.arange( 0, 1.01, 0.01 ) ) ]
    figsaver = utils.fig_saver( os.path.join( model_dir_path, "mass_analysis_" + os.path.basename(predsFileKernel).replace(".ftr", ".pdf")))
    for nn_cut_value in nn_cut:
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ] = {}
                
        ### Predicting Signal Candidates ###
        ### ---------------------------- ###
        signal_prediction_mask = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            signal_prediction_mask[ signal_prediction_strategy ] = orgData[ signal_prediction_strategy ] >= nn_cut_value
            
            ### Filling Out Cutflow ###
            ### ------------------- ###
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_candidates" ] = float( signal_prediction_mask[ signal_prediction_strategy ].sum() )
    
        ### Combining Truth Prediction and Signal Prediction ###
        ### ------------------------------------------------ ###
        final_prediction = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            final_prediction[ signal_prediction_strategy ] = {}
            for truth_prediction_strategy in truth_prediction_strategies:
                final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] = signal_prediction_mask[ signal_prediction_strategy ] & truth_prediction_mask[ truth_prediction_strategy ]
                
                ### Filling Out Cutflow ###
                ### ------------------- ###
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events" ] = float( final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ].sum() )
        
        ### Evaluating Overlap of Final Prediction with MC Truth ###
        ### ---------------------------------------------------- ###
        info_truth_matching = orgData[ "info_truth_matching" ]
        final_prediction_and_MC_truth = {}
        final_prediction_and_MC_lie   = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            final_prediction_and_MC_truth[ signal_prediction_strategy ] = {}
            final_prediction_and_MC_lie[ signal_prediction_strategy ] = {}
            for truth_prediction_strategy in truth_prediction_strategies:
                final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ] = final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] & info_truth_matching
                final_prediction_and_MC_lie[ signal_prediction_strategy ][ truth_prediction_strategy ] = final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] & ~info_truth_matching
                
                ### Filling Out Cutflow ###
                ### ------------------- ###
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events_with_correct_truth_prediction" ] = float( final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ].sum() )

        ### Plotting the Mass ###
        ### ----------------- ###
        """
        [ WIP ]
            - Need to isolate this part as a separate function.
            - Although, it would probably involve splitting up the `get_truth_efficiency` function itself into multiple parts in order to get the information within `final_prediction` which is required to plot the mass.
        """

        dsid_candidates = [ "300590", "300591", "30059[0-1]" ]
        for dsid_candidate in dsid_candidates:
            dsid = dsid_candidate
        #mass_name = "B_mass" if dsid == "300590" else "Bbar_mass"
        fig, axs, lenX, lenY = utils.create_subplots( 9, dsid + " MC in low_q2 USR: " + mass_name + " ( gnn1_score_cut: " + str( nn_cut_value ) + ")" )
        counter = 0
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
                true = orgData[mass_name].loc[ final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ] ]
                false = orgData[mass_name].loc[ final_prediction_and_MC_lie[ signal_prediction_strategy ][ truth_prediction_strategy ] ]
                if( len( true ) != 0 ):
                    axs[ row_idx, col_idx ].hist( true, edgecolor = "red"  , facecolor = "red" , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, alpha = 0.4,histtype = "stepfilled", label = "correct truth-prediction ( n = " + str(len(true)) + " )" )
                if( len( false ) !=0 ):
                    axs[ row_idx, col_idx ].hist( false, edgecolor = "blue", facecolor = "blue", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, alpha = 0.4,histtype = "stepfilled", label = "incorrect truth-prediction ( n = "+ str( len( false ) ) + " )" )
                axs[ row_idx, col_idx ].hist( orgData[ info_truth_matching ][ mass_name ], edgecolor = "purple", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC truth")
                axs[ row_idx, col_idx ].hist( orgData[ ~info_truth_matching ][ mass_name ], edgecolor = "black", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC non-truth")
                axs[ row_idx, col_idx ].legend( loc = "upper left" )
                axs[ row_idx, col_idx ].set_title( "signal prediction: " + signal_prediction_strategy + ", truth prediction: best " + r"$N=1$" + " " + truth_prediction_strategy )
                axs[ row_idx, col_idx ].set_xlabel( mass_name + " [ MeV ]", loc = "right" )
                axs[ row_idx, col_idx ].set_ylabel("candidates(1/event)", loc = "top" )
                counter += 1
        figsaver.save( fig )
                
        
        ### Evaluating How Many Selected Events Had MC Truth ###
        ### ------------------------------------------------ ###
        MC_truth_and_signal_prediction = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            """

            [ COMMENT ][ NOTE-TO-FUTURE-SELF ]
                - Notice that it would be incorrect to do the following: 
            ```

            MC_truth_and_signal_prediction[ signal_prediction_strategy ] = orgData[ [ "info_truth_matching", "info_event_number" ] ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ].groupby( "info_event_number" )[ "info_truth_matching" ].apply( lambda x: x.nlargest( 1 ) )

            ```
               - Here, it might happen that the `signal_prediction_mask` leaves out the MC truth-matched candidate of an event but does keep other candidates from the event. In such a case, we would get a smaller number for the number of such events that are predicted to be signal and contain MC truth. 
               - Moreover, it is a computationally wasteful to do `groupby` and `apply` inside the loop over `nn_cut`.

            """
            MC_truth_and_signal_prediction[ signal_prediction_strategy ] = idx_MC_truth & final_prediction[ signal_prediction_strategy ][ truth_prediction_strategies[ 0 ] ]
            
            ### Filling Out Cutflow ###
            ### ------------------- ###
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events_with_MC_truth" ] = float( MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() )
                
        
        ### Calculating the Truth Efficiency ###
        ### -------------------------------- ###
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                eff = round( float( final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ].sum()) / float( MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() ), 4 ) if MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() !=0 else 1.0
                efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ].append( eff )
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "truth_efficiency" ] = eff
        print( "%-80s | Cut: %-20s" % ( predsFileKernel, nn_cut_value ) )
    
    figsaver.close()
    
    if save:
        with open( os.path.join( model_dir_path , "truth_efficiency_matrix_" + predsFileKernel.replace(".ftr", ".json") ), "w" ) as f:
            json.dump( efficiency_matrix, f, indent = 4 )
        with open( os.path.join( model_dir_path , "truth_efficiency_cutflow_" + predsFileKernel.replace(".ftr", ".json") ), "w" ) as f:
            json.dump( cutflow_matrix, f, indent = 4 )



    return efficiency_matrix

def plot_efficiency_matrix( efficiency_matrix, title, filename, nn_cut = [ round( i, 2 ) for i in list( np.arange( 0, 1.01, 0.01 ) ) ] ):

    ### Setting Up Plotting Environment ###
    ### ------------------------------- ###
    plt.clf()
    plt.cla()
    figsaver = utils.fig_saver( filename )
    fig, axs, lenX, lenY = utils.create_subplots( 6, title )
    counter = 0
    x_major_ticks = np.arange( 0.00, 1.01, 0.20 )
    x_minor_ticks = np.arange( 0.00, 1.01, 0.10 )
    y_major_ticks = np.arange( 0.75, 1.02, 0.05 )
    y_minor_ticks = np.arange( 0.75, 1.02, 0.05 )
    
    ### Comparing Efficiencies of Various Truth Prediction Strategies for a Given Signal Prediction Strategy ###
    ### ---------------------------------------------------------------------------------------------------- ###
    for signal_prediction_strategy in signal_prediction_strategies:
        row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
        for truth_prediction_strategy in truth_prediction_strategies:
            axs[ row_idx, col_idx ].plot( np.array( nn_cut ), np.array( efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] ), color = colors_dict[ signal_prediction_strategy ], ls = linestyle_dict[ truth_prediction_strategy ] , label = "best " + r"$ N = 1 $" + " gnn1_" + truth_prediction_strategy )
        xmin, xmax = axs[ row_idx, col_idx ].get_xlim()
        axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 \ L_{ xy } / ( \Delta L_{ xy } \chi^2( B ) ) $" )
        axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1\ \chi^2( B ) $" )
        axs[ row_idx, col_idx ].set_ylim( [ 0.8, 1.05 ] )
        axs[ row_idx, col_idx ].set_xlabel( "gnn1 score", loc = "right" )
        axs[ row_idx, col_idx ].set_ylabel( "truth efficiency", loc = "top" )
        axs[ row_idx, col_idx ].legend( loc = "lower left" )
        axs[ row_idx, col_idx ].set_title( "signal prediction: gnn1_" + signal_prediction_strategy )
        axs[ row_idx, col_idx ].set_xticks( x_major_ticks )
        axs[ row_idx, col_idx ].set_xticks( x_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].set_yticks( y_major_ticks )
        axs[ row_idx, col_idx ].set_yticks( y_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].grid( which = "both", color = "gray", linestyle = "--", linewidth = 0.2, alpha = 0.5 )
        counter += 1
        
    ### Comparing Efficiencies of Various Signal Prediction Strategies for a Given Truth Prediction Strategy ###
    ### ---------------------------------------------------------------------------------------------------- ###
    for truth_prediction_strategy in truth_prediction_strategies:
        row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
        for signal_prediction_strategy in signal_prediction_strategies:
            axs[ row_idx, col_idx ].plot( np.array( nn_cut ), np.array( efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] ), color = colors_dict[ signal_prediction_strategy ], ls = linestyle_dict[ truth_prediction_strategy ] , label = "signal-selection: gnn1_" + signal_prediction_strategy )
        xmin, xmax = axs[ row_idx, col_idx ].get_xlim()
        axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 \ L_{ xy } / ( \Delta L_{ xy } \chi^2( B ) ) $" )
        axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1 \ \chi^2 ( B ) $" )
        axs[ row_idx, col_idx ].set_ylim( [ 0.8, 1.05 ] )
        axs[ row_idx, col_idx ].set_xlabel( "gnn1 score", loc = "right" )
        axs[ row_idx, col_idx ].set_ylabel( "truth efficiency", loc = "top" )
        axs[ row_idx, col_idx ].legend( loc = "lower left" )
        axs[ row_idx, col_idx ].set_title( "truth prediction: best " + r"$N=1$" + " gnn1_" + truth_prediction_strategy )
        axs[ row_idx, col_idx ].set_xticks( x_major_ticks )
        axs[ row_idx, col_idx ].set_xticks( x_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].set_yticks( y_major_ticks )
        axs[ row_idx, col_idx ].set_yticks( y_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].grid( which = "both", color = "gray", linestyle = "--", linewidth = 0.2, alpha = 0.2 )
        counter += 1
    figsaver.save( fig )
    figsaver.close()

def main( predsFileKernels, orgFiles ):
    counter = -1
    for predsFileKernel in predsFileKernels:
        counter += 1
        orgFile = orgFiles[ counter ]
        efficiency_matrix = get_truth_efficiency( predsFileKernel, orgFile )
        if( "300590" in predsFileKernel ):
            title = "300590 MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "300591" in predsFileKernel ):
            title = "300591 MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "30059[0-1]" in predsFileKernel ):
            title = "30059[0-1] MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        filename = os.path.join( model_dir_path, "truth_efficiency_multiclass_options_" + predsFileKernel.replace( ".ftr", ".pdf" ) )
        plot_efficiency_matrix( efficiency_matrix, title, filename )
    
if( __name__ == "__main__" ):
    predsFileKernels  = [
        "ntuple-300590_part_0[1-5]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
        "ntuple-300591_part_0[1-3]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    ]
    orgFiles          = [
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300590_part_0[1-5]_sample2_non_truth_matched.ftr",
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300591_part_0[1-3]_sample2_non_truth_matched.ftr",
    ]
   #main( predsFileKernels, orgFiles )
    predsFileKernels  = [
        "ntuple-30059[0-1]_part_0[1-5]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    ]
    orgFiles          = [
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-30059[0-1]_part_0[1-5]_sample2_non_truth_matched.ftr",
    ]
    main( predsFileKernels, orgFiles )
