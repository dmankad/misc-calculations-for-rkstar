import pandas as pd
from pandas import DataFrame as df
import numpy as np

from ml_share import utils

best_meaning = {}
best_meaning[ "B_chi2" ]                       = -1
best_meaning[ "Lxy_significance" ]             =  1
best_meaning[ "Lxy_significance_over_B_chi2" ] =  1

def truth_efficiency_best_N( data, methods, N = [ 1 ] ):

    data_grouped = data[ ["info_event_number" ] + methods ].groupby( "info_event_number" )

    ### Building the Truth Mask ###
    ### ----------------------- ###

    truth = data["info_truth_matching"]
    total_events_with_truth   = len( data[ truth ].drop_duplicates( subset = [ "info_event_number" ] ) )

    ### Building the Best N Mask ###
    ### ------------------------ ###

    mask = {}
    eff  = {}
    eff[ "best-N" ] = N
    for method in methods:
        print( "Best N %s:" % ( method ) )
        mask[ method ] = {}
        eff[ method ]  = []
        for n in N:
            print( "\tN = %s" % ( n ) )
            if best_meaning[ method ] == -1:
                _mask = data_grouped.apply( lambda x: x.nsmallest( n, method ) ).reset_index( level = "info_event_number", drop  = True ).index
            if best_meaning[ method ] ==  1:
                _mask = data_grouped.apply( lambda x: x.nlargest( n, method ) ).reset_index( level = "info_event_number", drop = True ).index
            _mask = data.index.isin( _mask )
            mask[ method ][ str( n ) ] = _mask
    
            ### Calculating the Truth-Efficiency ###
            ### -------------------------------- ###

            total_events_with_overlap = len( data[ truth & _mask ].drop_duplicates( subset = [ "info_event_number" ] ) )
            _eff = float( total_events_with_overlap ) / float( total_events_with_truth )
            print( "\t\tEff: %s" % ( _eff ) )
            eff[ method ].append( _eff )

    return df( eff )

def main():

    ### Loading Files ###
    ### ------------- ###

    files            = [ "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-30059[0-1]_part_0[1-5]_sample2_non_truth_matched.ftr" ]
    files            = utils.get_list_make_list( files )
    config_file      = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast_thesis.yaml"
    config           = utils.config_load( "configs/default.yaml", config_file )
    #selection_scheme = "( $selection_main ) & ( $selection_mc_no_truth & $selection_usr) & ( $selection_q2low ) "
    #selection_scheme = "( $selection_main ) & ( $selection_mc_no_truth & $selection_nn1trb_high) & ( $selection_q2low ) "
    selection_scheme = "( $selection_main ) & ( $selection_mc_no_truth & $selection_nn1trb_low) & ( $selection_q2low ) "
    selection        = utils.get_selection( selection_scheme, config[ "common" ] )
    data             = utils.df_load( files, selection = selection )

    ### Building Higher-Order Features ###
    ### ------------------------------ ###

    data[ "Lxy_significance_over_B_chi2" ] = data[ "Lxy_significance" ] / data[ "B_chi2" ]

    ### Calculation of Truth-Matching ###
    ### ----------------------------- ###

    methods  = [ "B_chi2",  "Lxy_significance", "Lxy_significance_over_B_chi2" ]
    N         = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30 ]
    eff_table = truth_efficiency_best_N( data, methods, N )

    ### Exporting the Results ###
    ### --------------------- ###

    eff_table.to_csv( "best-N-truth-eff-nn1trb-low.csv" )
    

if __name__ == "__main__":
    main()
