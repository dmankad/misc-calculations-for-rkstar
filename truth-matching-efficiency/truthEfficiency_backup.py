import pandas as pd
from pandas import DataFrame as df
import matplotlib
from matplotlib import pyplot as plt
import os
import numpy as np
from ml_share import utils
from ml_share import ml_config

configFile = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast.yaml"
dir_path = "/afs/cern.ch/user/d/dmankad/eoswork/gnnResults/gnn1_multiclass_v4d_sculpt_original_dense_broadcast_applied"
configFileDefault = "configs/default.yaml"

print("Loading config...")
config = utils.config_load( configFileDefault, configFile )
print("Getting selection")

selection_string = "( $selection_main ) & ( $selection_mc_no_truth & $selection_usr) & ( $selection_q2low ) "
selections          = utils.get_selection( selection_string, config["common"])


predsFileKernels = [ 
    "ntuple-300590_part_0[1]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    #"ntuple-300591_part_0[1-3]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    ]
orgFiles = [
    "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300590_part_0[1]_sample2_non_truth_matched.ftr",
    #"/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300591_part_0[1-3]_sample2_non_truth_matched.ftr",
    ]

print("selections to be passed", selections)

counter = -1
for predsFileKernel in predsFileKernels:
    counter += 1
    orgFile = orgFiles[ counter ]
    predsFile    = os.path.join( dir_path, predsFileKernel )
    orgData = utils.df_load( orgFile, selection = selections )
    preds    = utils.df_load( predsFile )


    orgData["signal"] = preds["300590"] + preds["300591"]
    orgData["max_signal_proba"] = preds[ ["300590", "300591"] ].max( axis = 1) 
    orgData["signal_purity"] = ( preds["300590"] - preds["300591"] ).abs()

    info_truth_matching = orgData["info_truth_matching"]

    selected_indices_by_score = orgData[["signal", "info_event_number"]].groupby("info_event_number")["signal"].apply(lambda x: x.nlargest(1)).reset_index( level = "info_event_number" ).index
    selected_indices_by_score_ind = orgData[["max_signal_proba", "info_event_number"]].groupby("info_event_number")["max_signal_proba"].apply(lambda x: x.nlargest(1)).reset_index( level = "info_event_number" ).index
    selected_indices_by_score_pure = orgData[["signal_purity", "info_event_number"]].groupby("info_event_number")["signal_purity"].apply(lambda x: x.nlargest(1)).reset_index( level = "info_event_number" ).index

    score_selection_mask = orgData.index.isin( selected_indices_by_score )
    score_selection_mask_ind = orgData.index.isin( selected_indices_by_score_ind )
    score_selection_mask_pure = orgData.index.isin( selected_indices_by_score_pure )

    nn_cut = list(np.linspace(0,1,100))
    efficiency = []
    efficiency_ind = []
    efficiency_pure = []

    efficiency_total_total = []
    efficiency_total_ind   = []
    efficiency_total_pure  = []
    efficiency_ind_total   = []
    efficiency_ind_ind     = []
    efficiency_ind_pure    = []
    efficiency_pure_total  = []
    efficiency_pure_ind    = []
    efficiency_pure_pure   = []
    
    for nn_cut_value in nn_cut:
        signal_mask = ( preds["300590"] + preds["300591"] >= nn_cut_value )
        signal_mask_ind = ( orgData["max_signal_proba"] >= nn_cut_value )
        signal_mask_pure = ( orgData["signal_purity"] >= nn_cut_value )

        final_selected = score_selection_mask & signal_mask
        final_selected_ind = score_selection_mask_ind & signal_mask_ind
        final_selected_pure = score_selection_mask_pure & signal_mask_pure

        final_selected_total_total = score_selection_mask      & signal_mask
        final_selected_total_ind   = score_selection_mask_ind  & signal_mask
        final_selected_total_pure  = score_selection_mask_pure & signal_mask
        final_selected_ind_total   = score_selection_mask      & signal_mask_ind
        final_selected_ind_ind     = score_selection_mask_ind  & signal_mask_ind
        final_selected_ind_pure    = score_selection_mask_pure & signal_mask_ind
        final_selected_pure_total  = score_selection_mask      & signal_mask_pure
        final_selected_pure_ind    = score_selection_mask_ind  & signal_mask_pure
        final_selected_pure_pure   = score_selection_mask_pure & signal_mask_pure

        final_selected_and_truth_total_total  = final_selected_total_total & info_truth_matching
        final_selected_and_truth_total_ind    = final_selected_total_ind & info_truth_matching
        final_selected_and_truth_total_pure   = final_selected_total_pure & info_truth_matching
        final_selected_and_truth_ind_total    = final_selected_ind_total & info_truth_matching
        final_selected_and_truth_ind_ind      = final_selected_ind_ind & info_truth_matching
        final_selected_and_truth_ind_pure     = final_selected_ind_pure & info_truth_matching
        final_selected_and_truth_pure_total   = final_selected_pure_total & info_truth_matching
        final_selected_and_truth_pure_ind     = final_selected_pure_ind & info_truth_matching
        final_selected_and_truth_pure_pure    = final_selected_pure_pure & info_truth_matching

        final_selected_and_truth      = final_selected & info_truth_matching
        final_selected_and_truth_ind  = final_selected_ind & info_truth_matching
        final_selected_and_truth_pure = final_selected_pure & info_truth_matching

        truth_and_final_selected       = orgData[["info_truth_matching", "info_event_number"]].loc[signal_mask].groupby("info_event_number")["info_truth_matching"].apply(lambda x: x.nlargest(1))
        truth_and_final_selected_ind   = orgData[["info_truth_matching", "info_event_number"]].loc[signal_mask_ind].groupby("info_event_number")["info_truth_matching"].apply(lambda x: x.nlargest(1))
        truth_and_final_selected_pure  = orgData[["info_truth_matching", "info_event_number"]].loc[signal_mask_pure].groupby("info_event_number")["info_truth_matching"].apply(lambda x: x.nlargest(1))

        eff = round( float(final_selected_and_truth.sum())/ float(truth_and_final_selected.sum()), 4 ) if truth_and_final_selected.sum() !=0 else 1
        eff_ind = round( float(final_selected_and_truth_ind.sum())/ float(truth_and_final_selected_ind.sum()), 4 ) if truth_and_final_selected_ind.sum() !=0 else 1
        eff_pure = round( float(final_selected_and_truth_pure.sum())/float(truth_and_final_selected_pure.sum()), 4) if truth_and_final_selected_pure.sum() !=0 else 1

        eff_total_total   = round( float(final_selected_and_truth_total_total.sum())/ float(truth_and_final_selected.sum()), 4 ) if truth_and_final_selected.sum() !=0 else 1
        eff_total_ind     = round( float(final_selected_and_truth_total_ind.sum()  )/ float(truth_and_final_selected.sum()), 4 ) if truth_and_final_selected.sum() !=0 else 1
        eff_total_pure    = round( float(final_selected_and_truth_total_pure.sum() )/ float(truth_and_final_selected.sum()), 4 ) if truth_and_final_selected.sum() !=0 else 1
        eff_ind_total     = round( float(final_selected_and_truth_ind_total.sum()  )/ float(truth_and_final_selected_ind.sum()), 4 ) if truth_and_final_selected_ind.sum() !=0 else 1
        eff_ind_ind       = round( float(final_selected_and_truth_ind_ind.sum()    )/ float(truth_and_final_selected_ind.sum()), 4 ) if truth_and_final_selected_ind.sum() !=0 else 1
        eff_ind_pure      = round( float(final_selected_and_truth_ind_pure.sum()   )/ float(truth_and_final_selected_ind.sum()), 4 ) if truth_and_final_selected_ind.sum() !=0 else 1
        eff_pure_total    = round( float(final_selected_and_truth_pure_total.sum() )/ float(truth_and_final_selected_pure.sum()), 4) if truth_and_final_selected_pure.sum() !=0 else 1
        eff_pure_ind      = round( float(final_selected_and_truth_pure_ind.sum()   )/ float(truth_and_final_selected_pure.sum()), 4) if truth_and_final_selected_pure.sum() !=0 else 1
        eff_pure_pure     = round( float(final_selected_and_truth_pure_pure.sum()  )/ float(truth_and_final_selected_pure.sum()), 4) if truth_and_final_selected_pure.sum() !=0 else 1


        efficiency.append( eff )
        efficiency_ind.append( eff_ind )
        efficiency_pure.append( eff_pure )

        efficiency_total_total.append( eff_total_total )
        efficiency_total_ind.append( eff_total_ind )
        efficiency_total_pure.append( eff_total_pure )
        efficiency_ind_total.append( eff_ind_total )
        efficiency_ind_ind.append( eff_ind_ind )
        efficiency_ind_pure.append( eff_ind_pure )
        efficiency_pure_total.append( eff_pure_total )
        efficiency_pure_ind.append( eff_pure_ind )
        efficiency_pure_pure.append( eff_pure_pure )
        eff_matrix = df( [(eff_total_total, eff_total_ind, eff_total_pure), ( eff_ind_total, eff_ind_ind, eff_ind_pure), (eff_pure_total, eff_pure_ind, eff_pure_pure)], columns = ["best N=1 total score", "best N=1 max score", "best N=1 clean score"], index = ["selection total score", "selection max score", "selection clean score"] )
        #print("%-80s | %-20s | Eff Total Score: %-10s | Eff Multiclass High Score: %-10s | Eff Multiclass Pure Score: %-10s" % ( predsFileKernel, nn_cut_value, eff, eff_ind, eff_pure ) )
        if( eff_total_total != eff or eff_ind_ind != eff_ind or eff_pure_pure != eff_pure ):
            print("ERROR!")
            quit()
        print("%-80s | Cut: %-20s" % ( predsFileKernel, nn_cut_value ) )
        print( eff_matrix )


    plt.clf()
    plt.cla()
    if( "300590" in predsFileKernel ): 
        title = "300590 MC in low_q2 USR ("+os.path.basename( configFile ).replace(".yaml","")+")"
    if( "300591" in predsFileKernel ):
        title = "300591 MC in low_q2 USR ("+os.path.basename( configFile ).replace(".yaml","")+")"
    plt.title( title, fontsize = 8 )
    plt.plot( np.array( nn_cut ), np.array( efficiency ), color = "tab:blue", label = "eff. of best N=1 gnn1_score_total" )
    plt.plot( np.array( nn_cut ), np.array( efficiency_ind ), color= "tab:red", label = "eff. of best N=1 gnn1_score_max" )
    plt.plot( np.array( nn_cut ), np.array( efficiency_pure ), color= "tab:purple", label = "eff. of best N=1 gnn1_score_clean" )
    plt.xlabel("gnn1_score")
    plt.ylabel("truth-matching efficiency")
    ax = plt.gca()
    ax.set_ylim([0.8, 1.05])
    xmin, xmax = ax.get_xlim()
    plt.axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    plt.axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green", alpha = 0.7, label = "eff. of best N=1 chi2")
    plt.grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    plt.legend( loc = "upper left")
    plt.savefig( os.path.join( dir_path, "truth_efficiency_multiclass_" + predsFileKernel.replace(".ftr", ".pdf") ) )
    plt.clf()
    plt.cla()
    figsaver = utils.fig_saver( os.path.join( dir_path, "truth_efficiency_multiclass_options_" + predsFileKernel.replace(".ftr", ".pdf") ) )
    fig, axs, lenX, lenY = utils.create_subplots( 6, title )

    axs[ 0, 0 ].plot( np.array( nn_cut ), np.array( efficiency_total_total ), color = "tab:blue", ls = "-" , label = "best N=1 gnn1_score_total" )
    axs[ 0, 0 ].plot( np.array( nn_cut ), np.array( efficiency_total_ind   ), color = "tab:blue", ls = "-.", label = "best N=1 gnn1_score_max"   )
    axs[ 0, 0 ].plot( np.array( nn_cut ), np.array( efficiency_total_pure  ), color = "tab:blue", ls = ":" , label = "best N=1 gnn1_score_clean" )

    axs[ 0, 0 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 0, 0 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 0, 0 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 0, 0 ].set_ylim( [ 0.8, 1.05 ] )
    axs[ 0, 0 ].set_xlabel( "gnn1_score" )
    axs[ 0, 0 ].set_ylabel( "truth_efficiency" )
    axs[ 0, 0 ].legend( loc = "upper left" )
    axs[ 0, 0 ].set_title( "signal-selection: gnn1_score_total" )

    axs[ 0, 1 ].plot( np.array( nn_cut ), np.array( efficiency_ind_total ), color = "tab:purple", ls = "-" , label = "best N=1 gnn1_score_total" )
    axs[ 0, 1 ].plot( np.array( nn_cut ), np.array( efficiency_ind_ind   ), color = "tab:purple", ls = "-.", label = "best N=1 gnn1_score_max"   )
    axs[ 0, 1 ].plot( np.array( nn_cut ), np.array( efficiency_ind_pure  ), color = "tab:purple", ls = ":" , label = "best N=1 gnn1_score_clean" )

    axs[ 0, 1 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 0, 1 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green", alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 0, 1 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 0, 1 ].set_ylim([0.8, 1.05])
    axs[ 0, 1 ].set_xlabel("gnn1_score")
    axs[ 0, 1 ].set_ylabel("truth_efficiency")
    axs[ 0, 1 ].legend( loc = "upper left" )
    axs[ 0, 1 ].set_title( "signal-selection: gnn1_score_max" )

    axs[ 0, 2 ].plot( np.array( nn_cut ), np.array( efficiency_pure_total ), color = "tab:red", ls = "-" , label = "best N=1 gnn1_score_total" )
    axs[ 0, 2 ].plot( np.array( nn_cut ), np.array( efficiency_pure_ind   ), color = "tab:red", ls = "-.", label = "best N=1 gnn1_score_max"   )
    axs[ 0, 2 ].plot( np.array( nn_cut ), np.array( efficiency_pure_pure  ), color = "tab:red", ls = ":" , label = "best N=1 gnn1_score_clean" )

    axs[ 0, 2 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 0, 2 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green", alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 0, 2 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 0, 2 ].set_ylim([0.8, 1.05])
    axs[ 0, 2 ].set_xlabel("gnn1_score")
    axs[ 0, 2 ].set_ylabel("truth_efficiency")
    axs[ 0, 2 ].legend( loc = "upper left" )
    axs[ 0, 2 ].set_title( "signal-selection: gnn1_score_clean" )

    axs[ 1, 0 ].plot( np.array( nn_cut ), np.array( efficiency_total_total ), color = "tab:blue", ls = "-" , label = "signal selection: gnn1_score_total" )
    axs[ 1, 0 ].plot( np.array( nn_cut ), np.array( efficiency_ind_total  ), color = "tab:purple", ls = "-", label = "signal selection: gnn1_score_max"   )
    axs[ 1, 0 ].plot( np.array( nn_cut ), np.array( efficiency_pure_total  ), color = "tab:red", ls = "-" , label = "signal selection: gnn1_score_clean" )

    axs[ 1, 0 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 1, 0 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 1, 0 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 1, 0 ].set_ylim( [ 0.8, 1.05 ] )
    axs[ 1, 0 ].set_xlabel( "gnn1_score" )
    axs[ 1, 0 ].set_ylabel( "truth_efficiency" )
    axs[ 1, 0 ].legend( loc = "upper left" )
    axs[ 1, 0 ].set_title( "best N=1 gnn1_score_total" )

    axs[ 1, 1 ].plot( np.array( nn_cut ), np.array( efficiency_total_ind ), color = "tab:blue", ls = "-." , label = "signal selection: gnn1_score_total" )
    axs[ 1, 1 ].plot( np.array( nn_cut ), np.array( efficiency_ind_ind   ), color = "tab:purple", ls = "-.", label = "signal selection: gnn1_score_max"   )
    axs[ 1, 1 ].plot( np.array( nn_cut ), np.array( efficiency_pure_ind  ), color = "tab:red", ls = "-." , label = "signal selection: gnn1_score_clean" )

    axs[ 1, 1 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 1, 1 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green", alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 1, 1 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 1, 1 ].set_ylim([0.8, 1.05])
    axs[ 1, 1 ].set_xlabel("gnn1_score")
    axs[ 1, 1 ].set_ylabel("truth_efficiency")
    axs[ 1, 1 ].legend( loc = "upper left" )
    axs[ 1, 1 ].set_title( "best N=1 gnn1_score_max" )

    axs[ 1, 2 ].plot( np.array( nn_cut ), np.array( efficiency_total_pure ), color = "tab:blue", ls = ":" , label = "signal selection: gnn1_score_total" )
    axs[ 1, 2 ].plot( np.array( nn_cut ), np.array( efficiency_ind_pure   ), color = "tab:purple", ls = ":", label = "signal selection: gnn1_score_max"   )
    axs[ 1, 2 ].plot( np.array( nn_cut ), np.array( efficiency_pure_pure  ), color = "tab:red", ls = ":" , label = "signal selection: gnn1_score_clean" )

    axs[ 1, 2 ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "eff. of best N=1 Lxy_sig/chi2")
    axs[ 1, 2 ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green", alpha = 0.7, label = "eff. of best N=1 chi2")

    axs[ 1, 2 ].grid( color = "gray", linestyle = "--", linewidth = 0.3 )
    axs[ 1, 2 ].set_ylim([0.8, 1.05])
    axs[ 1, 2 ].set_xlabel("gnn1_score")
    axs[ 1, 2 ].set_ylabel("truth_efficiency")
    axs[ 1, 2 ].legend( loc = "upper left" )
    axs[ 1, 2 ].set_title( "best N=1 gnn1_score_clean" )

    figsaver.save( fig )
    figsaver.close()


