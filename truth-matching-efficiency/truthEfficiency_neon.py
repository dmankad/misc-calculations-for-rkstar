### I/O Imports ###
### ----------- ###
import os
import json

### Matrix Manipulation Imports ###
### --------------------------- ###
import numpy as np
from pandas import DataFrame as df

### Plotting Imports ###
### ---------------- ###
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt

### Utils Imports ###
### ------------- ###
from ml_share import utils

### Declaring Paths ###
### --------------- ###
configFile        = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast.yaml"
configFileDefault = "configs/default.yaml"
model_dir_base    = "/afs/cern.ch/user/d/dmankad/eoswork/gnnResults/"
model_dir_path    = os.path.join( model_dir_base, os.path.basename( configFile ).replace( ".yaml", "" ) + "_applied" )

### Loading Configs/Selections ###
### -------------------------- ###
config           = utils.config_load( configFileDefault, configFile )
selection_scheme = "( $selection_main ) & ( $selection_mc_no_truth & $selection_usr) & ( $selection_q2low ) "
selection        = utils.get_selection( selection_scheme, config[ "common" ] )

### Declaring Strategies ###
### -------------------- ###
signal_prediction_strategies = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ]
truth_prediction_strategies  = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff", "Lxy_significance_over_B_chi2", "B_chi2" ]
nn_based_strategies          = [ "total_signal_score", "max_ind_signal_score", "signal_score_diff" ]
cut_based_strategies         = [ "Lxy_significance_over_B_chi2", "B_chi2" ]
mass_prediction_strategies   = [ "correct_B_mass", "B_mass_Kstar_mass_closer", "nn_predicted_B_mass" ]
define_best = {
    "total_signal_score"           : 1,
    "max_ind_signal_score"         : 1,
    "signal_score_diff"            : 1,
    "Lxy_significance_over_B_chi2" : 1,
    "B_chi2"                       : -1,
    "Lxy_significance"             : 1
}
colors_dict    = {}
linestyle_dict = {}
colors_dict[    "Lxy_significance_over_B_chi2"   ] = "orange"
colors_dict[    "B_chi2"                         ] = "tab:green"
colors_dict[    "total_signal_score"             ] = "tab:blue"
colors_dict[    "max_ind_signal_score"           ] = "tab:purple"
colors_dict[    "signal_score_diff"              ] = "tab:red"
linestyle_dict[ "Lxy_significance_over_B_chi2"   ] = "-"
linestyle_dict[ "B_chi2"                         ] = "-"
linestyle_dict[ "total_signal_score"             ] = "-"
linestyle_dict[ "max_ind_signal_score"           ] = "-."
linestyle_dict[ "signal_score_diff"              ] = ":"

### Getting Truth Efficiency ###
### ------------------------ ###
def get_truth_efficiency( predsFileKernel, orgFile, save = True ):

    ### Loading Files ###
    ### ------------- ###
    predsFile = utils.get_list_make_list( os.path.join( model_dir_path, predsFileKernel ) )
    orgFile   = utils.get_list_make_list( orgFile )
    orgData   = utils.df_load(
                   orgFile,
                   selection     = selection,
                   items_to_load = [ 
                       "B_mass", "Bbar_mass", "info_sample", "info_truth_matching", "info_event_number", "B_mass_Kstar_mass_closer", "Lxy_significance", "B_chi2" 
                   ] 
                )
    preds     = utils.df_load( predsFile )
    
    ### Creating Scores for Different Strategies ###
    ### ---------------------------------------- ###
    orgData[ "total_signal_score"           ] = preds[ "300590" ] + preds[ "300591" ]
    orgData[ "max_ind_signal_score"         ] = preds[ [ "300590", "300591" ] ].max( axis = 1 )
    orgData[ "signal_score_diff"            ] = ( preds[ "300590" ] - preds[ "300591" ] ).abs()
    orgData[ "Lxy_significance_over_B_chi2" ] = orgData[ "Lxy_significance" ] / orgData[ "B_chi2" ]
    orgData[ "correct_B_mass"               ] = orgData[ "info_sample" ].isin( [ "300590", "300592" ] ) * orgData[ "B_mass" ] + orgData[ "info_sample" ].isin( [ "300591", "300593" ] ) * orgData[ "Bbar_mass" ]
    orgData[ "nn_predicted_B_mass"          ] = orgData["B_mass"] * ( preds[ "300590" ] >= preds[ "300591" ] ) + orgData["Bbar_mass"] * ( preds["300590"] < preds["300591"] )

    ### Creating Different Empty Dictionaries Corresponding to Combination of Different Strategies ###
    ### ------------------------------------------------------------------------------------------ ###
    efficiency_matrix    = {}
    cutflow_matrix       = {}
    idx_sorted_for_truth = {}
    for signal_prediction_strategy in signal_prediction_strategies:
        efficiency_matrix[ signal_prediction_strategy ] = {}
        cutflow_matrix[ signal_prediction_strategy ]    = {}
        for truth_prediction_strategy in truth_prediction_strategies:
            efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] = []
            cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ]    = {}

    ### Sorting Indices for Truth Prediction ###
    ### ------------------------------------ ###
    for strategy in truth_prediction_strategies:
        idx_sorted_for_truth[ strategy ] = orgData.sort_values( strategy, ascending = True if define_best[ strategy ] < 0 else False )
    
    ### Candidates Coming from Events with MC Truth ###
    ### ------------------------------------------- ###
    idx_MC_truth =  orgData[ [ "info_truth_matching", "info_event_number" ] ].groupby( "info_event_number", sort = False )[ "info_truth_matching" ].transform("sum") > 0

    nn_cut = [ round( i, 2 ) for i in list( np.arange( 0, 1.01, 0.01 ) ) ]
    figsavers = {}
    for mass_prediction_strategy in mass_prediction_strategies:
        figsaver = utils.fig_saver( os.path.join( model_dir_path, mass_prediction_strategy + "_mass_analysis_v2_" + os.path.basename(predsFileKernel).replace(".ftr", ".pdf")))
        figsavers[ mass_prediction_strategy ] = figsaver
    for nn_cut_value in nn_cut:
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ] = {}
                
        ### Predicting Signal Candidates ###
        ### ---------------------------- ###
        signal_prediction_mask = {}
        final_prediction  = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            final_prediction[ signal_prediction_strategy ] = {}
            signal_prediction_mask[ signal_prediction_strategy ] = orgData[ signal_prediction_strategy ] >= nn_cut_value

            ### Predicting the Truth Candidate ###
            ### ------------------------------ ###
            for truth_prediction_strategy in truth_prediction_strategies:

                final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] = orgData.index.isin(
                        idx_sorted_for_truth[ truth_prediction_strategy ].loc[ 
                            idx_sorted_for_truth[ truth_prediction_strategy ].index.isin( 
                                orgData[ signal_prediction_mask[ signal_prediction_strategy ] ].index 
                                ) 
                            ].groupby( "info_event_number" ).head( 1 ).index )
            
        ### Evaluating Overlap of Final Prediction with MC Truth ###
        ### ---------------------------------------------------- ###
        info_truth_matching = orgData[ "info_truth_matching" ]
        final_prediction_and_MC_truth = {}
        final_prediction_and_MC_lie   = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            final_prediction_and_MC_truth[ signal_prediction_strategy ] = {}
            final_prediction_and_MC_lie[ signal_prediction_strategy ] = {}
            for truth_prediction_strategy in truth_prediction_strategies:
                final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ] = final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] & info_truth_matching
                final_prediction_and_MC_lie[ signal_prediction_strategy ][ truth_prediction_strategy ] = final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] & ~info_truth_matching
        
        ### Filling Out Cutflow ###
        ### ------------------- ###
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_candidates" ] = float( signal_prediction_mask[ signal_prediction_strategy ].sum() )
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events" ] = float( final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ].sum() )
  
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events_with_correct_truth_prediction" ] = float( final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ].sum() )
        ### Plotting the Mass ###
        ### ----------------- ###
        """
        [ WIP ]
            - Need to isolate this part as a separate function.
            - Although, it would probably involve splitting up the `get_truth_efficiency` function itself into multiple parts in order to get the information within `final_prediction` which is required to plot the mass.
        """
        """
        dsid_candidates = [ "300590", "300591", "30059[0-1]", "300592", "300593", "30059[2-3]" ]
        for dsid_candidate in dsid_candidates:
            if dsid_candidate in predsFileKernel:
                dsid = dsid_candidate
        num = len( signal_prediction_strategies ) * len( truth_prediction_strategies )
        for mass_prediction_strategy in mass_prediction_strategies:
            fig, axs, ratio_axs, lenX, lenY = utils.create_subplots_with_ratios( num, dsid + " MC in low_q2 USR ( gnn1_score_cut: " + str( nn_cut_value ) + ", B_mass_prediction_strategy: " + mass_prediction_strategy + " ) ", ratio_size = 3, v_margin = 2, h_margin = 2, num_ratio = 3 )
            counter = 0
            for signal_prediction_strategy in signal_prediction_strategies:
                for truth_prediction_strategy in truth_prediction_strategies:
                    row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
                    true  = orgData[mass_prediction_strategy].loc[ final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ] ]
                    false = orgData[mass_prediction_strategy].loc[ final_prediction_and_MC_lie[ signal_prediction_strategy ][ truth_prediction_strategy ] ]
                    both_true_false = orgData[ mass_prediction_strategy ].loc[ final_prediction[ signal_prediction_strategy ][ truth_prediction_strategy ] ]

                    if len( true ) != 0 :
                        hist_true , bins, _ = plt.hist( true , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                        axs[ counter ].hist( true, edgecolor = "red"  , facecolor = "red" , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, alpha = 0.4, histtype = "stepfilled", label = "Correct Truth-Prediction in NN-Selected Events ( Predicted Mass ) ( n = " + str(len(true)) + " )" )
                    if len( false ) !=0 :
                        hist_false, bins, _ = plt.hist( false, bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                        axs[ counter ].hist( false, edgecolor = "blue", facecolor = "blue", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, alpha = 0.4, histtype = "stepfilled", label = "Incorrect Truth-Prediction in NN-Selected Events( Predicted Mass ) ( n = "+ str( len( false ) ) + " )" )

                    hist_both_true_false, bins, _          = plt.hist( both_true_false, bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                    hist_truth, bins, _                    = plt.hist( orgData[ info_truth_matching ][ "correct_B_mass" ], bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                    hist_non_truth, bins, _                = plt.hist( orgData[ ~info_truth_matching ][ "correct_B_mass" ], bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                    hist_truth_in_selected_events, bins, _     = plt.hist( orgData[ info_truth_matching ][ "correct_B_mass" ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ], bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
                    hist_non_truth_in_selected_events, bins, _ = plt.hist( orgData[ ~info_truth_matching ][ "correct_B_mass" ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ], bins = list( np.linspace( 4000, 5700, 100 ) ), density = True )
 
                    axs[ counter ].hist( both_true_false, edgecolor = "tab:green" , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "Truth-Prediction in NN-Selected Events ( Predicted Mass ) ( n = " + str( len( both_true_false ) ) + " )"     )
                    axs[ counter ].hist( orgData[ info_truth_matching  ][ "correct_B_mass" ], edgecolor = "darkred" , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC Truth in All Events ( Correct Mass )"     )
                    axs[ counter ].hist( orgData[ ~info_truth_matching ][ "correct_B_mass" ], edgecolor = "darkblue", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC Non-Truth in All Events ( Correct Mass )" )
                    axs[ counter ].hist( orgData[ info_truth_matching  ][ "correct_B_mass" ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ], edgecolor = "tab:orange" , bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC Truth in NN-Selected Events ( Correct Mass )")
                    axs[ counter ].hist( orgData[ ~info_truth_matching ][ "correct_B_mass" ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ], edgecolor = "teal", bins = list( np.linspace( 4000, 5700, 100 ) ), density = True, histtype = "step", label = "MC Non-Truth in NN-Selected Events ( Correct Mass )")


                    axs[ counter ].legend( loc = "upper left", prop = { "size": 6 } )
                    axs[ counter ].set_title( "signal prediction: " + signal_prediction_strategy + ", truth prediction: best " + r"$N=1$" + " " + truth_prediction_strategy, fontsize = 6 )
                    #axs[ counter ].set_xlabel( "correct_B_mass" + " [ MeV ]", loc = "right" )
                    axs[ counter ].set_ylabel("candidates", loc = "top", fontsize = 6 )
                    if len( true ) != 0:
                        ratio_axs[ counter ][ 0 ].plot( bins[ :-1 ], hist_true  / hist_truth                       , label = "Correct Truth-Prediction in NN-Selected Events / MC-Truth in All Events"              , color = "darkred"    )
                        ratio_axs[ counter ][ 0 ].plot( bins[ :-1 ], hist_true  / hist_truth_in_selected_events    , label = "Correct Truth-Prediction in NN-Selected Events / MC-Truth in NN-Selected Events"      , color = "tab:orange" )
                    if len( false ) != 0: 
                        ratio_axs[ counter ][ 1 ].plot( bins[ :-1 ], hist_false / hist_non_truth                   , label = "Incorrect Truth-Prediction / MC Non-Truth in All Events"        , color = "darkblue" )
                        ratio_axs[ counter ][ 1 ].plot( bins[ :-1 ], hist_false / hist_non_truth_in_selected_events, label = "Incorrect Truth-Prediction / MC Non-Truth in NN-Selected Events", color = "teal"     )
                    ratio_axs[ counter ][ 2 ].plot( bins[ :-1 ], hist_both_true_false / hist_truth                   , label = "Truth-Prediction in NN-Selected Events / MC-Truth in All Events"                              , color = "tab:green"  )
                    ratio_axs[ counter ][ 2 ].plot( bins[ :-1 ], hist_both_true_false / hist_truth_in_selected_events, label = "Truth-Prediction in NN-Selected Events / MC-Truth in NN-Selected Events"                      , color = "darkgreen"  )
                    #ratio_axs[ counter ][ 0 ].set_xlabel(  "correct_B_mass" + " [ MeV ]", loc = "right"  )
                    ratio_axs[ counter ][ 0 ].set_ylabel( "ratio", fontsize = 6 )
                    ratio_axs[ counter ][ 0 ].legend( loc = "upper left", prop = { "size" : 6 } )
                    ratio_axs[ counter ][ 0 ].axhline( y = 1.0, linestyle = "--", color = "black" , alpha = 0.5 )
                    ratio_axs[ counter ][ 0 ].set_ylim( [ 0.0, 3.1 ] )
                    ratio_axs[ counter ][ 1 ].set_xlabel( "B_mass" + " [ MeV ]", loc = "right", fontsize = 6  )
                    ratio_axs[ counter ][ 1 ].set_ylabel( "ratio", fontsize = 6 )
                    ratio_axs[ counter ][ 1 ].legend( loc = "upper left", prop = { "size" : 6 } )
                    ratio_axs[ counter ][ 1 ].axhline( y = 1.0, linestyle = "--", color = "black" , alpha = 0.5 )
                    ratio_axs[ counter ][ 1 ].set_ylim( [ 0.0, 3.1 ] )
                    ratio_axs[ counter ][ 2 ].set_xlabel( "B_mass" + " [ MeV ]", loc = "right", fontsize = 6  )
                    ratio_axs[ counter ][ 2 ].set_ylabel( "ratio", fontsize = 6 )
                    ratio_axs[ counter ][ 2 ].legend( loc = "upper left", prop = { "size" : 6 } )
                    ratio_axs[ counter ][ 2 ].axhline( y = 1.0, linestyle = "--", color = "black" , alpha = 0.5 )
                    ratio_axs[ counter ][ 2 ].set_ylim( [ 0.0, 3.1 ] )
                    counter += 1
            figsavers[ mass_prediction_strategy ].save( fig )    
        """
        
        ### Evaluating How Many Selected Events Had MC Truth ###
        ### ------------------------------------------------ ###
        MC_truth_and_signal_prediction = {}
        for signal_prediction_strategy in signal_prediction_strategies:
            """

            [ COMMENT ][ NOTE-TO-FUTURE-SELF ]
                - Notice that it would be incorrect to do the following: 
            ```

            MC_truth_and_signal_prediction[ signal_prediction_strategy ] = orgData[ [ "info_truth_matching", "info_event_number" ] ].loc[ signal_prediction_mask[ signal_prediction_strategy ] ].groupby( "info_event_number" )[ "info_truth_matching" ].apply( lambda x: x.nlargest( 1 ) )

            ```
               - Here, it might happen that the `signal_prediction_mask` leaves out the MC truth-matched candidate of an event but does keep other candidates from the event. In such a case, we would get a smaller number for the number of such events that are predicted to be signal and contain MC truth. 
               - Moreover, it is a computationally wasteful to do `groupby` and `apply` inside the loop over `nn_cut`.

            """
            MC_truth_and_signal_prediction[ signal_prediction_strategy ] = idx_MC_truth & final_prediction[ signal_prediction_strategy ][ truth_prediction_strategies[ 0 ] ]
            
            ### Filling Out Cutflow ###
            ### ------------------- ###
            for truth_prediction_strategy in truth_prediction_strategies:
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "predicted_signal_events_with_MC_truth" ] = float( MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() )
                
        
        ### Calculating the Truth Efficiency ###
        ### -------------------------------- ###
        for signal_prediction_strategy in signal_prediction_strategies:
            for truth_prediction_strategy in truth_prediction_strategies:
                eff = round( float( final_prediction_and_MC_truth[ signal_prediction_strategy ][ truth_prediction_strategy ].sum()) / float( MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() ), 4 ) if MC_truth_and_signal_prediction[ signal_prediction_strategy ].sum() !=0 else 1.0
                efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ].append( eff )
                cutflow_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ][ str( nn_cut_value ) ][ "truth_efficiency" ] = eff
        print( "%-80s | Cut: %-20s" % ( predsFileKernel, nn_cut_value ) )
    
    for mass_prediction_strategy in mass_prediction_strategies:
        figsavers[ mass_prediction_strategy ].close()
    
    if save:
        with open( os.path.join( model_dir_path , "truth_efficiency_matrix_v2_" + predsFileKernel.replace(".ftr", ".json") ), "w" ) as f:
            json.dump( efficiency_matrix, f, indent = 4 )
        with open( os.path.join( model_dir_path , "truth_efficiency_cutflow_v2_" + predsFileKernel.replace(".ftr", ".json") ), "w" ) as f:
            json.dump( cutflow_matrix, f, indent = 4 )



    return efficiency_matrix

def plot_efficiency_matrix( efficiency_matrix, title, filename, nn_cut = [ round( i, 2 ) for i in list( np.arange( 0, 1.01, 0.01 ) ) ] ):

    ### Setting Up Plotting Environment ###
    ### ------------------------------- ###
    plt.clf()
    plt.cla()
    figsaver = utils.fig_saver( filename.replace("truth_efficiency", "truth_efficiency_compare_truth_prediction_strategies") )
    fig, axs, lenX, lenY = utils.create_subplots( len( signal_prediction_strategies ), title )
    counter = 0
    x_major_ticks = np.arange( 0.00, 1.01, 0.20 )
    x_minor_ticks = np.arange( 0.00, 1.01, 0.10 )
    y_major_ticks = np.arange( 0.75, 1.02, 0.05 )
    y_minor_ticks = np.arange( 0.75, 1.02, 0.05 )
    
    ### Comparing Efficiencies of Various Truth Prediction Strategies for a Given Signal Prediction Strategy ###
    ### ---------------------------------------------------------------------------------------------------- ###
    for signal_prediction_strategy in signal_prediction_strategies:
        row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
        for truth_prediction_strategy in truth_prediction_strategies:
            if truth_prediction_strategy in nn_based_strategies:
                axs[ row_idx, col_idx ].plot( np.array( nn_cut ), np.array( efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] ), color = colors_dict[ truth_prediction_strategy ], ls = "-" , label = "best " + r"$ N = 1 $" + " " + truth_prediction_strategy )
            else:
                axs[ row_idx, col_idx ].plot( np.array( nn_cut ), np.array( efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] ), color = colors_dict[ truth_prediction_strategy ], ls = linestyle_dict[ truth_prediction_strategy ] , label = "best " + r"$ N = 1 $" + " " + truth_prediction_strategy )
        xmin, xmax = axs[ row_idx, col_idx ].get_xlim()
        #axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 \ L_{ xy } / ( \Delta L_{ xy } \chi^2( B ) ) $ at baseline" )
        #axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1\ \chi^2( B ) $ at baseline" )
        axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 $ Lxy_significance_over_B_chi2 at baseline" )
        axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1 $ B_chi2 at baseline" )
        axs[ row_idx, col_idx ].set_ylim( [ 0.8, 1.05 ] )
        axs[ row_idx, col_idx ].set_xlabel( "gnn1 score", loc = "right" )
        axs[ row_idx, col_idx ].set_ylabel( "truth efficiency", loc = "top" )
        axs[ row_idx, col_idx ].legend( loc = "lower left" )
        axs[ row_idx, col_idx ].set_title( "signal prediction: " + signal_prediction_strategy )
        axs[ row_idx, col_idx ].set_xticks( x_major_ticks )
        axs[ row_idx, col_idx ].set_xticks( x_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].set_yticks( y_major_ticks )
        axs[ row_idx, col_idx ].set_yticks( y_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].grid( which = "both", color = "gray", linestyle = "--", linewidth = 0.2, alpha = 0.5 )
        counter += 1
    figsaver.save( fig )
    figsaver.close() 
    plt.clf()
    plt.cla()
    figsaver = utils.fig_saver( filename.replace("truth_efficiency", "truth_efficiency_compare_signal_prediction_strategies") )
    fig, axs, lenX, lenY = utils.create_subplots( len( truth_prediction_strategies ), title )
    counter = 0
    x_major_ticks = np.arange( 0.00, 1.01, 0.20 )
    x_minor_ticks = np.arange( 0.00, 1.01, 0.10 )
    y_major_ticks = np.arange( 0.75, 1.02, 0.05 )
    y_minor_ticks = np.arange( 0.75, 1.02, 0.05 )

    ### Comparing Efficiencies of Various Signal Prediction Strategies for a Given Truth Prediction Strategy ###
    ### ---------------------------------------------------------------------------------------------------- ###
    for truth_prediction_strategy in truth_prediction_strategies:
        row_idx, col_idx = utils.get_subplot_idx( counter, lenX, lenY )
        for signal_prediction_strategy in signal_prediction_strategies:
            axs[ row_idx, col_idx ].plot( np.array( nn_cut ), np.array( efficiency_matrix[ signal_prediction_strategy ][ truth_prediction_strategy ] ), color = colors_dict[ signal_prediction_strategy ], ls = "-" , label = "signal-selection: " + signal_prediction_strategy )
        xmin, xmax = axs[ row_idx, col_idx ].get_xlim()
        #axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 \ L_{ xy } / ( \Delta L_{ xy } \chi^2( B ) ) $ at baseline" )
        #axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1 \ \chi^2 ( B ) $ at baseline" )
        axs[ row_idx, col_idx ].axhline( y = 0.894, xmin = xmin, xmax = xmax, linestyle = "--", color = "orange", alpha = 0.7, label = "best "+ r"$ N = 1 $ Lxy_significance_over_B_chi2 at baseline" )
        axs[ row_idx, col_idx ].axhline( y = 0.834, xmin = xmin, xmax = xmax, linestyle = "--", color = "green" , alpha = 0.7, label = "best "+ r"$ N = 1 $ B_chi2 at baseline" )
        axs[ row_idx, col_idx ].set_ylim( [ 0.8, 1.05 ] )
        axs[ row_idx, col_idx ].set_xlabel( "gnn1 score", loc = "right" )
        axs[ row_idx, col_idx ].set_ylabel( "truth efficiency", loc = "top" )
        axs[ row_idx, col_idx ].legend( loc = "lower left" )
        axs[ row_idx, col_idx ].set_title( "truth prediction: best " + r"$N=1$" + " " + truth_prediction_strategy )
        axs[ row_idx, col_idx ].set_xticks( x_major_ticks )
        axs[ row_idx, col_idx ].set_xticks( x_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].set_yticks( y_major_ticks )
        axs[ row_idx, col_idx ].set_yticks( y_minor_ticks, minor = True )
        axs[ row_idx, col_idx ].grid( which = "both", color = "gray", linestyle = "--", linewidth = 0.2, alpha = 0.2 )
        counter += 1
    figsaver.save( fig )
    figsaver.close()

def main( predsFileKernels, orgFiles ):
    counter = -1
    for predsFileKernel in predsFileKernels:
        counter += 1
        orgFile = orgFiles[ counter ]
        efficiency_matrix = get_truth_efficiency( predsFileKernel, orgFile )
        if( "300590" in predsFileKernel ):
            title = "300590 MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "300592" in predsFileKernel ):
            title = "300592 MC in high_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "300591" in predsFileKernel ):
            title = "300591 MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "300593" in predsFileKernel ):
            title = "300593 MC in high_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "30059[0-1]" in predsFileKernel ):
            title = "30059[0-1] MC in low_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        if( "30059[2-3]" in predsFileKernel ):
            title = "30059[2-3] MC in high_q2 USR (" + os.path.basename( configFile ).replace( ".yaml", "" ) + ")"
        filename = os.path.join( model_dir_path, "truth_efficiency_v2_" + predsFileKernel.replace( ".ftr", ".pdf" ) )
        plot_efficiency_matrix( efficiency_matrix, title, filename )
    
if( __name__ == "__main__" ):
    predsFileKernels  = [
        "ntuple-300590_part_0[1-5]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
        "ntuple-300591_part_0[1-3]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    ]
    orgFiles          = [
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300590_part_0[1-5]_sample2_non_truth_matched.ftr",
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-300591_part_0[1-3]_sample2_non_truth_matched.ftr",
    ]
   #main( predsFileKernels, orgFiles )
    predsFileKernels  = [
        "ntuple-30059[0-1]_part_0[1-5]_sample2_non_truth_matched_q2low_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
        #"ntuple-30059[2-3]_part_0[1-5]_sample2_non_truth_matched_q2high_usr_gnn1_multiclass_v4d_sculpt_original_dense_broadcast_proba.ftr",
    ]
    orgFiles          = [
        "/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-30059[0-1]_part_0[1-5]_sample2_non_truth_matched.ftr",
        #"/afs/cern.ch/user/d/dmankad/eoswork/NTuples/ntuple-30059[2-3]_part_0[1-5]_sample2_non_truth_matched.ftr",
    ]
    main( predsFileKernels, orgFiles )
