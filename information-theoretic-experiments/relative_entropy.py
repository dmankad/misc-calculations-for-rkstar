import pandas as pd
from pandas import DataFrame as df
import numpy as np

from ml_share import utils

best_meaning = {}
best_meaning[ "B_chi2" ]                       = -1
best_meaning[ "Lxy_significance" ]             =  1
best_meaning[ "Lxy_significance_over_B_chi2" ] =  1

def get_relative_entropy( pdf1, pdf2, pdf = True, bins = None, range = None ):
    if pdf is False:
        pdf1, _ = np.histogram( pdf1, bins, range, density = True )
        pdf2, _ = np.histogram( pdf2, bins, range, density = True )
    entropy = np.sum( pdf1 * np.log( pdf1/pdf2 ) )
    return entropy

def get_bestN_relative_entropy( sig, bkg, features, methods, bins = 50, N = [ i + 1 for i in range( 10 ) ] + [ 15, 20, 25, 30 ] ):
    """
    comment-block:
        tags:
            - theory
            - statistics
        id: 161120221600
        comments:
            - Relative entropy between two probability distributions gives the discrimination information between the two probability distributions.
            - In our context, the relative entropy between the signal and the background histograms (normalized) of a feature tells us how useful the feature would be in discriminating between signal and background.
            - Mathematically, it's calculated as the Kullback–Leibler divergence between two probability distributions.
            - See, [Wikipedia article on Kullback-Leiber divergence](https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Discrimination_information).
    """
    mask = get_bestN_masks( bkg, methods, N )
    entropy = {}
    print( "Calculating Relative Entropies b/w Signal & Background..." )
    for method in methods:
        entropy[ method ] = {}
        for n in N:
            entropy[ method ][ str( n ) ] = {}
            for feature in features:
                _entropy = get_relative_entropy( sig[ feature ], bkg[ mask[ feature ][ str( n ) ] ][ feature ], pdf = False, bins = bins )
                entropy[ method ][ str( n ) ][ feature ] = _entropy
                print( "\tBest N = %s %s | Relative Entropy for %s: %s" % ( n, method, feature, _entropy ) )
    return entropy

def get_bestN_masks( data, methods = [ "B_chi2", "Lxy_sigifnicance", "Lxy_significance_over_B_chi2" ], N = [ i + 1 for i in range( 10 ) ] + [ 15, 20, 25, 30 ] ):

    data_grouped = data[ ["info_event_number" ] + methods ].groupby( "info_event_number" )

    ### Building the Best N Mask ###
    ### ------------------------ ###

    mask = {}
    print( "Building Best-N Masks..." )
    for method in methods:
        mask[ method ] = {}
        for n in N:
            print( "\tBest N = %s %s" % ( n, method ) )
            if best_meaning[ method ] == -1:
                _mask = data_grouped.apply( lambda x: x.nsmallest( n, method ) ).reset_index( level = "info_event_number", drop  = True ).index
            if best_meaning[ method ] ==  1:
                _mask = data_grouped.apply( lambda x: x.nlargest( n, method ) ).reset_index( level = "info_event_number", drop = True ).index
            _mask = data.index.isin( _mask )
            mask[ method ][ str( n ) ] = _mask
    
    return mask



def main( region, methods = [ "B_chi2", "Lxy_significance", "Lxy_significance_over_B_chi2" ], N = [ i + 1 for i in range( 10 ) ] + [ 15, 20, 25, 30 ] ):

    ### Listing Files ###
    ### ------------- ###
    
    sig_files = [ "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v07/ntuple-30059[0-1]_part_0[1-2].ftr" ]
    sig_files = utils.get_list_make_list( sig_files )
    bkg_files = [ "/eos/atlas/atlascerngroupdisk/phys-beauty/RKstarRun2/ntuples/feather/v07/ntuple-data18_13TeV_period[K]_part_[0][0-1].ftr" ]
    bkg_files = utils.get_list_make_list( bkg_files )
        
    ### Loading Files ###
    ### ------------- ###
        
    config_file = "configs/gnn1_multiclass_v4d_sculpt_original_dense_broadcast.yaml"
    config      = utils.config_load( "configs/default.yaml", config_file )
    if region == "NN1TR":
        selection_scheme_sig = "( $selection_main ) & ( $selection_mc & $selection_usr ) & ( $selection_q2low )"
        selection_scheme_bkg = "( $selection_main ) & ( $selection_data & $selection_nn1trb ) & ( $selection_q2low )"
    if region == "NN2TR":
        selection_scheme_sig = "( $selection_main ) & ( $selection_mc & $selection_usr ) & ( $selection_q2low )"
        selection_scheme_bkg = "( $selection_main ) & ( $selection_data & $selection_nn2trb ) & ( $selection_q2low )"
    selection_sig = utils.get_selection( selection_scheme_sig, config[ "common" ] )
    selection_bkg = utils.get_selection( selection_scheme_bkg, config[ "common" ] )
    features = config["common"].get("features", [])
    sig = utils.df_load( sig_files, selection = selection_sig )
    bkg = utils.df_load( bkg_files, selection = selection_bkg )

    ### Building Higher-Order Features ###
    ### ------------------------------ ###

    sig[ "Lxy_significance_over_B_chi2" ] = sig[ "Lxy_significance" ] / sig[ "B_chi2" ]
    bkg[ "Lxy_significance_over_B_chi2" ] = bkg[ "Lxy_significance" ] / bkg[ "B_chi2" ]
    
    ### Calculating the Relative Entropy ###
    ### -------------------------------- ###
    
    relative_entropy = get_bestN_relative_entropy( sig, bkg, features, methods = methods, N = N )
    for method in methods:
        df( relative_entropy_[ method ] ).to_csv( "best-N-" + method + "-feature-distinction_USR_%s.csv"%(region) )

if __name__ == "__main__":
    main( region = "NN1TR", methods = [ "B_chi2" ], N = [ 1 ] )
